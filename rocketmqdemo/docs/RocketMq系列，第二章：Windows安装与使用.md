**RocketMq系列，第二章：Windows安装与使用**

在第一章记录了：RocketMq在Linux环境下的快速安装与简单概述。由于在平时的学习和测试中我更愿意在windows环境下使用，今天将Windows环境下RocketMq的安装与使用简单记录一下。

# 一、RocketMq的安装与部署

## 1、下载

```xml
http://rocketmq.apache.org/release_notes/release-notes-4.7.1/
```

选择Binary进行下载

![](imgs\rocketmq\1.png)

## 2、解压与环境变量配置

![](imgs\rocketmq\2.png)

```xml
新增：ROCKETMQ_HOME
	变量名：ROCKETMQ_HOME
	变量值：D:\software\RocketMq

修改:PATH
	变量值：%ROCKETMQ_HOME%\bin
```

## 3、启动

### 3.1、启动nameserver

#### 3.1.1、修改启动配置文件

```XML
##修改文件runserver.cmd

##原来【占内存太大】：
set "JAVA_OPT=%JAVA_OPT% -server -Xms2g -Xmx2g -Xmn1g -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"

##修改后
set "JAVA_OPT=%JAVA_OPT% -server -Xms512m -Xmx512m -Xmn256m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"
```

#### 3.1.2、启动

```xml
命令：start mqnamesrv
```

![](imgs\rocketmq\3.png)

### 3.2、启动broker

#### 3.2.1、修改启动配置文件

```xml
##修改文件：runbroker.cmd

##原来【占内存太大】：
set "JAVA_OPT=%JAVA_OPT% -server -Xms2g -Xmx2g -Xmn1g"
##修改后
set "JAVA_OPT=%JAVA_OPT% -server -Xms512m -Xmx512m -Xmn256m"
```

#### 3.2.2、启动

```xml
命令：start mqbroker.cmd -n 127.0.0.1:9876 autoCreateTopicEnable=true

##开启Topic自动创建功能
```

![](imgs\rocketmq\4.png)

# 二、RocketMq插件部署

**RocketMq-console**

```XML
RocketMq-console是RocketMq项目的扩展插件，一个图形化管理控制台，提供Broker集群状态的查看，Topic管理，Producer、Consumer状态展示，消息查询等常用功能。
需独立安装运行
```

## 1、下载

```xml
https://github.com/apache/rocketmq-externals


如果嫌GitHub下载慢的话，可以在gitee码云上下载：
https://gitee.com/mirrors/RocketMQ-Externals
git clone https://gitee.com/mirrors/RocketMQ-Externals.git
```

![](imgs\rocketmq\5.png)



## 2、修改配置文件

目录：

```xml
rocketmq-console\src\main\resources\application.properties
```

修改：

```properties
server.address=0.0.0.0
##修改
server.port=8088

### SSL setting
#server.ssl.key-store=classpath:rmqcngkeystore.jks
#server.ssl.key-store-password=rocketmq
#server.ssl.keyStoreType=PKCS12
#server.ssl.keyAlias=rmqcngkey

#spring.application.index=true
spring.application.name=rocketmq-console
spring.http.encoding.charset=UTF-8
spring.http.encoding.enabled=true
spring.http.encoding.force=true
logging.level.root=INFO
logging.config=classpath:logback.xml
#修改：namesrv地址
rocketmq.config.namesrvAddr=127.0.0.1:9876
rocketmq.config.isVIPChannel=
#rocketmq-console's data path:dashboard/monitor
rocketmq.config.dataPath=/tmp/rocketmq-console/data
#set it false if you don't want use dashboard.default true
rocketmq.config.enableDashBoardCollect=true
#set the message track trace topic if you don't want use the default one
rocketmq.config.msgTrackTopicName=
rocketmq.config.ticketKey=ticket

#Must create userInfo file: ${rocketmq.config.dataPath}/users.properties if the login is required
rocketmq.config.loginRequired=false
```

## 3、编译启动

```XML
mvn clean package -Dmaven.test.skip=true

java -jar rocketmq-console-ng-2.0.0.jar

测试：
http://127.0.0.1:8088
```

![](C:\Users\Administrator\Desktop\进阶笔记\imgs\rocketmq\6.png)









