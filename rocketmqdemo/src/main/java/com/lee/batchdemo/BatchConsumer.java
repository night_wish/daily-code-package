package com.lee.batchdemo;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class BatchConsumer {

    public static void main(String[] args) throws MQClientException {
        BatchConsumer consumer = new BatchConsumer();

        //消费批量消息
        consumer.batchMessageConsumer();
    }

    public void batchMessageConsumer() throws MQClientException {
        //创建消息消费者-指定消费组
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("consumer_group_name");
        //指定namesrv
        consumer.setNamesrvAddr("127.0.0.1:9876");
        //订阅消息
        consumer.subscribe("TopicD","*");

        //设置消息监听回调函数处理消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for(MessageExt ext : msgs){
                    System.out.println("consumer===>"+ext.getQueueId()+"  "+ext.getTopic()+"  "+ext.getTags()+"  "+new String(ext.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });

        //开启消费者
        consumer.start();

        System.out.println("=====消息消费者开启====");
    }
}
