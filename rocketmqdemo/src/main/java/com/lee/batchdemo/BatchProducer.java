package com.lee.batchdemo;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * 批量消息生产者
 */
public class BatchProducer {

    public static void main(String[] args) throws Exception {
        BatchProducer producer = new BatchProducer();

        //批量生产消息
        producer.BatchMesssageProducer();
    }

    //批量生产消息
    public void BatchMesssageProducer() throws Exception {
        //创建消息生产者-指定生产组
        DefaultMQProducer producer = new DefaultMQProducer("producer_group_name");
        //指定namesrv
        producer.setNamesrvAddr("127.0.0.1:9876");
        //启动producer实例
        producer.start();

        //创建消息body
        String topicName = "TopicD";
        List<Message> messageList = new ArrayList<>();
        for(int i=0;i<10;i++){
            Message msg = new Message(topicName,"tag"+i, ("hello rocketmq,this is batch message : "+i).getBytes());
            messageList.add(msg);
        }
        
        //批量发送消息
        SendResult send = producer.send(messageList);
        System.out.println("producer------>"+send.getSendStatus()+" "+send.getMsgId()+" "+send.getMessageQueue().getQueueId()+" "+messageList);

        //关闭producer
        producer.shutdown();
    }

}
