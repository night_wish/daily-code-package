package com.lee.orderdemo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 模拟订单
 */
@Data
public class OrderStep {

    //订单ID
    private long orderId;
    //描述
    private String desc;

    //模拟 生成订单数据
    public List<OrderStep> buildOrders(){
        List<OrderStep> orderStepList = new ArrayList<>();

        //1 创建  付款  推送  完成
        //2 创建  付款  完成
        //3 创建  付款  完成
        OrderStep order = new OrderStep();
        order.setOrderId(1l);
        order.setDesc("创建");
        orderStepList.add(order);

        order = new OrderStep();
        order.setOrderId(2l);
        order.setDesc("创建");
        orderStepList.add(order);

        order = new OrderStep();
        order.setOrderId(1l);
        order.setDesc("付款");
        orderStepList.add(order);

        order = new OrderStep();
        order.setOrderId(3l);
        order.setDesc("创建");
        orderStepList.add(order);

        order = new OrderStep();
        order.setOrderId(1l);
        order.setDesc("推送");
        orderStepList.add(order);

        order = new OrderStep();
        order.setOrderId(2l);
        order.setDesc("付款");
        orderStepList.add(order);

        order = new OrderStep();
        order.setOrderId(2l);
        order.setDesc("完成");
        orderStepList.add(order);

        order = new OrderStep();
        order.setOrderId(3l);
        order.setDesc("付款");
        orderStepList.add(order);

        order = new OrderStep();
        order.setOrderId(3l);
        order.setDesc("完成");
        orderStepList.add(order);

        order = new OrderStep();
        order.setOrderId(1l);
        order.setDesc("完成");
        orderStepList.add(order);

        return orderStepList;
    }


}
