package com.lee.orderdemo;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 顺序消息：生产者
 */
public class OrderProducer {

    public static void main(String[] args) throws Exception {
        OrderProducer producer = new OrderProducer();

        //生产顺序消息
        producer.orderMessageProducer();
    }


    //生产顺序消息
    public void orderMessageProducer() throws Exception {
        //创建消息生产者-指明消息生产组
        DefaultMQProducer producer = new DefaultMQProducer("producer_mq_group");
        //指明namesrv
        producer.setNamesrvAddr("127.0.0.1:9876");
        //开启producer实例
        producer.start();

        String tags[] = {"TagA","TagB","TagC"};

        //生成订单列表
        List<OrderStep> orderStepList = new OrderStep().buildOrders();

        LocalTime time = LocalTime.now();

        //创建消息-发送消息
        for(int i=0;i<10;i++){

            String body = time+"  "+"hello rocketMq"+"  "+orderStepList.get(i);
            Message msg = new Message("TopicTest",tags[i%tags.length],body.getBytes(RemotingHelper.DEFAULT_CHARSET));

            //orderStepList.get(i).getOrderId()对应着Object arg入参
            SendResult res = producer.send(msg, new MessageQueueSelector() {
                @Override
                public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
                    System.out.println("---->" + arg);
                    Long id = (long) arg;//订单ID，根据订单ID选择发送的queue--这样相同的ID选择的queue是相同的
                    long index = id % mqs.size();
                    MessageQueue messageQueue = mqs.get((int) index);
                    return messageQueue;
                }
            }, orderStepList.get(i).getOrderId());

            System.out.println("producer:  queueId:{ "+res.getMessageQueue().getQueueId()+" }, " +
                    " status:{ "+res.getSendStatus()+" }," +
                    " body:{ "+body+" }");

        }

        producer.shutdown();
    }




}
