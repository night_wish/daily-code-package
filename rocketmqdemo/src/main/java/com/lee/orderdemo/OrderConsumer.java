package com.lee.orderdemo;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

/**
 * 顺序消息：消费者
 */
public class OrderConsumer {

    public static void main(String[] args) throws MQClientException {
        OrderConsumer consumer = new OrderConsumer();

        //消费顺序消息
        consumer.orderMessageConsumer();
    }

    //消费顺序消息
    public void orderMessageConsumer() throws MQClientException {
        //创建消息消费者-指定消费组
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("consumer_group_name");
        //指定namesrv
        consumer.setNamesrvAddr("127.0.0.1:9876");

        //设置consumer第一次启动，是从队列头部开始消费还是从队列尾部开始消费
        //如果非第一次启动，从上次消费位置接着消费
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);

        //订阅消息
        consumer.subscribe("TopicTest","TagA || TagB || TagC");

        consumer.registerMessageListener(new MessageListenerOrderly() {
            @Override
            public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs, ConsumeOrderlyContext context) {
                context.setAutoCommit(true);
                for(MessageExt ext : msgs){
                    System.out.println("threadName: "+Thread.currentThread().getName()+"   queueId: "+ext.getQueueId()+"  body: "+new String(ext.getBody()));
                }
                return ConsumeOrderlyStatus.SUCCESS;
            }
        });

        consumer.start();
        System.out.println("=====>消费者启动");
    }
}
