package com.lee.scheduledemo;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.time.LocalTime;
import java.util.List;

/**
 * 延时消息
 */
public class ScheduleConsumer {

    public static void main(String[] args) throws MQClientException {
        ScheduleConsumer consumer = new ScheduleConsumer();
        //消费延时消息
        consumer.ScheduleMessageConsumer();
    }

    //消费延时消息
    public void ScheduleMessageConsumer() throws MQClientException {
        //创建消息消费者-指定消费组
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("consumer_group_name");
        //指定namesrv
        consumer.setNamesrvAddr("127.0.0.1:9876");
        //订阅消息
        consumer.subscribe("TopicA","*");
        //设置消息回调函数，处理消息
        consumer.setMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt ext: msgs) {
                    LocalTime time = LocalTime.now();
                    System.out.println("consumer===>"+time+"  "+ext.getQueueId()+"  "+ext.getMsgId()+" "+new String(ext.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });

        //启动消费者
        consumer.start();
        System.out.println("消费者启动");
    }
}
