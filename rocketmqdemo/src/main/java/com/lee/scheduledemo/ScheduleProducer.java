package com.lee.scheduledemo;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.time.LocalTime;

/**
 * 延时消息
 */
public class ScheduleProducer {

    public static void main(String[] args) throws Exception {
        ScheduleProducer producer = new ScheduleProducer();

        //生产延时消息
        producer.ScheduleMessageProducer();
    }

    //生产延时消息：如订单生成后，发送延时消息，等1H后判断是否付款 否则取消订单释放库存
    public void ScheduleMessageProducer() throws Exception {
        //创建消息生产者-指定生产组
        DefaultMQProducer producer = new DefaultMQProducer("producer_group_name");
        //指定namesrv
        producer.setNamesrvAddr("127.0.0.1:9876");
        //开启producer实例
        producer.start();

        //生产消息-发送消息
        for (int i = 0; i <10 ; i++) {
            Message msg  =  new Message("TopicA","TagA",("Hello Schedule Message"+i).getBytes());

            //设置延时等级-3对应10s
            msg.setDelayTimeLevel(3);
            LocalTime time = LocalTime.now();
            //发送消息
            SendResult send = producer.send(msg);
            System.out.println("producer----->"+time+"  "+send.getSendStatus()+"  "+send.getMsgId()+"  "+new String(msg.getBody()));
        }

        //关闭生产者
        producer.shutdown();
    }

}
