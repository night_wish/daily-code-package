package com.lee.basedemo;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

/**
 * 基本案例：生产者
 */
public class BaseProducer {

    public static void main(String[] args) throws Exception {
        BaseProducer producer = new BaseProducer();
        //发送同步消息
        producer.SyncProducer();

        //发送同步消息
        //producer.ASyncProducer();

        //单向发送消息
        //producer.OneWayProducer();
    }


    /**
     *  3、单向发送消息：这种方式主要用在不特别关心发送结果的场景，例如日志发送。
     */
    public void OneWayProducer() throws Exception {
        //创建消息生产者-指定生产组
        DefaultMQProducer producer = new DefaultMQProducer("producer_group_name");
        //指定namesrc
        producer.setNamesrvAddr("127.0.0.1:9876");
        //启动producer实例
        producer.start();
        //创建消息体-发送消息
        for(int i=0;i<10;i++){
            Message msg = new Message("TopicC","TagC",("hello_world : "+i).getBytes());
            producer.sendOneway(msg);
        }
        //关闭生产者
        producer.shutdown();
    }


    /**
     * 2、发送异步消息：对相应时间敏感的业务场景
     */
    public void ASyncProducer() throws Exception {
        //创建消息生产者-指定生产组
        DefaultMQProducer producer = new DefaultMQProducer("producer_group_name");
        //指定namesrc
        producer.setNamesrvAddr("127.0.0.1:9876");
        //启动producer实例
        producer.start();
        //设置消息发送失败后重试次数
        producer.setRetryTimesWhenSendAsyncFailed(2);
        //创建消息体-发送消息
        for(int i=0;i<10;i++){
            Message msg = new Message("TopicB","TagB",("你好 rocketmq: "+i).getBytes(RemotingHelper.DEFAULT_CHARSET));
            producer.send(msg, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                    System.out.println("producer---success-->"+sendResult);
                }

                @Override
                public void onException(Throwable e) {
                    System.out.println("producer--error-->"+e.toString());
                }
            });
        }
        //关闭发送不成功的
    }



    /**
     * 1、发送同步消息：可靠性强
     */
    public void SyncProducer() throws Exception {
        //创建消息生产者-指定生产组
        DefaultMQProducer producer = new DefaultMQProducer("producer_group_name");
        //设置namesrv
        producer.setNamesrvAddr("127.0.0.1:9876");
        //启动producer实例
        producer.start();
        //创建消息体-发送消息
        for(int i=0;i<10;i++){
            Message msg = new Message("TopicA","TagA",("hello_world : "+i).getBytes());
            SendResult res = producer.send(msg);
            System.out.println("producer：----->"+res);
        }
        //关闭生产者
        producer.shutdown();
    }

}
