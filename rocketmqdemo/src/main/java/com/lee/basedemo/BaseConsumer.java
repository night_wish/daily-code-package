package com.lee.basedemo;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

import java.util.List;

/**
 * 基本案例：消费者
 */
public class BaseConsumer {

    public static void main(String[] args) throws MQClientException {
        BaseConsumer baseConsumer =  new BaseConsumer();
        //负载均衡模式
        //baseConsumer.loadBalanceConsumer();

        //广播模式
        baseConsumer.BroadCastConsumer();
    }


    /**
     * 2、广播模式：每个消费者消费的消息都是相同的
     */
    public void BroadCastConsumer() throws MQClientException {
        //创建消息消费者，指定消费者组
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("consumer_group_name");
        //指定namesrv
        consumer.setNamesrvAddr("127.0.0.1:9876");
        //订阅Topic
        consumer.subscribe("TopicA","*");
        //设置消费者消费模式
        consumer.setMessageModel(MessageModel.BROADCASTING);
        //设置消息回调，处理消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for(MessageExt ext : msgs){
                    System.out.println("consumer消费消息：====>"+Thread.currentThread().getName()+"{ topic: "+ext.getTopic()
                            +"  tags:  "+ext.getTags()
                            +"  body:  "+new String(ext.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });

        //启动消费者
        consumer.start();
        System.out.println("consumer:======>消费者启动");
    }



    /**
     * 1、负载均衡的模式消费消息：多个消费者 共同 消费  消息队列中的消息。
     *      每个消费者处理的消息不同
     */
    public void loadBalanceConsumer() throws MQClientException {
        //创建消息消费者，指定消费者组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("consumer_group_name");
        //指定namesrv
        consumer.setNamesrvAddr("127.0.0.1:9876");
        //订阅Topic
        consumer.subscribe("TopicC","*");
        //设置消费者模式：负载均衡模式
        consumer.setMessageModel(MessageModel.CLUSTERING);
        //设置消息回调函数，处理消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                for(MessageExt ext : list){
                    System.out.println("consumer消费消息：====>"+Thread.currentThread().getName()+"{ topic: "+ext.getTopic()
                            +"  tags:  "+ext.getTags()
                            +"  body:  "+new String(ext.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //启动消费者
        consumer.start();
        System.out.println("consumer:======>消费者启动");

    }
}
