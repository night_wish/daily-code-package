package com.lee.transactiondemo;

import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

/**
 * 事务消息生产者
 */
public class TransactionProducer {


    public static void main(String[] args) throws Exception {
        TransactionProducer producer = new TransactionProducer();

        //生产者创建事务消息
        producer.TransactionMessageProducer();
    }

    public void TransactionMessageProducer() throws Exception {

        //创建事务消息生产者
        TransactionMQProducer producer = new TransactionMQProducer("TRANSACTION_GROUP_NAME");
        //设置namesrv
        producer.setNamesrvAddr("127.0.0.1:9876");

        //设置监听器
        TransactionListener listener = new MyTransactionListener();
        producer.setTransactionListener(listener);

        //启动消息生产者
        producer.start();
        String[] tags = new String[]{"TagA","TagB","TagC"};

        for(int i=0;i<3;i++){
            Message msg = new Message("TransactionMessageTopic",tags[i%tags.length],
                    ("KEY:"+i+" HELLO TRANSACTION MQ MESSAGE").getBytes(RemotingHelper.DEFAULT_CHARSET));
            TransactionSendResult result = producer.sendMessageInTransaction(msg, null);
            System.out.println("Producer:---->"+result);
        }

        //关闭生产者
//        producer.shutdown();
    }
}
