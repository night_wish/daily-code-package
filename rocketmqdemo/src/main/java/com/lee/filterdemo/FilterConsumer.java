package com.lee.filterdemo;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.MessageSelector;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

/**
 * 过滤消息
 */
public class FilterConsumer {

    public static void main(String[] args) throws MQClientException {
        FilterConsumer consumer = new FilterConsumer();

        //过滤消息
        consumer.FilterMessageConsumer();
    }

    //过滤消息
    public void FilterMessageConsumer() throws MQClientException {
        //创建消息消费者，指定消费者组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("consumer_group_name");
        //指定namesrv
        consumer.setNamesrvAddr("127.0.0.1:9876");
        //订阅Topic--过滤消息【两种方式1:bySql 2:byTag】
        consumer.subscribe("TopicE",MessageSelector.bySql("myParam between 1 and 5"));
        //设置消费者模式：负载均衡模式
        //consumer.setMessageModel(MessageModel.CLUSTERING);
        //设置消息回调函数，处理消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                for(MessageExt ext : list){
                    System.out.println("consumer消费消息：====>"+Thread.currentThread().getName()+"{ topic: "+ext.getTopic()
                            +"  tags:  "+ext.getTags()
                            +"  body:  "+new String(ext.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //启动消费者
        consumer.start();
        System.out.println("consumer:======>消费者启动");
    }


}
