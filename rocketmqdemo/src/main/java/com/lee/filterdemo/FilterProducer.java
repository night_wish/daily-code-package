package com.lee.filterdemo;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

/**
 * 消息生产者
 */
public class FilterProducer {

    public static void main(String[] args) throws Exception {
        FilterProducer producer = new FilterProducer();

        //生产消息
        producer.filterMessageProducer();
    }

    //生产消息
    public void filterMessageProducer() throws Exception {
        //创建消息生产者-指定生产组
        DefaultMQProducer producer = new DefaultMQProducer("producer_group_name");
        //设置namesrv
        producer.setNamesrvAddr("127.0.0.1:9876");
        //启动producer实例
        producer.start();
        //创建消息体-发送消息
        for(int i=0;i<10;i++){
            Message msg = new Message("TopicE","tage"+i,("hello_world filter rocketmq : "+i).getBytes());

            //设置消息参数--为消费判断做前提
            msg.putUserProperty("myParam",String.valueOf(i));

            SendResult res = producer.send(msg);
            System.out.println("producer：----->"+res);
        }
        //关闭生产者
        producer.shutdown();
    }
}
