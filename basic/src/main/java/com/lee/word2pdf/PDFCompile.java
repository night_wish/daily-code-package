package com.lee.word2pdf;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

/**
 * @author Lee
 * @version 1.0
 * @description 破解aspose-pdf只能转4页的问题
 *     通过反射将com.aspose.pdf.ADocument 中的 lI 和  int方法修改，然后删除 jar包中 37E3C32D.SF 、37E3C32D.RSA 两个文件，
 *     将反编译的 ADocument.class 替换了原来jar包里的 ADocument.class
 * @date 2022/10/20 16:18
 */
public class PDFCompile {

    public static void main(String[] args) throws Exception {
        compile();
    }

    public static void compile() throws Exception{
        ClassPool.getDefault().insertClassPath("D:\\repo\\com\\aspose\\aspose-pdf\\21.6\\aspose-pdf-21.6.jar");
        CtClass ctClass = ClassPool.getDefault().getCtClass("com.aspose.pdf.ADocument");

        CtMethod[] declaredMethods = ctClass.getDeclaredMethods();

        for (CtMethod method: declaredMethods) {

            CtClass[] ps = method.getParameterTypes();

            if (ps.length==2
                      && method.getName().equals("lI")
                      && ps[0].getName().equals("com.aspose.pdf.ADocument")
                      && ps[1].getName().equals("int")){
                // 最多只能转换4页 处理
                System.out.println(method.getReturnType());
                System.out.println(ps[1].getName());
                method.setBody("{return false;}");
            }

            if (ps.length==0&& method.getName().equals("lt")){
                // 水印处理
                method.setBody("{return true;}");
            }
        }
        ctClass.writeFile("D:\\repo\\com\\aspose\\aspose-pdf\\21.6\\");
    }
}
