package com.lee.word2pdf;

import com.alibaba.fastjson.JSON;
import com.aspose.pdf.Document;
import com.aspose.pdf.License;
import com.aspose.pdf.SaveFormat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/7/31 7:48
 */
@Slf4j
public class Pdf2WordAsposeUtils {


    public static void main(String[] args) throws InterruptedException, IOException {

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        InputStream is = contextClassLoader.getResourceAsStream("license.xml");
        int b=0;
        while((b=is.read())!=-1){
            System.out.println((char)b);
        }
        log.info("loader : {}", JSON.toJSONString(contextClassLoader));

        String pdfPath = "C:\\Users\\james\\Desktop\\汉语国际教育本科生专业认同研究_徐康康.pdf";
        String docPath = "C:\\Users\\james\\Desktop\\汉语国际教育本科生专业认同研究_徐康康.docx";
        Pdf2WordAsposeUtils.pdf2word(pdfPath,docPath);
    }

    public static boolean pdf2word(String inPath, String outPath) {
        if (!getLicense()) { // 验证License 若不验证则转化出的pdf文档会有水印产生
            return false;
        }

        FileOutputStream os = null;
        try {
            long old = System.currentTimeMillis();
            File file = new File(outPath); // 新建一个空白docx文档
            os = new FileOutputStream(file);
            Document doc = new Document(inPath); // Address是将要被转化的word文档
            doc.save(os, SaveFormat.DocX);// 全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF,
            // EPUB, XPS, SWF 相互转换
            long now = System.currentTimeMillis();
            System.out.println("word转换成功，共耗时：" + ((now - old) / 1000.0) + "秒"); // 转化用时
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (os != null) {
                try {
                    os.flush();
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }


    //获取证书
    public static boolean getLicense() {
        boolean result = false;
        InputStream is = null;
        try {
            Resource resource = new ClassPathResource("license.xml");
            is = resource.getInputStream();
            License aposeLic = new License();
            aposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
