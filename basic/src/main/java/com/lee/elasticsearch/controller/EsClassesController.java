package com.lee.elasticsearch.controller;

import com.alibaba.fastjson.JSON;
import com.lee.elasticsearch.entity.EsClasses;
import com.lee.elasticsearch.entity.EsUser;
import com.lee.elasticsearch.service.EsClassesService;
import com.lee.elasticsearch.service.EsUserService;
import com.lee.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.shaded.org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/3/15 17:13
 */
@Slf4j
@RestController
@RequestMapping("/es/classes")
public class EsClassesController {

    @Autowired
    private EsClassesService esClassesService;

    @Autowired
    private EsUserService esUserService;

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @GetMapping("/deleteAllClasses")
    public String deleteAllClasses() {
        esUserService.deleteAll();
        esClassesService.deleteAll();
        return "success";
    }


    @GetMapping("/addAllClasses")
    public String addAllClasses() {
        List<EsClasses> esClassesList = new ArrayList<>();

        EsClasses esClasses1 = new EsClasses();
        List<EsUser> userList1 = new ArrayList<>();
        EsUser esUser1 = new EsUser();
        esUser1.setId("001").setUsername("u001").setPassword("p001").setGender("male").setBirthDay(DateUtils.stringToDate("2020-03-15 12:22:22", "yyyy-MM-dd HH:mm:ss")).setAge(4);
        userList1.add(esUser1);
        esClasses1.setId("001").setClassName("c001").setGrade(1).setEsUserList(userList1);
        esClassesList.add(esClasses1);

        //=========================================
        EsClasses esClasses2 = new EsClasses();
        List<EsUser> userList2 = new ArrayList<>();
        EsUser esUser2 = new EsUser();
        esUser2.setId("002").setUsername("u002").setPassword("p002").setGender("Fmale").setBirthDay(DateUtils.stringToDate("2020-03-15 12:22:22", "yyyy-MM-dd HH:mm:ss")).setAge(4);
        userList2.add(esUser2);

        EsUser esUser3 = new EsUser();
        esUser3.setId("003").setUsername("u003").setPassword("p003").setGender("male").setBirthDay(DateUtils.stringToDate("2019-03-15 12:22:22", "yyyy-MM-dd HH:mm:ss")).setAge(5);
        userList2.add(esUser3);
        esClasses2.setId("002").setClassName("c002").setGrade(2).setEsUserList(userList2);
        esClassesList.add(esClasses2);

        Iterable<EsClasses> esClasses = esClassesService.saveAll(esClassesList);
        return JSON.toJSONString(esClasses);
    }

    @GetMapping("/createIndex")
    public String createIndex() throws IOException {

        boolean index = elasticsearchRestTemplate.createIndex(EsClasses.class);
        return "";
    }

    @GetMapping("/findClasses")
    public String findClasses() {
//        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
//        BoolQueryBuilder mainBoolQuery = new BoolQueryBuilder();
//        mainBoolQuery.must(QueryBuilders.termQuery("esUserList.gender", "male"));
//        mainBoolQuery.must(QueryBuilders.termQuery("esUserList.username", "u003"));
//        builder.withQuery(mainBoolQuery);
//        SearchQuery searchQuery = builder.withPageable(PageRequest.of(0, 10)).build();
//        Page<EsClasses> search = esClassesService.search(searchQuery);

//        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
//        BoolQueryBuilder mainBoolQuery = new BoolQueryBuilder();
//        mainBoolQuery.must(QueryBuilders.termQuery("esUserList.gender", "female"));
//        NestedQueryBuilder nestedQueryBuilder = new NestedQueryBuilder("esUserList",mainBoolQuery, ScoreMode.None);
//        QueryBuilder query = nestedQueryBuilder.query();
//        builder.withQuery(query);
//        SearchQuery searchQuery = builder.withPageable(PageRequest.of(0, 10)).build();
//        Page<EsClasses> search = esClassesService.search(searchQuery);

//        BoolQueryBuilder mainBoolQuery = new BoolQueryBuilder();
//        mainBoolQuery.must(QueryBuilders.nestedQuery("esUserList", QueryBuilders.termQuery("esUserList.gender", "male"), ScoreMode.None));
//        mainBoolQuery.must(QueryBuilders.nestedQuery("esUserList", QueryBuilders.termQuery("esUserList.username", "u001"), ScoreMode.None));
//
//        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
//        builder.withQuery(mainBoolQuery);
//        SearchQuery searchQuery = builder.withPageable(PageRequest.of(0, 10)).build();
//        Page<EsClasses> search = esClassesService.search(searchQuery);


//        BoolQueryBuilder mainBoolQuery = new BoolQueryBuilder();
//        NestedQueryBuilder nestedQueryBuilder1 = QueryBuilders.nestedQuery("esUserList", QueryBuilders.termQuery("esUserList.gender", "female"), ScoreMode.None);
//        mainBoolQuery.must(nestedQueryBuilder1);
//        NestedQueryBuilder nestedQueryBuilder2 = QueryBuilders.nestedQuery("esUserList", QueryBuilders.termQuery("esUserList.username", "u003"), ScoreMode.None);
//        mainBoolQuery.must(nestedQueryBuilder2);
//        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
//        builder.withQuery(mainBoolQuery);
//        SearchQuery searchQuery = builder.withPageable(PageRequest.of(0, 10)).build();
//        Page<EsClasses> search = esClassesService.search(searchQuery);

//        BoolQueryBuilder mainBoolQuery = new BoolQueryBuilder();
//        BoolQueryBuilder nestedBoolQuery = QueryBuilders.boolQuery();
//        nestedBoolQuery.must(QueryBuilders.termQuery("esUserList.gender", "female"));
//        nestedBoolQuery.must(QueryBuilders.termQuery("esUserList.username", "u002"));
//        NestedQueryBuilder nestedQueryBuilder = QueryBuilders.nestedQuery("esUserList", nestedBoolQuery, ScoreMode.None);
//        mainBoolQuery.must(nestedQueryBuilder);
//        mainBoolQuery.must(QueryBuilders.termQuery("className","c001"));
//
//
//        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
//        builder.withQuery(mainBoolQuery);
//        SearchQuery searchQuery = builder.withPageable(PageRequest.of(0, 10)).build();
//        Page<EsClasses> search = esClassesService.search(searchQuery);
//
//        return JSON.toJSONString(search);
        return "";
    }
}
