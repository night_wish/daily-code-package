package com.lee.elasticsearch.controller;

import com.alibaba.fastjson.JSON;
import com.lee.elasticsearch.entity.EsUser;
import com.lee.elasticsearch.service.EsUserService;
import com.lee.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/3/15 14:22
 */
@Slf4j
@RestController
@RequestMapping("/es/user")
public class EsUserController {

    @Autowired
    private EsUserService esUserService;

    @Autowired
    private ElasticsearchRestTemplate restTemplate;

    @GetMapping("/addUser")
    public String addEsUser(){
        EsUser esUser = new EsUser();
        esUser.setId("001").setUsername("u001").setPassword("p001").setGender("male").setBirthDay(DateUtils.stringToDate("2020-03-15 12:22:22","yyyy-MM-dd HH:mm:ss")).setAge(4);
        EsUser res = esUserService.save(esUser);
        return JSON.toJSONString(res);
    }

    @GetMapping("/addAllUser")
    public String addAllEsUser(){
        List<EsUser> userList = new ArrayList<>();

        EsUser esUser2 = new EsUser();
        esUser2.setId("002").setUsername("u002").setPassword("p002").setGender("Fmale").setBirthDay(DateUtils.stringToDate("2020-03-15 12:22:22","yyyy-MM-dd HH:mm:ss")).setAge(4);
        userList.add(esUser2);

        EsUser esUser3 = new EsUser();
        esUser3.setId("003").setUsername("u003").setPassword("p003").setGender("male").setBirthDay(DateUtils.stringToDate("2019-03-15 12:22:22","yyyy-MM-dd HH:mm:ss")).setAge(5);
        userList.add(esUser3);

        EsUser esUser4 = new EsUser();
        esUser4.setId("004").setUsername("u004").setPassword("p004").setGender("Fmale").setBirthDay(DateUtils.stringToDate("2017-03-15 12:22:22","yyyy-MM-dd HH:mm:ss")).setAge(7);
        userList.add(esUser4);

        EsUser esUser5 = new EsUser();
        esUser5.setId("005").setUsername("u005").setPassword("p005").setGender("male").setBirthDay(DateUtils.stringToDate("2022-03-15 12:22:22","yyyy-MM-dd HH:mm:ss")).setAge(2);
        userList.add(esUser5);

        Iterable<EsUser> esUsers = esUserService.saveAll(userList);
        return JSON.toJSONString(esUsers);
    }

    @GetMapping("/findUserById/{id}")
    public String findEsUserById(@PathVariable("id")String id){
        Optional<EsUser> oEsUser = esUserService.findById(id);
        EsUser esUser = oEsUser.get();
        return JSON.toJSONString(esUser);
    }

    @GetMapping("/findUser")
    public String findEsUser(){
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        BoolQueryBuilder bQuery = QueryBuilders.boolQuery();
        bQuery.must(QueryBuilders.termQuery("gender","male"));
        queryBuilder.withQuery(bQuery);

        NativeSearchQuery build = queryBuilder.withPageable(PageRequest.of(0, 10)).build();
        Page<EsUser> search = esUserService.search(build);
        List<EsUser> content = search.getContent();
        int totalPages = search.getTotalPages();
        long totalElements = search.getTotalElements();
        log.info("总数量:{},总页数:{},当前页内容:{}",totalElements,totalPages,content);
        return JSON.toJSONString(search);
    }
}
