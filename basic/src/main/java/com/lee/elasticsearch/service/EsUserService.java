package com.lee.elasticsearch.service;

import com.lee.elasticsearch.entity.EsUser;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/3/15 14:15
 */
public interface EsUserService extends ElasticsearchRepository<EsUser,String> {

}
