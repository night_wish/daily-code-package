package com.lee.elasticsearch.service;

import com.lee.elasticsearch.entity.EsClasses;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/3/15 14:16
 */
public interface EsClassesService extends ElasticsearchRepository<EsClasses,String> {
}
