package com.lee.elasticsearch.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/3/15 14:13
 */
@Data
@Accessors(chain = true)
@Document(indexName = "esclasses", type = "doc")
public class EsClasses implements Serializable {
    private static final long serialVersionUID = 3714608688802364720L;

    @Id
    private String id;

    @Field(type = FieldType.Text)
    private String className;

    @Field(type = FieldType.Integer)
    private Integer grade;

    @Field(type = FieldType.Nested)
    private List<EsUser> esUserList;
}

/**
 * {"mappings":{"properties":{"_class":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"className":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"esUserList":{"type":"nested","properties":{"age":{"type":"long"},"birthDay":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"gender":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"id":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"password":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"username":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}}}},"grade":{"type":"long"},"id":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}}}}}
 */