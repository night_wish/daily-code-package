package com.lee.elasticsearch.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/3/15 14:08
 */
@Data
@Accessors(chain = true)
@Document(indexName = "esuser", shards = 1, replicas = 1)
public class EsUser implements Serializable {
    private static final long serialVersionUID = 558378738879544878L;

    @Id
    private String id;

    @Field(type = FieldType.Text)
    private String username;

    @Field(type = FieldType.Text)
    private String password;

    @Field(type = FieldType.Keyword)
    private String gender;

    @Field(type = FieldType.Date,format = DateFormat.basic_date_time)
    private Date birthDay;

    @Field(type = FieldType.Integer)
    private Integer age;
}
