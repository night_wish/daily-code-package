package com.lee.elasticsearch.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/3/15 17:11
 */
@Data
@Accessors(chain = true)
@Document(indexName = "esScore", shards = 1, replicas = 1)
public class EsScore implements Serializable {

    @Id
    private String id;

    @Field(type = FieldType.Text)
    private String subject;

    @Field(type = FieldType.Double)
    private Double score;
}
