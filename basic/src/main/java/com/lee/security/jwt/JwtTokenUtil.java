//package com.lee.security.jwt;
//
//import cn.hutool.core.date.DateUtil;
//import com.alibaba.fastjson.JSON;
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.SignatureAlgorithm;
//import lombok.extern.slf4j.Slf4j;
//import org.eclipse.jetty.util.StringUtil;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.util.ResourceUtils;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.security.*;
//import java.security.spec.InvalidKeySpecException;
//import java.security.spec.PKCS8EncodedKeySpec;
//import java.security.spec.X509EncodedKeySpec;
//import java.util.*;
//
///**
// * @author Lee
// * @version 1.0
// * @description
// * @date 2023-12-18 15:21
// */
//@Slf4j
//public class JwtTokenUtil {
//
//
//    //主体
//    private static final String CLAIM_KEY_USERNAME = "sub";
//    //创建时间
//    private static final String CLAIM_KEY_CREATED = "created";
//    //密钥长度 for RSA256
//    private static final int DEFAULT_KEY_SIZE = 2048;
//
//    @Value("${jwt.secret:siyaomishi}")
//    private String secret;
//
//    @Value("${jwt.expiration:604800}")
//    private Long expiration;
//
//    @Value("${jwt.tokenHead:'Bearer '}")
//    private String tokenHead;
//
//    //================HS256==================
//
//    /**
//     * 根据用户信息生成token
//     *
//     * @param userDetails 数据库中查询出来的用户信息
//     * @return
//     */
//    public String generateToken(UserDetails userDetails) {
//        Map<String, Object> claims = new HashMap<>();
//        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
//        claims.put(CLAIM_KEY_CREATED, new Date());
//        return generateToken(claims);
//    }
//
//    /**
//     * 刷新token
//     *
//     * @param oldToken
//     * @return
//     */
//    public String refreshHeadToken(String oldToken) {
//        if (StringUtil.isBlank(oldToken)) {
//            return null;
//        }
//        String token = oldToken.substring(tokenHead.length());
//        if (StringUtil.isBlank(token)) {
//            return null;
//        }
//
//        //token校验
//        Claims claims = getClaimsFromToken(token);
//        if (claims == null) {
//            return null;
//        }
//        //token过期，不支持刷新
//        if (isTokenExpire(token)) {
//            return null;
//        }
//
//        //token在30分钟之内刷新过，返回原token
//        if (tokenRefreshJustBefore(token, 30 * 60)) {
//            return token;
//        } else {
//            //生成新的token
//            claims.put(CLAIM_KEY_CREATED, new Date());
//            return generateToken(claims);
//        }
//    }
//
//    /**
//     * 判断token是否在指定时间内刷新过
//     *
//     * @param token 原token
//     * @param time  指定时间s
//     * @return
//     */
//    private boolean tokenRefreshJustBefore(String token, int time) {
//        Claims claim = getClaimsFromToken(token);
//        Date created = claim.get(CLAIM_KEY_CREATED, Date.class);
//        Date refreshDate = new Date();
//        if (refreshDate.after(created) && refreshDate.before(DateUtil.offsetSecond(created, time))) {
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * 从token中获取用户名
//     *
//     * @param token
//     * @return
//     */
//    public String getUserNameFromToken(String token) {
//        String username;
//        try {
//            Claims claims = getClaimsFromToken(token);
//            username = claims.getSubject();
//        } catch (Exception ex) {
//            return "";
//        }
//        return username;
//    }
//
//    /**
//     * 验证token是否有效
//     *
//     * @param token       客户端传入的token
//     * @param userDetails 从数据库中查询出来的用户信息
//     * @return
//     */
//    public boolean validateToken(String token, UserDetails userDetails) {
//        String username = getUserNameFromToken(token);
//        return username.equals(userDetails.getUsername()) && !isTokenExpire(token);
//    }
//
//    /**
//     * 判断token是否过期失效
//     *
//     * @param token
//     * @return
//     */
//    private boolean isTokenExpire(String token) {
//        Date expireDate = getExporeDateFromToken(token);
//        return expireDate.before(new Date());
//    }
//
//    /**
//     * 从token中获取过期时间
//     *
//     * @param token
//     * @return
//     */
//    private Date getExporeDateFromToken(String token) {
//        Claims claims = getClaimsFromToken(token);
//        return claims.getExpiration();
//    }
//
//
//    /**
//     * 根据payload生成jwt的token
//     */
//    private String generateToken(Map<String, Object> claims) {
//        return Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate())
//                     .signWith(SignatureAlgorithm.HS256, secret).compact();
//    }
//
//    /**
//     * 从token找那个获取荷载payload
//     */
//    private Claims getClaimsFromToken(String token) {
//        return Jwts.parser()
//                     .setSigningKey(secret).parseClaimsJws(token)
//                     .getBody();
//    }
//
//    /**
//     * 生成过期时间
//     */
//    private Date generateExpirationDate() {
//        return new Date(System.currentTimeMillis() + (expiration==null?604800:expiration) * 1000);
//    }
//
//
//    //=================RS256===================
//
//    /**
//     * 根据私钥生成token
//     *
//     * @param userDetails
//     * @param privateKey
//     * @return
//     */
//    public String generateRsaToken(UserDetails userDetails, PrivateKey privateKey) {
//        Map<String, Object> claims = new HashMap<>();
//        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
//        claims.put(CLAIM_KEY_CREATED, new Date());
//        return Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate()).signWith(SignatureAlgorithm.RS256, privateKey).compact();
//    }
//
//    /**
//     * 根据公钥解析token
//     * @param token
//     * @param publicKey
//     * @return
//     */
//    public Claims getClaimsFromRsaToken(String token, PublicKey publicKey) {
//        return Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();
//    }
//
//    /**
//     * 从文件中读取公钥
//     *
//     * @param filename 公钥保存路径，相对于classpath
//     * @return PublicKey 公钥对象
//     * @throws Exception
//     */
//    public static PublicKey getPublicKey(String filename) throws Exception {
//        byte[] bytes = readFile(filename);
//        return getPublicKey(bytes);
//    }
//
//    /**
//     * 从文件中读取密钥
//     *
//     * @param filename 私钥保存路径，相对于classpath
//     * @return PrivateKey 私钥对象
//     * @throws Exception
//     */
//    public static PrivateKey getPrivateKey(String filename) throws Exception {
//        byte[] bytes = readFile(filename);
//        return getPrivateKey(bytes);
//    }
//
//    /**
//     * 获取公钥
//     * 公钥的字节形式。
//     *
//     * @param bytes 公钥的字节形式
//     * @return
//     * @throws Exception
//     */
//    private static PublicKey getPublicKey(byte[] bytes) throws Exception {
//        bytes = Base64.getDecoder().decode(bytes);
//        X509EncodedKeySpec spec = new X509EncodedKeySpec(bytes);
//        KeyFactory factory = KeyFactory.getInstance("RSA");
//        return factory.generatePublic(spec);
//    }
//
//    /**
//     * 获取密钥
//     *
//     * @param bytes 私钥的字节形式
//     * @return
//     * @throws Exception
//     */
//    private static PrivateKey getPrivateKey(byte[] bytes) throws NoSuchAlgorithmException, InvalidKeySpecException {
//        bytes = Base64.getDecoder().decode(bytes);
//        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(bytes);
//        KeyFactory factory = KeyFactory.getInstance("RSA");
//        return factory.generatePrivate(spec);
//    }
//
//    /**
//     * 读文件
//     * @param fileName
//     * @return
//     * @throws Exception
//     */
//    private static byte[] readFile(String fileName) throws Exception {
//        return Files.readAllBytes(new File(fileName).toPath());
//    }
//
//    /**
//     * 写文件
//     * @param destPath
//     * @param bytes
//     * @throws IOException
//     */
//    private static void writeFile(String destPath, byte[] bytes) throws IOException {
//        File dest = new File(destPath);
//        if (!dest.exists()) {
//            dest.createNewFile();
//        }
//        Files.write(dest.toPath(), bytes);
//    }
//
//    /**
//     * 生成  公钥+私钥
//     */
//    public static void main(String[] args) throws Exception {
//        String secret = "mysecret";//生成密钥的密文
//        int keySize = 1024;//密钥长度
//        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
//        SecureRandom secureRandom = new SecureRandom(secret.getBytes());
//        keyPairGenerator.initialize(Math.max(keySize, DEFAULT_KEY_SIZE), secureRandom);
//        KeyPair keyPair = keyPairGenerator.genKeyPair();
//        //获取公钥
//        byte[] publicKeyBytes = keyPair.getPublic().getEncoded();
//        publicKeyBytes = Base64.getEncoder().encode(publicKeyBytes);
//        log.info("publicKeyBytes:{}", new String(publicKeyBytes));
//        String rsaPubPath = ResourceUtils.getFile("classpath:rsa/id_rsa_pub").getPath();
//        log.info("rsaPubPath:{}", rsaPubPath);
//        writeFile(rsaPubPath, publicKeyBytes);
//
//        //获取私钥
//        byte[] privateKeyBytes = keyPair.getPrivate().getEncoded();
//        privateKeyBytes = Base64.getEncoder().encode(privateKeyBytes);
//        log.info("privateKeyBytes:{}", new String(privateKeyBytes));
//        String rsaPrivPath = ResourceUtils.getFile("classpath:rsa/id_rsa").getPath();
//        log.info("rsaPrivPath:{}", rsaPrivPath);
//        writeFile(rsaPrivPath, privateKeyBytes);
//
//        //根据私钥生成token
//        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
//        SimpleGrantedAuthority authority = new SimpleGrantedAuthority("admin");
//        authorities.add(authority);
//        User userDetails = new User("lee","admin123",authorities);
//        PrivateKey privateKey = getPrivateKey(rsaPrivPath);
//        JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();
//        String token = jwtTokenUtil.generateRsaToken(userDetails, privateKey);
//        log.info("token:{}",token);
//
//        //根据公钥解析token
//        PublicKey publicKey = getPublicKey(rsaPubPath);
//        Claims claims = jwtTokenUtil.getClaimsFromRsaToken(token, publicKey);
//        log.info("claims:{}", JSON.toJSONString(claims));
//    }
//
//}
