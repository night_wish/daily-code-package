package com.lee.security.jwt;

import com.alibaba.fastjson.JSON;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Lee
 * @version 1.0
 * @description jwt工具和学习
 * @date 2023/7/26 10:44
 */
@Slf4j
public class JwtTokenTest {

    private static final int DEFAULT_KEY_SIZE = 2048;

    @Test
    public void testGenerateRSAKey() throws NoSuchAlgorithmException, IOException {
        String secret = "mysecret";//生成密钥的密文
        int keySize = 1024;//密钥长度
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        SecureRandom secureRandom = new SecureRandom(secret.getBytes());
        keyPairGenerator.initialize(Math.max(keySize, DEFAULT_KEY_SIZE), secureRandom);
        KeyPair keyPair = keyPairGenerator.genKeyPair();
        //获取公钥
        byte[] publicKeyBytes = keyPair.getPublic().getEncoded();
        publicKeyBytes = Base64.getEncoder().encode(publicKeyBytes);
        writeFile("C:\\Users\\le'e\\Desktop\\publicKey.txt",publicKeyBytes);

        //获取私钥
        byte[] privateKeyBytes = keyPair.getPrivate().getEncoded();
        privateKeyBytes = Base64.getEncoder().encode(privateKeyBytes);
        writeFile("C:\\Users\\le'e\\Desktop\\privateKey.txt",privateKeyBytes);
    }

    private static byte[] readFile(String fileName) throws Exception {
        return Files.readAllBytes(new File(fileName).toPath());
    }

    private static void writeFile(String destPath, byte[] bytes) throws IOException {
        File dest = new File(destPath);
        if (!dest.exists()) {
            dest.createNewFile();
        }
        Files.write(dest.toPath(), bytes);
    }


    /**
     * 解析jwt
     */
    @Test
    public void decodeJwt() {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiYWRtaW4iOnRydWUsInVzZXJuYW1lIjoiUmVuIiwiZXhwIjoxNzAyODgzMjEwfQ.pX7BZQkJd0NnMGePuDD7zoOBg0eMwuhLTmPQ0LrP0KE";

        //解析header信息
        JwsHeader header = Jwts.parser()
                                 .setSigningKey("siyaomishi")//私钥密匙(签名算法的盐值key)
                                 .parseClaimsJws(token)
                                 .getHeader();
        log.info("header:{}", JSON.toJSONString(header));
        log.info("alg:{}", header.get("alg"));
        log.info("typ:{}", header.get("typ"));


        //解析荷载payload信息
        Claims payload = Jwts.parser()
                               .setSigningKey("siyaomishi")//私钥密匙（签名算法的盐值key）
                               .parseClaimsJws(token)
                               .getBody();
        log.info("payload:{}", JSON.toJSONString(payload));
        log.info("sub:{}", payload.get("sub"));
        log.info("username:{}", payload.get("username"));
        log.info("admin:{}", payload.get("admin"));

        //解析签名signature信息
        String signature = Jwts.parser()
                                 .setSigningKey("siyaomishi")//私钥密匙（签名算法的盐值key）
                                 .parseClaimsJws(token)
                                 .getSignature();

        log.info("signature:{}", JSON.toJSONString(signature));

    }

    /**
     * 生成jwt
     */
    @Test
    public void generateJwt() {
        //header头部信息
        Map<String, Object> header = new HashMap<>();
        header.put("alg", "HS256");
        header.put("typ", "JWT");

        //payload荷载
        Map<String, Object> payload = new HashMap<>();
        payload.put("sub", "1234567890");
        payload.put("username", "Ren");
        payload.put("admin", true);

        //申明token失效时间
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.SECOND, 300);//300s

        //生成Token
        String token = Jwts.builder()
                             .setHeader(header)//设置头部
                             .addClaims(payload)//设置荷载
                             .setExpiration(instance.getTime())//设置过期时间
                             .signWith(SignatureAlgorithm.HS256, "siyaomishi")//设置自己的私钥密匙
                             .compact();//压缩生成xxx.xxx.xxx

        log.info(token);

        //生成了三次，发现每次生成的header是相同的，payload和signature每次都不相同
        //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
        //eyJzdWIiOiIxMjM0NTY3ODkwIiwiYWRtaW4iOnRydWUsInVzZXJuYW1lIjoiUmVuIiwiZXhwIjoxNzAyODgxNzY0fQ.
        //uW6ISO9lRHzZQs9LqjOyCqx3Od4IJQCg7c5oc9bNb8c

        //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
        //eyJzdWIiOiIxMjM0NTY3ODkwIiwiYWRtaW4iOnRydWUsInVzZXJuYW1lIjoiUmVuIiwiZXhwIjoxNzAyODgxNzkxfQ.
        //B7lYTi4z24JlgmES44u49hFkQFhs5BVydi_U3kzk1W8

        //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
        //eyJzdWIiOiIxMjM0NTY3ODkwIiwiYWRtaW4iOnRydWUsInVzZXJuYW1lIjoiUmVuIiwiZXhwIjoxNzAyODgxODg2fQ.
        //JNg-1zPv0J5nE7maPIEmAzMmSA-Cq_KiEdCbLvUvzRc
    }
}
