package com.lee.security.springSecurity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023-12-21 11:22
 */
@RestController
public class HelloSecurityController {

    @GetMapping("/hello")
    public String hello(){
        return "hello spring security";
    }
}
