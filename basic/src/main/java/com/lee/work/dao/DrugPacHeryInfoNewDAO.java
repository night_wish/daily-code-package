package com.lee.work.dao;


import com.lee.work.tce.entity.DrugPacHeryInfoNew;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DrugPacHeryInfoNewDAO {

    int insert(@Param("lists") List<DrugPacHeryInfoNew> record);

}