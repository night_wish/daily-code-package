package com.lee.work.dao;


import com.lee.work.tce.entity.DrugPacHeryInfoOri;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DrugPacHeryInfoOriDAO {

    int insert(@Param("lists") List<DrugPacHeryInfoOri> record);

}