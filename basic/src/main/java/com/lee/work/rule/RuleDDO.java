package com.lee.work.rule;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 智能监管规则信息表
 * </p>
 *
 * @author WANGHONGBIN
 * @since 2021-12-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class RuleDDO {

    private static final long serialVersionUID = 1L;


    private String ruleCode;
    private String ruleVer;
    private String enable;


}
