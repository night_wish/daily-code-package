package com.lee.work.rule;

import lombok.Data;

import java.util.List;

/**
 * 规则模板
 *
 */
@Data
public class RuleTemplateJson {

    private String rid;

    private String progCode;//程序编码----JA0001

    private String ruleTmplName;//规则模板名称----患者频繁入院_事后

    private Integer ruleSevDeg;//严重程度----1高度可疑

    private Integer volaBhvrType;//违规定性----对患者分解住院

    private Integer ruleLv;//规则级别----1国家级

    private String ruleSouc;//规则来源----

    private String volaEvid;//违规依据----

    private String ruleDscr;//规则说明----

//    private List<OptionItem> optionItemList;//选项----如：触发场景、数据对象、数据分组、违规输出、阈值、自定义知识

    private List<TmplDic> tmplDicList;

}






//@Data
//class  OptionItem{
//
//    private String optionCodg;//选项编码----如：0001/0003
//
//    private String optionName;//选项名称----如：数据对象/数据分组
//
//    private Boolean dispFlag;//是否显示-----0不显示1显示
//
//    private List<TabsItem> tabsItemList;//标签列表----如：医疗就诊类型
//}
//
//@Data
//class TabsItem{
//
//    private String tabsCode;//标签编码----如：TEXT_OUTPUT/BUSINESS_GROUP_TYPE
//
//    private String tabsName;//标签名称----如：违规内容/业务分组类别
//
//    private Integer validType;//验证类型----如：数据字典、自定义字典、memo、input
//
//    private String defaultVal;//默认值----如：患者使用【{0}/{1}】，触发{2}【{3}】
//
//    private String memo;//备注----如：{"{0}":"医保项目名称","{1}":"医院项目名称","{2}":"禁忌类型","{3}":"禁忌内容"}
//
//    private Boolean dispFlag;//是否显示-----0不显示1显示
//
//    private List<UnitItem> unitItemList;//单元列表
//}
//
//@Data
//class UnitItem{
//    private String unitCode;//单元编码---如：AGE_UPLMT
//
//    private String unitName;//单元名称---如：年龄上限
//
//    private Integer validType;//验证类型----如：数据字典、自定义字典、memo、input
//}
