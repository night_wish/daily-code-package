package com.lee.work.rule;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Types;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@SpringBootTest
public class Test {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @org.junit.jupiter.api.Test
    public void test1() {
        String sql = "select * from tmpl_dic order by tmpl_group,sort ";
        List<TmplDic> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(TmplDic.class));


        List<TmplDic> tmplDics = TmplDic.buildDeptTree(list);

        RuleTemplateJson ruleTmpl = new RuleTemplateJson();
        ruleTmpl.setRid("123456");
        ruleTmpl.setProgCode("JA0001");
        ruleTmpl.setRuleTmplName("患者频繁入院_事后_模板");
        ruleTmpl.setRuleSevDeg(1);
        ruleTmpl.setVolaBhvrType(2);
        ruleTmpl.setRuleLv(1);
        ruleTmpl.setRuleSouc("来源");
        ruleTmpl.setVolaEvid("违规依据");
        ruleTmpl.setRuleDscr("描述");
        ruleTmpl.setTmplDicList(tmplDics);

        log.info("----->" + JSON.toJSONString(ruleTmpl));
    }


    @org.junit.jupiter.api.Test
    public void test2() {
        List<String> admdvList = new ArrayList<>();
//        String admdvs = "430000";//省
//        String admdvs = "430700";//市
        String admdvs = "430712";//区县

        String admdvslik = admdvs.substring(0, 4);

        if (admdvs.charAt(3) - 48 > 0)
            admdvList.add(admdvslik.substring(0, 3) + "0");
        if (admdvslik.charAt(2) - 48 > 0)
            admdvList.add(admdvslik.substring(0, 2) + "0");
        admdvList.add(admdvslik);

        //省 4300
        //市 "4310","430","4311"
        //区县 "4300","4301"
        log.info("--->{}", JSON.toJSONString(admdvList));
    }

    @org.junit.jupiter.api.Test
    public void test3() {
        List<RuleDDO> ruleDDOList = new ArrayList<>();
        RuleDDO r1 = new RuleDDO().setRuleCode("code1").setRuleVer("v1").setEnable("0");
        RuleDDO r2 = new RuleDDO().setRuleCode("code2").setRuleVer("v1").setEnable("0");
        RuleDDO r3 = new RuleDDO().setRuleCode("code3").setRuleVer("v1").setEnable("0");
        RuleDDO r4 = new RuleDDO().setRuleCode("code4").setRuleVer("v1").setEnable("0");
        ruleDDOList.add(r1);
        ruleDDOList.add(r2);
        ruleDDOList.add(r3);
        ruleDDOList.add(r4);


        if (ruleDDOList != null && ruleDDOList.size() > 0) {
            List<String> enableRuleCodeList = new ArrayList<>();
            List<String> enableRuleCodeAndVerList = new ArrayList<>();
            List<String> unableRuleCodeAndVerList = new ArrayList<>();
            ruleDDOList.stream().forEach(r -> {
                if ("1".equals(r.getEnable())) {
                    enableRuleCodeList.add(r.getRuleCode());
                    enableRuleCodeAndVerList.add(r.getRuleCode() + ":" + r.getRuleVer());
                } else {
                    unableRuleCodeAndVerList.add(r.getRuleCode() + ":" + r.getRuleVer());
                }
            });
            log.info("---->{}", JSON.toJSONString(enableRuleCodeList));
            log.info("---->{}", JSON.toJSONString(enableRuleCodeAndVerList));
            log.info("---->{}", JSON.toJSONString(unableRuleCodeAndVerList));
        }
    }

    @org.junit.jupiter.api.Test
    public void test4() {
        String provSlfLvAdmdvs = "439900";
        String provCapitalAdmdvs = "430100";
        String sqlFormat = "xxx as k on $1whereAdmdvsOn inner join (select ...)";
        String whereAdmdvsOn = "$1whereAdmdvsOn";


        ConditionParamDTO createTaskDTO = new ConditionParamDTO();
        createTaskDTO.setAdmdvsList(Arrays.asList("430100"));
        createTaskDTO.setRemoSettFlag("1");


        String whereAdmdvsOnValue = "";
        if (createTaskDTO.getAdmdvsList().size() == 1 && provSlfLvAdmdvs.equals(createTaskDTO.getAdmdvsList().get(0)) && "0".equals(createTaskDTO.getRemoSettFlag())) {
            //省本级 且 非异地就医：
            whereAdmdvsOnValue = " m.INSU_ADMDVS = k.ADMDVS ";
        } else if (createTaskDTO.getAdmdvsList().size() == 1 && provCapitalAdmdvs.equals(createTaskDTO.getAdmdvsList().get(0)) && "0".equals(createTaskDTO.getRemoSettFlag())) {
            //省会(长沙) 且  非异地就医：
            whereAdmdvsOnValue = " m.FIX_BLNG_ADMDVS = k.ADMDVS AND m.INSU_ADMDVS NOT IN (" + provSlfLvAdmdvs + ")";
        } else {
            //其他
            whereAdmdvsOnValue = " m.FIX_BLNG_ADMDVS = k.ADMDVS ";
        }
        sqlFormat = sqlFormat.replace(whereAdmdvsOn, whereAdmdvsOnValue);


        log.info("-------->:{}" + sqlFormat);
    }

    @org.junit.jupiter.api.Test
    public void test5(){
        String taskid = "202211211612110360000211110000";
        Map<String, Object> krTaskExeInfoDTO = new HashMap<>();
        String sql = "select TASK_ID taskId,ADMDVS admdvs,ADMDVS_NAME admdvsName from task_exe_info_d where task_id =?";
        try {
            int[] argTypes = {Types.VARCHAR};
            krTaskExeInfoDTO = jdbcTemplate.queryForMap(sql, new Object[]{taskid},argTypes);
        } catch (DataAccessException e) {
            log.info("error with prcsCfgInfoCont,task_exe_info_d " + e.getMessage());
        }

        log.info("====result:{}",JSON.toJSONString(krTaskExeInfoDTO));
    }

    @org.junit.jupiter.api.Test
    public void test6(){
        int recordCounts = 1327736;
        int PAGE_SIZE = 1000;


        final int pageNum1 = (recordCounts - 1) / PAGE_SIZE + 1;
        final int pageNum2 = recordCounts % PAGE_SIZE==0 ? recordCounts/PAGE_SIZE : (recordCounts/PAGE_SIZE)+1;

        log.info("=====>pageNum1:{}, pageNum2:{}",pageNum1,pageNum2);
    }


}
