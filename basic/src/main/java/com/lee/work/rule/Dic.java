package com.lee.work.rule;

import lombok.Data;

import java.util.List;

/**
 * 1、{type:DicEnum.type1.getKey(),name:医疗就诊类型 ,defaultValue:,[{code:m1,name:门诊，value:1}，{code:z1，name:住院,value:2}]}
 * 2、{type:DicEnum.type2.getKey(),name:xxx,defaultValue:, [{code:c1,name:n1,value:1}，{code:c2，name:n2,value:2}]}
 * 3、{type:DicEnum.type3.getKey(),code:xxx,name:诊断过滤,defaultValue:, [{code:r1,name:入院主诊断,value:1}，{code:c1，name:出院主诊断,value:2}]}
 * 4、{type:DicEnum.type4.getKey(),code:jggl,name:机构过滤 ,defaultValue:,[{code:orgcode,name:机构代码，value: }，{code:orgName,name:机构名称，value:2}]}
 * 5、{type:DicEnum.type5.getKey(),code:wgnr,name:违规内容, defaultValue:xxxx,[{code:{0},name:医保目录代码,value:},{code:{1},name:医保项目名称,value:}]}
 * 6、{type:DicEnum.type6.getKey(),code:wgfw,name:违规范围，defaultValue:,[{code:qbjz,name:全部就诊,value:1},{code:sjwdjz,name:时间晚的就诊,value:2}]}
 */
@Data
public class Dic {

    public static void main(String[] args) {
        DicEnum.type1.getKey();
    }

    private Integer type;

    private String code;

    private String name;

    private String defaultValue;

    private List<DicArray> dicArrayList;

    @Data
    class DicArray{
        private String code;

        private String name;

        private String value;
    }

}
