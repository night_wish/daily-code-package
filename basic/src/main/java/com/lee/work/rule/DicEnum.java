package com.lee.work.rule;

import lombok.Data;

public enum DicEnum {
    type1(1,"数据字典checkbox"),
    type2(2,"数据字典radio"),
    type3(3,"自定义checkbox"),
    type4(4,"自定义input"),
    type5(5,"自定义违规"),
    type6(6,"自定义radio")

    ;

    private Integer key;

    private String name;

    DicEnum(Integer key, String name) {
        this.key = key;
        this.name = name;
    }

    public Integer getKey() {
        return key;
    }

    public String getName() {
        return name;
    }
}
