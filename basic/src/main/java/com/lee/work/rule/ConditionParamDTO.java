package com.lee.work.rule;

import lombok.Data;

import java.util.List;

@Data
public class ConditionParamDTO {

    private List<String> admdvsList;
    private String remoSettFlag;
}
