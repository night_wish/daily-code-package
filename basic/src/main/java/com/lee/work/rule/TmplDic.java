package com.lee.work.rule;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 模板字典
 */
@Data
public class TmplDic {

    private String rid;

    private String tmplCode;//字典编码

    private String tmplName;//字典名称

    private String tmplVal;//字典值

    private String tmplType;//字典类型

    private Integer tmplLv;//等级

    private String tmplGroup;//字典分组

    private Integer sort;//排序

    private String dscr;//描述

    private List<TmplDic> children;

    //=========================
    private String defaultVal;//默认值




    /**
     * 转换成树状结构
     * @param childrenDtoList
     * @return
     */
    public static List<TmplDic> buildDeptTree(List<TmplDic> childrenDtoList)
    {
        List<TmplDic> returnList = new ArrayList<TmplDic>();
        List<String> tempList = new ArrayList<String>();
        for (TmplDic dto : childrenDtoList)
        {
            tempList.add(dto.getTmplCode());
        }
        for (Iterator<TmplDic> iterator = childrenDtoList.iterator(); iterator.hasNext();)
        {
            TmplDic dto = (TmplDic) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dto.getTmplGroup()))
            {
                recursionFn(childrenDtoList, dto);
                returnList.add(dto);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = childrenDtoList;
        }
        return returnList;
    }

    private static void recursionFn(List<TmplDic> list, TmplDic t)
    {
        // 得到子节点列表
        List<TmplDic> childList = getChildList(list, t);
        t.setChildren(childList);
        for (TmplDic tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    private static List<TmplDic> getChildList(List<TmplDic> list, TmplDic t)
    {
        List<TmplDic> tlist = new ArrayList<TmplDic>();
        Iterator<TmplDic> it = list.iterator();
        while (it.hasNext())
        {
            TmplDic n = (TmplDic) it.next();
            if (StringUtils.isNotBlank(n.getTmplGroup()) && n.getTmplGroup().equals(t.getTmplCode()))
            {
                tlist.add(n);
            }
        }

        return tlist;
    }

    private static boolean hasChild(List<TmplDic> list, TmplDic t)
    {
        return getChildList(list, t).size() > 0 ? true : false;
    }
}
