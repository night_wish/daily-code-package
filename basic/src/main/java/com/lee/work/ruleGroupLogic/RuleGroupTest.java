package com.lee.work.ruleGroupLogic;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 规则分组逻辑
 * 为什么要进行规则分组？
 *
 * 文件名称：tmp_input_${ruleGroupId}_${taskId}
 * 内容：第一列是：${ruleGroupId}
 * 也就是说 同一个规则组下的 数据是放在同一个 input文件里的
 *
 * 1、同一条规则分组 他的BUSINESS_GROUP_TYPE、BUSINESS_GROUP_DEPEND、BUSINESS_GROUP_ENCOUNTER、BUSINESS_GROUP_ORDER以及他们的值都是一样的，
 * 导致他们的数据准备的筛选条件是一样的，SQL也是一样的，因此数据是一样的，为了节省空间，可以使用同一个input文件里的数据
 *
 * 2、风控分析的时候也是
 * List<DataGroupDTO> dataGroupDtoList
 *
 * DataGroupDTO{
 *   private Map<String, DataEncounterDTO> dataEncounterMap;//每一行input文件中的就诊数据
 *   private Map<String, DataOrderDTO> dataOrderMap;//每一行input文件中的处方数据
 *   private Map<String, DataDiagnoseDTO> dataDiagnoseMap;//每一行input文件中的诊断数据
 *   private Map<String, DataOperationDTO> dataOperationMap;//每一行input文件中的手术数据
 *   AnalyzeInDTO{
 *       private String syscode;//IMS
 * 	  private String taskId;//任务
 * 	  private String trigScen;//3 事后
 * 	  private List<String> ruleIds;//根据input文件的第一列数据ruleGroupId从ruleGroupTypesCache中找出其对应的ruleIds
 *   }
 * }
 *
 * Map<String, List<String>> ruleGroupTypesCache{
 * 	key: ruleGroupId,
 * 	value: ruleIds
 * }
 *
 * ruleGroupTypesCache存储在 RuleMetaDataDTO.getInstance()中， 元数据是单例的
 */
@Slf4j
public class RuleGroupTest {


    /**
     * BUSINESS_GROUP_TYPE ： 0 单就诊 1日 2月 4年
     * MED_MDTRT_TYPE 就诊类型： 1门诊、2住院、3购药、4其他
     *
     * dic规则医疗机构分类 用于筛选数据，作用于：医疗就诊类型（门诊、住院、购药）
     *
     * 1、所有的 单就诊都会分到一个组
     * 2、跨就诊  根据BUSINESS_GROUP_TYPE、BUSINESS_GROUP_DEPEND、BUSINESS_GROUP_ENCOUNTER、BUSINESS_GROUP_ORDER，以及他们的值分组
     * BUSINESS_GROUP_TYPE_0-BUSINESS_GROUP_DEPEND_ADM_TIME-BUSINESS_GROUP_ENCOUNTER_PATN_ID-BUSINESS_GROUP_ORDER_ -> {ArrayList@1070}  size = 2
     * BUSINESS_GROUP_TYPE_0-BUSINESS_GROUP_DEPEND_ADM_TIME-BUSINESS_GROUP_ENCOUNTER_MDTRT_ID-BUSINESS_GROUP_ORDER_ -> {ArrayList@1072}  size = 1
     */
    public static void main(String[] args) {
        List<RuleKngConfigDO> kngDataOfGroup = new ArrayList<>();
        RuleKngConfigDO d1 = new RuleKngConfigDO("rule1","BUSINESS_GROUP_TYPE","1","pc001"); //日
        RuleKngConfigDO d2 = new RuleKngConfigDO("rule1","BUSINESS_GROUP_DEPEND","ADM_TIME,DSCG_TIME","pc001");
        RuleKngConfigDO d3 = new RuleKngConfigDO("rule1","BUSINESS_GROUP_ENCOUNTER","PATN_ID,PATN_ID2","pc001");

        RuleKngConfigDO d4 = new RuleKngConfigDO("rule2","BUSINESS_GROUP_TYPE","1","pc002");//日
        RuleKngConfigDO d5 = new RuleKngConfigDO("rule2","BUSINESS_GROUP_DEPEND","ADM_TIME","pc002");
        RuleKngConfigDO d6 = new RuleKngConfigDO("rule2","BUSINESS_GROUP_ENCOUNTER","PATN_ID","pc002");

        RuleKngConfigDO d7 = new RuleKngConfigDO("rule3","BUSINESS_GROUP_TYPE","1","pc003");//月
        RuleKngConfigDO d8 = new RuleKngConfigDO("rule3","BUSINESS_GROUP_DEPEND","ADM_TIME","pc003");
        RuleKngConfigDO d9 = new RuleKngConfigDO("rule3","BUSINESS_GROUP_ENCOUNTER","MDTRT_ID","pc003");
        RuleKngConfigDO d10 = new RuleKngConfigDO("rule3","MED_MDTRT_TYPE","3","pc003");
        kngDataOfGroup.add(d1);
        kngDataOfGroup.add(d2);
        kngDataOfGroup.add(d3);
        kngDataOfGroup.add(d4);
        kngDataOfGroup.add(d5);
        kngDataOfGroup.add(d6);
        kngDataOfGroup.add(d7);
        kngDataOfGroup.add(d8);
        kngDataOfGroup.add(d9);
        kngDataOfGroup.add(d10);



        List<Map<String, String>> dataList = new ArrayList<>();

        //key: ruleId   value: rule对应的知识List
        Map<String, List<RuleKngConfigDO>> collect = (Map<String, List<RuleKngConfigDO>>)kngDataOfGroup.stream().collect(Collectors.groupingBy(RuleKngConfigDO::getRuleId));

        //组装rule对应的kng
        //将同样的key的value，用逗号分割组成字符串,空的key用空字符串代替
        collect.entrySet().stream().forEach(entry -> {
            List<RuleKngConfigDO> value = (List<RuleKngConfigDO>)entry.getValue();//rule对应的知识List
            Map<String, String> kngMap = new HashMap<>();
            valueOfGroupHandler(value, kngMap);
            if (!kngMap.containsKey("BUSINESS_GROUP_DEPEND")) {
                kngMap.put("BUSINESS_GROUP_DEPEND", "");
            }
            if (!kngMap.containsKey("BUSINESS_GROUP_ENCOUNTER")) {
                kngMap.put("BUSINESS_GROUP_ENCOUNTER", "");
            }
            if (!kngMap.containsKey("BUSINESS_GROUP_ORDER")) {
                kngMap.put("BUSINESS_GROUP_ORDER", "");
            }
            kngMap.put("PROG_CODE", ((RuleKngConfigDO)value.get(0)).getProgCode());
            kngMap.put("RULE_ID", (String)entry.getKey());
            dataList.add(kngMap);
        });

        List<RuleGroupTypeDTO> ruleTypeDTOS = groupTypeDTOS(kngDataOfGroup, dataList);

        log.info("ruleTypeDtos:{}", JSON.toJSONString(ruleTypeDTOS));
    }


    /**
     * 1、封装rule对应的kngMap
     * 参数：value 单个rule对应的 知识list,kngMap 为null
     * 将同样的key的value，用逗号分割组成字符串
     */
    private static void valueOfGroupHandler(List<RuleKngConfigDO> value, Map<String, String> kngMap) {
        for (RuleKngConfigDO ruleKngConfigDO : value) {
            //因为就诊 和 处方  是 复选框,所以单独拎出来
            if ("BUSINESS_GROUP_ENCOUNTER".equals(ruleKngConfigDO.getKngCode()) || "BUSINESS_GROUP_ORDER".equals(ruleKngConfigDO.getKngCode()))
                if (StringUtils.isNotEmpty(ruleKngConfigDO.getKngCont())) {
                    List<String> strList = Arrays.asList(ruleKngConfigDO.getKngCont().split(","));
                    Collections.sort(strList);
                    kngMap.put(ruleKngConfigDO.getKngCode(), KngCfgUtils.getStrFromList(strList));
                    continue;
                }
            if (StringUtils.isEmpty(ruleKngConfigDO.getKngCont())) {
                kngMap.put(ruleKngConfigDO.getKngCode(), "");
                continue;
            }
            kngMap.put(ruleKngConfigDO.getKngCode(), ruleKngConfigDO.getKngCont());
        }
    }

    private static List<RuleGroupTypeDTO> groupTypeDTOS(List<RuleKngConfigDO> kngDataOfGroup, List<Map<String, String>> dataList) {
        //根据BUSINESS_GROUP_TYPE_值1 - BUSINESS_GROUP_DEPEND_值1 - BUSINESS_GROUP_ENCOUNTER_值1 - BUSINESS_GROUP_ORDER_值1，以及他们的值分组
        Map<String, List<Map<String, String>>> listMap = (Map<String, List<Map<String, String>>>)dataList.stream().collect(Collectors.groupingBy(e -> "BUSINESS_GROUP_TYPE_" + (String)e.get("BUSINESS_GROUP_TYPE") + "-" + "BUSINESS_GROUP_DEPEND" + "_" + (String)e.get("BUSINESS_GROUP_DEPEND") + "-" + "BUSINESS_GROUP_ENCOUNTER" + "_" + (String)e.get("BUSINESS_GROUP_ENCOUNTER") + "-" + "BUSINESS_GROUP_ORDER" + "_" + (String)e.get("BUSINESS_GROUP_ORDER")));
        List<String> keysList = new ArrayList<>(listMap.keySet());
        Collections.sort(keysList);

        //医疗就诊类型 为了组装Dic,
        //MED_MDTRT_TYPE 1门诊、2住院、3购药、4其他
        Map<String, String> medMdtrtTypeMap = (Map<String, String>)kngDataOfGroup.stream().filter(e -> "MED_MDTRT_TYPE".equals(e.getKngCode())).collect(Collectors.toMap(RuleKngConfigDO::getRuleId, RuleKngConfigDO::getKngCont));
        RuleGroupTypeDTO ruleGroupTypeDTOTemp = new RuleGroupTypeDTO();
        List<String> idsTemp = new ArrayList<>();
        List<String> dics = new ArrayList<>();


        List<RuleGroupTypeDTO> ruleTypeDTOS = getLoopData(ruleGroupTypeDTOTemp, keysList, medMdtrtTypeMap, idsTemp, dics, listMap);//单就诊为null


        if (CollectionUtils.isNotEmpty(idsTemp)) { //单次就诊外层处理
            ruleGroupTypeDTOTemp.setRuleGroupId("0");
            ruleGroupTypeDTOTemp.setRuleIds(idsTemp);
            ruleGroupTypeDTOTemp.setDic(new ArrayList(new HashSet(dics)));
            ruleGroupTypeDTOTemp.setBusinessGroupOrder("");
            ruleTypeDTOS.add(0, ruleGroupTypeDTOTemp);
        }
        return ruleTypeDTOS;
    }

    private static List<RuleGroupTypeDTO> getLoopData(RuleGroupTypeDTO ruleGroupTypeDTOTemp, List<String> keysList, Map<String, String> medMdtrtTypeMap, List<String> idsTemp, List<String> dics, Map<String, List<Map<String, String>>> listMap) {
        List<RuleGroupTypeDTO> ruleTypeDTOS = new ArrayList<>();
        int ruleGroupId = 1;
        int size = keysList.size();
        for (int i = 0; i < size; i++) {
            RuleGroupTypeDTO ruleTypeDTO = new RuleGroupTypeDTO();
            List<Map<String, String>> value = listMap.get(keysList.get(i));
            List<String> ids = (List<String>)value.stream().map(e -> (String)e.get("RULE_ID")).collect(Collectors.toList());
            List<String> ruleGroupTypeDicList = getRuleGroupTypeDicList(ids, medMdtrtTypeMap);//医疗就诊类型
            if ("0".equals(((Map)value.get(0)).get("BUSINESS_GROUP_TYPE"))) {//就诊类型==单就诊
                ruleGroupTypeDTOTemp.setBusinessGroupType((String)((Map)value.get(0)).get("BUSINESS_GROUP_TYPE"));//分组类型
                ruleGroupTypeDTOTemp.setBusinessGroupEncounter((String)((Map)value.get(0)).get("BUSINESS_GROUP_ENCOUNTER"));//就诊分组
                idsTemp.addAll(ids);
                dics.addAll(ruleGroupTypeDicList);
            } else {//就诊类型==跨就诊
                ruleTypeDTO.setBusinessGroupType((String)((Map)value.get(0)).get("BUSINESS_GROUP_TYPE"));
                ruleTypeDTO.setBusinessGroupDepend((String)((Map)value.get(0)).get("BUSINESS_GROUP_DEPEND"));
                ruleTypeDTO.setBusinessGroupEncounter((String)((Map)value.get(0)).get("BUSINESS_GROUP_ENCOUNTER"));
                ruleTypeDTO.setBusinessGroupOrder((String)((Map)value.get(0)).get("BUSINESS_GROUP_ORDER"));
                ruleTypeDTO.setRuleIds(ids);
                ruleTypeDTO.setDic(ruleGroupTypeDicList);
                ruleTypeDTO.setRuleGroupId(String.valueOf(ruleGroupId));
                ruleGroupId++;
                ruleTypeDTOS.add(ruleTypeDTO);
            }
        }
        return ruleTypeDTOS;
    }

    /**
     * 组装dic，即数据筛选中的：MED_MDTRT_TYPE IN （dic）
     * 1门诊、2住院、3购药、4其他
     *
     * 如果 规则对应的知识 MED_MDTRT_TYPE 为 空，则 1234
     * 如果 规则对应的知识 MED_MDTRT_TYPE 为 多个(大于1)，则1234
     * 如果 规则对应的知识 MED_MDTRT_TYPE 为 1个，则就是那个
     */
    private static List<String> getRuleGroupTypeDicList(List<String> ids, Map<String, String> medMdtrtTypeMap) {
        Set<String> strings = new HashSet<>();
        ids.stream().forEach(e -> {
            String string = (String)medMdtrtTypeMap.get(e);
            if (StringUtils.isEmpty(string)) {
                strings.addAll(Arrays.asList("1,2,3,4".split(",")));
            } else {
                String[] split = string.split(",");
                if (split.length == 1 && "1".equals(string)) {
                    strings.add("1");
                } else if (split.length == 1 && "2".equals(string)) {
                    strings.add("2");
                } else if (split.length == 1 && "3".equals(string)) {
                    strings.add("3");
                } else {
                    strings.addAll(Arrays.asList("1,2,3,4".split(",")));
                }
            }
        });
        return new ArrayList<>(strings);
    }


}

