package com.lee.work.ruleGroupLogic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class RuleKngConfigDO {

    private String ruleId;

    private String kngCode;

    private String kngCont;

    private String progCode;

}
