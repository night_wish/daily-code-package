package com.lee.work.ruleGroupLogic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RuleGroupTypeDTO {

    private String ruleGroupId;

    private String businessGroupType;

    private String businessGroupDepend;

    private String businessGroupEncounter;

    private String businessGroupOrder;

    private String selectMdtrt;

    private String selectOrder;

    private String selectDiagnose;

    private String tableGroup;

    private List<String> ruleIds;

    private List<String> dic;
}

