package com.lee.work.ruleGroupLogic;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

public class KngCfgUtils {

    public static void main(String[] args) {
        List<String> strList = Arrays.asList("1","2","3","4");
        String collect = strList.stream().collect(Collectors.joining(","));
        System.out.println(collect);

        String strFromList = getStrFromList(strList);
        System.out.println(strFromList);
    }

    public static String getStrFromList(List<String> list) {
        StringBuilder stringBuilder = new StringBuilder();
        if (CollectionUtils.isEmpty(list))
            return stringBuilder.toString();
        for (String s : list)
            stringBuilder.append(s).append(",");
        return stringBuilder.toString().substring(0, stringBuilder.toString().lastIndexOf(','));
    }

    public static Set<String> getUnionPrimaryKeyStringList(List<String> customTableHeadOfPrimaryList, List<Map<String, Object>> ruleAllCustomKnowledge) {
        Set<String> primaryValueSet = new HashSet<>();
        if (CollectionUtils.isEmpty(customTableHeadOfPrimaryList))
            return primaryValueSet;
        ruleAllCustomKnowledge.stream().forEach(e -> primaryValueSet.add(getUnionPrimaryKeyString(customTableHeadOfPrimaryList, e)));
        return primaryValueSet;
    }

    public static String getUnionPrimaryKeyString(List<String> customTableHeadOfPrimaryList, Map<String, Object> ruleKnowledge) {
        StringBuilder unionPrimaryKeyString = new StringBuilder();
        for (String primaryKey : customTableHeadOfPrimaryList) {
            if (ruleKnowledge.get(primaryKey)!=null)
                unionPrimaryKeyString.append(ruleKnowledge.get(primaryKey).toString());
        }
        return unionPrimaryKeyString.toString();
    }

    public static boolean checkPrimaryValue(List<String> customTableHeadOfPrimaryList, Map<String, Object> ruleKnowledge) {
        for (String key : customTableHeadOfPrimaryList) {
            if (StringUtils.isEmpty(ruleKnowledge.get(key).toString()))
                return true;
        }
        return false;
    }

}
