package com.lee.work.tce.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.lee.poi.easyexcel.StringLengthLimitConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lee
 * @version 1.0
 * @description 西药中成药药品代码基础表
 * @date 2024/12/13 15:41
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class WmTcmpatInfoLibDO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ExcelIgnore
    private String id;

    /**
     * 药品代码
     */
    @ExcelProperty(value = "药品代码", index = 0, converter = StringLengthLimitConverter.class)
    private String medListCodg;

    /**
     * 注册名称
     */
    @ExcelProperty(value = "注册名称", index = 1, converter = StringLengthLimitConverter.class)
    private String regName;

    /**
     * 注册剂型
     */
    @ExcelProperty(value = "注册剂型", index = 2, converter = StringLengthLimitConverter.class)
    private String regDosform;

    /**
     * 注册规格
     */
    @ExcelProperty(value = "注册规格", index = 3, converter = StringLengthLimitConverter.class)
    private String regSpec;

    /**
     * 最小包装数量
     */
    @ExcelProperty(value = "最小包装数量", index = 4)
    private String minPacCnt;

    /**
     * 最小制剂单位
     */
    @ExcelProperty(value = "最小制剂单位", index = 5, converter = StringLengthLimitConverter.class)
    private String minPrepunt;

    /**
     * 最小包装单位
     */
    @ExcelProperty(value = "最小包装单位", index = 6, converter = StringLengthLimitConverter.class)
    private String minPacunt;

    /**
     * 生产企业名称
     */
    @ExcelProperty(value = "生产企业名称", index = 7, converter = StringLengthLimitConverter.class)
    private String prodentpName;

    /**
     * 上市许可证持有人
     */
    @ExcelProperty(value = "上市许可证持有人", index = 8, converter = StringLengthLimitConverter.class)
    private String lstdLicHolder;

    /**
     * 批准文号
     */
    @ExcelProperty(value = "批准文号", index = 9, converter = StringLengthLimitConverter.class)
    private String aprvno;

    /**
     * 药品本位码
     */
    @ExcelProperty(value = "药品本位码", index = 10)
    private String drugstdcode;

    /**
     * 药品标识码
     */
    @ExcelProperty(value = "药品标识码", index = 11)
    private String drugIdCode;

    /**
     * 发码机构
     */
    @ExcelProperty(value = "发码机构", index = 12, converter = StringLengthLimitConverter.class)
    private String issuingOrg;

    /**
     * 商品条码
     */
    @ExcelProperty(value = "商品条码", index = 13)
    private String barCode;

    /**
     * 版本号
     */
    @ExcelProperty(value = "版本号", index = 14)
    private String ver;

    /**
     * 有效标志{1：有效|0：失效}
     */
    @ExcelProperty(value = "有效标志", index = 15, converter = StringLengthLimitConverter.class)
    private String valiFlag;

    /**
     * 数据唯一记录号
     */
    @ExcelIgnore
    private String rid;

    /**
     * 数据创建时间
     */
    @ExcelIgnore
    private Date crteTime;

    /**
     * 数据更新时间
     */
    @ExcelIgnore
    private Date updtTime;

    /**
     * 创建人ID
     */
    @ExcelIgnore
    private String crterId;

    /**
     * 创建人姓名
     */
    @ExcelIgnore
    private String crterName;

    /**
     * 经办人ID
     */
    @ExcelIgnore
    private String opterId;

    /**
     * 经办人姓名
     */
    @ExcelIgnore
    private String opterName;

    /**
     * 经办时间
     */
    @ExcelIgnore
    private Date optTime;
}
