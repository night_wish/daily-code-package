package com.lee.work.tce;

import cn.hsa.ims.common.util.StringUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import com.lee.base.strings.StringUtils;
import com.lee.mybatis.mapper.DrugListMapper;
import com.lee.work.tce.entity.WmTcmpatInfoLibDO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2025/2/14 9:10
 */
@Slf4j
@SpringBootTest
public class WmTcmpatInfoLibTest {

    @Resource
    private DrugListMapper drugListDAO;

    @Resource
    private JdbcTemplate jdbcTemplate;

    private static final Integer batchSize = 500;

    @Test
    public void readAndWriteFile() throws IOException {
        //String path = "C:\\Users\\james\\Desktop\\C_新增对接企业名单_三码库结果表.xlsx";
        String path = "C:\\Users\\james\\Downloads\\C_新增两家企业名单_三码库结果表0217.xlsx";
        //创建输入流
        FileInputStream fileInputStream = new FileInputStream(path);
        //获得poi输入流
        Date currentDate = new Date();
        EasyExcel.read(fileInputStream, WmTcmpatInfoLibDO.class, new PageReadListener<WmTcmpatInfoLibDO>(dataList -> {
            for (WmTcmpatInfoLibDO dto : dataList) {
                dto.setRid(UUID.randomUUID().toString().replace("-", ""));
                dto.setId(UUID.randomUUID().toString().replace("-", ""));
                dto.setCrteTime(currentDate);
                dto.setUpdtTime(currentDate);
                dto.setCrterId("001");
                dto.setCrterName("system");
                dto.setOpterId("001");
                dto.setOpterName("system");
                dto.setOptTime(currentDate);
                if(StringUtil.isNotBlank(dto.getDrugIdCode())){
                    dto.setDrugIdCode(encrypt(dto.getDrugIdCode()));
                }
                if(StringUtil.isNotEmpty(dto.getBarCode())){
                    dto.setBarCode(encrypt(dto.getBarCode()));
                }
            }
            drugListDAO.batchSaveTcmpatList(dataList);
        }, 500)).sheet().headRowNumber(1).doRead();
        fileInputStream.close();
    }

    public static void main(String[] args) {
        WmTcmpatInfoLibTest test = new WmTcmpatInfoLibTest();
        String decrypt1 = test.decrypt("0bdc113187ebf78f12b8b4674ba592f7");
        String decrypt2 = test.decrypt("728b1ca28e9db73da6702dc439c5e17a");
        System.out.println("|"+decrypt1+"|");
        System.out.println("|"+decrypt2+"|");
    }

    /**
     * 加密
     */
    private String encrypt(String params) {
        String key = "gxjkhsaftcl01234";
        String iv = "0102030405060708";
        AES aes = new AES(Mode.CBC, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        return StrUtil.isNotEmpty(params) ? aes.encryptHex(params) : "";
    }

    /**
     * 解密
     */
    private String decrypt(String params) {
        String key = "gxjkhsaftcl01234";
        String iv = "0102030405060708";
        AES aes = new AES(Mode.CBC, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        //AES aes = new AES(Mode.CTS, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        return StrUtil.isNotEmpty(params) ? aes.decryptStr(params) : "";
    }

}
