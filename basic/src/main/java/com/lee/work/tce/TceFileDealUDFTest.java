package com.lee.work.tce;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;
import com.google.common.collect.Lists;
import com.lee.utils.CollectionUtil;
import lombok.extern.slf4j.Slf4j;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.AesKeyStrength;
import net.lingala.zip4j.model.enums.EncryptionMethod;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/12/23 15:49
 */
@Slf4j
@SpringBootTest
public class TceFileDealUDFTest {

    @Resource
    private JdbcTemplate jdbcTemplate;

    @Test
    public void writeFile() throws IOException {
//        List<String> oriData = Arrays.asList("8176022", "8176022", "8358977", "8134823", "8134823", "8134823", "8134826", "8134827", "8352219", "8352219", "8138382", "8352443", "8352443", "8352443", "8147543", "8350209", "8350209", "8350209", "8350209", "8352617", "8352617", "8352616", "8352617", "8352617", "8352619", "8352619", "8352619");
//        List<String> drugIdCodeList = oriData.stream().map(t -> encrypt(t)).distinct().collect(Collectors.toList());
//        String drugIdCodeStr = CollectionUtil.listToInStr(drugIdCodeList);
//        log.info("===============>{}", drugIdCodeStr);
//        String sql = "SELECT DRUG_ID_CODE,MED_LIST_CODG FROM wm_tcmpat_info_lib_b WHERE VALI_FLAG='1' AND DRUG_ID_CODE in " + drugIdCodeStr;

        String sql = "SELECT DRUG_ID_CODE,MED_LIST_CODG " +
                           "FROM wm_tcmpat_info_lib_b b " +
                           "where 1=1 " +
                           "AND b.VALI_FLAG='1' " +
                           "and (b.PRODENTP_NAME LIKE '贵州信邦制药股份有限公司%' " +
                           "or b.PRODENTP_NAME LIKE '贵阳新天药业股份有限公司%') " +
                           "ORDER BY PRODENTP_NAME ";

        List<Map<String, Object>> wmTcmpatInfos = jdbcTemplate.queryForList(sql);
        log.info("查询数量:{}", wmTcmpatInfos.size());
        File file = ResourceUtils.getFile("G:\\LearnSpace\\daily-code-package\\basic\\src\\main\\resources\\tce\\tcmpatMatchFileUDFTest.txt");
        /*StringBuilder sb = new StringBuilder();
        for (Map<String, Object> wmTcmpatInfo : wmTcmpatInfos) {
            sb = sb.append(wmTcmpatInfo.get("DRUG_ID_CODE") + "β" + wmTcmpatInfo.get("MED_LIST_CODG")).append("γ").append("\n");
        }*/

        StringBuilder sb = new StringBuilder();
        Map<String, List<Map<String, Object>>> medListCodg = wmTcmpatInfos.stream().collect(Collectors.groupingBy(t -> (String) t.get("DRUG_ID_CODE")));
        for (String key : medListCodg.keySet()) {
            sb = sb.append(key).append("β").append(medListCodg.get(key).stream().map(t->(String)t.get("MED_LIST_CODG")).collect(Collectors.joining(","))).append("γ");
        }
        String result = sb.toString().substring(0, sb.toString().length() - 1);
        String encryptStr = encrypt(result);
        FileUtil.appendString(encryptStr, file, "utf-8");
    }

    /**
     * 压缩
     */
    @Test
    public void compressFile() throws FileNotFoundException {
        try {
            // 使用 ResourceUtils 获取文件路径
            File fileToZip = ResourceUtils.getFile("G:\\LearnSpace\\daily-code-package\\basic\\src\\main\\resources\\tce\\tcmpatMatchFileUDFTest.txt");

            File outFile = new File("G:\\LearnSpace\\daily-code-package\\basic\\src\\main\\resources\\tce\\tcmpatMatchFileUDFTest.dat");

            // 创建 ZipFile 对象并指定输出文件路径
            ZipFile zipFile = new ZipFile(outFile);

            // 设置压缩文件的密码
            zipFile.setPassword("d72e477e0ab415r".toCharArray());

            // 设置压缩参数，包括加密方法
            ZipParameters parameters = new ZipParameters();
            parameters.setEncryptFiles(true);
            parameters.setEncryptionMethod(EncryptionMethod.AES); // 推荐使用 AES 加密
            parameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);

            // 添加文件到压缩包中
            zipFile.addFile(fileToZip, parameters);

            System.out.println("文件已成功压缩并加密！");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void zipPwdEncry() {
        String enStr = encrypt("d72e477e0ab415r");
        System.out.println(enStr);
        //aedd3fa481ba69dfb873beaa87f9283b
    }

    /**
     * 解压
     */
    @Test
    public void unCompressFile() throws FileNotFoundException {
        try {
            // 指定压缩文件路径
            File inFile = new File("G:\\LearnSpace\\daily-code-package\\basic\\src\\main\\resources\\tce\\tcmpatMatchFileUDFTest.dat");

            // 创建 ZipFile 对象并指定输入文件路径
            ZipFile zipFile = new ZipFile(inFile);
            // 如果压缩文件有密码，则设置密码
            if (zipFile.isEncrypted()) {
                String pwd = "aedd3fa481ba69dfb873beaa87f9283b";
                String decrypt = decrypt(pwd);
                System.out.println("============>" + decrypt);
                zipFile.setPassword(decrypt.toCharArray());
            }
            // 获取解压后的文件列表
            List<FileHeader> fileHeaders = zipFile.getFileHeaders();
            for (FileHeader fileHeader : fileHeaders) {
                if (!fileHeader.isDirectory()) { // 确保不是目录
                    try (InputStream is = zipFile.getInputStream(fileHeader);
                         BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                        String str;
                        while ((str = reader.readLine()) != null) {
                            String[] split = decrypt(str).split("γ");
                            for (String s : split) {
                                String[] line = s.split("β");
                                if (line.length >= 2) {
                                    System.out.println(decrypt(line[0]) + "====>" + line[1]);
                                }
                            }

                            /*String[] line = decrypt(str).split("β");
                            if (line.length >= 2) {
                                System.out.println(line[0] + "====>" + line[1]);
                            }*/
                        }
                    }
                }
            }
            System.out.println("文件已成功解压并读取！");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除目录及其所有内容
     */
    private void deleteDirectory(File directory) {
        if (directory != null && directory.exists() && directory.isDirectory()) {
            File[] entries = directory.listFiles();
            if (entries != null) {
                for (File entry : entries) {
                    if (entry.isDirectory()) {
                        deleteDirectory(entry);
                    } else {
                        entry.delete();
                    }
                }
            }
            directory.delete();
        }
    }

    /**
     * 加密
     */
    private String encrypt(String params) {
        String key = "gxjkhsaftcl01234";
        String iv = "0102030405060708";
        AES aes = new AES(Mode.CBC, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        return StrUtil.isNotEmpty(params) ? aes.encryptHex(params) : "";
    }

    /**
     * 解密
     */
    private String decrypt(String params) {
        String key = "gxjkhsaftcl01234";
        String iv = "0102030405060708";
        AES aes = new AES(Mode.CBC, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        //AES aes = new AES(Mode.CTS, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        return StrUtil.isNotEmpty(params) ? aes.decryptStr(params) : "";
    }

    public static void main(String[] args) {
        TceFileDealUDFTest test = new TceFileDealUDFTest();
        String a = "dd00855d7ea337c22b628cdd31c12337";
        String decrypt = test.decrypt(a);
        System.out.println(decrypt);//8421274\8450472\8430216
    }
}
