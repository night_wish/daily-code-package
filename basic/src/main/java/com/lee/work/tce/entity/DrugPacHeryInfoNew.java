package com.lee.work.tce.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @description
 * @author Lee
 * @date 2025/2/19 9:41
 * @version 1.0
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DrugPacHeryInfoNew implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    private String drugPacHeryInfoId;

    @ApiModelProperty(value = "药品类型")
    private String drugType;

    @ApiModelProperty(value = "商品名")
    private String prodName;

    @ApiModelProperty(value = "生产企业")
    private String prodentpName;

    @ApiModelProperty(value = "药品通用名")
    private String drugGenname;

    @ApiModelProperty(value = "剂型")
    private String dosform;

    @ApiModelProperty(value = "制剂规格")
    private String prepSpec;

    @ApiModelProperty(value = "包装规格")
    private String pacspec;

    @ApiModelProperty(value = "批准文号")
    private String drugAprvno;

    @ApiModelProperty(value = "包装比例")
    private String pacRatio;

    @ApiModelProperty(value = "发码机构")
    private String issuingOrg;

    @ApiModelProperty(value = "一级药品标识码")
    private String lv1TracProdCodg;

    @ApiModelProperty(value = "二级药品标识码")
    private String lv2TracProdCodg;

    @ApiModelProperty(value = "三级药品标识码")
    private String lv3TracProdCodg;

    @ApiModelProperty(value = "四级药品标识码")
    private String lv4TracProdCodg;

    @ApiModelProperty(value = "五级药品标识码")
    private String lv5TracProdCodg;

    @ApiModelProperty(value = "版本号")
    private String ver;

    @ApiModelProperty(value = "数据类型：1-国产、2-进口")
    private String dataType;

    @ApiModelProperty(value = "全局唯一编码")
    private String rid;

    @ApiModelProperty(value = "有效标志")
    private String valiFlag;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;

    @ApiModelProperty(value = "创建人ID")
    private String crterId;

    @ApiModelProperty(value = "创建人姓名")
    private String crterName;

    @ApiModelProperty(value = "创建机构编号")
    private String crteOptinsNo;

    @ApiModelProperty(value = "经办人ID")
    private String opterId;

    @ApiModelProperty(value = "经办人姓名")
    private String opterName;

    @ApiModelProperty(value = "经办时间")
    private Date optTime;

    @ApiModelProperty(value = "经办机构编号")
    private String optinsNo;
}
