package com.lee.work.tce.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/12/18 11:13
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class WmTcmpatInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String drugIdCode;

    private String medListCodg;
}
