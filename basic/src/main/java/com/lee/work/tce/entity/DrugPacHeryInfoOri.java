package com.lee.work.tce.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.lee.poi.easyexcel.StringLengthLimitConverter;
import com.lee.poi.utils.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2025/2/19 9:39
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DrugPacHeryInfoOri implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @ExcelIgnore
    private String drugPacHeryInfoId;

    @ApiModelProperty(value = "药品类型")
    @ExcelProperty(value = "药品类型", index = 0, converter = StringLengthLimitConverter.class)
    private String drugType;

    @ApiModelProperty(value = "生产企业")
    @ExcelProperty(value = "生产企业", index = 1, converter = StringLengthLimitConverter.class)
    private String prodentpName;

    @ApiModelProperty(value = "药品通用名")
    @ExcelProperty(value = "药品通用名", index = 2, converter = StringLengthLimitConverter.class)
    private String drugGenname;

    @ApiModelProperty(value = "剂型")
    @ExcelProperty(value = "剂型", index = 3, converter = StringLengthLimitConverter.class)
    private String dosform;

    @ApiModelProperty(value = "制剂规格")
    @ExcelProperty(value = "制剂规格", index = 4, converter = StringLengthLimitConverter.class)
    private String prepSpec;

    @ApiModelProperty(value = "包装规格")
    @ExcelProperty(value = "包装规格", index = 5, converter = StringLengthLimitConverter.class)
    private String pacspec;

    @ApiModelProperty(value = "批准文号")
    @ExcelProperty(value = "批准文号", index = 6, converter = StringLengthLimitConverter.class)
    private String drugAprvno;

    @ApiModelProperty(value = "商品名")
    @ExcelProperty(value = "商品名", index = 7, converter = StringLengthLimitConverter.class)
    private String prodName;

    @ApiModelProperty(value = "产品编码")
    @ExcelProperty(value = "产品编码", index = 8, converter = StringLengthLimitConverter.class)
    private String prodCode;

    @ApiModelProperty(value = "包装比例")
    @ExcelProperty(value = "包装比例", index = 9, converter = StringLengthLimitConverter.class)
    private String pacRatio;

    @ApiModelProperty(value = "包装级别")
    @ExcelProperty(value = "包装级别", index = 10, converter = StringLengthLimitConverter.class)
    private String pacLevel;

    @ApiModelProperty(value = "发码机构")
    @ExcelIgnore
    private String issuingOrg;

    @ApiModelProperty(value = "版本号")
    @ExcelIgnore
    private String ver;

    @ApiModelProperty(value = "数据类型：1-国产、2-进口")
    @ExcelProperty(value = "国产/进口", index = 11, converter = StringLengthLimitConverter.class)
    private String dataType;

    @ApiModelProperty(value = "全局唯一编码")
    @ExcelIgnore
    private String rid;

    @ApiModelProperty(value = "有效标志")
    @ExcelIgnore
    private String valiFlag;

    @ApiModelProperty(value = "创建时间")
    @ExcelIgnore
    private Date crteTime;

    @ApiModelProperty(value = "更新时间")
    @ExcelIgnore
    private Date updtTime;

    @ApiModelProperty(value = "创建人ID")
    @ExcelIgnore
    private String crterId;

    @ApiModelProperty(value = "创建人姓名")
    @ExcelIgnore
    private String crterName;

    @ApiModelProperty(value = "创建机构编号")
    @ExcelIgnore
    private String crteOptinsNo;

    @ApiModelProperty(value = "经办人ID")
    @ExcelIgnore
    private String opterId;

    @ApiModelProperty(value = "经办人姓名")
    @ExcelIgnore
    private String opterName;

    @ApiModelProperty(value = "经办时间")
    @ExcelIgnore
    private Date optTime;

    @ApiModelProperty(value = "经办机构编号")
    @ExcelIgnore
    private String optinsNo;
}
