package com.lee.work.tce;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.HexUtil;
import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.symmetric.SM4;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.AesKeyStrength;
import net.lingala.zip4j.model.enums.EncryptionMethod;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/12/18 11:07
 */
@Slf4j
@SpringBootTest
public class TceFileDeal {

    @Resource
    private JdbcTemplate jdbcTemplate;

    private static final String sm4Key = "f82e47760ee415add475d48d5face93f";

    @Test
    public void writeFile() throws IOException {
        String sql = "SELECT DRUG_ID_CODE,MED_LIST_CODG,ISSUING_ORG,BAR_CODE FROM wm_tcmpat_info_lib_b WHERE VALI_FLAG='1' ";
        List<Map<String, Object>> wmTcmpatInfos = jdbcTemplate.queryForList(sql);
        log.info("查询数量:{}", wmTcmpatInfos.size());
        File file = ResourceUtils.getFile("G:\\LearnSpace\\daily-code-package\\basic\\src\\main\\resources\\tce\\tcmpatMatchFile.txt");
        List<String> lines = new ArrayList<>();

        //新逻辑
        Map<String, List<Map<String, Object>>> medListCodg = wmTcmpatInfos.stream().collect(Collectors.groupingBy(t -> (String) t.get("DRUG_ID_CODE")));
        for (String key : medListCodg.keySet()) {
            String str = key + "β" + medListCodg.get(key).stream().map(t -> (String) t.get("MED_LIST_CODG")).collect(Collectors.joining(",")) + "β" + medListCodg.get(key).get(0).get("ISSUING_ORG") + "β" + medListCodg.get(key).get(0).get("BAR_CODE");
            String encrypt = encrypt(str, sm4Key);//加密
            lines.add(encrypt);
        }

        /*for (Map<String, Object> wmTcmpatInfo : wmTcmpatInfos) {
            String str = wmTcmpatInfo.get("DRUG_ID_CODE") + "β" + wmTcmpatInfo.get("MED_LIST_CODG") + "β" + wmTcmpatInfo.get("ISSUING_ORG");
            String encrypt = encrypt(str, sm4Key);//加密
            lines.add(encrypt);
        }*/
        List<List<String>> partition = Lists.partition(lines, 1000);
        partition.forEach(t -> {
            FileUtil.appendLines(t, file, "utf-8");
        });
    }

    /**
     * 压缩
     */
    @Test
    public void compressFile() throws FileNotFoundException {
        try {
            // 使用 ResourceUtils 获取文件路径
            File fileToZip = ResourceUtils.getFile("G:\\LearnSpace\\daily-code-package\\basic\\src\\main\\resources\\tce\\tcmpatMatchFile.txt");

            File outFile = new File("G:\\LearnSpace\\daily-code-package\\basic\\src\\main\\resources\\tce\\tcmpatMatchFile.dat");

            // 创建 ZipFile 对象并指定输出文件路径
            ZipFile zipFile = new ZipFile(outFile);

            // 设置压缩文件的密码
            zipFile.setPassword("d72e477e0ab415r".toCharArray());

            // 设置压缩参数，包括加密方法
            ZipParameters parameters = new ZipParameters();
            parameters.setEncryptFiles(true);
            parameters.setEncryptionMethod(EncryptionMethod.AES); // 推荐使用 AES 加密
            parameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);

            // 添加文件到压缩包中
            zipFile.addFile(fileToZip, parameters);

            System.out.println("文件已成功压缩并加密！");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void zipPwdEncry() {
        String enStr = encrypt("d72e477e0ab415r", sm4Key);
        System.out.println(enStr);
        //2724d741a7507dc426cd9853649f0250
    }

    /**
     * 解压
     */
    @Test
    public void unCompressFile() throws FileNotFoundException {
        try {
            // 指定压缩文件路径
            File inFile = new File("G:\\LearnSpace\\daily-code-package\\basic\\src\\main\\resources\\tce\\tcmpatMatchFile.dat");
            // 创建 ZipFile 对象并指定输入文件路径
            ZipFile zipFile = new ZipFile(inFile);
            // 如果压缩文件有密码，则设置密码
            if (zipFile.isEncrypted()) {
                String pwd = "2724d741a7507dc426cd9853649f0250";
                String decrypt = decrypt(pwd, sm4Key);
                System.out.println("============>" + decrypt);
                zipFile.setPassword(decrypt.toCharArray());
            }
            // 获取解压后的文件列表
            List<FileHeader> fileHeaders = zipFile.getFileHeaders();
            for (FileHeader fileHeader : fileHeaders) {
                if (!fileHeader.isDirectory()) { // 确保不是目录
                    try (InputStream is = zipFile.getInputStream(fileHeader);
                         BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                        String str;
                        while ((str = reader.readLine()) != null) {
                            String[] line = decrypt(str, sm4Key).split("β");
                            if (line.length >= 2) {
                                System.out.println(line[0] + "====>" + line[1] + "=====>" + line[2] + "=======>" + line[3]);
                            }
                        }
                    }
                }
            }

            System.out.println("文件已成功解压并读取！");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getSm4Key() {
        SM4 sm4 = SmUtil.sm4();
        SecretKey secretKey = sm4.getSecretKey();
        byte[] encoded = secretKey.getEncoded();
        String a = HexUtil.encodeHexStr(encoded);
        System.out.println("======>" + a);
    }

    /**
     * sm4加密
     *
     * @param content 文本
     * @param sm4Key  sm4秘钥
     * @return 加密后的结果
     */
    public static String encrypt(String content, String sm4Key) {
        SM4 sm4 = new SM4(HexUtil.decodeHex(sm4Key));
        return sm4.encryptHex(content);
    }

    /**
     * @param content
     * @param sm4Key
     * @return 解密后的结果
     */
    public static String decrypt(String content, String sm4Key) {
        SM4 sm4 = new SM4(HexUtil.decodeHex(sm4Key));
        return sm4.decryptStr(content);
    }
}
