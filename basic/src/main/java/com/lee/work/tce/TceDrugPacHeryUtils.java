package com.lee.work.tce;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import com.lee.work.dao.DrugPacHeryInfoNewDAO;
import com.lee.work.dao.DrugPacHeryInfoOriDAO;
import com.lee.work.tce.entity.DrugPacHeryInfoNew;
import com.lee.work.tce.entity.DrugPacHeryInfoOri;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2025/2/6 10:19
 */
@Slf4j
@SpringBootTest
public class TceDrugPacHeryUtils {

    @Resource
    private JdbcTemplate jdbcTemplate;

    @Resource
    private DrugPacHeryInfoOriDAO drugPacHeryInfoOriDAO;

    @Resource
    private DrugPacHeryInfoNewDAO drugPacHeryInfoNewDAO;

    @Test
    public void readAndWrite() throws IOException {
        String path = "C:\\Users\\james\\Downloads\\C_新增两家企业名单_包装结果表0217.xlsx";
        //创建输入流
        FileInputStream fileInputStream = new FileInputStream(path);
        //获得poi输入流
        Date currentDate = new Date();
        EasyExcel.read(fileInputStream, DrugPacHeryInfoOri.class, new PageReadListener<DrugPacHeryInfoOri>(dataList -> {
            for (DrugPacHeryInfoOri dto : dataList) {
                dto.setRid(UUID.randomUUID().toString().replace("-", ""));
                dto.setDrugPacHeryInfoId(UUID.randomUUID().toString().replace("-", ""));
                dto.setCrteTime(currentDate);
                dto.setUpdtTime(currentDate);
                dto.setCrterId("001");
                dto.setCrterName("system");
                dto.setOpterId("001");
                dto.setOpterName("system");
                dto.setOptTime(currentDate);
                dto.setIssuingOrg("码上放心");
                dto.setVer(DateUtil.format(currentDate, "yyyyMMdd"));
                dto.setValiFlag("1");
                if ("进口".equals(dto.getDataType())) {
                    dto.setDataType("2");
                } else {
                    dto.setDataType("1");
                }
            }
            drugPacHeryInfoOriDAO.insert(dataList);
        }, 500)).sheet().headRowNumber(1).doRead();
        fileInputStream.close();
    }

    @Test
    public void readAndDeal2New() {
        String sql = "SELECT \n" +
                             "  drug.drug_type,\n" +
                             "  drug.prodentp_name,\n" +
                             "  drug.drug_genname,\n" +
                             "  drug.dosform,\n" +
                             "  drug.prep_spec,\n" +
                             "  drug.pacspec,\n" +
                             "  drug.drug_aprvno,\n" +
                             "  drug.pac_ratio,\n" +
                             "  drug.prod_name,\n" +
                             "  GROUP_CONCAT(drug.prod_code ORDER BY drug.pac_level SEPARATOR ',') AS prod_code,\n" +
                             "  GROUP_CONCAT(drug.pac_level ORDER BY drug.pac_level SEPARATOR ',') AS pac_level\n" +
                             "FROM \n" +
                             "  drug_pac_hery_info_ori drug\n" +
                             "GROUP BY \n" +
                             "  drug.drug_type,\n" +
                             "  drug.prodentp_name,\n" +
                             "  drug.drug_genname,\n" +
                             "  drug.dosform,\n" +
                             "  drug.prep_spec,\n" +
                             "  drug.pacspec,\n" +
                             "  drug.drug_aprvno,\n" +
                             "  drug.prod_name,\n" +
                             " drug.pac_ratio";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        Date currentDate = new Date();
        List<DrugPacHeryInfoNew> list = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            DrugPacHeryInfoNew dto = new DrugPacHeryInfoNew();
            dto.setRid(UUID.randomUUID().toString().replace("-", ""));
            dto.setDrugPacHeryInfoId(UUID.randomUUID().toString().replace("-", ""));
            dto.setCrteTime(currentDate);
            dto.setUpdtTime(currentDate);
            dto.setCrterId("001");
            dto.setCrterName("system");
            dto.setOpterId("001");
            dto.setOpterName("system");
            dto.setOptTime(currentDate);
            dto.setIssuingOrg("码上放心");
            dto.setVer(DateUtil.format(currentDate, "yyyyMMdd"));
            dto.setValiFlag("1");

            dto.setDrugType(map.get("drug_type").toString());
            dto.setProdentpName(map.get("prodentp_name").toString());
            dto.setDrugGenname(map.get("drug_genname").toString());
            dto.setDosform(map.get("dosform").toString());
            dto.setPrepSpec(map.get("prep_spec").toString());
            dto.setDrugAprvno(map.get("drug_aprvno").toString());
            dto.setPacRatio(map.get("pac_ratio").toString());
            if(map.get("prod_name")!=null){
                dto.setProdName(map.get("prod_name").toString());
            }
            String prodCode = map.get("prod_code").toString();
            if (StrUtil.isNotBlank(prodCode)) {
                String[] split = prodCode.split(",");
                for (int i = 0; i < split.length; i++) {
                    if (i == 0) {
                        dto.setLv1TracProdCodg(encrypt(split[i]));
                    } else if (i == 1) {
                        dto.setLv2TracProdCodg(encrypt(split[i]));
                    } else if (i == 2) {
                        dto.setLv3TracProdCodg(encrypt(split[i]));
                    } else if (i == 3) {
                        dto.setLv4TracProdCodg(encrypt(split[i]));
                    } else if (i == 4) {
                        dto.setLv5TracProdCodg(encrypt(split[i]));
                    }
                }
            }
            list.add(dto);
        }
        drugPacHeryInfoNewDAO.insert(list);
    }


    @Test
    public void readAndUpdateRecords() {
        String sql = "SELECT * FROM drug_pac_hery_info_d";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> map : maps) {
            String updtSql = "UPDATE drug_pac_hery_info_d set ";
            //LV1_TRAC_PROD_CODG=" + encrypt(lv1) + ",LV2_TRAC_PROD_CODG=" + encrypt(lv2) + ",LV3_TRAC_PROD_CODG=" + encrypt(lv3) + ",LV4_TRAC_PROD_CODG=" + encrypt(lv4) + ",LV5_TRAC_PROD_CODG=" + encrypt(lv5) +
            // " WHERE DRUG_PAC_HERY_INFO_ID=" + id;
            String id = (String) map.get("DRUG_PAC_HERY_INFO_ID");
            String lv1 = (String) map.get("LV1_TRAC_PROD_CODG");
            if (!StringUtils.isEmpty(lv1)) {
                updtSql = updtSql + " LV1_TRAC_PROD_CODG='" + encrypt(lv1) + "' ";
            }

            String lv2 = (String) map.get("LV2_TRAC_PROD_CODG");
            if (!StringUtils.isEmpty(lv2)) {
                updtSql = updtSql + ", LV2_TRAC_PROD_CODG='" + encrypt(lv2) + "' ";
            }
            String lv3 = (String) map.get("LV3_TRAC_PROD_CODG");
            if (!StringUtils.isEmpty(lv3)) {
                updtSql = updtSql + ", LV3_TRAC_PROD_CODG='" + encrypt(lv3) + "' ";
            }


            String lv4 = (String) map.get("LV4_TRAC_PROD_CODG");
            if (!StringUtils.isEmpty(lv4)) {
                updtSql = updtSql + ", LV4_TRAC_PROD_CODG='" + encrypt(lv4) + "' ";
            }


            String lv5 = (String) map.get("LV5_TRAC_PROD_CODG");
            if (!StringUtils.isEmpty(lv5)) {
                updtSql = updtSql + ", LV5_TRAC_PROD_CODG='" + encrypt(lv5) + "' ";
            }

            updtSql = updtSql + " WHERE DRUG_PAC_HERY_INFO_ID = '" + id + "'";

            jdbcTemplate.update(updtSql);
        }
    }


    /**
     * 加密
     */
    private String encrypt(String params) {
        if (StringUtils.isEmpty(params)) {
            return null;
        }
        String key = "gxjkhsaftcl01234";
        String iv = "0102030405060708";
        AES aes = new AES(Mode.CBC, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        return StrUtil.isNotEmpty(params) ? aes.encryptHex(params) : "";
    }

    /**
     * 解密
     */
    private String decrypt(String params) {
        if (StringUtils.isEmpty(params)) {
            return null;
        }
        String key = "gxjkhsaftcl01234";
        String iv = "0102030405060708";
        AES aes = new AES(Mode.CBC, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        //AES aes = new AES(Mode.CTS, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        return StrUtil.isNotEmpty(params) ? aes.decryptStr(params) : "";
    }
}
