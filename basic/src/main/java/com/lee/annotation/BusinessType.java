package com.lee.annotation;

/**
 * 业务类型
 */
public enum BusinessType {

    OTHER, //其他

    SELECT,//查询

    INSERT, //添加

    UPDATE, //修改

    DELETE, //删除

    EXPORT, //导出

    IMPORT //导入
}
