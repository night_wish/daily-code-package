package com.lee.annotation;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomLog {

    //操作名称
    public String title() default "";

    //操作类型
    public BusinessType businessType() default BusinessType.OTHER;
}
