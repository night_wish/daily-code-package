package com.lee.annotation;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.aspectj.lang.reflect.SourceLocation;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Aspect
@Component
public class LogAspect {

    @Pointcut("@annotation(com.lee.annotation.CustomLog)")
    public void logPointCut(){}


    @AfterReturning(value = "logPointCut()",returning = "jsonResult")
    public void doAfterReturning(JoinPoint joinPoint, Object jsonResult){
        System.out.println("=========正常返回===========start");

        Object target = joinPoint.getTarget();
        Signature signature = joinPoint.getSignature();
        Object[] args = joinPoint.getArgs();
        for(Object obj : args){
            System.out.println("--->"+obj.toString());
        }
        Object aThis = joinPoint.getThis();
        System.out.println("--->"+aThis);
        SourceLocation sourceLocation = joinPoint.getSourceLocation();
        System.out.println("--->"+sourceLocation.toString());

        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        String declaringTypeName = methodSignature.getDeclaringTypeName();
        CustomLog annotation = methodSignature.getMethod().getAnnotation(CustomLog.class);

        System.out.println("执行时间："+LocalDateTime.now() +" 执行目标 : "+signature+"\n"
                    +" 执行名称 :"+annotation.title()+"  执行类型："+annotation.businessType());
        System.out.println("=========正常返回===========end");
    }


}
