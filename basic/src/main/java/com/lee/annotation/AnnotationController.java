package com.lee.annotation;

import org.springframework.web.bind.annotation.*;

@RequestMapping("/annotationController")
@RestController
public class AnnotationController {



    @CustomLog(title = "test1 查询",businessType = BusinessType.SELECT)
    @GetMapping("/test1")
    public String test1(){
        return "this is annotation controller test1 ： 查询";
    }


    @CustomLog(title = "test2 新增",businessType = BusinessType.INSERT)
    @PostMapping("/test2")
    public String test2(@RequestBody String myuser){
        return "this is annotation controller test2 ====>"+myuser+"： 新增";
    }




}
