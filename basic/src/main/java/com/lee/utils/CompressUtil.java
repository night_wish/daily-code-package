package com.lee.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 压缩工具类
 */
public class CompressUtil {

    /**
     * 压缩单个文件
     *
     * @param filePath           文件路径
     * @param zipFilePathAndName 压缩文件的保存路径和名称
     */
    public static void compressFiles(String filePath, String zipFilePathAndName) {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new RuntimeException("文件不存在");
        }

        File zipFile = new File(zipFilePathAndName);
        if (zipFile.exists()) {
            zipFile.delete();
        }

        try (FileOutputStream fos = new FileOutputStream(zipFile);
             ZipOutputStream zos = new ZipOutputStream(fos)) {
            File[] files = file.listFiles();
            for (File f : files) {
                if (f.isDirectory()) {
                    compressFile(f.getAbsolutePath(), zos);
                } else {
                    zos.putNextEntry(new ZipEntry(f.getName()));
                    try (FileInputStream fis = new FileInputStream(f)) {
                        int len;
                        byte[] buffer = new byte[1024];
                        while ((len = fis.read(buffer)) != -1) {
                            zos.write(buffer, 0, len);
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 压缩多个文件
     *
     * @param sourceFiles 多个文件
     * @param destPath    压缩文件的保存路径
     */
    public static void compressFiles(List<File> sourceFiles, String destPath) {
        if (sourceFiles == null || sourceFiles.isEmpty()) {
            throw new RuntimeException("文件列表为空");
        }

        File zipFile = new File(destPath);
        if (zipFile.exists()) {
            zipFile.delete();
        }

        try (FileOutputStream fos = new FileOutputStream(zipFile);
             ZipOutputStream zos = new ZipOutputStream(fos)) {
            for (File file : sourceFiles) {
                zos.putNextEntry(new ZipEntry(file.getName()));
                try (FileInputStream fis = new FileInputStream(file)) {
                    int len;
                    byte[] buffer = new byte[1024];
                    while ((len = fis.read(buffer)) != -1) {
                        zos.write(buffer, 0, len);
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    private static void compressFile(String filePath, ZipOutputStream zos) throws IOException {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new RuntimeException("文件不存在");
        }

        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                compressFile(f.getAbsolutePath(), zos);
            }
        } else {
            zos.putNextEntry(new ZipEntry(file.getName()));
            try (FileInputStream fis = new FileInputStream(file)) {
                int len;
                byte[] buffer = new byte[1024];
                while ((len = fis.read(buffer)) != -1) {
                    zos.write(buffer, 0, len);
                }
            }
        }
    }



    public static void main(String[] args) throws IOException {
        // 根据文件夹压缩多个文件
        compressFiles("G:\\home\\admin\\logs", "G:\\home\\admin\\a.zip");

        //根据文件名压缩多个文件
        List<File> sourceFiles = new ArrayList<>();
        sourceFiles.add(new File("G:\\home\\admin\\logs\\ims-web.2023-10-24.0.log"));
        sourceFiles.add(new File("G:\\home\\admin\\logs\\ims-web.2023-10-31.0.log"));
        compressFiles(sourceFiles,"G:\\home\\admin\\b.zip");
    }

}

