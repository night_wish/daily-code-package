package com.lee.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PropertiesLoaderSupport;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Properties;

/**
 * @author Lee
 * @version 1.0
 * @description 读取异常配置文件
 * @date 2022/2/28 18:20
 */
@Component
public class ErrorPropertyReader extends PropertiesLoaderSupport implements ResourceLoaderAware, InitializingBean {
    private static ResourceLoader resourceLoader;

    private static Properties mergeProperties;

    private static final String DEFAULT_ERROR_CODE_CONFIG_PATH = "classpath*:/errorCode/*.properties";

    private static final String ENCODING = "UTF-8";

    public static void setResource(ResourceLoader resourceLoader) {
        ErrorPropertyReader.resourceLoader = resourceLoader;
    }

    public static void setMergeProperties(Properties mergeProperties) {
        ErrorPropertyReader.mergeProperties = mergeProperties;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        setResource(resourceLoader);
    }

    @Override
    public void afterPropertiesSet() throws IOException {
        ResourcePatternResolver resourcePatternResolver = (ResourcePatternResolver) resourceLoader;
        Resource[] defaultResources = resourcePatternResolver.getResources(DEFAULT_ERROR_CODE_CONFIG_PATH);
        setFileEncoding(ENCODING);
        setLocations(defaultResources);
        setMergeProperties(mergeProperties());
    }

    public static final String getExceptionMessage(Integer code) {
        return mergeProperties.getProperty(String.valueOf(code));
    }

    public static final String getExceptionMessage(Integer code, String defaultMsg) {
        String oriMessage = mergeProperties.getProperty(String.valueOf(code));
        if (StringUtils.isBlank(oriMessage)) {
            oriMessage = defaultMsg;
        }
        return oriMessage;
    }

    public static final String getExceptionMessage(Integer code, String[] dynaValues) {
        String oriMessage = mergeProperties.getProperty(String.valueOf(code));
        String value = oriMessage;
        for (int i = 0; i < dynaValues.length; i++) {
            String dynaValue = dynaValues[i];
            value = StringUtils.replace(value, "{" + i + "}", dynaValue);
        }
        return value;
    }
}
