package com.lee.utils;

/**
 *
 * @author lishang Create on2019/8/28
 * @version 1.0
 */
public final class ImsConstants {

    /**
     * 拼接DOC_ID字段字符
     */
    public static final String AUDIT_DOC_ID_JOIN = "β";
    /**
     * 复审提交 key
     */
    public static final String AUDIT_DOC_SUBMIT = "AUDIT:DOC:SUBMIT:";
    /**
     * 申诉节点-批量提交 key
     */
    public static final String AUDIT_HOSPITAL_APPEAL_SUBMIT = "AUDIT:HOSPITAL:APPEAL:SUBMIT:";

    /**
     * 默认的业务数据库数据源beanId
     */
    public static final String  DEFAULT_BUSINESS_DATA_SOURCE_NAME = "ims-druidDataSource-drds";
    /**
     * 数据库公共字段RID
     */
    public static final String  COLUMN_R_ID = "RID";
    /**
     * 默认的缓存名称
     */
    public static final String  CACHE_NAME = "hsaf-redis";

    /**
     * 默认的redisTemplate的BeanId
     */
    public static final String  DEFAULT_REDIS_TEMPLATE_BEAN_NAME = "hsafRedisTemplate";

    /**
     * 时间格式 年月日时分秒
     */
    public static final String  DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 时间格式 年月日
     */
    public static final String  DATE_FORMAT_Y_M_D= "yyyy-MM-dd";

    /**
     * 入口函数无参
     */
    public static final String  ENTER_FUNCTION= "Enter Function Params";
    /**
     * HashMap初始化容量
     */
    public static final int INIT_HASHMAP_CAPACITY = 16;

    /**
     *应用的名称
     */
    public static final String  APP_NAME = "IMS";

    /**
     * Audit_business
     * 审核业务文件存储路径
     */
    public static final String  AUDIT_FILE_BUSINESS = "AUDITBUSINESS";

    /**
     * 医保局项目
     */
    public static final String  HSA_APP_NAME = "HSA";

    /**
     * redis缓存的KEY前缀
     */
    public static final String  REDIS_KEY_PREFIX = HSA_APP_NAME + ":" + APP_NAME + ":";

    /**
     * 入口函数单个参
     */
    public static final String  ENTER_FUNCTION_SINGLE_PARAM = ENTER_FUNCTION + ": {}";

    /**
     * 入口函数两个参
     */
    public static final String  ENTER_FUNCTION_DOUBLE_PARAM = ENTER_FUNCTION_SINGLE_PARAM + ",{}";

    /**
     * 入口函数三个参
     */
    public static final String  ENTER_FUNCTION_THIRD_PARAM = ENTER_FUNCTION_DOUBLE_PARAM + ",{}";

    /**
     * 出口函数一个参
     */
    public static final String  EXIST_FUNCTION_PARAM = "Exist Function Params: {}";

    /**
     * result 对象中的key
     */
    public static final String  RESULT_LIST="list";
    public static final String  RESULT_DATA="data";
    public static final String  RESULT_CHILD="children";
    public static final String  CHECK_LIST="checkList";

    /**
     * 出口函数无参
     */
    public static final String  EXIST_FUNCTION = "Exist Function Param";

    public static final String  EXCEL_FILE_SUFFIX=".xlsx";
    public static final String  EXCEL_FILE_SUFFIX_XLS=".xls";

    /**
     *字符格式的时间格式带毫秒
     */
    public static final String  DATE_TYPE_STR_FULL = "yyyyMMddHHmmssSSS";
    /**
     *字符格式的时间格式不带毫秒
     */
    public static final String  DATE_STR="yyyyMMddHHmmss";
    public static final String  PERCENT="%";

    /**
     * 排序方式-降序
     */
    public static final String  SORT_TYPE_DESC = "DESC";
    /**
     * 排序方式-升序
     */
    public static final String  SORT_TYPE_ASC = "ASC";
    public static final String  YEAR="年";
    public static final String  MONTH="月";

    /**
     * 长文本设置输入范围
     */
    public static final Integer TEXTAREA_MIN_LENGTH = 1;
    public static final Integer TEXTAREA_MAX_LENGTH = 500;
    public static final Integer TEXTAREA_KMAX_LENGTH = 1000;
    public static final String STRING_MIN_RATIO = "1";
    public static final String STRING_MAX_RATIO = "9999";

    public static final String  WINDOWS_SPLIT = "\\";
    public static final char CHAR_WINDOWS_SPLIT = '\\';
    public static final String  OBLIQUE_LINE = "/";
    public static final char CHAR_OBLIQUE_LINE = '/';
    public static final String  OS_NAME_FLAG_WIN = "Win";
    public static final String  OS_NAME = "os.name";

    public static final String  DATE_YEAR = "yyyy";
    public static final String  DATE_MONTH = "MM";
    /**
     * 季度 Q1,Q2,Q3,Q4
     */
    public static final String  Q1 = "Q1";
    public static final String  Q2 = "Q2";
    public static final String  Q3 = "Q3";
    public static final String  Q4 = "Q4";

    /**
     * 进销存比对分析excel
     */
    public static final String  SELT_SEL_CPR = "比对分析";

    /**
     * 热力图部分常量
     */
    public static final String  HEI = "黑";
    public static final String  NEI = "内";
    public static final String  XIN = "新疆建设兵团";

    /**
     * 字符串连接分隔符
     */
    public static final String  SEPARATOR = ",";

    /**
     * 折线图单位
     */
    public static final String  RC_RS_VALUE="人";
    public static final String  WG_KK_VALUE="万元";
    /**
     * 导出excel时间格式
     */
    public static final String  YYYY_MM_DD_HH = "yyyyMMddHH";

    /**
     * 构建异常信息的key
     */
    public static final String  CODE="code";
    public static final String  MESSAGE="message";
    /**
     * 比对分析导出文件名称
     */
    public static final String  SETL_SEL_FILE_NAME="药品结算销售比对统计表";

    /**
     * 最大上传文件数量
     */
    public static final int UPLOAD_FILE_MAX_COUNT = 5;

    /**
     * 文件上传大小(M)
     */
    public static final int UPLOAD_FILE_SIZE_UNT_M = 1 << 20;

    public static final String  ERROR="ERROR: ";

    public static final String  HTMLS = "htmls";

    /**
     * 分页参数: 总数
     */
    public static final String  TOTAL="total";
    /**
     * 分页参数: 每页的数量
     */
    public static final String  PAGESIZE="pageSize";
    /**
     * 分页参数: 第几页
     */
    public static final String  CURRENTPAGE="currentPage";

    /**
     * 百分号
     */
    public static final String  PRECENT = "%";

    /**
     * 程序打包出来的jar包名称,jar包名称有变这个也需要修改
     */
//    public static final String  PROJECT_NAME="hsa-ims-web";

    /**
     * 成功的编码
     */
    public static final String  SUCCESS_CODE="1";
    /**
     * excel导出的路径
     */
    public static final String  TEMPLATE_PATH="template/";
    /**
     * 空格
     */
    public static final String  BLANK_SPACE= " ";
    /**
     * 行政区划
     */
    public static final String  ADMDVS="admdvs";
    /**
     * 前端下拉框key
     */
    public static final String  LABEL="label";

    /**
     * 出库下钻
     */
    public static final String  EXPORTLISTDRUGSTOOUTDETLNEXTFILENAME = "出库下钻";


    /**
     *  SONAR:Define a constant instead of duplicating this literal "E10009" 3 times.
     */
    public static final String  E_CODE = "E10009";

    /**
     * 上报附件id
     */
    public static final String ATT_UPLOAD_ID="attUploadId";
    /**
     * 错误信息的长度
     */
    public static final int FUND_TASK_ERR_MSG_LENG = 200;

    /**
     * 系统默认的操作人ID
     */
    public static final String SYSTEM_OPTER_ID="0";
    /**
     * 系统默认的操作人名字
     */
    public static final String SYSTEM_OPTER_NAME="system";
    /**
     * 系统默认的操作人机构
     */
    public static final String SYSTEM_OPTER_OPTINS="0";
    /**
     * 有效
     */
    public static final String VALID = "1";

    /**
     * 无效
     */
    public static final String INVALID = "0";

    /**
     * 初审提交明细 中间状态缓存key
     */
    public static final String AUDIT_DOC_SUBMIT_PREFIX = "AUDIT:DOC:SUBMIT:";
    /**
     * pdf文件的后缀名   .pdf
     */
    public static final String PDF_SUFFIX = ".pdf";
    /**
     * 错误码静态常量
     */
    public static final int CODE_210036 = 210036;
    public static final int CODE_210037 = 210037;
    public static final int CODE_210176 = 210176;
    public static final int CODE_210302 = 210302;
    public static final int CODE_210303 = 210303;
    public static final int CODE_210304 = 210304;
    public static final int CODE_210306 = 210306;
    public static final int CODE_210307 = 210307;
    public static final int CODE_INT_210308 = 210308;
    public static final int CODE_INT_210309 = 210309;
    public static final int CODE_INT_210325 = 210325;
    public static final int CODE_210310 = 210310;
    public static final int CODE_INT_210311 = 210311;
    public static final int CODE_INT_210312 = 210312;
    public static final int CODE_210314 = 210314;
    public static final int CODE_INT_210315 = 210315;
    public static final int CODE_INT_210316 = 210316;
    public static final int CODE_INT_210319 = 210319;
    public static final int CODE_INT_210320 = 210320;
    public static final int CODE_210318 = 210318;
    public static final int CODE_210322 = 210322;
    public static final int CODE_210323 = 210323;
    public static final int CODE_INT_210327 = 210327;
    public static final int CODE_210328 = 210328;
    public static final int CODE_210330 = 210330;
    public static final int CODE_210341 = 210341;
    public static final int CODE_210343 = 210343;
    public static final int CODE_210054 = 210054;
    public static final int CODE_INT_210359 = 210359;
    public static final int CODE_210364 = 210364;
    public static final int CODE_210365 = 210365;
    public static final int CODE_210233 = 210233;
    public static final int CODE_214119 = 214119;
    public static final int CODE_210443 = 210443;
    public static final int CODE_210384 = 210384;
    public static final int CODE_INT_210437 = 210437;
    public static final int CODE_INT_210438 = 210438;
    public static final int CODE_INT_210443 = 210443;
    public static final int CODE_210447 = 210447;
    public static final int CODE_210501 = 210501;
    public static final int CODE_210807 = 210807;
    public static final int CODE_214120 = 214120;
    public static final int CODE_210397 = 210397;
    public static final int CODE_INT_214101 = 214101;
    public static final int CODE_INT_210455 = 210455;
    public static final int CODE_210454 = 210454;
    public static final int CODE_214118 = 214118;
    public static final int CODE_INT_210482 = 210482;
    public static final int CODE_INT_210436 = 210436;
    public static final int CODE_210483 = 210483;
    public static final int CODE_INT_210442 = 210442;
    public static final int CODE_INT_210435 = 210435;
    public static final int CODE_INT_210434 = 210434;
    public static final int CODE_210738 = 210738;
    public static final int CODE_214109 = 214109;
    public static final int CODE_210039 = 210039;
    public static final int CODE_210340 = 210340;
    /**
     *  审核中间状态： 99-处理中
     */
    public static final String AUDIT_PROCESSING_STATE = "99";
    /**
     * 不设置过期时长
     */
    public final static long NOT_EXPIRE = -1;

    /**
     * 默认过期时长，单位：秒	30分钟
     */
    public final static long DEFAULT_EXPIRE = 60 * 30;

    /**
     * 复审明细 中间状态缓存key
     */
    public static final String AUDIT_DOC_DETAILS_PREFIX = "AUDIT:DOC:DETAILS:";

    /**
     * 大数据量导出锁key
     */
    public static final String  REDIS_LOCK_KEY_PREFIX_BTN= "REDIS_LOCK_KEY_PREFIX" +"BTN:";

    /**
     * 无入参
     */
    public static final String ENTER = "[enterParams]";
    /**
     * 入口参数：单个
     */
    public static final String ENTER_ONE = ENTER + ":{}";
    /**
     * 入口参数：两个
     */
    public static final String ENTER_TWO = ENTER_ONE + ",{}";

    /**
     * 入口参数：三个
     */
    public static final String ENTER_THREE = ENTER_TWO + ",{}";
    /**
     * 无出参
     */
    public static final String EXIT = "[exitParam]";

    /**
     * 出口参数：一个
     */
    public static final String EXIT_ONE = "[exitParams]:{}";

    public static final String ZERO = "0";
    public static final String ONE = "1";
    public static final String TWO = "2";
    public static final String THREE = "3";
    public static final String FOUR = "4";
    public static final String FIVE = "5";
    public static final String SIX = "6";
    public static final String SEVEN = "7";


    /**
     * 00
     **/
    public static final String TWO_ZERO = "00";
}
