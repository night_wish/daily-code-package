package com.lee.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringContextUtils implements ApplicationContextAware {
    private static ApplicationContext applicationContext;
    private static final Log LOGGER = LogFactory.getLog(SpringContextUtils.class);

    public SpringContextUtils() {
    }

    public static void setContext(ApplicationContext context) {
        applicationContext = context;
    }

    @Override
    public void setApplicationContext(ApplicationContext context) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("SpringContextUtil注入applicaitonContext");
        }

        setContext(context);
    }

    public static ApplicationContext getApplicationContext() {
        return hasLoadContext() ? applicationContext : null;
    }

    private static boolean hasLoadContext() {
        if (applicationContext == null) {
            throw new IllegalStateException("SpringContextUtil初始化失败,请在applicationContext.xml中定义SpringContextUtil");
        } else {
            return true;
        }
    }

    public static boolean containsBean(String name) {
        return applicationContext.containsBean(name);
    }
}
