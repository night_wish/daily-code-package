package com.lee.utils;

/**
 * @author Lee
 * @version 1.0
 * @description 业务异常
 * @date 2022/2/28 18:28
 */
public class BizException extends RuntimeException {
    private Integer code = -1;

    public BizException(Integer code) {
        super(ErrorPropertyReader.getExceptionMessage(code));
        setCode(code);
    }

    public BizException(String message) {
        super(message);
    }

    public BizException(Integer code, String[] dynaValues) {
        super(ErrorPropertyReader.getExceptionMessage(code, dynaValues));
        setCode(code);
    }

    public BizException(Integer code, String message) {
        super(message);
        setCode(code);
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
