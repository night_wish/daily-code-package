package com.lee.utils;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * @author Lee
 * @version 1.0
 * @description  导出下载工具类
 * @date 2024/3/18 9:28
 */
public class DownloadUtils {

    /**
     * 导出Excel设置响应头
     */
    private static void setHeader(HttpServletResponse response, XSSFWorkbook workbook, String fileName) {
        try (OutputStream os = new BufferedOutputStream(response.getOutputStream())) {
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = requestAttributes.getRequest();
            String agent = request.getHeader("User-Agent");
            //设置response的Header
            if (agent != null && agent.toLowerCase().indexOf("firefox") != -1) {
                response.setHeader("Content-Disposition", String.format("attachment;filename*=utf-8'zh_cn'%s", URLEncoder.encode(fileName, "UTF-8")));
            } else {
                response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8"));
            }
            response.setContentType("APPLICATION/OCTET-STREAM");
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            //将excel写入到输出流中
            workbook.write(os);
            os.flush();
            closeStreamOfOut(os);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("设置浏览器下载失败");
        }
    }

    /**
     * 关闭输出流
     */
    public static void closeStreamOfOut(OutputStream outputStream) {
        try {
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
