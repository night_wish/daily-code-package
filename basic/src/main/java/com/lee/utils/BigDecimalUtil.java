package com.lee.utils;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/10/18 15:11
 */
public class BigDecimalUtil {

    public static final int INT_ZERO = 0;
    public static final BigDecimal ZERO;
    public static final int DEFAULT_PRECISION = 2;
    public static final RoundingMode DEFAULT_ROUNDING_MODE;
    public static final int BIG_RESULT = 1;
    public static final int SAME_RESULT = 0;
    public static final int SMALL_RESULT = -1;

    public BigDecimalUtil() {
    }

    public static BigDecimal buildBigDecimal(BigDecimal obj) {
        return null == obj ? ZERO : obj;
    }

    public static BigDecimal buildBigDecimal(Integer obj) {
        return null == obj ? ZERO : new BigDecimal(obj);
    }

    public static BigDecimal buildBigDecimal(Long obj) {
        return null == obj ? ZERO : new BigDecimal(obj);
    }

    public static BigDecimal buildBigDecimal(Double obj) {
        return null == obj ? ZERO : new BigDecimal(obj);
    }

    public static BigDecimal buildBigDecimal(String obj) {
        return null != obj && !StringUtils.isEmpty(obj) ? new BigDecimal(obj) : ZERO;
    }

    public static BigDecimal getBigDecimal(Object obj) {
        if (null == obj) {
            return ZERO;
        } else if (obj instanceof BigDecimal) {
            return (BigDecimal) obj;
        } else if (obj instanceof Integer) {
            return new BigDecimal((Integer) obj);
        } else if (obj instanceof Long) {
            return new BigDecimal((Long) obj);
        } else if (obj instanceof Double) {
            return new BigDecimal((Double) obj);
        } else {
            return obj instanceof String ? new BigDecimal((String) obj) : null;
        }
    }

    public static boolean isZero(BigDecimal target) {
        if (null == target) {
            return false;
        } else {
            return ZERO.compareTo(target) == 0;
        }
    }

    public static boolean isIntZero(BigDecimal target) {
        if (null == target) {
            return false;
        } else {
            return target.intValue() == 0;
        }
    }

    public static boolean isBig(BigDecimal value1, BigDecimal value2) {
        if (null == value1) {
            return false;
        } else if (null == value2) {
            return false;
        } else {
            return value1.compareTo(value2) == 1;
        }
    }

    public static boolean isSame(BigDecimal value1, BigDecimal value2) {
        if (null == value1) {
            return false;
        } else if (null == value2) {
            return false;
        } else {
            return value1.compareTo(value2) == 0;
        }
    }

    public static boolean isSmall(BigDecimal value1, BigDecimal value2) {
        if (null == value1) {
            return false;
        } else if (null == value2) {
            return false;
        } else {
            return value1.compareTo(value2) == -1;
        }
    }

    public static BigDecimal divide(BigDecimal value1, BigDecimal value2, int scale, RoundingMode roundingMode) {
        if (null == value1) {
            return null;
        } else if (null == value2) {
            return null;
        } else if (isZero(value2)) {
            return null;
        } else {
            return isZero(value2) ? null : value1.divide(value2, scale, roundingMode);
        }
    }

    public static BigDecimal divide(BigDecimal value1, BigDecimal value2) {
        return divide(value1, value2, 2, DEFAULT_ROUNDING_MODE);
    }

    public static BigDecimal add(BigDecimal value1, BigDecimal value2) {
        BigDecimal first = getBigDecimal(value1);
        BigDecimal addValue = getBigDecimal(value2);
        first = null == first ? ZERO : first;
        addValue = null == addValue ? ZERO : addValue;
        return first.add(addValue);
    }

    static {
        ZERO = BigDecimal.ZERO;
        DEFAULT_ROUNDING_MODE = RoundingMode.HALF_UP;
    }
}
