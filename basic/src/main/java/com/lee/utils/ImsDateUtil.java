package com.lee.utils;

import cn.hsa.ims.common.util.CollectionUtil;
import cn.hsa.ims.common.util.DateUtil;
import cn.hsa.ims.common.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * 智能监管时间工具类
 * </p>
 *
 * @author lishang Create on2019/8/26
 * @version 1.0
 */
public class ImsDateUtil extends DateUtil {

    public static final String DATE_Y_M_D_H_S = "yyyy-MM-dd HH:mm";

    /**
     * 时间格式 年月日时分秒
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /**
     * yyyy-MM-dd HH:mm:ss:SSS
     */
    public static final String DATE_FORMAT_SSS = "yyyy-MM-dd HH:mm:ss";

    /**
     * 时间格式导出使用 年月日时分秒
     */
    public static final String DATE_FORMAT_FOR_EXPORT = "yyyyMMddHHmmss";
    /**
     * 时间格式导出使用 年月日
     */
    public static final String DATE_FORMAT_FOR_Y_M_D = "yyyyMMdd";

    /**
     * 时间格式 年月日
     */
    public static final String DATE_FORMAT_Y_M_D = "yyyy-MM-dd";

    public static final String DATE_FORMAT_Y_M = "yyyyMM";

    /**
     * 日期格式 YYYY-MM-DD 正则
     */
    public static final String DATE_REGEX_Y_M_D = "[0-9]{4}-[0-9]{2}-[0-9]{2}";

    /**
     * 日期格式 YYYY-MM 正则
     */
    public static final String DATE_REGEX_Y_M = "[0-9]{4}-[0-9]{2}";

    private static final String BEGIN_DAY = "-1-1";
    private static final String END_DAY = "-12-31";

    /**
     * h获取两个时间间隔的天数
     *
     * @param start
     * @param end
     * @return
     */
    public static int getIntervalDays(Date start, Date end) {
        Objects.requireNonNull(start, "start date must be not null");
        Objects.requireNonNull(end, "end date must be not null");
        int days = (int) ((end.getTime() - start.getTime()) / (1000 * 3600 * 24));
        return days;
    }

    /**
     * 获取两个时间之间所间隔的月份
     *
     * @param start 开始时间
     * @param end   结果时间
     * @return 间隔的月份
     */
    public static int getIntervalMonth(Date start, Date end) {
        Objects.requireNonNull(start, "start date must be not null");
        Objects.requireNonNull(end, "end date must be not null");
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(start);
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(end);
        Calendar interim = Calendar.getInstance();
        interim.setTime(end);
        interim.add(Calendar.DATE, 1);

        int year = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
        int month = endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);

        if (startCalendar.get(Calendar.DATE) == 1 && interim.get(Calendar.DATE) == 1) {
            return year * 12 + month + 1;
        } else if (startCalendar.get(Calendar.DATE) != 1 && interim.get(Calendar.DATE) == 1) {
            return year * 12 + month;
        } else if (startCalendar.get(Calendar.DATE) == 1 && interim.get(Calendar.DATE) != 1) {
            return year * 12 + month;
        } else {
            return year * 12 + month - 1 < 0 ? 0 : year * 12 + month;
        }
    }

    /**
     * 获取时间中的年月数组
     * 第一位是年，第二位是月.
     * 月份是两位的，01-12
     *
     * @param date 指定时间
     * @return 结果数组
     */
    public static String[] getYearAndMonth(Date date) {
        Objects.requireNonNull(date, "target date must be not null");
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        String year = String.valueOf(c.get(Calendar.YEAR));
        String month = String.valueOf(c.get(Calendar.MONTH) + 1);
        if (month.length() == 1) {
            month = "0" + month;
        }
        return new String[]
                {year, month};
    }

    /**
     * 获取时间中的月份 01--12
     *
     * @param date 指定时间
     * @return 可以阅读的月份
     */
    public static String getHumanMonth(Date date) {
        Objects.requireNonNull(date, "target date must be not null");
        int month = getMonth(date) + 1;
        String value = String.valueOf(month);
        if (value.length() == 1) {
            value = "0" + value;
        }
        return value;
    }

    /**
     * @param date
     * @return java.lang.Integer
     * @description 获取日期中的日
     * @author gengxiu
     * @date 2019/9/12 15:00
     */
    public static Integer getDay(Date date) {
        if (null == date) {
            return null;
        } else {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            return c.get(Calendar.DATE);
        }
    }

    /**
     * @param date
     * @return java.lang.Integer
     * @description 获取日期中的时
     * @author gengxiu
     * @date 2019/9/16 19:46
     */
    public static Integer getHour(Date date) {
        if (null == date) {
            return null;
        } else {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            return c.get(Calendar.HOUR_OF_DAY);
        }
    }

    /**
     * @param date
     * @return java.lang.Integer
     * @description 获取日期中的分
     * @author gengxiu
     * @date 2019/9/16 19:47
     */
    public static Integer getMinute(Date date) {
        if (null == date) {
            return null;
        } else {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            return c.get(Calendar.MINUTE);
        }
    }


    /**
     * 转换为时间（天,时:分:秒.毫秒）
     *
     * @param timeMillis
     * @return
     */
    public static String formatDateTime(long timeMillis) {
        long day = timeMillis / (24 * 60 * 60 * 1000);
        long hour = (timeMillis / (60 * 60 * 1000) - day * 24);
        long min = ((timeMillis / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (timeMillis / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        long sss = (timeMillis - day * 24 * 60 * 60 * 1000 - hour * 60 * 60 * 1000 - min * 60 * 1000 - s * 1000);
        return (day > 0 ? day + "," : "") + hour + ":" + min + ":" + s + "." + sss;
    }

    /**
     * 转换为时间"yyyy-MM-dd HH:mm:ss:SSS"
     *
     * @param timeMillis
     * @return
     */
    public static String formatTime(long timeMillis, String dateFormat) {
        if(StringUtils.isEmpty(dateFormat)){
            dateFormat = DATE_FORMAT_SSS;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Date date = new Date(timeMillis);
        return sdf.format(date);
    }

    /**
     * @param date
     * @return java.lang.String
     * @description Date格式转String
     * @author gengxiu
     * @date 2019/9/19 19:25
     */
    public static String dateConvertToString(Date date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat formatter = new SimpleDateFormat(ImsConstants.DATE_FORMAT);
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * @description: string转日期（yyyy-mm-dd）
     * @param dateStr
     * @return: java.util.Date
     * @author: wangaogao
     * @date: 2020/6/12 19:11
     */
    public static Date stringConvertToDate(String dateStr) {
        if (StringUtils.isBlank(dateStr)) {
            return null;
        }

        Date date;
        try {
            date = parseDate(dateStr, ImsConstants.DATE_FORMAT_Y_M_D);
        } catch (ParseException e) {
            throw new RuntimeException();
        }

        return date;
    }

    /**
     * @param date
     * @param formart
     * @return java.lang.String
     * @description 传时间格式
     * @author Fang Kun
     * @date 2019/9/26 19:22
     */
    public static String dateConvertToString(Date date, String formart) {
        if (date == null) {
            return null;
        }
        if (StringUtils.isEmpty(formart)) {
            return dateConvertToString(date);
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(formart);
            String dateString = formatter.format(date);
            return dateString;
        }
    }

    /**
     * @param date
     * @return java.lang.String
     * @description Date格式转String 格式:yyyy-MM-dd
     * @author Fang Kun
     * @date 2019/9/26 17:28
     */
    public static String dateConvertToStringYMD(Date date) {
        String ymd = dateConvertToString(date, ImsConstants.DATE_FORMAT);
        return ymd;
    }


    /**
     * 获取上个月的第一天
     *
     * @return
     * @author lishang
     */
    public static Date getFirstDayOfLastMonth() {
        return getFirstDayOfAddMonth(-1);
    }

    /**
     * @description: 获取i个月后的第一天
     * @param i 增量值
     * @return: java.util.Date
     * @author: wangaogao
     * @date: 2020/8/18 15:42
     */
    public static Date getFirstDayOfAddMonth(int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, i);
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * @description: 获取i年后的第一天
     * @param i 增量值
     * @return: java.util.Date
     * @author: wangaogao
     * @date: 2020/8/18 15:42
     */
    public static Date getFirstDayOfAddYear(int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, i);
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取上个月的最后一天
     *
     * @return
     * @author lishang
     */
    public static Date getLastDayOfLastMonth() {
        return getLastDayOfAddMonth(-1);
    }

    /**
     * @description: 获取指定年月的第一天
     *
     * @author: Fang Kun
     * @param year
     * @param month
     * @date: 2021/1/4 16:33
     * @return: java.lang.String
     */
    public static String getFirstDayOfDateMonth(int year, int month){
        Calendar cal = Calendar.getInstance();
        // 设置年
        cal.set(Calendar.YEAR, year);
        // 设置月份
        cal.set(Calendar.MONTH, month - 1);
        // 获取某月最小天数
        int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        // 设置日历中月份的最小天数
        cal.set(Calendar.DAY_OF_MONTH, firstDay);
        // 格式化日期，获取开始时刻
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_Y_M_D);
        String firstDayOfMonth = sdf.format(cal.getTime()) + " 00:00:00";
        return firstDayOfMonth;
    }

    /**
     * @description: 获取指定年月的最后一天
     *
     * @author: Fang Kun
     * @param year
     * @param month
     * @date: 2021/1/4 16:34
     * @return: java.lang.String
     */
    public static String getLastDayOfDateMonth(int year, int month){
        Calendar cal = Calendar.getInstance();
        // 设置年
        cal.set(Calendar.YEAR, year);
        // 设置月份
        cal.set(Calendar.MONTH, month - 1);
        // 获取月份的最大天数
        int lastDay =lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        // 设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        // 格式化日期，获取最后时刻
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_Y_M_D);
        String lastDayOfMonth = sdf.format(cal.getTime()) + " 23:59:59";
        return lastDayOfMonth;
    }

    /**
     * @description: 获取i个月后的最后一天
     * @param i 增量值
     * @return: java.util.Date
     * @author: wangaogao
     * @date: 2020/8/18 15:45
     */
    public static Date getLastDayOfAddMonth(int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, i);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取一年前的今天
     * @return
     */
    public static Date getYearInterval() {
        Date date = new Date();//获取当前时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, -1);
        return calendar.getTime();
    }

    /**
     * @description: 获取i年后的最后一天
     * @param i 增量值
     * @return: java.util.Date
     * @author: wangaogao
     * @date: 2020/8/18 15:45
     */
    public static Date getLastDayOfAddYear(int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, i);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 导出时间格式 yyyyMMddHHmmss
     *
     * @param date
     * @return
     */
    public static String convertDateStringForExport(Date date) {
        return dateConvertToString(date, DATE_FORMAT_FOR_EXPORT);
    }

    /**
     * 生成时间格式 yyyyMMdd
     *
     * @param date
     * @return
     */
    public static String getDateString(Date date) {
        return dateConvertToString(date, DATE_FORMAT_FOR_Y_M_D);
    }

    /**
     * 获取时间段之间的所有月份集合
     *
     * @param minDate 201901
     * @param maxDate 202012
     */
    public static List<String> getDateInfo(String minDate, String maxDate) {
        ArrayList<String> result = new ArrayList<>();
        //格式化为年月
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_Y_M);

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        try {
            min.setTime(sdf.parse(minDate));
            min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

            max.setTime(sdf.parse(maxDate));
            max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);
        } catch (ParseException e) {
            LoggerFactory.getLogger(ImsDateUtil.class).error(e.toString());
        }

        Calendar curr = min;
        while (curr.before(max)) {
            result.add(sdf.format(curr.getTime()));
            curr.add(Calendar.MONTH, 1);
        }

        return result;
    }

    /**
     * yyyyMM拼接yyyy年MM月
     *
     * @param list
     * @return
     */
    public static List<String> subStringDateString(List<String> list) {
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        StringBuilder sb;
        List<String> resultList = new ArrayList<>();
        for (String s : list) {
            sb = new StringBuilder();
            resultList.add(String.valueOf(sb.append(s).insert(4, "-")));
        }
        return resultList;
    }

    /**
     * 获取i月之前的时间
     *
     * @param date
     * @param i
     * @return
     * @author wangsongtao
     */
    public static Date getAgoDate(Date date, int i) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, -i + 1);
        return c.getTime();
    }

    /**
     * 对指定时间进行格式化字符串
     *
     * @param target  指定时间
     * @param pattern 格式化
     * @return 格式化后的字符串
     * @see ImsConstants
     */
    public static String formateDate(Date target, String pattern) {
        if (null == target) {
            return null;
        }
        String usePattern = pattern;
        if (StringUtils.isEmpty(usePattern)) {
            usePattern = ImsConstants.DATE_FORMAT_Y_M_D;
        }
        FastDateFormat fastDateFormat = FastDateFormat.getInstance(usePattern);
        return fastDateFormat.format(target);
    }

    /**
     * 将字符串格式 YYYY-MM-DD 或 YYYY-MM 格式化为YYYYMM
     *
     * @param target 指定时间
     * @return 格式化后的字符串
     * @author Hao.Y
     */
    public static String formateYM(String target) {
        target = target.trim();
        if (StringUtil.isBlank(target) || target.length() < 7 || !(dateValidate(target, DATE_REGEX_Y_M_D) || dateValidate(target, DATE_REGEX_Y_M))) {
            throw new RuntimeException();
        }
        return target.substring(0, 4) + target.substring(5, 7);
    }

    /**
     * 将字符串格式 YYYYMMDD 或 YYYYMM 格式化为YYYY-MM
     *
     * @param target 指定时间
     * @return 格式化后的字符串
     * @author Hao.Y
     */
    public static String formateY_M(String target) {
        target = target.trim();
        if (StringUtil.isBlank(target) || target.length() < 6 || !StringUtils.isNumeric(target)) {
            throw new RuntimeException();
        }
        return target.substring(0, 4) + "-" + target.substring(4, 6);
    }

    /**
     * 将字符串格式YYYYMMDD格式化为YYYY-MM-DD
     *
     * @param target 指定时间
     * @return 格式化后的字符串
     * @author Hao.Y
     */
    public static String formateYMD(String target) {
        target = target.trim();
        if (StringUtil.isBlank(target) || target.length() != 8 || !StringUtils.isNumeric(target)) {
            throw new RuntimeException();
        }
        return target.substring(0, 4) + "-" + target.substring(4, 6) + "-" + target.substring(6);
    }

    /**
     * 将字符串格式 YYYY-MM-DD 格式化为 YYYYMMDD
     *
     * @param target 指定时间
     * @return 格式化后的字符串
     * @author Hao.Y
     */
    public static String formateY_M_D(String target) {
        if(StringUtils.isBlank(target)){
            return target;
        }
        target = target.trim();
        if (StringUtil.isBlank(target) || target.length() != 10 || !dateValidate(target, DATE_REGEX_Y_M_D)) {
            throw new RuntimeException();
        }
        return target.substring(0, 4) + target.substring(5, 7) + target.substring(8);
    }

    /**
     * 日期格式正则校验
     *
     * @param target
     * @return
     * @author Hao.Y
     */
    private static boolean dateValidate(String target, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(target);
        return m.matches();
    }


    /**
     * 判断开始时间小于等于截至时间
     *
     * @param startStr
     * @param endStr
     * @param parseRegex
     * @return
     * @author gaoyaopeng
     */
    public static void judgeDate(String startStr, String endStr, String parseRegex) {
        try {
            Date startDate = ImsDateUtil.parseDate(startStr, parseRegex);
            Date endDate = ImsDateUtil.parseDate(endStr, parseRegex);
            judgeDate(startDate, endDate);
        } catch (ParseException e) {
            throw new RuntimeException();
        }
    }

    /**
     * 判断开始时间小于等于截至时间
     *
     * @param startDate
     * @param endDate
     * @return
     * @author gaoyaopeng
     */
    public static void judgeDate(Date startDate, Date endDate) {
        long t = startDate.getTime() - endDate.getTime();
        if (t > 0) {
            throw new RuntimeException();
        }
    }

    /**
     * @param year 年份字符串
     * @param size 集合的长度
     * @description: 获取一个指定长度的年份的字符串数组
     * @author: Fang Kun
     * @date: 2020/4/15 16:49
     * @return: java.util.List<java.lang.String>
     */
    public static List<String> getYearList(String year, int size){
        int yearTemp=0;
        if(StringUtils.isEmpty(year)){
            yearTemp = getYear(new Date());
        }else{
            yearTemp = Integer.parseInt(year);
        }
        int sizeTemp=size-1;
        List<String> yearArr=new ArrayList<>(sizeTemp+1);
        for (int i = sizeTemp; i >= 0; i--) {
            yearArr.add(String.valueOf(yearTemp-i));
        }
        return yearArr;
    }


    /**
     * @param statYear 年份
     * @description: 获取对应年份的所有月份
     * @return: java.util.List<java.lang.String> yyyyMM
     * @author: wangaogao
     * @date: 2020/4/15 19:18
     */
    public static List<String> getMonthsByYear(String statYear) {
        if (StringUtils.isBlank(statYear)) {
            return null;
        }
        List<String> result = new ArrayList<>();

        for (int i = 1; i <= 12; i++) {
            StringBuilder sb = new StringBuilder();
            sb.append(statYear);
            if (i < 10) {
                sb.append("0");
            }
            result.add(sb.append(i).toString());
        }

        return result;
    }

    /**
     * @param statYear 年份
     * @description: 获取对应年份的所有月份（本年度截止到当前月）
     * @return: java.util.List<java.lang.String> yyyyMM
     * @author: wangaogao
     * @date: 2020/4/15 19:18
     */
    public static List<String> getMonthsByYearToNow(String statYear) {
        if (StringUtils.isBlank(statYear)) {
            return null;
        }
        List<String> result = new ArrayList<>();

        String year = String.valueOf(LocalDate.now().getYear());
        int count = year.equals(statYear) ? LocalDate.now().getMonthValue() : 12;

        for (int i = 1; i <= count; i++) {
            StringBuilder sb = new StringBuilder();
            sb.append(statYear);
            if (i < 10) {
                sb.append("0");
            }
            result.add(sb.append(i).toString());
        }

        return result;
    }

    /**
     * @param yearMon yyyyMM字符串
     * @description: 将yyyyMM转为yyyy年MM月
     * @return: java.lang.String yyyy年MM月 格式字符串
     * @author: wangaogao
     * @date: 2020/4/15 20:30
     */
    public static String getYearAndMonthChineseFromString(String yearMon) {
        StringBuilder sb = new StringBuilder();
        sb.append(yearMon).insert(4, "年").append("月");

        return sb.toString();
    }

    /**
     * @param monthList yyyyMM格式数组
     * @description: 将yyyyMM转为yyyy年MM月
     * @return: java.util.List<java.lang.String> yyyy年MM月格式数组
     * @author: wangaogao
     * @date: 2020/4/15 20:09
     */
    public static List<String> getYearAndMonthChineseFromList(List<String> monthList) {
        if (CollectionUtil.isEmpty(monthList)) {
            return null;
        }

        List<String> result = new ArrayList<>();
        for (String yearMon : monthList) {
            result.add(getYearAndMonthChineseFromString(yearMon));
        }

        return result;
    }

    /**
     * @param year
     * @description: 根据年份获取当年结束时间
     * @author: gengxiu
     * @date: 2020/3/31 15:44
     * @return: java.util.Date
     */
    public static Date getYearEndTime(String year) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ImsConstants.DATE_FORMAT_Y_M_D);
        Date date = null;
        try {
            date = ImsDateUtil.getDayEndDateTime(simpleDateFormat.parse(year + END_DAY));
        } catch (ParseException e) {
            LoggerFactory.getLogger(ImsDateUtil.class).error(e.toString());
        }
        return date;
    }

    /**
     * @param year
     * @description: 根据年份获取当年起始时间
     * @author: gengxiu
     * @date: 2020/3/31 15:44
     * @return: java.util.Date
     */
    public static Date getYearBeginTime(String year) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ImsConstants.DATE_FORMAT_Y_M_D);
        Date date = null;
        try {
            date = ImsDateUtil.getDayBeginDateTime(simpleDateFormat.parse(year + BEGIN_DAY));
        } catch (ParseException e) {
            LoggerFactory.getLogger(ImsDateUtil.class).error(e.toString());
        }
        return date;
    }

    /**
     * @description: 计算年龄
     * @author: hebiao
     * @param birthDay
     * @date: 2020/5/27 17:10
     * @return: int
     */
    public static int getAgeByBirth(Date birthDay) {
        Calendar cal = Calendar.getInstance();
        //当前年份
        final int yearNow = cal.get(Calendar.YEAR);
        //当前月份
        final int monthNow = cal.get(Calendar.MONTH);
        //当前日期
        final int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);
        final int yearBirth = cal.get(Calendar.YEAR);
        final int monthBirth = cal.get(Calendar.MONTH);
        final int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        //计算整岁数
        int age = yearNow - yearBirth;
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth){
                    //当前日期在生日之前，年龄减一
                    age--;
                }
            } else {
                //当前月份在生日之前，年龄减一
                age--;
            }
        }
        return age;
    }

    /**
     * @description: 获取近一年所有月份（含当前月）
     * @return: java.util.List<java.lang.String> 格式yyyymm
     * @author: wangaogao
     * @date: 2020/8/19 10:53
     */
    public static List<String> getAllMonthFromNearYear(){
        // 建一个容器
        List<String> months=new ArrayList<>();
        // 获取日历对象
        Calendar calendar = Calendar.getInstance();
        // 调整到12个月以前
        calendar.add(Calendar.MONTH,-11);
        // 循环12次获取12个月份
        for (int i = 0; i <= 11; i++) {
            // 日历对象转为Date对象
            Date date = calendar.getTime();
            // 将date转为字符串
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_Y_M);
            String dateStr = sdf.format(date);
            // 向list集合中添加
            months.add(dateStr);
            // 每次月份+1
            calendar.add(Calendar.MONTH,1);
        }

        return months;
    }

    /**
     * @param date 日期
     * @description: 返回指定日期的季度
     * @return: int 所属季度（1,2,3,4）
     * @author: wangaogao
     * @date: 2020/10/16 20:29
     */
    public static int getQuarterOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH) / 3 + 1;
    }

    /**
     * @param num 所属季度
     * @description: 获取某季度的第一天和最后一天(YYYYMMDD)
     * @return: java.lang.String[] 所属季度第一天和最后一天
     * @author: wangaogao
     * @date: 2020/10/16 20:31
     */
    public static String[] getCurrQuarter(int num) {
        String[] s = new String[2];
        String str;
        // 设置本年的季
        Calendar quarterCalendar;
        switch (num) {
            case 1: // 本年到现在经过了一个季度，在加上前4个季度
                quarterCalendar = Calendar.getInstance();
                quarterCalendar.set(Calendar.MONTH, 3);
                quarterCalendar.set(Calendar.DATE, 1);
                quarterCalendar.add(Calendar.DATE, -1);
                str = dateConvertToString(quarterCalendar.getTime(), DATE_FORMAT_FOR_Y_M_D);
                if(str != null && !"".equals(str) && str.length() >= 4) {
                    s[0] = str.substring(0, 4) + "0101";
                    s[1] = str;
                }
                break;
            case 2: // 本年到现在经过了二个季度，在加上前三个季度
                quarterCalendar = Calendar.getInstance();
                quarterCalendar.set(Calendar.MONTH, 6);
                quarterCalendar.set(Calendar.DATE, 1);
                quarterCalendar.add(Calendar.DATE, -1);
                str = dateConvertToString(quarterCalendar.getTime(), DATE_FORMAT_FOR_Y_M_D);
                if(str != null && !"".equals(str) && str.length() >= 4) {
                    s[0] = str.substring(0, 4) + "0401";
                    s[1] = str;
                }
                break;
            case 3:// 本年到现在经过了三个季度，在加上前二个季度
                quarterCalendar = Calendar.getInstance();
                quarterCalendar.set(Calendar.MONTH, 9);
                quarterCalendar.set(Calendar.DATE, 1);
                quarterCalendar.add(Calendar.DATE, -1);
                str = dateConvertToString(quarterCalendar.getTime(), DATE_FORMAT_FOR_Y_M_D);
                if(str != null && !"".equals(str) && str.length() >= 4) {
                    s[0] = str.substring(0, 4) + "0701";
                    s[1] = str;
                }
                break;
            case 4:// 本年到现在经过了四个季度，在加上前一个季度
                quarterCalendar = Calendar.getInstance();
                str = dateConvertToString(quarterCalendar.getTime(), DATE_FORMAT_FOR_Y_M_D);
                if(str != null && !"".equals(str) && str.length() >= 4) {
                    s[0] = str.substring(0, 4) + "1001";
                    s[1] = str.substring(0, 4) + "1231";
                }
                break;
            default:
                break;
        }
        return s;
    }

    /**
     * 根据身份证的号码算出当前身份证持有者的年龄
     * @return
     */
    public static Integer countAge(String idNumber, Date admTime) {
        if(idNumber.length() != 18 && idNumber.length() != 15){
//            throw new IllegalArgumentException("身份证号长度错误");
            return null;
        }
        String year;
        String yue;
        String day;
        if(idNumber.length() == 18){
            year = idNumber.substring(6).substring(0, 4);// 得到年份
            yue = idNumber.substring(10).substring(0, 2);// 得到月份
            day = idNumber.substring(12).substring(0,2);//得到日
        }else{
            year = "19" + idNumber.substring(6, 8);// 年份
            yue = idNumber.substring(8, 10);// 月份
            day = idNumber.substring(10, 12);//日
        }
        Date date = new Date();// 得到当前的系统时间
        if ( admTime != null ) {
            date = admTime;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String fyear = format.format(date).substring(0, 4);// 当前年份
        String fyue = format.format(date).substring(5, 7);// 月份
        String fday=format.format(date).substring(8,10);//
        int age = 0;
        if(Integer.parseInt(yue) == Integer.parseInt(fyue)){//如果月份相同
            if(Integer.parseInt(day) <= Integer.parseInt(fday)){//说明已经过了生日或者今天是生日
                age = Integer.parseInt(fyear) - Integer.parseInt(year);
            }
        }else{

            if(Integer.parseInt(yue) < Integer.parseInt(fyue)){
                //如果当前月份大于出生月份
                age = Integer.parseInt(fyear) - Integer.parseInt(year);
            }else{
                //如果当前月份小于出生月份,说明生日还没过
                age = Integer.parseInt(fyear) - Integer.parseInt(year) - 1;
            }
        }
        return age;
    }

    /**
     * 获取性别
     * @param patnGend
     * @return
     */
    public static String getGend(String patnGend){
        if ( StringUtils.isEmpty(patnGend) ){
            return null;
        }
        int gend = Integer.valueOf(patnGend);
        if (gend % 2 == 0) {
            return  "女";
        }else{
            return  "男";
        }
    }

    /**
     * @description: 根据年度获取所有周末日期
     * @param date 日期
     * @return java.util.List<java.lang.String>
     * @author wangaogao
     * @date 21/03/29 17:14
     */
    public static List<String> getAllWeekDaysByYear(Date date) {
        return getAllWeekDaysByYear(date, 0, 0);
    }
    /**
     * @param date 日期
     * @param bf   上浮
     * @param af   下浮
     * @return java.util.List<java.lang.String>
     * @description: 根据年度获取所有周末日期（多年度）
     * @author wangaogao
     * @date 21/03/17 16:43
     */
    public static List<String> getAllWeekDaysByYear(Date date, int bf, int af) {
        List<String> resultList = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        c.setTime( date );
        int year = c.get( Calendar.YEAR );
        Calendar cStart = Calendar.getInstance();
        cStart.set( year - bf, 0, 1 );
        Calendar cEnd = Calendar.getInstance();
        cEnd.set( year + 1 + af, 0, 1 );

        while (cStart.compareTo( cEnd ) < 0) {
            if (cStart.get( Calendar.DAY_OF_WEEK ) == Calendar.SATURDAY || cStart.get( Calendar.DAY_OF_WEEK ) == Calendar.SUNDAY) {
                int m = cStart.get( Calendar.MONTH ) + 1;
                int d = cStart.get( Calendar.DAY_OF_MONTH );
                String month = m < 10 ? "0" + m : "" + m;
                String day = d < 10 ? "0" + d : "" + d;

                resultList.add( cStart.get( Calendar.YEAR ) + "-" + month + "-" + day );
            }
            // 日期+1
            cStart.add( Calendar.DATE, 1 );
        }

        return resultList;
    }

    /**
     * @param date 指定时间
     * @param num  为时间添加或减去的时间天数
     * @return
     * @description: 根据当前时间，添加或减去指定的时间量。例如，要从当前日历时间减去 5 天，可以通过调用以下方法做到这一点：
     * add(Calendar.DAY_OF_MONTH, -5)。
     */
    public static Date getBeforeOrAfterDate(Date date, int num) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( date );
        calendar.add( Calendar.DATE, num );
        return calendar.getTime();
    }
    /**
     * @param beginDate 开始日期
     * @param endDate   结束日期
     * @return boolean
     * @description: 判断两个日期是否同月
     * @author wangaogao
     * @date 21/03/18 19:00
     */
    public static boolean isSimilarMonth(Date beginDate, Date endDate) {
        if (beginDate == null || endDate == null) {
            return false;
        }
        Calendar calendarBegin = Calendar.getInstance();
        calendarBegin.setTime( beginDate );
        Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.setTime( endDate );

        return calendarBegin.get( Calendar.MONTH ) == calendarEnd.get( Calendar.MONTH );
    }
    /**
     * @description: 获取两个日期之间的所有月份集合
     * @param beginDate 开始日期
     * @param endDate 结束日期
     * @return java.util.List<java.lang.String>
     * @author wangaogao
     * @date 21/03/18 19:58
     */
    public static List<String> getAllMonthFromBetweenDate(Date beginDate, Date endDate) {
        List<String> months = new ArrayList<>();
        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();
        min.setTime( beginDate );
        min.set( min.get( Calendar.YEAR ), min.get( Calendar.MONTH ), 1 );
        max.setTime( endDate );
        max.set( max.get( Calendar.YEAR ), max.get( Calendar.MONTH ), 2 );
        while (min.before( max )) {
            months.add( dateConvertToString( min.getTime(), DATE_FORMAT_Y_M ) );
            min.add( Calendar.MONTH, 1 );
        }
        return months;
    }

    /**
     * 获取这一年的所有日期
     * @param year
     * @return
     */
    public static List<String> getDatesOfYear(int year) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT_Y_M_D);
        LocalDate startDate = LocalDate.of(year, 1, 1);
        LocalDate endDate = LocalDate.of(year, 12, 31);
        List<String> dates = new ArrayList<>();
        while (!startDate.isAfter(endDate)) {
            dates.add(startDate.format(formatter));
//            dates.add( Date.from(startDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
            startDate = startDate.plusDays(1);
        }
        return dates;
    }
}
