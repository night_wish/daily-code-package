package com.lee.utils;

import cn.hsa.ims.common.generator.IdGeneratorWrapper;
import cn.hsa.ims.common.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.context.ApplicationContext;

/**
 * <p>
 * 生成唯一ID
 * </p>
 *
 * @author lishang Created on 2020年3月19日
 * @version 1.0
 */
public class IDUtils {

    private static final String PREFIX =  "TABLE:";
    /**
     * 智能监管的groupcode
     */
    private static final String GROUP_CODE = "10";


    private static final String SYSTEM_ADMDVS = "areaCode";


    /**
     * 返回生成的唯一id
     * 医保区划+年月日+序列号
     *
     * @return
     */
    public static synchronized String generatorId(String key) {
        if (StringUtils.isBlank(key)) {
            throw new RuntimeException();
        }
        ApplicationContext applicationContext = SpringContextUtils.getApplicationContext();
        String systemAdmdvs = applicationContext.getEnvironment().getProperty(SYSTEM_ADMDVS);
        IdGeneratorWrapper idGeneratorWrapper = applicationContext.getBean(IdGeneratorWrapper.class);
        String id = idGeneratorWrapper.generatorStringId(new StringBuilder(PREFIX).append(key.toUpperCase()).append(":PK").toString()).toString();
        return new StringBuilder(systemAdmdvs).append(ImsDateUtil.convertDateStringForExport(ImsDateUtil.getCurrentDate())).append(id).toString();
    }

    /**
     * 生成唯一的RId
     *
     * @param key
     * @return
     *
     */
    public static String generatorRId(String key) {
        ApplicationContext applicationContext = SpringContextUtils.getApplicationContext();
        String systemAdmdvs = applicationContext.getEnvironment().getProperty(SYSTEM_ADMDVS);
        IdGeneratorWrapper idGeneratorWrapper = applicationContext.getBean(IdGeneratorWrapper.class);
        String rid = idGeneratorWrapper.generatorRId(systemAdmdvs, GROUP_CODE, new StringBuilder(PREFIX).append(key.toUpperCase()).append(":RID").toString());
        return rid;
    }

    /**
     * 生成唯一服务事项实例id:子系统编号两位（采用RID分组标识规则）+时间14位（YYYYMMDDhhmmss）+行政区划前四位+4位顺序号
     *
     * @return
     */
    public static String generatorServMattInstId() {
        ApplicationContext applicationContext = SpringContextUtils.getApplicationContext();
        String systemAdmdvs = applicationContext.getEnvironment().getProperty(SYSTEM_ADMDVS);
        String servMattInstId = "ims/" + GROUP_CODE + DateFormatUtils.format(DateUtil.getCurrentDate(), "yyyyMMddHHmmss") + systemAdmdvs;
        return servMattInstId;
    }

}
