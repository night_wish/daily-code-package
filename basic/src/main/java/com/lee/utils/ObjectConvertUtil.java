package com.lee.utils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.DateConverter;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023-12-09 17:02
 */
public class ObjectConvertUtil {

    public static <T, S> T convert(T target, S source) {
        if (null == source || null == target){
            return null;
        }
        try {
            ConvertUtils.register((Converter) new DateConverter(null), Date.class);
            BeanUtils.copyProperties(source, target);
        } catch (Exception e) {
            throw new BizException(130003, "对象类型转换异常" + e.getMessage());
        }
        return target;
    }

    public static Map<String, Object> transBean2Map(Object obj) {
        if (obj == null)
            return null;
        Map<String, Object> map = new HashMap<>();
        PropertyDescriptor[] propertyDescriptors = getPropertyDescriptor(obj);
        Arrays.<PropertyDescriptor>stream(propertyDescriptors)
              .filter(property -> !"class".equals(property.getName()))
              .forEach(property -> {
                  Method getter = property.getReadMethod();
                  Object value = null;
                  String key = property.getName();
                  try {
                      value = getter.invoke(obj, new Object[0]);
                  } catch (IllegalAccessException | java.lang.reflect.InvocationTargetException e) {
                      e.printStackTrace();
                  }
                  map.put(key, value);
              });
        return map;
    }

    private static PropertyDescriptor[] getPropertyDescriptor(Object obj) {
        BeanInfo beanInfo = null;
        try {
            beanInfo = Introspector.getBeanInfo(obj.getClass());
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return (beanInfo != null) ? beanInfo.getPropertyDescriptors() : new PropertyDescriptor[0];
    }

    public static <T> T transMap2Bean(Map<String, Object> map, T obj) {
        if (obj == null)
            return null;
        PropertyDescriptor[] propertyDescriptors = getPropertyDescriptor(obj);
        Arrays.<PropertyDescriptor>stream(propertyDescriptors)
              .filter(property -> !"class".equals(property.getName()))
              .forEach(property -> {
                  Method setter = property.getWriteMethod();
                  String key = property.getName();
                  try {
                      Object value = map.get(key);
                      if (value != null)
                          setter.invoke(obj, new Object[]{value});
                  } catch (IllegalAccessException | java.lang.reflect.InvocationTargetException e) {
                      e.printStackTrace();
                  }
              });
        return obj;
    }

    public static <T> T map2Bean(Map<String, Object> map, T t) {
        if (map == null || t == null)
            return null;
        try {
            BeanUtils.populate(t, map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }
}
