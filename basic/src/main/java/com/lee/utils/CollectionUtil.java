package com.lee.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 项目名称：hsa-ims-engine-crhms
 * 类 名 称：CollectionUtil
 * 类 描 述：
 * 创建时间：2021-08-11 09:42
 * 创 建 人：chenhaitao
 */
public class CollectionUtil {

    private CollectionUtil() {
    }

    /**
     * 按指定大小切割列表
     *
     * @param <T>
     * @param list         被切割列表
     * @param sizeOneGroup 字列表元素个数
     * @return
     */
    public static <T> List<List<T>> splitList(List<T> list, int sizeOneGroup) {
        List<List<T>> result = new ArrayList<>();
        int size = list == null ? 0 : list.size();
        for (int i = 0; i < size; i += sizeOneGroup) {
            int min = Math.min((i + sizeOneGroup), size);
            List<T> subList = list.subList(i, min);
            result.add(subList);
        }
        return result;
    }

    /**
     * List转Sql中in关键字后面的字符串
     *
     * @param tList
     * @return
     */
    public static String listToInStr(List<String> tList) {
        if(tList!=null && tList.size()>0){
            String str = tList.toString();
            str = str.replace("[", "('");
            str = str.replace(", ", "','");
            str = str.replace("]", "')");
            return str;
        }
        return "";
    }
}
