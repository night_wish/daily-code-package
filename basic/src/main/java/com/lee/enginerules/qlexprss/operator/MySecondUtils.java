package com.lee.enginerules.qlexprss.operator;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/6/11 14:13
 */
public class MySecondUtils {


    /**
     * 转为大写
     */
    public static String toUpper(String s) {
        return s.toUpperCase();
    }

    /**
     * 是否包含
     */
    public Boolean anyContains(String oriStr, String subStr) {
        return oriStr.contains(subStr);
    }
}
