package com.lee.enginerules.qlexprss;

import com.alibaba.fastjson.JSONObject;
import com.lee.enginerules.qlexprss.operator.MyFirstOperator;
import com.lee.enginerules.qlexprss.operator.MySecondUtils;
import com.lee.enginerules.qlexprss.operator.MyThirdOperator;
import com.ql.util.express.DefaultContext;
import com.ql.util.express.DynamicParamsUtil;
import com.ql.util.express.ExpressRunner;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Test;

/**
 * @author Lee
 * @version 1.0
 * @description QLExpress 引擎
 * @date 2024/6/11 9:34
 */
@Slf4j
public class QLExpressTest {

    /**
     * 简单操作--API测试
     */
    public void simple_demo_12(){
        ExpressRunner expressRunner = new ExpressRunner();
    }

    /**
     * 复杂操作---集合的快速遍历
     */
    @Test
    public void complex_demo_11() throws Exception {
        ExpressRunner expressRunner = new ExpressRunner(false,false);
        DefaultContext<String, Object> context = new DefaultContext<>();
        String express =
                      "map = new HashMap();\n" +
                      "map.put(\"a\", \"a_value\");\n" +
                      "map.put(\"b\", \"b_value\");\n" +
                      "keySet = map.keySet();\n" +
                      "objArr = keySet.toArray();\n" +
                      "for (i = 0; i < objArr.length; i++) {\n" +
                      "    key = objArr[i];\n" +
                      "    System.out.println(map.get(key));\n" +
                      "}";

        Object res_1 = expressRunner.execute(express, context, null, false, false);
    }

    /**
     * 复杂操作---集合的快速写法
     */
    @Test
    public void complex_demo_10()throws Exception{
        ExpressRunner expressRunner = new ExpressRunner(false,false);
        expressRunner.addOperator("append", new MyThirdOperator());
        //expressRunner.addFunctionOfClassMethod("appendStr",QLExpressTest.class.getName(),"addStr",new Class[]{String.class,String.class},null);
        expressRunner.addFunctionOfServiceMethod("appendStr",new QLExpressTest(),"addStr",new Class[]{String.class,String.class},null);
        DefaultContext<String, Object> context = new DefaultContext<>();
        String express_1 = "abc = NewMap('A':'aa','B':'bb'); return abc.get('A') append abc.get('B');";
        Object res_1 = expressRunner.execute(express_1, context, null, false, false);
        log.info("execute result - > {}", res_1.toString());


        String express_2 = "defg = NewList(1,2,3); return defg.get(1) + defg.get(2);";
        Object res_2_2 = expressRunner.execute(express_2, context, null, false, false);
        log.info("execute result - > {}", res_2_2.toString());

        String express_3 = "abc = ['b1','b2','b3']; return appendStr(abc[0],abc[2]);";
        Object res_3 = expressRunner.execute(express_3, context, null, false, false);
        log.info("execute result - > {}", res_3.toString());
    }

    public String addStr(String s1,String s2){
        return s1 + s2;
    }

    /**
     * 复杂操作---不定参数的使用
     */
    @Test
    public void complex_demo_9() throws Exception {
        ExpressRunner expressRunner = new ExpressRunner(false, false);

        //1、方式一
        expressRunner.addFunctionOfServiceMethod("append",this,"appendObjToString",new Class[]{Object[].class},null);
        DefaultContext<String, Object> context = new DefaultContext<>();
        String express = "append(['a','1','c','2','e','3'])";
        Object res = expressRunner.execute(express, context, null, false, false);
        log.info("execute result - > {}", res.toString());

        //2、方式二
        String express2 = "append('c','f','l')";
        DynamicParamsUtil.supportDynamicParams = true;
        Object res2 = expressRunner.execute(express2, context, null, false, false);
        log.info("execute result - > {}", res2.toString());
    }

    public Object appendObjToString(Object... params)throws Exception {
        StringBuilder sb = new StringBuilder();
        for (Object param : params) {
            sb.append(param + ",");
        }
        String content = sb.toString();
        if (content.endsWith(",")) {
            return content.substring(0, content.length() - 1);
        }
        return content;
    }

    /**
     * 复杂操作---查询外部需要定义的参数和函数
     */
    @Test
    public void complex_demo_8() throws Exception {
        String express = " 平均分 = (语文+数学+英语+其他科目)/4.0; return 平均分";
        ExpressRunner expressRunner = new ExpressRunner(false, false);
        String[] outVarNames = expressRunner.getOutVarNames(express);
        for (String outVarName : outVarNames) {
            log.info("{}", outVarName);
        }
    }

    /**
     * 复杂操作---宏定义
     */
    @Test
    public void complex_demo_7() throws Exception {
        ExpressRunner expressRunner = new ExpressRunner();
        expressRunner.addMacro("计算平均成绩", "(语文+数学+英语)/3.0");
        expressRunner.addMacro("是否优秀", "计算平均成绩>90");

        DefaultContext<String, Object> context = new DefaultContext<>();
        context.put("语文", 91);
        context.put("数学", 98);
        context.put("英语", 91);
        Object res = expressRunner.execute("是否优秀", context, null, false, false);
        log.info("execute result - > {}", res.toString());
    }


    /**
     * 复杂操作6---绑定java类或者对象的method
     */
    @Test
    public void complex_demo_6() throws Exception {
        ExpressRunner expressRunner = new ExpressRunner();
        expressRunner.addFunctionOfClassMethod("转为大写", MySecondUtils.class.getName(), "toUpper", new Class[]{String.class}, null);

        DefaultContext<String, Object> context = new DefaultContext<>();
        context.put("content", "abc");
        String express = "转为大写(content)";
        Object res = expressRunner.execute(express, context, null, false, false);
        log.info("execute result - > {}", res.toString());

        expressRunner.addFunctionOfServiceMethod("是否包含", new MySecondUtils(), "anyContains", new Class[]{String.class, String.class}, null);
        context.put("原始字符串", "abcdefg");
        context.put("子字符串", "def");
        String express2 = "是否包含(原始字符串,子字符串)";
        Object res2 = expressRunner.execute(express2, context, null, false, false);
        log.info("execute result - > {}", res2.toString());
    }


    /**
     * 复杂操作5---自定义Operator
     */
    @Test
    public void complex_demo_5() throws Exception {
        //添加 Operator
        ExpressRunner expressRunner = new ExpressRunner();
        expressRunner.addOperator("add", new MyFirstOperator());
        DefaultContext<String, Object> context = new DefaultContext<>();
        Object execute1 = expressRunner.execute("1 add 2 add 3", context, null, false, false);
        log.info("execute result - > {}", execute1.toString());

        //替换 Operator
        expressRunner.replaceOperator("+", new MyFirstOperator());
        Object execute2 = expressRunner.execute("1 + 2 + 3", context, null, false, false);
        log.info("execute result - > {}", execute2.toString());

        //添加  function
        expressRunner.addFunction("join", new MyFirstOperator());
        Object execute3 = expressRunner.execute("join(1,2,3)", context, null, false, false);
        log.info("execute result - > {}", execute3.toString());
    }

    /**
     * 简单入门4---复杂逻辑表达式
     */
    @Test
    public void simple_demo_4() throws Exception {
        ExpressRunner expressRunner = new ExpressRunner();
        expressRunner.addOperatorWithAlias("如果", "if", null);
        expressRunner.addOperatorWithAlias("或", "||", null);
        expressRunner.addOperatorWithAlias("且", "&&", null);
        expressRunner.addOperatorWithAlias("等于", "==", null);
        expressRunner.addOperatorWithAlias("大于", ">", null);
        expressRunner.addOperatorWithAlias("小于", "<", null);
        expressRunner.addOperatorWithAlias("则", "then", null);
        expressRunner.addOperatorWithAlias("否则", "else", null);
        expressRunner.addOperatorWithAlias("返回", "return", null);
        expressRunner.addFunctionOfClassMethod("字符串等于", QLExpressTest.class.getName(), "equals", new Class[]{String.class, String.class}, null);
        expressRunner.addFunctionOfClassMethod("获取JSON中的值", QLExpressTest.class.getName(), "getValueByKey", new Class[]{String.class}, null);


        DefaultContext<String, Object> context = new DefaultContext<>();
        String express = "如果 ( 字符串等于( 获取JSON中的值('message'),'success') ) 则 {返回 true;} 否则 {返回 false}";
        Object execute = expressRunner.execute(express, context, null, false, false);
        log.info("execute result - > {}", execute.toString());
    }

    public String getValueByKey(String key) {
        String json = "{'code':1,'data':'aaa','message':'success'}";
        return JSONObject.parseObject(json).getString("message");
    }

    public Boolean equals(String v1, String v2) {
        return StringUtils.equals(v1, v2);
    }

    /**
     * 简单入门3---逻辑运算表达式--别名
     */
    @Test
    public void simple_demo_3() throws Exception {
        ExpressRunner expressRunner = new ExpressRunner();
        expressRunner.addOperatorWithAlias("如果", "if", null);
        expressRunner.addOperatorWithAlias("否则", "else", null);
        expressRunner.addOperatorWithAlias("返回", "return", null);
        DefaultContext<String, Object> context = new DefaultContext<>();
        context.put("语文", 120);
        context.put("数学", 115);
        context.put("英语", 100);
        String express = "如果(语文+数学+英语>350){返回 '优秀';}否则 如果(语文+数学+英语>300){返回 '良好'}否则{返回 '其他';}";
        String result = (String) expressRunner.execute(express, context, null, false, false);
        log.info("execute result - > {}", result);
    }

    /**
     * 简单入门2---逻辑运算表达式
     */
    @Test
    public void simple_demo_2() throws Exception {
        ExpressRunner expressRunner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();
        context.put("语文", 120);
        context.put("数学", 115);
        context.put("英语", 130);
        String express = "if(语文+数学+英语>350){return '优秀';}else if(语文+数学+英语>300){return '良好'}else{return '其他';}";
        String result = (String) expressRunner.execute(express, context, null, false, false);
        log.info("execute result - > {}", result);
    }

    /**
     * 简单入门1---简单运算表达式
     */
    @Test
    public void simple_demo_1() throws Exception {
        ExpressRunner expressRunner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();
        context.put("a", 2);
        context.put("b", 2);
        context.put("c", 3);
        String express = "a + b * c";
        Object result = expressRunner.execute(express, context, null, false, false);
        log.info("execute result - > {}", result);
    }

}
