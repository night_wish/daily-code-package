package com.lee.enginerules.qlexprss.operator;

import com.alibaba.fastjson.JSON;
import com.ql.util.express.Operator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description 集成Operator
 * @date 2024/6/11 13:47
 */
public class MyFirstOperator extends Operator {

    @Override
    public Object executeInner(Object[] list) throws Exception {
        System.out.println("=====>"+JSON.toJSON(list));
        Object o1 = list[0];
        Object o2 = list[1];
        if(o1 instanceof List){
            ((List) o1).add(o2);
            return o1;
        }else{
            List res = new ArrayList<>();
            for (Object o : list) {
                res.add(o);
            }
            return res;
        }
    }
}
