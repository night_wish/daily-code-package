package com.lee.enginerules.qlexprss.operator;

import com.alibaba.fastjson.JSON;
import com.ql.util.express.Operator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/6/11 16:44
 */
public class MyThirdOperator extends Operator {

    @Override
    public Object executeInner(Object[] objects) throws Exception {
        System.out.println("=====>"+JSON.toJSON(objects));
        Object o1 = objects[0];
        Object o2 = objects[1];
        if(o1 instanceof List){
            ((List)o1).add(o2);
            return o1;
        }else{
            List res = new ArrayList();
            for (Object o : objects) {
                res.add(o);
            }
            return res;
        }
    }
}
