package com.lee.Redis.model;

import lombok.*;

import java.io.Serializable;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/4/4 17:15
 */
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RuleDTO implements Serializable {
    private static final long serialVersionUID = 5395490970353851340L;

    private String ruleId;

    private String ruleName;
}
