package com.lee.Redis;

import com.alibaba.fastjson.JSON;
import com.lee.Redis.model.RuleDTO;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 项目名称：basic
 * 类 名 称：ResdisTemplateTest
 * 类 描 述：
 * 创建时间：2021-08-16 16:59
 * 创 建 人：Lee
 */
@RestController
public class ResdisTemplateTest {

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private StringRedisTemplate redisTemplate2;

    @GetMapping("/testSet/{ruleName}")
    public void testSet(@PathVariable("ruleName")String ruleName){
        String ruleKey = "DRG_";
        RuleDTO ruleDTO001 = RuleDTO.builder().ruleId("001").ruleName("name001").build();
        RuleDTO ruleDTO002 = RuleDTO.builder().ruleId("002").ruleName("name002").build();
        RuleDTO ruleDTO003 = RuleDTO.builder().ruleId("003").ruleName("name003").build();

//        redisTemplate.opsForHash().put(ruleKey,ruleDTO001.getRuleName(),ruleDTO001);
//        redisTemplate.opsForHash().put(ruleKey,ruleDTO002.getRuleName(),ruleDTO002);
//        redisTemplate.opsForHash().put(ruleKey,ruleDTO003.getRuleName(),ruleDTO003);
        redisTemplate.opsForValue().set(ruleKey+"1"+"_"+ruleDTO001.getRuleName(),ruleDTO001);
        redisTemplate.opsForValue().set(ruleKey+"2"+"_"+ruleDTO002.getRuleName(),ruleDTO002);
        redisTemplate.opsForValue().set(ruleKey+"3"+"_"+ruleDTO003.getRuleName(),ruleDTO003);

        //Set<String> keys = redisTemplate.keys(ruleKey+"*");
//        Set<String> keys = redisTemplate.keys("*"+ruleName+"*");
//        Set<String> keys = redisTemplate.keys(ruleKey+"*"+ruleName+"*");
        Set<String> keys = redisTemplate.keys(ruleKey+"*"+"*");

        List<String> keyList = keys.stream().collect(Collectors.toList());
        System.out.println(keyList.size());
        System.out.println(JSON.toJSONString(keyList));
        List<RuleDTO> list = redisTemplate.opsForValue().multiGet(keyList);
        System.out.println(JSON.toJSONString(list));

    }

    @GetMapping("/test")
    public String hashAddTest(){

        Map<String,String> nomalMap = new HashMap<>();
        nomalMap.put("aaa1","bbb1");
        nomalMap.put("aaa2","bbb2");
        nomalMap.put("aaa3","bbb3");
        HashOperations<String, Object, Object> nomalOps = redisTemplate.opsForHash();
        nomalOps.putAll("IMS:NORMAL",nomalMap);


        redisTemplate.executePipelined(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection conn) throws DataAccessException {
                conn.openPipeline();

                conn.hMSet("SMS:PIPEL".getBytes(),convertMap(nomalMap));
                return null;
            }
        });


        return "success";
    }


    @GetMapping("/sendMsg")
    public void redisSendMsg(){
        String msg = "this is my first redis queue data";
        redisTemplate2.convertAndSend("IMS:TASKCANCEL",msg);
        System.out.println("------------------"+msg);
    }


    @GetMapping("/test2")
    public Map<Object,Object> hashGetTest(){
        Map<Object, String> nomal = redisTemplate.opsForHash().entries("IMS:NORMAL");
        Map<Object, Object> pipel = redisTemplate.opsForHash().entries("SMS:PIPEL".getBytes());
        Object aaa1 = redisTemplate.opsForHash().get("SMS:PIPEL".getBytes(), "aaa1");
        return pipel;

    }


    public Map<byte[],byte[]> convertMap(Map<String,String> objectMap){
        //序列化成字节数组
        Map<byte[],byte[]> byteMap=new HashMap<>();
        for(Map.Entry<String,String> objectEntry:objectMap.entrySet()){
            String key=objectEntry.getKey();
            final byte[] keyBytes = redisTemplate.getStringSerializer().serialize(key);
            Object value=objectEntry.getValue();
            final byte[] valueBytes =SerializeUtil.serialize(value);
            byteMap.put(keyBytes,valueBytes);
        }
        return byteMap;
    }


}
