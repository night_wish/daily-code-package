package com.lee.poi.renyaling;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/10/6 10:00
 */
@Slf4j
public class ReadAndCover20221006 {

    private static final String sourcePath = "C:\\Users\\le'e\\Desktop\\source.xlsx";

    private static final String sourceSheetName = "Sheet1";

    private static final String destPath = "C:\\Users\\le'e\\Desktop\\dest.xlsx";

    private static final String destSheetName = "Sheet1";

    private static NumberFormat numberFormat = NumberFormat.getInstance();

    static {
        numberFormat.setGroupingUsed(false);
    }

    public static void main(String[] args) throws IOException {
        //读取source数据
        List<Map<String, String>> dataSource = readExcel(sourcePath, sourceSheetName);
//        Integer close = 0;
        for (Map<String, String> map : dataSource) {
//            if (close == 1) {
//                break;
//            }
            String ksh = map.get("ksh");
            //System.out.println("=========="+ksh);
            for (Map.Entry<String, String> entry : map.entrySet()) {
                //log.info(entry.getKey() + " : " + entry.getValue());
                //写dest数据
                writeExcel(destPath, destSheetName, ksh, entry);
            }
//            close++;
        }


    }

    public static void writeExcel(String path, String sheetName, String ksh, Map.Entry<String, String> entry) throws IOException {
        List<Map<String, String>> resList = new ArrayList<>();
        //创建输入流
        FileInputStream fileInputStream = new FileInputStream(path);
        //获得文档
        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
        //根据name获取sheet表
        XSSFSheet sheet = workbook.getSheet(sheetName);

        int lastRow = sheet.getLastRowNum();//获得行数，下标从0开始
        XSSFRow titleRow = sheet.getRow(0);//获取第一行（第一行一般是标题）
        XSSFRow row = sheet.getRow(1);//获取第二行（第一行一般是标题）
        int lastCell = row.getLastCellNum();//获得列数，下标从1开始

        for (int i = 1; i <= lastRow; i++) { //从第二行开始写
            Map<String, String> rowMap = new HashMap<>();
            row = sheet.getRow(i);
            if (row != null) {
                for (int j = 0; j < lastCell; j++) {
                    if (ksh.equals(row.getCell(0).getStringCellValue())) {
                        if(entry.getKey().equals(titleRow.getCell(j).getStringCellValue())){
                            if(row!=null && row.getCell(j)!=null && row.getCell(j).getCellType()!=null){
                                if (row.getCell(j).getCellType().toString().equals(CellType.STRING.toString())) {
                                    log.info(entry.getKey()+"匹配上了，原值"+row.getCell(j).getStringCellValue()+"  新值"+entry.getValue());
                                } else if (row.getCell(j).getCellType().toString().equals(CellType.NUMERIC.toString())) {
                                    log.info(entry.getKey()+"匹配上了，原值"+row.getCell(j).getNumericCellValue()+"  新值"+entry.getValue());
                                }
                                row.getCell(j).setCellValue(entry.getValue());
                            }
                        }
                    }
                }
            }
        }

        try (OutputStream fileOut = new FileOutputStream(path)) {  //获取文件流
            workbook.write(fileOut);   //将workbook写入文件流
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static List<Map<String, String>> readExcel(String path, String sheetName) throws IOException {
        List<Map<String, String>> resList = new ArrayList<>();
        //创建输入流
        FileInputStream fileInputStream = new FileInputStream(path);
        //获得文档
        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
        //根据name获取sheet表
        XSSFSheet sheet = workbook.getSheet(sheetName);

        int lastRow = sheet.getLastRowNum();//获得行数，下标从0开始
        XSSFRow titleRow = sheet.getRow(0);//获取第一行（第一行一般是标题）
        XSSFRow row = sheet.getRow(1);//获取第二行（第一行一般是标题）
        int lastCell = row.getLastCellNum();//获得列数，下标从1开始

        for (int i = 1; i <= lastRow; i++) { //从第二行开始读
            Map<String, String> rowMap = new HashMap<>();
            row = sheet.getRow(i);
            if (row != null) {
                for (int j = 0; j < lastCell; j++) {
                    if(row.getCell(j)!=null && row.getCell(j).getCellType()!=null){
                        if (row.getCell(j).getCellType().toString().equals(CellType.STRING.toString())) {
                            rowMap.put(titleRow.getCell(j).getStringCellValue(), row.getCell(j).getStringCellValue());
                        } else if (row.getCell(j).getCellType().toString().equals(CellType.NUMERIC.toString())) {
                            rowMap.put(titleRow.getCell(j).getStringCellValue(), numberFormat.format(row.getCell(j).getNumericCellValue()));
                        }
                    }
                }
                resList.add(rowMap);
            }
        }
        return resList;
    }
}


