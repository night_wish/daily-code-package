package com.lee.poi.renyaling;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/10/11 11:34
 */
public class ReadAndCover20221011 {

    private static final String sourcePath = "C:\\Users\\le'e\\Desktop\\1\\source.xlsx";

    private static final String sourceSheetName = "Sheet1";

    private static final String destPath = "C:\\Users\\le'e\\Desktop\\1\\zhuan2.xlsx";

    private static final String destSheetName = "教育学";

    public static void main(String[] args) throws IOException {
        List<Map<String, String>> dataSource = readExcel(sourcePath, sourceSheetName);
        System.out.println(dataSource.size());
        for (Map<String, String> map : dataSource) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                writeExcel(destPath, destSheetName, entry.getKey(),entry.getValue());
                //System.out.println(entry.getKey()+"-----"+entry.getValue());
            }
        }
    }

    //判5改9
    public static void writeExcel(String path, String sheetName, String key,String value) throws IOException {
        List<Map<String, String>> resList = new ArrayList<>();
        //创建输入流
        FileInputStream fileInputStream = new FileInputStream(path);
        //获得文档
        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
        //根据name获取sheet表
        XSSFSheet sheet = workbook.getSheet(sheetName);

        int lastRow = sheet.getLastRowNum();//获得行数，下标从0开始
        XSSFRow titleRow = sheet.getRow(1);//获取第一行（第一行一般是标题）
        XSSFRow row = sheet.getRow(2);//获取第二行（第一行一般是标题）
        int lastCell = row.getLastCellNum();//获得列数，下标从1开始
        for (int i = 1; i <= lastRow; i++) { //从第二行开始写
            row = sheet.getRow(i);
            if (row != null) {
                String[] split = key.split(":");
                System.out.println("源："+split[0]+"   "+split[1]);
                System.out.println("目标："+row.getCell(5).getStringCellValue().trim()+"   "+row.getCell(3).getStringCellValue().trim());

                if(split[0].equals(row.getCell(5).getStringCellValue().trim()) && split[1].equals(row.getCell(3).getStringCellValue().trim())){
                    row.getCell(7).setCellValue(value);
                    break;
                }
            }
        }


        try (OutputStream fileOut = new FileOutputStream(path)) {  //获取文件流
            workbook.write(fileOut);   //将workbook写入文件流
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static List<Map<String, String>> readExcel(String path, String sheetName) throws IOException {
        List<Map<String, String>> resList = new ArrayList<>();
        //创建输入流
        FileInputStream fileInputStream = new FileInputStream(path);
        //获得文档
        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
        //根据name获取sheet表
        XSSFSheet sheet = workbook.getSheet(sheetName);

        int lastRow = sheet.getLastRowNum();//获得行数，下标从0开始
        XSSFRow titleRow = sheet.getRow(1);//获取第一行（第一行一般是标题）
        XSSFRow row = sheet.getRow(2);//获取第二行（第一行一般是标题）
        int lastCell = row.getLastCellNum();//获得列数，下标从1开始

        for (int i = 1; i <= lastRow; i++) { //从第二行开始读
            Map<String, String> rowMap = new HashMap<>();
            row = sheet.getRow(i);
            if (row != null) {
                rowMap.put(row.getCell(1).getStringCellValue().trim()+":"+row.getCell(4).getStringCellValue().trim(),row.getCell(6).getStringCellValue());
                resList.add(rowMap);
            }
        }
        return resList;
    }

}
