package com.lee.poi;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/4/2 9:44
 */
@Slf4j
public class InportJsonToExcel {

    public static void main(String[] args) throws IOException {

        File file = ResourceUtils.getFile("classpath:menu/rule/规则上报.json");
        String str = readJsonFile(file);
        JSONArray jsonArray = JSONObject.parseArray(str);
        List<Menu> menuList = new ArrayList<>();
        Menu menu = null;
        if(jsonArray!=null && jsonArray.size()>0){
            for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                menu = new Menu();
                menu.setResuCodg((String) jsonObj.get("resuCodg"));
                menu.setResuName((String) jsonObj.get("resuName"));
                menu.setResuType((String) jsonObj.get("resuType"));
                menu.setDscr((String) jsonObj.get("dscr"));
                menuList.add(menu);
            }
            log.info("size:{}",menuList.size());
        }

        List<List<String>> ret = new ArrayList<>();
        for (int i = 0; i < menuList.size(); i++) {
            List<String> obj2List = menuList.get(i).convertObj2List();
            ret.add(obj2List);
        }
        WriteExcelUtils.createWorkBook("规则引擎调度", Arrays.asList(new String[]{"功能编码","功能名称","功能类型","功能描述"}),ret,"C:\\Users\\le'e\\Desktop\\1.xlsx");

    }


    public static String readJsonFile(File jsonFile) {
        String jsonStr = "";
        log.info("开始读取" + jsonFile.getPath() + "文件");
        try {
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            log.info("读取" + jsonFile.getPath() + "文件结束!");
            return jsonStr;
        } catch (Exception e) {
            log.info("读取" + jsonFile.getPath() + "文件出现异常，读取失败!");
            e.printStackTrace();
            return null;
        }
    }

}
