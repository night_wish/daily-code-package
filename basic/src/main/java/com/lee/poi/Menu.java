package com.lee.poi;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/4/2 10:13
 */
@Data
public class Menu {

    private String resuCodg;

    private String resuName;

    private String resuType;

    private String dscr;

    public List<String> convertObj2List() {
        List<String> ret = new ArrayList<>();
        ret.add(this.resuCodg + "");
        ret.add(this.resuName);
        ret.add(this.resuType + "");
        ret.add(this.dscr);
        return ret;
    }
}
