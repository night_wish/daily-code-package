package com.lee.poi;

import com.lee.poi.entity.DocDTO;
import com.lee.poi.entity.KngBaseRpupCDTO;
import com.lee.poi.utils.ApiModelProperty;
import com.lee.poi.utils.Size;
import lombok.Data;
import lombok.ToString;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @version 1.0
 * @description 实体类字段反射
 * @date 2022/9/22 17:26
 */
public class EntityReflectFieldUtils {

    //不需要转的字段
    private static Map<String, String> filterMap = new HashMap<>();

    public static void main(String[] args) {
        List<RowObj> rowObjList = getDeclaredFieldsInfo(DocDTO.class);
        for (RowObj row : rowObjList){
//            System.out.println("'"+row.getEngName()+"'"+":"+"' '");
            System.out.println(row.getEngName()+":"+row.getChiName());
        }

    }

    //获取实体的注释
    public static List<RowObj> getDeclaredFieldsInfo(Class<?> clazz){
        List<RowObj> rowObjList = new ArrayList<>();
        Field[] fields=clazz.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            if(!filterMap.containsValue(fields[i].getName())) {
                boolean isApiExists = fields[i].isAnnotationPresent(ApiModelProperty.class);
                if (isApiExists) {
                    RowObj rowObj = new RowObj();
                    rowObj.setRowNo(Integer.toString(i));//行号
                    // 获取注解字段名称
                    String name = fields[i].getAnnotation(ApiModelProperty.class).value();
                    rowObj.setChiName(name.trim());//中文名
                    rowObj.setEngName(fields[i].getName());//英文名
                    rowObj.setType(fields[i].getType().getSimpleName().trim());//类型
                    boolean isSizeExists = fields[i].isAnnotationPresent(Size.class);
                    if(isSizeExists){
                        int size = fields[i].getAnnotation(Size.class).max();
                        rowObj.setRange(Integer.toString(size));//值范围
                    }
                    rowObj.setIsMust("否");
                    rowObj.setDescr("");
                    rowObjList.add(rowObj);
                }
            }
        }
        return rowObjList;
    }
}