package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 单据疑点明细信息表
 * </p>
 *
 * @author yehailong
 * @since 2023-05-05
 */
@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
public class DocSusptDetlDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "单据疑点明细ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String docSusptDetlId;

    @ApiModelProperty(value = "数据唯一记录号")
    @Size(max=40,message="不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "单据疑点ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String docSusptId;

    @ApiModelProperty(value = "单据ID")
    @Size(max=90,message="不能超过最大长度：90")
    private String docId;

    @ApiModelProperty(value = "任务ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String taskId;

    @ApiModelProperty(value = "统筹区编号")
    @Size(max=6,message="不能超过最大长度：6")
    private String poolareaNo;

    @ApiModelProperty(value = "医保区划")
    @Size(max=6,message="不能超过最大长度：6")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    @Size(max=100,message="不能超过最大长度：100")
    private String admdvsName;

    @ApiModelProperty(value = "涉及数据类型")
    @Size(max=3,message="不能超过最大长度：3")
    private String relDatatype;

    @ApiModelProperty(value = "涉及明细数据ID")
    @Size(max=100,message="不能超过最大长度：100")
    private String relDetlDataId;

    @ApiModelProperty(value = "确认违规金额")
    private BigDecimal cnfmVolaAmt;

    @ApiModelProperty(value = "违规扣款金额")
    private BigDecimal volaDetAmt;

    @ApiModelProperty(value = "审核删除标志")
    @Size(max=3,message="不能超过最大长度：3")
    private String chkDelFlag;

    @ApiModelProperty(value = "费用明细ID")
    @Size(max=60,message="不能超过最大长度：60")
    private String feedetlId;

    @ApiModelProperty(value = "结算ID")
    @Size(max=30,message="不能超过最大长度：30")
    private String setlId;

    @ApiModelProperty(value = "医保目录编码")
    @Size(max=50,message="不能超过最大长度：50")
    private String hilistCode;

    @ApiModelProperty(value = "医保目录名称")
    @Size(max=200,message="不能超过最大长度：200")
    private String hilistName;

    @ApiModelProperty(value = "医保目录等级")
    @Size(max=3,message="不能超过最大长度：3")
    private String hilistLv;

    @ApiModelProperty(value = "医疗收费项目类别")
    @Size(max=6,message="不能超过最大长度：6")
    private String medChrgitmType;

    @ApiModelProperty(value = "清算类别")
    @Size(max=6,message="不能超过最大长度：6")
    private String clrType;

    @ApiModelProperty(value = "医保目录剂型")
    @Size(max=50,message="不能超过最大长度：50")
    private String hilistDosform;

    @ApiModelProperty(value = "医药机构目录编码")
    @Size(max=150,message="不能超过最大长度：150")
    private String medinsListCodg;

    @ApiModelProperty(value = "医药机构目录名称")
    @Size(max=100,message="不能超过最大长度：100")
    private String medinsListName;

    @ApiModelProperty(value = "费用发生时间")
    private Date feeOcurTime;

    @ApiModelProperty(value = "数量")
    private BigDecimal cnt;

    @ApiModelProperty(value = "单价")
    private BigDecimal pric;

    @ApiModelProperty(value = "总金额")
    private BigDecimal sumamt;

    @ApiModelProperty(value = "规格")
    @Size(max=500,message="不能超过最大长度：500")
    private String spec;

    @ApiModelProperty(value = "规格计量单位")
    @Size(max=100,message="不能超过最大长度：100")
    private String specUnt;

    @ApiModelProperty(value = "医嘱医师编码")
    @Size(max=30,message="不能超过最大长度：30")
    private String drordDrCodg;

    @ApiModelProperty(value = "医嘱医师姓名")
    @Size(max=50,message="不能超过最大长度：50")
    private String drordDrName;

    @ApiModelProperty(value = "就诊号")
    @Size(max=30,message="不能超过最大长度：30")
    private String mdtrtsn;

    @ApiModelProperty(value = "创建人ID")
    @Size(max=50,message="不能超过最大长度：50")
    private String crterId;

    @ApiModelProperty(value = "创建人姓名")
    @Size(max=50,message="不能超过最大长度：50")
    private String crterName;

    @ApiModelProperty(value = "数据创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "创建机构编号")
    @Size(max=50,message="不能超过最大长度：50")
    private String crteOptinsNo;

    @ApiModelProperty(value = "经办人ID")
    @Size(max=60,message="不能超过最大长度：60")
    private String opterId;

    @ApiModelProperty(value = "经办人姓名")
    @Size(max=50,message="不能超过最大长度：50")
    private String opterName;

    @ApiModelProperty(value = "经办时间")
    private Date optTime;

    @ApiModelProperty(value = "经办机构编号")
    @Size(max=50,message="不能超过最大长度：50")
    private String optinsNo;

    @ApiModelProperty(value = "数据更新时间")
    private Date updtTime;

    @ApiModelProperty(value = "清算方式")
    @Size(max=6,message="不能超过最大长度：6")
    private String clrWay;

    @ApiModelProperty(value = "规则id")
    @Size(max=40,message="不能超过最大长度：40")
    private String ruleId;

    @ApiModelProperty(value = "规则名称")
    @Size(max=64,message="不能超过最大长度：64")
    private String ruleName;

    @ApiModelProperty(value = "机构id")
    @Size(max=40,message="不能超过最大长度：40")
    private String medinsId;

    @ApiModelProperty(value = "机构名称")
    @Size(max=32,message="不能超过最大长度：32")
    private String medinsName;

    @ApiModelProperty(value = "模块编码")
    @Size(max=6,message="不能超过最大长度：6")
    private String moduCode;

    @ApiModelProperty(value = "审核月份")
    @Size(max=16,message="不能超过最大长度：16")
    private String auditMonth;

    @ApiModelProperty(value = "违规项目类型 1-违规项 2-涉及项")
    @Size(max=3,message="不能超过最大长度：3")
    private String volaItemType;

    @ApiModelProperty(value = "违规金额")
    private BigDecimal volaAmt;

    @ApiModelProperty(value = "标记：0-待标记1-标记下发2-标记不下发")
    @Size(max=3,message="不能超过最大长度：3")
    private String markIsuFlag;

    @ApiModelProperty(value = "标记人ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String markerId;

    @ApiModelProperty(value = "标记人名称")
    @Size(max=32,message="不能超过最大长度：32")
    private String markerName;

    @ApiModelProperty(value = "标记时间")
    private Date markTime;

    @ApiModelProperty(value = "下发标识：0-未下发 1-已下发")
    @Size(max=3,message="不能超过最大长度：3")
    private String isuFlag;

    @ApiModelProperty(value = "下发时间")
    private Date isuTime;

    @ApiModelProperty(value = "下发人ID")
    @Size(max=32,message="不能超过最大长度：32")
    private String isuerId;

    @ApiModelProperty(value = "下发人名称")
    @Size(max=32,message="不能超过最大长度：32")
    private String isuerName;

    @ApiModelProperty(value = "机构反馈理由")
    @Size(max=500,message="不能超过最大长度：500")
    private String orgFbckRea;

    @ApiModelProperty(value = "机构反馈结果 1-认扣 2-不认扣")
    @Size(max=3,message="不能超过最大长度：3")
    private String orgFbckStas;

    @ApiModelProperty(value = "机构反馈人ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String orgFbckerId;

    @ApiModelProperty(value = "机构反馈人名称")
    @Size(max=16,message="不能超过最大长度：16")
    private String orgFbckerName;

    @ApiModelProperty(value = "机构处理时间")
    private Date orgDspoTime;

    @ApiModelProperty(value = "机构反馈时间（提交）")
    private Date orgFbckTime;

    @ApiModelProperty(value = "机构二次反馈理由")
    private String rorgFbckRea;

    @ApiModelProperty(value = "机构二次反馈结果 0-待反馈 1-认扣 2-不认扣")
    private String rorgFbckStas;

    @ApiModelProperty(value = "机构二次反馈人ID")
    private String rorgFbckerId;

    @ApiModelProperty(value = "机构二次反馈人名称")
    private String rorgFbckerName;

    @ApiModelProperty(value = "机构二次反馈处理时间")
    private Date rorgDspoTime;

    @ApiModelProperty(value = "机构二次反馈时间(提交)")
    private Date rorgFbckTime;

    @ApiModelProperty(value = "申诉截止时间")
    private Date applDdlnTime;

    @ApiModelProperty(value = "初审处理人")
    @Size(max=40,message="不能超过最大长度：40")
    private String chkOpterId;

    @ApiModelProperty(value = "初审处理人名称")
    @Size(max=32,message="不能超过最大长度：32")
    private String chkOpterName;

    @ApiModelProperty(value = "初审驳回时间")
    private Date chkRejtTime;

    @ApiModelProperty(value = "初审驳回原因")
    @Size(max=3,message="不能超过最大长度：3")
    private String chkRejtRea;

    @ApiModelProperty(value = "初审驳回说明")
    @Size(max=255,message="不能超过最大长度：3")
    private String chkRejtDscr;

    @ApiModelProperty(value = "初审确认时间")
    private Date chkCfmTime;

    @ApiModelProperty(value = "初审调整方法 1-按百分比 2-按审核人决定")
    @Size(max=3,message="不能超过最大长度：3")
    private String chkAdjmMtd;

    @ApiModelProperty(value = "初审调整金额")
    private BigDecimal chkAdjmAmt;

    @ApiModelProperty(value = "初审调整说明")
    @Size(max=255,message="不能超过最大长度：255")
    private String chkAdjmDscr;

    @ApiModelProperty(value = "初审提交时间")
    private Date chkSubmitTime;

    @ApiModelProperty(value = "复审处理人")
    @Size(max=40,message="不能超过最大长度：40")
    private String rchkOpterId;

    @ApiModelProperty(value = "复审处理人名称")
    @Size(max=32,message="不能超过最大长度：32")
    private String rchkOpterName;

    @ApiModelProperty(value = "复审驳回时间")
    private Date rchkRejtTime;

    @ApiModelProperty(value = "复审驳回原因")
    @Size(max=3,message="不能超过最大长度：3")
    private String rchkRejtRea;

    @ApiModelProperty(value = "复审驳回说明")
    @Size(max=255,message="不能超过最大长度：255")
    private String rchkRejtDscr;

    @ApiModelProperty(value = "复审确认时间")
    private Date rchkCfmTime;

    @ApiModelProperty(value = "复审提交时间")
    private Date rchkSubmitTime;

    @ApiModelProperty(value = "复审调整方法 1-按百分比 2-按审核人决定")
    @Size(max=255,message="不能超过最大长度：255")
    private String rchkAdjmMtd;

    @ApiModelProperty(value = "复审调整金额")
    private BigDecimal rchkAdjmAmt;

    @ApiModelProperty(value = "复审调整金额")
    @Size(max=255,message="不能超过最大长度：255")
    private String rchkAdjmDscr;

    @ApiModelProperty(value = "明细处理状态：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    @Size(max=3,message="不能超过最大长度：3")
    private String susptDetlStas;

    @ApiModelProperty(value = "先行自付金额")
    private BigDecimal preselfpayAmt;

    @ApiModelProperty(value = "符合范围金额")
    private BigDecimal inscpAmt;

    @ApiModelProperty(value = "主院机构编码")
    private String mainMedinsId;

    @ApiModelProperty(value = "主院机构名称")
    private String mainMedinsName;

    @ApiModelProperty(value = "实际数量")
    private Double realityCnt;

    @ApiModelProperty(value = "实际交易费用")
    private BigDecimal realityAmt;

    @ApiModelProperty(value = "违规内容")
    private String volaCont;

    @ApiModelProperty(value = "明细处理状态：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    @Size(max=3,message="不能超过最大长度：3")
    private List<String> oldSusptDetlStasList;

    @ApiModelProperty(value = "疑点明细IDs")
    private List<String> docSusptDetlIdList;

    @ApiModelProperty(value = "医疗机构IDs")
    private List<String> medinsIdList;

    @ApiModelProperty(value = "当前节点编码")
    private String crtNodeCode;

    @ApiModelProperty(value = "明细处理状态：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    @Size(max=3,message="不能超过最大长度：3")
    private String oldSusptDetlStas;

    @ApiModelProperty(value = "主院机构编码s")
    private List<String> mainMedinsIdList;

}
