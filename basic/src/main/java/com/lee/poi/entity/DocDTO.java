package com.lee.poi.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/9/22 17:45
 */
@Data
public class DocDTO extends AbstractImsDTO {

    @ApiModelProperty(value = "数据唯一记录号")
    private String rid;

    @ApiModelProperty(value = "单据ID")
    private String docId;

    @ApiModelProperty(value = "任务ID")
    private String taskId;

    @ApiModelProperty(value = "任务ID集合")
    private List<String> taskIds;

    @ApiModelProperty(value = "就诊号")
    private String mdtrtsn;

    @ApiModelProperty(value = "当前环节代码")
    private String crtNodeCode;

    @ApiModelProperty(value = "违规标志")
    private String volaFlag;

    @ApiModelProperty(value = "单据状态")
    private String docStas;

    @ApiModelProperty(value = "疑点状态")
    private String susptStas;

    @ApiModelProperty(value = "疑点状态列表")
    private List<String> susptStasList;

    @ApiModelProperty(value = "更新疑点状态")
    private String susptStasNew;

    @ApiModelProperty(value = "统筹区编号")
    private String poolareaNo;

    @ApiModelProperty(value = "统筹区名称")
    private String poolareaName;

    @ApiModelProperty(value = "医保区划")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    private String admdvsName;

    @ApiModelProperty(value = "医保区划集合")
    private List<String> admdvsList;

    @ApiModelProperty(value = "省医保区划")
    private String provAdmdvs;

    @ApiModelProperty(value = "省医保区划名称")
    private String provAdmdvsName;

    @ApiModelProperty(value = "医疗机构编码")
    private String medinsCode;

    @ApiModelProperty(value = "医疗机构ID")
    private String medinsId;

    @ApiModelProperty(value = "医疗机构ID集合")
    private List<String> medinsIdList;

    @ApiModelProperty(value = "零售药店ID")
    private String rtalPhacId;

    @ApiModelProperty(value = "零售药店ID列表")
    private List<String> rtalPhacIdList;

    @ApiModelProperty(value = "任务ID集合")
    private String medinsName;

    @ApiModelProperty(value = "医疗机构等级")
    private String medinsLv;

    @ApiModelProperty(value = "医疗服务机构类型")
    private String medinsType;

    @ApiModelProperty(value = "医疗类别")
    private String medType;

    @ApiModelProperty(value = "患者ID")
    private String patnId;

    @ApiModelProperty(value = "患者姓名")
    private String patnName;

    @ApiModelProperty(value = "性别")
    private String gend;

    @ApiModelProperty(value = "患者证件类型")
    private String patnCertType;

    @ApiModelProperty(value = "患者身份证件")
    private String patnIdcard;

    @ApiModelProperty(value = "患者社保卡号")
    private String patnSscno;

    @ApiModelProperty(value = "入院时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date admTime;

    @ApiModelProperty(value = "出院时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date dscgTime;

    @ApiModelProperty(value = "结算时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date setlTime;

    @ApiModelProperty(value = "入院疾病诊断编码")
    private String admDisediagCodg;

    @ApiModelProperty(value = "入院疾病诊断名称")
    private String admDisediagName;

    @ApiModelProperty(value = "出院主要疾病诊断编码")
    private String dscgMainDisediagCodg;

    @ApiModelProperty(value = "出院主要疾病诊断名称")
    private String dscgMainDisediagName;

    @ApiModelProperty(value = "出院科室编码")
    private String dscgDeptCodg;

    @ApiModelProperty(value = "出院科室名称")
    private String dscgDeptName;

    @ApiModelProperty(value = "医师代码")
    private String drCode;

    @ApiModelProperty(value = "医师姓名")
    private String drName;

    @ApiModelProperty(value = "医师证件类型")
    private String drCertType;

    @ApiModelProperty(value = "医师身份证件")
    private String drIdcard;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "医疗费总额")
    private BigDecimal medfeeSumamt;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "审核进度")
    private BigDecimal chkPrgs;

    @ApiModelProperty(value = "有效标志")
    private String valiFlag;

    @ApiModelProperty(value = "审核进度")
    private String chkProgress;

    @ApiModelProperty(value = "创建人ID")
    private String crterId;

    @ApiModelProperty(value = "创建人姓名")
    private String crterName;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date crteTime;

    @ApiModelProperty(value = "创建机构编号")
    private String crteOptinsNo;

    @ApiModelProperty(value = "经办人ID")
    private String opterId;

    @ApiModelProperty(value = "经办人姓名")
    private String opterName;

    @ApiModelProperty(value = "经办时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date optTime;

    @ApiModelProperty(value = "经办机构编号")
    private String optinsNo;

    @ApiModelProperty(value = "开始出院时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date dscgTimeStart;

    @ApiModelProperty(value = "结束出院时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date dscgTimeEnd;

    @ApiModelProperty(value = "开始结算时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date setlTimeStart;

    @ApiModelProperty(value = "结束结算时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date setlTimeEnd;

    @ApiModelProperty(value = "开始经办时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date optTimeStart;

    @ApiModelProperty(value = "结束经办时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date optTimeEnd;

    @ApiModelProperty(value = "规则id")
    private String ruleId;

    @ApiModelProperty(value = "规则id集合")
    private List<String> ruleIdList;

    @ApiModelProperty(value = "单据id集合")
    private List<String> docIds;

    @ApiModelProperty(value = "申诉期限")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date applDdln;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "初审违规金额")
    private BigDecimal chkVolaAmt;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "确认违规金额")
    private BigDecimal cnfmVolaAmt;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "违规扣款金额")
    private BigDecimal volaDetAmt;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "申诉补差金额")
    private BigDecimal appyRvisAmt;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "补差金额")
    private BigDecimal rvisAmt;

    @ApiModelProperty(value = "补差时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date rvisTime;

    @ApiModelProperty(value = "审核人")
    private String chkEr;

    @ApiModelProperty(value = "审核违规标志")
    private String chkVolaFlag;

    @ApiModelProperty(value = "申诉违规标志")
    private String applVolaFlag;

    @ApiModelProperty(value = "复审违规标志")
    private String rchkVolaFlag;

    @ApiModelProperty(value = "合议违规标志")
    private String clglVolaFlag;

    @ApiModelProperty(value = "合议结果")
    private String clglRslt;

    @ApiModelProperty(value = "扣款标志")
    private String detFlag;

    @ApiModelProperty(value = "扣款时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date detTime;

    @ApiModelProperty(value = "补差标志")
    private String rvisFlag;

    @ApiModelProperty(value = "标志")
    private String flag;

    @ApiModelProperty(value = "天数")
    private Integer days;

    @ApiModelProperty(value = "疑点添加方式")
    private String susptAddWay;

    @ApiModelProperty(value = "复核意见")
    private String rchkOpnn;

    @ApiModelProperty(value = "审核标志集合")
    private List<String> auditFlag;

    @ApiModelProperty(value = "单据疑点ID")
    private String docSusptId;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "总金额")
    private BigDecimal sumamt;

    @ApiModelProperty(value = "工作流流程ID")
    private String wflwPrcsId;

    @ApiModelProperty(value = "工作流任务ID")
    private String wflwTaskId;

    @ApiModelProperty(value = "进入初审标志")
    private String inpoolChkFlag;

    @ApiModelProperty(value = "任务类型")
    private String taskType;

    @ApiModelProperty(value = "服务事项环节实例ID")
    private String servMattNodeInstId;

    @ApiModelProperty(value = "事件实例ID")
    private String evtInstId;

    @ApiModelProperty(value = "服务事项实例ID")
    private String servMattInstId;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;
    private String chgType;
    protected String volaBhvrType;
    private String docSusptDetlId;

    @ApiModelProperty(value = "审核扣款ID")
    private String chkDetId;

    @ApiModelProperty(value = "审核扣款ID列表")
    private List<String> chkDetIds;

    @ApiModelProperty(value = "医疗机构等级")
    private String medinsLvList;

    @ApiModelProperty(value = "就诊ID")
    private String mdtrtId;

    @ApiModelProperty(value = "疑点ID列表")
    private List<String> susptIds;

    @ApiModelProperty(value = "审核超时标志")
    private String chkOtFlag;
    @ApiModelProperty(value = "结算发起扣款标识")
    private String callFeeFlag;
    @ApiModelProperty(value = "实际扣款金额")
    private BigDecimal actDetAmt;
    @ApiModelProperty(value = "复审回退原因")
    private String rchkRetnRea;

    @ApiModelProperty(value = "是否上传附件 1-是 0-否")
    private String upldAttFlag;

    //补支补扣条件 ENUM : ClrFlag
    @ApiModelProperty(value = "清算标记 0-未清算 1-已清算")
    private String clrFlag;

    //补支补扣上报条件
    @ApiModelProperty(value = "提交清算标识：0-未提交、1已提交")
    private String rpupClrFlag;

    //补支补扣条件 ENUM : RvisDealStas
    @ApiModelProperty(value = "补支补扣处理状态 0-未处理 1-已补支 2-已补扣")
    private List<String> rvisDealStasList;
}
