package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * <p>
 * 单据机构统计信息表
 * </p>
 *
 * @author Lee
 * @since 2023-05-05
 */
@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
public class DocOrgSttDQueryDTO extends AbstractImsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "单据机构统计信息ID")
    private String docOrgSttId;

    @ApiModelProperty(value = "唯一ID")
    private String rid;

    @ApiModelProperty(value = "模块编码")
    private String moduCode;

    @ApiModelProperty(value = "审核月份")
    private String auditMonth;

    @ApiModelProperty(value = "任务下发时间")
    private Date isuTime;

    @ApiModelProperty(value = "医保区划")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    private String admdvsName;

    @ApiModelProperty(value = "机构ID")
    private String medinsId;

    @ApiModelProperty(value = "机构名称")
    private String medinsName;

    @ApiModelProperty(value = "触发规则数")
    private Integer checkoutRuletime;

    @ApiModelProperty(value = "疑似违规人次")
    private Integer checkoutPsntime;

    @ApiModelProperty(value = "疑似违规金额")
    private BigDecimal checkoutAmt;

    @ApiModelProperty(value = "当前环节代码：1-机构反馈 2-初审 3-机构二次反馈 4-复审 5-关闭")
    private String crtNodeCode;

    @ApiModelProperty(value = "机构反馈认扣数量")
    private Integer orgAgreDetNum;

    @ApiModelProperty(value = "机构反馈不认扣数量")
    private Integer orgRejectDetNum;

    @ApiModelProperty(value = "机构反馈人ID")
    private String orgFbckerId;

    @ApiModelProperty(value = "机构反馈时间（提交时间）")
    private Date orgFbckTime;

    @ApiModelProperty(value = "机构反馈人名称")
    private String orgFbckerName;

    @ApiModelProperty(value = "机构处理时间")
    private Date orgDspoTime;

    @ApiModelProperty(value = "反馈截止时间")
    private Date applDdlnTime;

    @ApiModelProperty(value = "初审处理人")
    private String chkOpterId;

    @ApiModelProperty(value = "初审处理人名称")
    private String chkOpterName;

    @ApiModelProperty(value = "初审驳回时间")
    private Date chkRejtTime;

    @ApiModelProperty(value = "初审确认时间")
    private Date chkCfmTime;

    @ApiModelProperty(value = "初审提交时间")
    private Date chkSubmitTime;

    @ApiModelProperty(value = "初审调整金额")
    private BigDecimal chkAdjmAmt;

    @ApiModelProperty(value = "机构二次反馈认扣数量")
    private Integer rorgAgreDetNum;

    @ApiModelProperty(value = "机构二次反馈不认扣数量")
    private Integer rorgRejectDetNum;

    @ApiModelProperty(value = "机构二次反馈人ID")
    private String rorgFbckerId;

    @ApiModelProperty(value = "机构二次反馈时间（提交时间）")
    private Date rorgFbckTime;

    @ApiModelProperty(value = "机构二次反馈人名称")
    private String rorgFbckerName;

    @ApiModelProperty(value = "机构二次反馈处理时间")
    private Date rorgDspoTime;

    @ApiModelProperty(value = "复审处理人")
    private String rchkOpterId;

    @ApiModelProperty(value = "复审处理人名称")
    private String rchkOpterName;

    @ApiModelProperty(value = "复审驳回时间")
    private Date rchkRejtTime;

    @ApiModelProperty(value = "复审确认时间")
    private Date rchkCfmTime;

    @ApiModelProperty(value = "复审提交时间")
    private Date rchkSubmitTime;

    @ApiModelProperty(value = "复审调整金额")
    private BigDecimal rchkAdjmAmt;

    @ApiModelProperty(value = "机构处理状态：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    private String orgDspoStas;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;

    @ApiModelProperty(value = "机构处理状态s：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    private List<String> orgDspoStasList;

    @ApiModelProperty(value = "医保区划模糊查询")
    private String admdvsLike;

    @ApiModelProperty(value = "医疗机构IDs,医疗机构的数据权限使用")
    private List<String> medinsIdList;

    @ApiModelProperty(value = "医疗机构IDs,页面筛选条件使用")
    private List<String> searchMedinsIdList;

    @ApiModelProperty(value = "医保区划s")
    private List<String> admdvsList;


    @ApiModelProperty(value = "是否超过申诉截止时间")
    private Date ifOutApplDdlnTime;

    @ApiModelProperty(value = "审核月份")
    private List<String> auditMonthList;

    @ApiModelProperty(value = "当前环节代码：1-机构反馈 2-初审 3-机构二次反馈 4-复审 5-关闭")
    private List<String> crtNodeCodeList;

    @ApiModelProperty(value = "上海市市中心 医疗机构ID")
    private List<String> centerAdmdvsMedinsIdList;

    @ApiModelProperty("页面查询条件标记")
    private String flag;

}
