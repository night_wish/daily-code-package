package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.Map;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/9/14 10:00
 */
@Data
public class SysUactDTO {
       @ApiModelProperty("RID")
       private String rid;
       @ApiModelProperty("用户帐号ID")
       private String uactId;
       @ApiModelProperty("经办人编号")
       private String opterNo;
       @ApiModelProperty("密码")
       private String pwd;
       @ApiModelProperty("盐")
       private String salt;
       @ApiModelProperty("说明")
       private String dscr;
       @ApiModelProperty("用户帐号")
       private String uact;
       @ApiModelProperty("密码最后一次修改时间")
       private Date pwdLastModiTime;
       @ApiModelProperty("帐号状态")
       private String uactStas;
       @ApiModelProperty("帐号锁定原因")
       private String uactLckRea;
       @ApiModelProperty("有效标志")
       private String valiFlag;
       @ApiModelProperty("初始密码修改标志")
       private String initPwdModiFlag;
       @ApiModelProperty("创建人")
       private String crterId;
       @ApiModelProperty("创建人姓名")
       private String crterName;
       @ApiModelProperty("创建时间")
       private Date crteTime;
       @ApiModelProperty("创建机构ID")
       private String crteOptinsId;
       @ApiModelProperty("创建机构")
       private String crteOptinsNo;
       @ApiModelProperty("经办人")
       private String opterId;
       @ApiModelProperty("经办人姓名")
       private String opterName;
       @ApiModelProperty("经办时间")
       private Date optTime;
       @ApiModelProperty("经办机构ID")
       private String optinsId;
       @ApiModelProperty("经办机构")
       private String optinsNo;
       @ApiModelProperty("统筹区")
       private String poolareaNo;
       @ApiModelProperty("姓名")
       private String userName;
       @ApiModelProperty("证件类型")
       private String certType;
       @ApiModelProperty("用户身份证号")
       private String certNO;
       @ApiModelProperty("邮箱")
       private String email;
       @ApiModelProperty("手机号码")
       private String mob;
       @ApiModelProperty("电话号码")
       private String tel;
       @ApiModelProperty("备注")
       private String memo;
       @ApiModelProperty("拓展信息")
       private Map<String, Object> userExtInfo;
       @ApiModelProperty("旧密码")
       private String oldPwd;
       @ApiModelProperty("新密码")
       private String newPwd;
       @ApiModelProperty("密码是否变化")
       private Boolean isPwdChanged;
       @ApiModelProperty("帐号是否变化")
       private Boolean isAcctChanged;
       @ApiModelProperty("组织机构是否变化")
       private Boolean isUnitChanged;
       @ApiModelProperty("组织单元ID")
       private String orguntId;
       @ApiModelProperty("组织机构名称")
       private String orgName;
       @ApiModelProperty("组织机构编码")
       private String orgCodg;
       @ApiModelProperty("业务角色ID")
       private String roleId;
       @ApiModelProperty("经办人员类型")
       private String opterType;
       @ApiModelProperty("用户id")
       private String userId;
       @ApiModelProperty("可见标志")
       private String visFlag;
       @ApiModelProperty("帐号开始时间")
       private Date uactBegntime;
       @ApiModelProperty("帐号结束时间")
       private Date uactEndtime;
       @ApiModelProperty("登录方式")
       private String lginWay;
       @ApiModelProperty("医保区划")
       private String admdvs;

}
