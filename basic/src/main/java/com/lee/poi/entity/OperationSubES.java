package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * @program: hsa-sims-acq
 * @see： cn.hsa.acq.esinit.model.OperationSubESDO
 * @description: [请填写]-实现类
 * @author: taominyao
 * @create: 2023-03-15 13:49
 **/
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class OperationSubES {

    @ApiModelProperty(value = "结算清单手术操作ID")
    @Id
    @Field(type = FieldType.Text, store = true)
    private String setlListOprnId;
    @ApiModelProperty(value = "结算ID")
    @Field(type = FieldType.Text, store = true)
    private String setlId;
    @ApiModelProperty(value = "人员编号")
    @Field(type = FieldType.Text, store = true)
    private String psnNo;
    @ApiModelProperty(value = "就诊ID")
    @Field(type = FieldType.Text, store = true)
    private String mdtrtId;
    @ApiModelProperty(value = "主手术操作标志")
    @Field(type = FieldType.Text, store = true)
    private String mainPprnFlag;
    @ApiModelProperty(value = "手术操作名称")
    @Field(type = FieldType.Text, store = true)
    private String oprnOprtName;
    @ApiModelProperty(value = "手术操作代码")
    @Field(type = FieldType.Text, store = true)
    private String oprnOprtCode;
    @ApiModelProperty(value = "手术操作日期")
    @Field(type = FieldType.Date, store = true)
    private Date oprnOprtDate;
    @ApiModelProperty(value = "麻醉方式")
    @Field(type = FieldType.Text, store = true)
    private String anstWay;
    @ApiModelProperty(value = "术者医师姓名")
    @Field(type = FieldType.Text, store = true)
    private String operDrName;
    @ApiModelProperty(value = "术者医师代码")
    @Field(type = FieldType.Text, store = true)
    private String operDrCode;
    @ApiModelProperty(value = "麻醉医师姓名")
    @Field(type = FieldType.Text, store = true)
    private String anstDrName;
    @ApiModelProperty(value = "麻醉医师代码")
    @Field(type = FieldType.Text, store = true)
    private String anstDrCode;
}
    
