package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 单据机构统计信息表
 * </p>
 *
 * @author Lee
 * @since 2023-05-05
 */
@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
public class DocOrgSttDDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "单据机构统计信息ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String docOrgSttId;

    @ApiModelProperty(value = "唯一ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "模块编码")
    @Size(max=6,message="不能超过最大长度：6")
    private String moduCode;

    @ApiModelProperty(value = "审核月份")
    @Size(max = 16, message = "不能超过最大长度：16")
    private String auditMonth;

    @ApiModelProperty(value = "任务下发时间")
    private Date isuTime;

    @ApiModelProperty(value = "医保区划")
    @Size(max = 6, message = "不能超过最大长度：6")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    @Size(max = 16, message = "不能超过最大长度：16")
    private String admdvsName;

    @ApiModelProperty(value = "机构ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String medinsId;

    @ApiModelProperty(value = "机构名称")
    @Size(max = 16, message = "不能超过最大长度：16")
    private String medinsName;

    @ApiModelProperty(value = "触发规则数")
    private Integer checkoutRuletime;

    @ApiModelProperty(value = "疑似违规人次")
    private Integer checkoutPsntime;

    @ApiModelProperty(value = "疑似违规金额")
    private BigDecimal checkoutAmt;

    @ApiModelProperty(value = "当前环节代码：1-机构反馈 2-初审 3-复审 4-关闭")
    @Size(max = 3, message = "不能超过最大长度：3")
    private String crtNodeCode;

    @ApiModelProperty(value = "机构反馈认扣数量")
    private Integer orgAgreDetNum;

    @ApiModelProperty(value = "机构反馈不认扣数量")
    private Integer orgRejectDetNum;

    @ApiModelProperty(value = "机构反馈人ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String orgFbckerId;

    @ApiModelProperty(value = "机构反馈时间（提交时间）")
    private Date orgFbckTime;

    @ApiModelProperty(value = "机构反馈人名称")
    @Size(max = 16, message = "不能超过最大长度：16")
    private String orgFbckerName;

    @ApiModelProperty(value = "机构处理时间")
    private Date orgDspoTime;

    @ApiModelProperty(value = "反馈截止时间")
    private Date applDdlnTime;

    @ApiModelProperty(value = "初审处理人")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String chkOpterId;

    @ApiModelProperty(value = "初审处理人名称")
    @Size(max = 32, message = "不能超过最大长度：32")
    private String chkOpterName;

    @ApiModelProperty(value = "初审驳回时间")
    private Date chkRejtTime;

    @ApiModelProperty(value = "初审确认时间")
    private Date chkCfmTime;

    @ApiModelProperty(value = "初审提交时间")
    private Date chkSubmitTime;

    @ApiModelProperty(value = "初审调整金额")
    private BigDecimal chkAdjmAmt;

    @ApiModelProperty(value = "机构二次反馈认扣数量")
    private Integer rorgAgreDetNum;

    @ApiModelProperty(value = "机构二次反馈不认扣数量")
    private Integer rorgRejectDetNum;

    @ApiModelProperty(value = "机构二次反馈人ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String rorgFbckerId;

    @ApiModelProperty(value = "机构二次反馈时间（提交时间）")
    private Date rorgFbckTime;

    @ApiModelProperty(value = "机构二次反馈人名称")
    @Size(max=16,message="不能超过最大长度：16")
    private String rorgFbckerName;

    @ApiModelProperty(value = "机构二次反馈处理时间")
    private Date rorgDspoTime;

    @ApiModelProperty(value = "复审处理人")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String rchkOpterId;

    @ApiModelProperty(value = "复审处理人名称")
    @Size(max = 32, message = "不能超过最大长度：32")
    private String rchkOpterName;

    @ApiModelProperty(value = "复审驳回时间")
    private Date rchkRejtTime;

    @ApiModelProperty(value = "复审确认时间")
    private Date rchkCfmTime;

    @ApiModelProperty(value = "复审提交时间")
    private Date rchkSubmitTime;

    @ApiModelProperty(value = "复审调整金额")
    private BigDecimal rchkAdjmAmt;

    @ApiModelProperty(value = "机构处理状态：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    @Size(max = 3, message = "不能超过最大长度：3")
    private String orgDspoStas;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;

    @ApiModelProperty(value = "当前节点名称：1-机构反馈 2-初审 3-复审 4-关闭")
    private String crtNodeName;

    @ApiModelProperty(value = "机构处理状态：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    private String orgDspoStasName;

    @ApiModelProperty(value = "检出疑似违规机构数")
    private Integer checkoutMedinsNumber;

    @ApiModelProperty(value = "机构处理状态：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    @Size(max = 3, message = "不能超过最大长度：3")
    private String oldOrgDspoStas;

    @ApiModelProperty(value = "机构处理状态：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    private List<String> oldOrgDspoStasList;

    @ApiModelProperty(value = "当前环节代码：1-机构反馈 2-初审 3-复审 4-关闭")
    @Size(max = 3, message = "不能超过最大长度：3")
    private String oldCrtNodeCode;

    @ApiModelProperty(value = "医疗机构ID")
    private List<String> medinsIdList;

    @ApiModelProperty(value = "机构处理状态数")
    private Integer orgDspoStasNumber;

    @ApiModelProperty(value = "医保区划查询权限")
    private String admdvsLike;

    @ApiModelProperty(value = "查询当前节点列表")
    private List<String> searchNodeCodeList;

    @ApiModelProperty(value = "扣减人次")
    private Long detPsnTimes;


    @ApiModelProperty(value = "医保区划列表--页面查询条件")
    private List<String> admdvsList;

    @ApiModelProperty(value = "医疗机构ID--页面查询条件")
    private List<String> searchMedinsIdList;

    @ApiModelProperty("审核批次名称")
    private String auditMonthName;


}
