package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/9/22 17:45
 */
@Data
public class AbstractImsDTO implements Serializable {
    private static final long serialVersionUID = -3924479238646073826L;

    @ApiModelProperty(value = "页码")
    protected Integer pageNumber;

    @ApiModelProperty(value = "页面数据大小")
    protected Integer pageSize;

    @ApiModelProperty(value = "排序字段")
    protected String orderColumn;

    @ApiModelProperty(value = "排序类型")
    protected String orderType;

    @ApiModelProperty(value = "当前用户ID")
    private String currentUserId;

    @ApiModelProperty(value = "当前用户名称")
    private String currentUserName;

    @ApiModelProperty(value = "当前用户所属机构ID")
    private String currentOrgId;

    @ApiModelProperty(value = "当前用户所属机构编码")
    private String currentOrgCodg;

    @ApiModelProperty(value = "当前用户所属机构名称")
    private String currentOrgName;

    @ApiModelProperty(value = "当前用户uactid")
    public String currentUactId;
}
