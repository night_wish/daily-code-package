package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import com.lee.poi.utils.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author Lee
 * @version 1.0
 * @description 知识分析统计DTO
 * @date 2022/6/17 13:23
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class KngAnalyzeDTO {

    //知识类型
    @ApiModelProperty(value = "知识类型")
    @Size(max=40,message="不能超过最大长度：40")
    private String kngBaseType;

    //知识类型名称
    @ApiModelProperty(value = "知识类型名称")
    @Size(max=40,message="不能超过最大长度：40")
    private String kngBaseTypeName;

    //类型数量
    @ApiModelProperty(value = "类型数量")
    @Size(max=11,message="不能超过最大长度：40")
    private String typeNum;

    //创建年月
    @ApiModelProperty(value = "创建年月")
    @Size(max=11,message="不能超过最大长度：40")
    private String crtMonth;

    //知识数量
    @ApiModelProperty(value = "知识数量")
    @Size(max=11,message="不能超过最大长度：40")
    private String kngNum;
}
