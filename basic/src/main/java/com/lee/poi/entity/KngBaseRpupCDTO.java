package com.lee.poi.entity;

import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 知识上报表
 * </p>
 *
 * @author Lee
 * @since 2021-07-12
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@ApiModel(value="KngBaseRpupCDTO", description="知识上报表")
public class KngBaseRpupCDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据唯一标识")
    @Size(max=40,message="不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "知识ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String kngBaseRid;

    @ApiModelProperty(value = "知识类型：1-医保管理类，2-临床诊断类，3-药品知识类，4-诊疗项目类，5-临床路径类 6-单病种类 7-病案校验类 8-DRG分组合理性 9-DIP分组合理性 10-手术操作类")
    private Integer kngBaseType;

    @ApiModelProperty(value = "子类型：1-1、医保管理类，2-1、诊断与性别对应关系，2-2、诊断与疾病对应关系，2-3、诊断与科室对应关系，3-1、药物过敏史，3-2、药物相互作用，3-3、禁忌症，3-4、副作用，3-5、注射剂体外配伍 4-1、诊疗项目类知识 5-1、临床路径 6-1、单病种类知识 7-1、单病种类知识 8-1、DRG分组合理性 9-1、DIP分组合理性 10-1、手术操作类")
    private Integer kngBaseChildType;

    @ApiModelProperty(value = "知识名称")
    @Size(max=128,message="不能超过最大长度：128")
    private String kngBaseName;

    @ApiModelProperty(value = "采集区域：1-省局，2-国家局")
    private Integer clctRegn;

    @ApiModelProperty(value = "审核人ID")
    @Size(max=45,message="不能超过最大长度：45")
    private String chkerId;

    @ApiModelProperty(value = "审核人姓名")
    @Size(max=64,message="不能超过最大长度：64")
    private String chkerName;

    @ApiModelProperty(value = "审核状态：1-待审核，2-审核中，3-审核通过，4-审核不通过")
    private Integer chkStas;

    @ApiModelProperty(value = "驳回原因")
    @Size(max=128,message="不能超过最大长度：128")
    private String rejtRea;

    @ApiModelProperty(value = "统筹区域编号")
    @Size(max=64,message="不能超过最大长度：64")
    private String poolareaNo;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;

    @ApiModelProperty(value = "医保区划")
    @Size(max=8,message="不能超过最大长度：64")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    @Size(max=32,message="不能超过最大长度：64")
    private String admdvsName;

    @ApiModelProperty(value = "创建人")
    @Size(max=40,message="不能超过最大长度：64")
    private String crterId;

    @ApiModelProperty(value = "创建人姓名")
    @Size(max=32,message="不能超过最大长度：64")
    private String crterName;

    @ApiModelProperty(value = "有效状态：1-启用，2-禁用")
    @Size(max=2,message="不能超过最大长度：64")
    private String valiStas;

}
