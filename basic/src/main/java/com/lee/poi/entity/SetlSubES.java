package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @program: hsa-sims-acq
 * @see： cn.hsa.acq.esinit.model.SetlSubESDO
 * @description: [结算信息]-ES映射对象
 * @author: taominyao
 * @create: 2023-03-15 13:40
 **/
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class SetlSubES {

    @ApiModelProperty(value = "结算ID")
    @Id
    @Field(type = FieldType.Text, store = true)
    private String setlId;
    @ApiModelProperty(value = "清算经办机构")
    @Field(type = FieldType.Text, store = true)
    private String clrOptins;
    @ApiModelProperty(value = "险种类型")
    @Field(type = FieldType.Text, store = true)
    private String insuType;
    @ApiModelProperty(value = "公务员标志")
    @Field(type = FieldType.Text, store = true)
    private String cvlservFlag;

    @ApiModelProperty(value = "公务员等级")
    @Field(type = FieldType.Text, store = true)
    private String cvlservLv;

    @ApiModelProperty(value = "特殊人员类型")
    @Field(type = FieldType.Text, store = true)
    private String spPsnType;

    @ApiModelProperty(value = "特殊人员类型等级")
    @Field(type = FieldType.Text, store = true)
    private String spPsnTypeLv;
    @ApiModelProperty(value = "结算时间")
    @Field(type = FieldType.Date, store = true)
    private Date setlTime;
    @ApiModelProperty(value = "医疗类别")
    @Field(type = FieldType.Date, store = true)
    private String medType;
    @ApiModelProperty(value = "结算类别")
    @Field(type = FieldType.Date, store = true)
    private String setlType;
    @ApiModelProperty(value = "清算类别")
    @Field(type = FieldType.Text, store = true)
    private String clrType;
    @ApiModelProperty(value = "清算方式")
    @Field(type = FieldType.Text, store = true)
    private String clrWay;
    @ApiModelProperty(value = "医疗费总额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal medfeeSumamt;
    @ApiModelProperty(value = "全自费金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal fulamtOwnpayAmt;
    @ApiModelProperty(value = "超限价自费费用")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal overlmtSelfpay;
    @ApiModelProperty(value = "先行自付金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal preselfpayAmt;
    @ApiModelProperty(value = "符合范围金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal inscpAmt;
    @ApiModelProperty(value = "统筹基金支出")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal hifpPay;
    @ApiModelProperty(value = "公务员医疗补助资金支出")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal cvlservPay;
    @ApiModelProperty(value = "补充医疗保险基金支出")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal hifesPay;
    @ApiModelProperty(value = "大病补充医疗保险基金支出")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal hifmiPay;
    @ApiModelProperty(value = "大额医疗补助基金支出")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal hifobPay;
    @ApiModelProperty(value = "伤残人员医疗保障基金支出")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal hifdmPay;
    @ApiModelProperty(value = "医疗救助基金支出")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal mafPay;
    @ApiModelProperty(value = "其它基金支付")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal othfundPay;
    @ApiModelProperty(value = "基金支付总额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal fundPaySumamt;
    @ApiModelProperty(value = "个人支付金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal psnPay;
    @ApiModelProperty(value = "个人账户支出")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal acctPay;
    @ApiModelProperty(value = "现金支付金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal cashPayamt;
    @ApiModelProperty(value = "自费中医院负担部分")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal ownpayHospPart;
    @ApiModelProperty(value = "按病种结算支付金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal bydiseSetlPayamt;
    @ApiModelProperty(value = "除外项目基金支付金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal exctItemFundPayamt;
    @ApiModelProperty(value = "处方信息（费用明细、医嘱信息）")
    @Field(type = FieldType.Nested, store = true)
    private List<OrderSubES> orderSubDtos;
}
    
