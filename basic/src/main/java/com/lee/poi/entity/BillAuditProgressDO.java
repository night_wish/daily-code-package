package com.lee.poi.entity;


import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "BillAuditProgressDO", description = "单据审核流程DO")
public class BillAuditProgressDO implements Serializable {
    private static final long serialVersionUID = -1058347788144051204L;

    @ApiModelProperty(value = "医保区划名称")
    private String admdvsName;

    @ApiModelProperty(value = "疑点数")
    private String doubtfulPointCount;

    @ApiModelProperty(value = "涉及违规金额")
    private String cnfmVolaAmt;

    @ApiModelProperty(value = "违规人次")
    private String cnfmVolaTime;

    @ApiModelProperty(value = "医疗机构数量")
    private String medinsCount;

    @ApiModelProperty(value = "待初审数量")
    private String chkVolaCount;

    @ApiModelProperty(value = "待申诉数量")
    private String applCount;

    @ApiModelProperty(value = "待复审数量")
    private String rchkVolaCount;

    @ApiModelProperty(value = "待合议数量")
    private String appyClglCount;

    @ApiModelProperty(value = "待终审数量")
    private String finlChkerCount;

    @ApiModelProperty(value = "审核进度")
    private String chkPrgs;

    @ApiModelProperty(value = "违规率")
    private String violationRate;

    public String getFinlChkerCount() {
        return finlChkerCount;
    }

    public void setFinlChkerCount(String finlChkerCount) {
        this.finlChkerCount = finlChkerCount;
    }

    public String getAdmdvsName() {
        return this.admdvsName;
    }

    public void setAdmdvsName(String admdvsName) {
        this.admdvsName = admdvsName;
    }

    public String getDoubtfulPointCount() {
        return this.doubtfulPointCount;
    }

    public void setDoubtfulPointCount(String doubtfulPointCount) {
        this.doubtfulPointCount = doubtfulPointCount;
    }

    public String getCnfmVolaAmt() {
        return this.cnfmVolaAmt;
    }

    public void setCnfmVolaAmt(String cnfmVolaAmt) {
        this.cnfmVolaAmt = cnfmVolaAmt;
    }

    public String getCnfmVolaTime() {
        return this.cnfmVolaTime;
    }

    public void setCnfmVolaTime(String cnfmVolaTime) {
        this.cnfmVolaTime = cnfmVolaTime;
    }

    public String getMedinsCount() {
        return this.medinsCount;
    }

    public void setMedinsCount(String medinsCount) {
        this.medinsCount = medinsCount;
    }

    public String getChkVolaCount() {
        return this.chkVolaCount;
    }

    public void setChkVolaCount(String chkVolaCount) {
        this.chkVolaCount = chkVolaCount;
    }

    public String getApplCount() {
        return this.applCount;
    }

    public void setApplCount(String applCount) {
        this.applCount = applCount;
    }

    public String getRchkVolaCount() {
        return this.rchkVolaCount;
    }

    public void setRchkVolaCount(String rchkVolaCount) {
        this.rchkVolaCount = rchkVolaCount;
    }

    public String getAppyClglCount() {
        return this.appyClglCount;
    }

    public void setAppyClglCount(String appyClglCount) {
        this.appyClglCount = appyClglCount;
    }

    public String getViolationRate() {
        return this.violationRate;
    }

    public void setViolationRate(String violationRate) {
        this.violationRate = violationRate;
    }

    public String getChkPrgs() {
        return this.chkPrgs;
    }

    public void setChkPrgs(String chkPrgs) {
        this.chkPrgs = chkPrgs;
    }
}

