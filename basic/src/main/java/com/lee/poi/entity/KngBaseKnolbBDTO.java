package com.lee.poi.entity;

import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;
import com.lee.poi.utils.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 知识库管理：本地知识库
 * </p>
 *
 * @author Lee
 * @since 2022-08-02
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@ApiModel(value="KngBaseKnolbBDTO", description="知识库管理：本地知识库")
public class KngBaseKnolbBDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "知识库ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String knolbId;

    @ApiModelProperty(value = "数据唯一标识")
    @Size(max=40,message="不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "知识库编码SID")
    @Size(max=32,message="不能超过最大长度：32")
    private String knolbCode;

    @ApiModelProperty(value = "知识库名称")
    @Size(max=64,message="不能超过最大长度：64")
    private String knolbName;

    @ApiModelProperty(value = "知识库连接")
    @Size(max=512,message="不能超过最大长度：512")
    private String knolbLink;

    @ApiModelProperty(value = "知识库用户名")
    @Size(max=32,message="不能超过最大长度：32")
    private String knolbUserName;

    @ApiModelProperty(value = "知识库密码")
    @Size(max=64,message="不能超过最大长度：64")
    private String knolbPwd;

    @ApiModelProperty(value = "知识库描述")
    @Size(max=255,message="不能超过最大长度：255")
    private String knolbDscr;

    @ApiModelProperty(value = "医保区划")
    @Size(max=6,message="不能超过最大长度：6")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    @Size(max=40,message="不能超过最大长度：40")
    private String admdvsName;

    @ApiModelProperty(value = "共享状态：1-未共享，2-已共享")
    @Size(max=3,message="不能超过最大长度：3")
    private String shrStas;

    @ApiModelProperty(value = "区划编码（谁分享）")
    @Size(max=6,message="不能超过最大长度：6")
    private String shrAdmdvs;

    @ApiModelProperty(value = "区划名称（谁分享）")
    @Size(max=40,message="不能超过最大长度：40")
    private String shrAdmdvsName;

    @ApiModelProperty(value = "分享时间")
    private Date shrTime;

    @ApiModelProperty(value = "创建人")
    @Size(max=64,message="不能超过最大长度：64")
    private String crterId;

    @ApiModelProperty(value = "创建人姓名")
    @Size(max=45,message="不能超过最大长度：45")
    private String crterName;

    @ApiModelProperty(value = "创建时间")
    private Date crterTime;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;

    //=============Transient=========
    //医保区划列表（批量分享用）
    @ApiModelProperty(value = "医保区划编码列表")
    private List<String> admdvsList;


    public String getKnolbId() {
        return knolbId;
    }

    public void setKnolbId(String knolbId) {
        this.knolbId = knolbId;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getKnolbCode() {
        return knolbCode;
    }

    public void setKnolbCode(String knolbCode) {
        this.knolbCode = knolbCode;
    }

    public String getKnolbName() {
        return knolbName;
    }

    public void setKnolbName(String knolbName) {
        this.knolbName = knolbName;
    }

    public String getKnolbLink() {
        return knolbLink;
    }

    public void setKnolbLink(String knolbLink) {
        this.knolbLink = knolbLink;
    }

    public String getKnolbUserName() {
        return knolbUserName;
    }

    public void setKnolbUserName(String knolbUserName) {
        this.knolbUserName = knolbUserName;
    }

    public String getKnolbPwd() {
        return knolbPwd;
    }

    public void setKnolbPwd(String knolbPwd) {
        this.knolbPwd = knolbPwd;
    }

    public String getKnolbDscr() {
        return knolbDscr;
    }

    public void setKnolbDscr(String knolbDscr) {
        this.knolbDscr = knolbDscr;
    }

    public String getAdmdvs() {
        return admdvs;
    }

    public void setAdmdvs(String admdvs) {
        this.admdvs = admdvs;
    }

    public String getAdmdvsName() {
        return admdvsName;
    }

    public void setAdmdvsName(String admdvsName) {
        this.admdvsName = admdvsName;
    }

    public String getShrStas() {
        return shrStas;
    }

    public void setShrStas(String shrStas) {
        this.shrStas = shrStas;
    }

    public String getShrAdmdvs() {
        return shrAdmdvs;
    }

    public void setShrAdmdvs(String shrAdmdvs) {
        this.shrAdmdvs = shrAdmdvs;
    }

    public String getShrAdmdvsName() {
        return shrAdmdvsName;
    }

    public void setShrAdmdvsName(String shrAdmdvsName) {
        this.shrAdmdvsName = shrAdmdvsName;
    }

    public Date getShrTime() {
        return shrTime;
    }

    public void setShrTime(Date shrTime) {
        this.shrTime = shrTime;
    }

    public String getCrterId() {
        return crterId;
    }

    public void setCrterId(String crterId) {
        this.crterId = crterId;
    }

    public String getCrterName() {
        return crterName;
    }

    public void setCrterName(String crterName) {
        this.crterName = crterName;
    }

    public Date getCrterTime() {
        return crterTime;
    }

    public void setCrterTime(Date crterTime) {
        this.crterTime = crterTime;
    }

    public Date getUpdtTime() {
        return updtTime;
    }

    public void setUpdtTime(Date updtTime) {
        this.updtTime = updtTime;
    }

    public List<String> getAdmdvsList() {
        return admdvsList;
    }

    public void setAdmdvsList(List<String> admdvsList) {
        this.admdvsList = admdvsList;
    }

    @Override
    public String toString() {
        return "KngBaseKnolbB{" +
                     "knolbId=" + knolbId +
                     ", rid=" + rid +
                     ", knolbCode=" + knolbCode +
                     ", knolbName=" + knolbName +
                     ", knolbLink=" + knolbLink +
                     ", knolbUserName=" + knolbUserName +
                     ", knolbPwd=" + knolbPwd +
                     ", knolbDscr=" + knolbDscr +
                     ", admdvs=" + admdvs +
                     ", admdvsName=" + admdvsName +
                     ", shrStas=" + shrStas +
                     ", shrAdmdvs=" + shrAdmdvs +
                     ", shrAdmdvsName=" + shrAdmdvsName +
                     ", shrTime=" + shrTime +
                     ", crterId=" + crterId +
                     ", crterName=" + crterName +
                     ", crterTime=" + crterTime +
                     ", updtTime=" + updtTime +
                     "}";
    }
}
