package com.lee.poi.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 智能监管规则模板信息表
 * </p>
 *
 * @author WANGHONGBIN
 * @since 2021-12-23
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@ApiModel(value="RuleTmplRsltBDTO", description="智能监管规则模板信息表")
public class RuleTmplRsltDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "唯一标识")
    @Size(max=40,message="不能超过最大长度：40")
    private String rid;

    @NotNull(message = "模板编码不能为空")
    @ApiModelProperty(value = "模板编码")
    @Size(max=50,message="不能超过最大长度：50")
    private String tmplCode;

    @NotNull(message = "模板名称不能为空")
    @ApiModelProperty(value = "模板名称")
    @Size(max=50,message="不能超过最大长度：50")
    private String tmplName;

    @NotNull(message = "模板类别不能为空")
    @ApiModelProperty(value = "模板类别（ims:智能监管 drg:DRG dip:DIP）")
    @Size(max=10,message="不能超过最大长度：10")
    private String tmplCat;

    @NotNull(message = "模板类型不能为空")
    @ApiModelProperty(value = "模板类型 0:基础模板类型1:医保管理类 2:临床诊断类 3:药品类 4:诊疗项目类 5:临床路径类 6:手术操作类 99:其他类")
    @Size(max=10,message="不能超过最大长度：10")
    private String tmplType;

    @ApiModelProperty(value = "模板选项名称")
    @Size(max=50,message="不能超过最大长度：50")
    private String tmplItemName;

    @NotNull(message = "模板选项编码不能为空")
    @ApiModelProperty(value = "模板选项编码")
    @Size(max=40,message="不能超过最大长度：40")
    private String tmplItemCode;

    @NotNull(message = "父级模板选项编码不能为空")
    @ApiModelProperty(value = "父级模板选项编码")
    @Size(max=50,message="不能超过最大长度：50")
    private String tmplItemCodePrnt;

    @NotNull(message = "模板选项类型不能为空")
    @ApiModelProperty(value = "模板选项类型0:表头显示1:字典复选(查接口)2:字典复选(使用MEMO值)3:带添加的输入框4:字典单选(使用MEMO值)5:使用表达式生成的文本输入框6:{}占位符样式8:文本输入框10:开关类型12:下拉框类型 13:配置项类型")
    @Size(max=2,message="不能超过最大长度：2")
    private String tmplItemType;

    @ApiModelProperty(value = "模板选项排序")
    private Integer tmplItemOrder;

    @ApiModelProperty(value = "模板选项默认值")
    @Size(max=20000,message="tmplItemDefVal不能超过最大长度：2000")
    private String tmplItemDefVal;

    @ApiModelProperty(value = "模板选项等级")
    private Integer tmplItemLv;

    @ApiModelProperty(value = "模板选项选择值")
    @Size(max=20000,message="tmplItemVal不能超过最大长度：2000")
    private String tmplItemVal;

    @ApiModelProperty(value = "提示")
    @Size(max=100,message="不能超过最大长度：100")
    private String tmplItemWarn;

    @ApiModelProperty(value = "模板选项显隐(0:隐藏 1:显示 )")
    //@Size(max=2,message="不能超过最大长度：2")
    private Integer tmplItemDisp;

    @ApiModelProperty(value = "有效标识(0:无效 1:有效)")
    @Size(max=2,message="不能超过最大长度：2")
    private String itemValiFlag;

    @ApiModelProperty(value = "模板描述")
    private String tmplDscr;

    @ApiModelProperty(value = "程序编码")
    private String tmplProgCode;

    @NotNull(message = "模板版本不能为空")
    @ApiModelProperty(value = "模板版本")
    @Size(max=10,message="不能超过最大长度：10")
    private String tmplVer;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "创建人ID")
    @Size(max=20,message="不能超过最大长度：20")
    private String crterId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "创建人姓名")
    @Size(max=20,message="不能超过最大长度：20")
    private String crterName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "数据创建时间")
    private Date crteTime;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "数据更新时间")
    private Date updtTime;

    private List<RuleTmplRsltDTO> children;

}
