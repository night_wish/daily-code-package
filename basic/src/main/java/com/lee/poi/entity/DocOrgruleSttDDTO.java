package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 单据规则统计信息
 * </p>
 *
 * @author Lee
 * @since 2023-05-05
 */
@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
public class DocOrgruleSttDDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "单据规则统计信息ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String docRuleSttId;

    @ApiModelProperty(value = "唯一ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "单据机构统计信息")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String docOrgSttId;

    @ApiModelProperty(value = "模块编码")
    @Size(max = 6, message = "不能超过最大长度：6")
    private String moduCode;

    @ApiModelProperty(value = "审核月份")
    @Size(max = 16, message = "不能超过最大长度：16")
    private String auditMonth;

    @ApiModelProperty(value = "机构ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String medinsId;

    @ApiModelProperty(value = "机构名称")
    @Size(max = 32, message = "不能超过最大长度：32")
    private String medinsName;

    @ApiModelProperty(value = "规则ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String ruleId;

    @ApiModelProperty(value = "规则名称")
    @Size(max = 32, message = "不能超过最大长度：32")
    private String ruleName;

    @ApiModelProperty(value = "疑似违规人次")
    private Integer checkoutPsntime;

    @ApiModelProperty(value = "疑似违规金额")
    private BigDecimal checkoutAmt;

    @ApiModelProperty(value = "任务下发时间")
    private Date isuTime;

    @ApiModelProperty(value = "单据规则统计处理状态：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    @Size(max = 3, message = "不能超过最大长度：3")
    private String docRuleSttStas;

    @ApiModelProperty(value = "机构反馈结果 1-认扣 2-不认扣")
    @Size(max = 3, message = "不能超过最大长度：3")
    private String orgFbckStas;

    @ApiModelProperty(value = "机构反馈人ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String orgFbckerId;

    @ApiModelProperty(value = "机构反馈人名称")
    @Size(max = 16, message = "不能超过最大长度：16")
    private String orgFbckerName;

    @ApiModelProperty(value = "机构处理时间")
    private Date orgDspoTime;

    @ApiModelProperty(value = "机构反馈时间（提交）")
    private Date orgFbckTime;

    @ApiModelProperty(value = "申诉截止时间")
    private Date applDdlnTime;

    @ApiModelProperty(value = "初审处理人")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String chkOpterId;

    @ApiModelProperty(value = "初审处理人名称")
    @Size(max = 32, message = "不能超过最大长度：32")
    private String chkOpterName;

    @ApiModelProperty(value = "初审驳回时间")
    private Date chkRejtTime;

    @ApiModelProperty(value = "初审驳回原因")
    @Size(max = 255, message = "不能超过最大长度：255")
    private String chkRejtRea;

    @ApiModelProperty(value = "初审确认时间")
    private Date chkCfmTime;

    @ApiModelProperty(value = "初审提交时间")
    private Date chkSubmitTime;

    @ApiModelProperty(value = "初审调整金额")
    private BigDecimal chkAdjmAmt;

    @ApiModelProperty(value = "机构二次反馈结果 0-未反馈 1-认扣 2-不认扣")
    @Size(max = 3, message = "不能超过最大长度：3")
    private String rorgFbckStas;

    @ApiModelProperty(value = "机构二次反馈人ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String rorgFbckerId;

    @ApiModelProperty(value = "机构二次反馈人名称")
    @Size(max = 16, message = "不能超过最大长度：16")
    private String rorgFbckerName;

    @ApiModelProperty(value = "机构二次反馈处理时间")
    private Date rorgDspoTime;

    @ApiModelProperty(value = "机构二次反馈时间（提交）")
    private Date rorgFbckTime;

    @ApiModelProperty(value = "复审处理人")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String rchkOpterId;

    @ApiModelProperty(value = "复审处理人名称")
    @Size(max = 32, message = "不能超过最大长度：32")
    private String rchkOpterName;

    @ApiModelProperty(value = "复审驳回时间")
    private Date rchkRejtTime;

    @ApiModelProperty(value = "复审驳回原因")
    @Size(max = 255, message = "不能超过最大长度：255")
    private String rchkRejtRea;

    @ApiModelProperty(value = "复审确认时间")
    private Date rchkCfmTime;

    @ApiModelProperty(value = "复审提交时间")
    private Date rchkSubmitTime;

    @ApiModelProperty(value = "复审调整金额")
    private BigDecimal rchkAdjmAmt;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;


    @ApiModelProperty(value = "审核状态名称")
    private String docRuleSttStasName;

    @ApiModelProperty(value = "机构一次反馈结果 1-认扣 2-不认扣")
    private String orgFbckStasName;

    @ApiModelProperty(value = "机构二次反馈结果 1-认扣 2-不认扣")
    private String rorgFbckStasName;

    @ApiModelProperty(value = "当前环节代码：1-机构反馈 2-初审 3-复审 4-关闭")
    private String crtNodeCode;

    @ApiModelProperty(value = "当前环节代码：1-机构反馈 2-初审 3-复审 4-关闭")
    private String crtNodeName;

    @ApiModelProperty(value = "单据规则统计处理状态：1-机构一次反馈待处理 2-机构一次反馈已处理 3-机构一次反馈已提交/初审待处理 4-初审已处理 5-初审已驳回 6-初审已提交/机构二次反馈待处理 7-机构二次反馈已处理  8-机构二次反馈已提交/复审待处理 9-复审已处理 10-复审已驳回 11-复审已提交/关闭 12-已扣款清算")
    @Size(max = 3, message = "不能超过最大长度：3")
    private String oldDocRuleSttStas;

    @ApiModelProperty(value = "单据规则统计处理状态")
    private List<String> oldDocRuleSttStasList;

    @ApiModelProperty(value = "医疗机构IDs")
    private List<String> medinsIdList;

    @ApiModelProperty("单据规则统计处理状态数量")
    private Integer docRuleSttStasNumber;

    @ApiModelProperty(value = "医保区划权限")
    private String admdvsLike;

    @ApiModelProperty("初审拒绝原因名称")
    private String chkRejtReaName;

    @ApiModelProperty("复审拒绝原因名称")
    private String rchkRejtReaName;

    @ApiModelProperty("审核批次名称")
    private String auditMonthName;

    @ApiModelProperty("医保区划编码")
    private String admdvs;

    @ApiModelProperty("医保区划名称")
    private String admdvsName;

}
