package com.lee.poi.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/9/22 13:21
 */
@Data
@ApiModel("稽核扣款实扣信息DTO")
public class AuditDetEvtDTO {

    @ApiModelProperty("事件流水号")
    private String evtsn;
    @ApiModelProperty("稽核扣款事件ID")
    private String audtDetEvtId;
    @ApiModelProperty("服务事项实例ID")
    private String servMattInstId;
    @ApiModelProperty("服务事项环节实例ID")
    private String servMattNodeInstId;
    @ApiModelProperty("事件实例ID")
    private String evtInstId;
    @ApiModelProperty("事件类型")
    private String evtType;
    @ApiModelProperty("定点医药机构编号")
    private String fixmedinsCode;
    @ApiModelProperty("定点医药机构名称")
    private String fixmedinsName;
    @ApiModelProperty("定点医疗服务机构类型")
    private String fixmedinsType;
    @ApiModelProperty("基金支付类型")
    private String fundPayType;
    @ApiModelProperty("统筹区基金支付类型")
    private String poolareaFundPayType;
    @ApiModelProperty("统筹区基金款项名称")
    private String poolareaFundPayName;
    @ApiModelProperty("稽核扣款金额")
    private BigDecimal audtDetAmt;
    @ApiModelProperty("稽核扣款来源")
    private String audtDetSouc;
    @ApiModelProperty("稽核扣款描述")
    private String audtDetDscr;
    @ApiModelProperty("已实扣金额")
    private BigDecimal paidDetdAmt;
    @ApiModelProperty("实扣状态")
    private String detdStas;
    @ApiModelProperty("有效标志")
    private String valiFlag;
    @ApiModelProperty("审核状态")
    private String rchkFlag;
    @ApiModelProperty("唯一记录号")
    private String rid;
    @ApiModelProperty("更新时间")
    @JsonFormat(
          pattern = "yyyy-MM-dd HH:mm:ss",
          timezone = "GMT+8"
    )
    private Date updtTime;
    @ApiModelProperty("创建人")
    private String crterId;
    @ApiModelProperty("创建人姓名")
    private String crterName;
    @ApiModelProperty("创建时间")
    @JsonFormat(
          pattern = "yyyy-MM-dd HH:mm:ss",
          timezone = "GMT+8"
    )
    private Date crteTime;
    @ApiModelProperty("创建机构")
    private String crteOptinsNo;
    @ApiModelProperty("经办人")
    private String opterId;
    @ApiModelProperty("经办人姓名")
    private String opterName;
    @ApiModelProperty("经办时间")
    @JsonFormat(
          pattern = "yyyy-MM-dd HH:mm:ss",
          timezone = "GMT+8"
    )
    private Date optTime;
    @ApiModelProperty("经办机构")
    private String optinsNo;
    @ApiModelProperty("统筹区")
    private String poolareaNo;

}
