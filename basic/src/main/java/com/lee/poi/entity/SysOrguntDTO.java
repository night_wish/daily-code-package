package com.lee.poi.entity;

import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/9/22 13:56
 */
@Data
@ApiModel("稽核扣款实扣信息DTO")
public class SysOrguntDTO {

    @ApiModelProperty("RID")
    private String rid;

    @ApiModelProperty("组织机构ID")
    private String orguntId;

    @ApiModelProperty("组织机构编码")
    private String orgCodg;

    @ApiModelProperty("组织机构名称")
    private String orgName;

    @ApiModelProperty("医保区划")
    private String admdvs;

    @ApiModelProperty("描述")
    private String dscr;

    @ApiModelProperty("删除标志")
    private Boolean delFlag;

    @ApiModelProperty("删除时间")
    private Date delTime;

    @ApiModelProperty("统筹区编码")
    private String poolareaNo;

    @ApiModelProperty("父机构ID")
    private String prntOrgId;

    @ApiModelProperty("父路径")
    private String prntPath;

    @ApiModelProperty("是否叶子节点")
    private String leafnodFlag;

    @ApiModelProperty("等级")
    private int lv;

    @ApiModelProperty("编号")
    private int seq;
}
