package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 机构规则维度统计
 * </p>
 *
 * @author yehailong
 * @since 2023-04-28
 */
@Data
@NoArgsConstructor
public class OrgRuleSttDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "审核月份")
    @Size(max=16,message="不能超过最大长度：16")
    private String auditMonth;

    @ApiModelProperty(value = "医保区划")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    private String admdvsName;

    @ApiModelProperty(value = "医疗机构ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String medinsId;

    @ApiModelProperty(value = "医疗机构名称")
    @Size(max=32,message="不能超过最大长度：32")
    private String medinsName;

    @ApiModelProperty(value = "规则ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String ruleId;

    @ApiModelProperty(value = "规则名称")
    @Size(max=64,message="不能超过最大长度：64")
    private String ruleName;

    @ApiModelProperty(value = "疑似违规人次")
    private Integer chkoutPsntime;

    @ApiModelProperty(value = "疑似违规金额")
    private BigDecimal chkoutVolaAmt;

}
