package com.lee.poi.entity;


import com.lee.poi.utils.ApiModelProperty;

import java.util.Date;
import java.util.List;

public class AuditFbckMatlDTO {

    @ApiModelProperty(value = "RID")
    private String rid;

    @ApiModelProperty(value = "反馈材料ID")
    private String fbckMatlId;

    @ApiModelProperty(value = "模块编码")
    private String moduCode;

    @ApiModelProperty(value = "审核月份")
    private String auditMonth;

    @ApiModelProperty(value = "主院医疗机构ID")
    private String medinsId;

    @ApiModelProperty(value = "主院医疗机构名称")
    private String medinsName;

    @ApiModelProperty(value = "医保区划编码")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    private String admdvsName;

    @ApiModelProperty(value = "材料说明")
    private String matlDscr;

    @ApiModelProperty(value = "材料名称")
    private String matlName;

    @ApiModelProperty(value = "材料格式")
    private String matlFmt;

    @ApiModelProperty(value = "材料采纳状态:1-待提交 2-已提交 3-已采纳 4-未采纳  99-已删除")
    private String auditStas;



    @ApiModelProperty(value = "上传时间")
    private Date upldTime;



    @ApiModelProperty(value = "上传人ID")
    private String upldId;

    @ApiModelProperty(value = "上传人名称")
    private String upldName;

    @ApiModelProperty(value = "提交人ID")
    private String sbmterId;

    @ApiModelProperty(value = "提交人名称")
    private String sbmterName;

    @ApiModelProperty(value = "提交时间")
    private Date sbmtTime;

    @ApiModelProperty(value = "审核人ID")
    private String auditerId;

    @ApiModelProperty(value = "审核人名称")
    private String auditerName;

    @ApiModelProperty(value = "审核时间")
    private Date auditerTime;

    @ApiModelProperty(value = "驳回原因 1-材料不符 2-材料漏传")
    private String rejtReaType;


    @ApiModelProperty(value = "驳回说明")
    private String rejtReaDscr;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;

    @ApiModelProperty(value = "实际医疗机构ID")
    private String realMedinsId;

    @ApiModelProperty(value = "实际医疗机构名称")
    private String realMedinsName;

    @ApiModelProperty(value = "材料采纳状态:1-待提交 2-已提交 3-已采纳 4-未采纳  99-已删除")
    private String auditStasDesc;


    @ApiModelProperty(value = "驳回原因 1-材料不符 2-材料漏传")
    private String rejtReaTypeDesc;

    @ApiModelProperty(value = "上传时间-查询条件")
    private String upldMonth;

    @ApiModelProperty(value = "上传文件名称-查询条件")
    private String fileName;

    @ApiModelProperty(value = "补充材料开关")
    private String switchFlag;

    //===========================

    @ApiModelProperty(value = "审核批次名称")
    private String auditMonthName;

    @ApiModelProperty("页面查询医疗机构条件")
    private List<String > searchMedinsIdList;

    @ApiModelProperty("页面查询医保区划条件")
    private List<String> admdvsList;

    @ApiModelProperty("页面查询条件标记")
    private String flag;

    @ApiModelProperty(value = "上海市市中心 医疗机构ID")
    private List<String> centerAdmdvsMedinsIdList;

}
