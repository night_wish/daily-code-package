package com.lee.poi.entity;

import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/9/22 13:31
 */
@Data
@ApiModel("稽核扣款实扣信息DTO")
public class SetlChkDetCDTO {
    private static final long serialVersionUID = 8356921746856486846L;

    @ApiModelProperty("任务ID")
    private String taskID;
    /**
     * 结算审核扣款ID
     */
    @ApiModelProperty("结算审核扣款ID")
    private String setlChkDetId;     // 非必传

    /**
     * 就诊ID
     */
    @ApiModelProperty("就诊ID")
    private String mdtrtId;

    /**
     * 结算ID
     */
    @ApiModelProperty("结算ID")
    private String setlId;

    /**
     * 定点医药机构编号
     */
    @ApiModelProperty("定点医药机构编号")
    private String fixmedinsCode;

    /**
     * 定点医药机构名称
     */
    @ApiModelProperty("定点医药机构名称")
    private String fixmedinsName;

    /**
     * 清算类别
     */
    @ApiModelProperty("清算类别")
    private String clrType;

    /**
     * 定点归属机构
     */
    @ApiModelProperty("定点归属机构")
    private String fixBlngAdmdvs;

    /**
     * 医疗费总额
     */
    @ApiModelProperty("医疗费总额")
    private BigDecimal medfeeSumamt;

    /**
     * 全自费金额
     */
    @ApiModelProperty("全自费金额")
    private BigDecimal fulamtOwnpayAmt;

    /**
     * 超限价自付费用
     */
    @ApiModelProperty("超限价自付费用")
    private BigDecimal overlmtSelfpay;

    /**
     * 先行自付金额
     */
    @ApiModelProperty("先行自付金额")
    private BigDecimal preselfpayAmt;

    /**
     * 符合范围金额
     */
    @ApiModelProperty("符合范围金额")
    private BigDecimal inscpAmt;

    /**
     * 审核扣款金额
     */
    @ApiModelProperty("审核扣款金额")
    private BigDecimal chkDetAmt;

    /**
     * 审核扣款来源
     */
    @ApiModelProperty("审核扣款来源")
    private String chkDetSouc;

    /**
     * 审核扣款原因
     */
    @ApiModelProperty("审核扣款原因")
    private String chkDetRea;

    /**
     * 审核扣款描述
     */
    @ApiModelProperty("审核扣款描述")
    private String chkDetDscr;

    /**
     * 有效标志
     */
    @ApiModelProperty("有效标志")
    private String valiFlag;

    /**
     * 审核状态
     */
    @ApiModelProperty("审核状态")
    private String chkStas;   // 非必传

    /**
     * 唯一记录号
     */
    @ApiModelProperty("唯一记录号")
    private String rid;   // 非必传

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private Date updtTime;   // 非必传

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String crterId;

    /**
     * 创建人姓名
     */
    @ApiModelProperty("创建人姓名")
    private String crterName;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date crteTime;   // 非必传

    /**
     * 创建机构
     */
    @ApiModelProperty("创建机构")
    private String crteOptinsNo;

    /**
     * 经办人
     */
    @ApiModelProperty("经办人")
    private String opterId;

    /**
     * 经办人姓名
     */
    @ApiModelProperty("经办人姓名")
    private String opterName;

    /**
     * 经办时间
     */
    @ApiModelProperty("经办时间")
    private Date optTime;   // 非必传

    /**
     * 经办机构
     */
    @ApiModelProperty("经办机构")
    private String optinsNo;

    /**
     * 统筹区
     */
    @ApiModelProperty("统筹区")
    private String poolareaNo;  // 非必传


    /************************ 扩展属性 **/
    /**
     * 人员编号
     */
    @ApiModelProperty("人员编号")
    private String psnNo;

    @ApiModelProperty("参保地医保区划")
    private String insuAdmdvs;
}
