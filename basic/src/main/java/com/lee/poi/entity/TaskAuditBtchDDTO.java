package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 任务审核批次信息表
 * </p>
 *
 * @author Lee
 * @since 2023-05-24
 */
@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
public class TaskAuditBtchDDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "全局唯一ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "任务审核批次ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String taskAuditBtchId;

    @ApiModelProperty(value = "模块编码")
    @Size(max = 6, message = "不能超过最大长度：6")
    private String moduCode;

    @ApiModelProperty(value = "审核批次编码")
    @Size(max = 16, message = "不能超过最大长度：16")
    private String btchCode;

    @ApiModelProperty(value = "审核批次名称")
    @Size(max = 32, message = "不能超过最大长度：32")
    private String btchName;

    @ApiModelProperty(value = "是否生成患者批次聚合数据0未生成  大于1已生成")
    @Size(max = 3, message = "不能超过最大长度：3")
    private String ifGenPatnaggData;

    @ApiModelProperty(value = "业务开始时间")
    private Date startDate;

    @ApiModelProperty(value = "业务结束时间")
    private Date endDate;

    @ApiModelProperty(value = "创建人")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String crterId;

    @ApiModelProperty(value = "创建人名称")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String crterName;

    @ApiModelProperty(value = "数据创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "数据更新时间")
    private Date updtTime;

}
