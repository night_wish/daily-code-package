package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @program: hsa-sims-acq
 * @see： cn.hsa.acq.esinit.model.EncounterSubESDO
 * @description: [就诊信息]-ES映射对象
 * @author: taominyao
 * @create: 2023-03-15 13:34
 **/
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class EncounterSubES {
    @ApiModelProperty(value = "就诊ID（就诊记录唯一ID）")
    @Id
    @Field(type = FieldType.Keyword, store = true)
    private String mdtrtId;

    @ApiModelProperty(value = "医疗费总额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal medfeeSumamt;

    @ApiModelProperty(value = "险种类型")
    @Field(type = FieldType.Text, store = true)
    private String insuType;
    @ApiModelProperty(value = "人员类别")
    @Field(type = FieldType.Text, store = true)
    private String psnType;
    @ApiModelProperty(value = "参保所属医保区划")
    @Field(type = FieldType.Text, store = true)
    private String insuAdmdvs;
    @ApiModelProperty(value = "支付地点类别")
    @Field(type = FieldType.Text, store = true)
    private String payLoc;
    @ApiModelProperty(value = "定点医药机构编号")
    @Field(type = FieldType.Keyword, store = true)
    private String fixmedinsCode;
    @ApiModelProperty(value = "定点医药机构名称")
    @Field(type = FieldType.Text, store = true)
    private String fixmedinsName;
    @ApiModelProperty(value = "医疗机构类型字典：[定点药店、门诊部…]")
    @Field(type = FieldType.Text, store = true)
    private String medinsType;
    @ApiModelProperty(value = "医院等级")
    @Field(type = FieldType.Text, store = true)
    private String hospLv;
    @ApiModelProperty(value = "定点所属区划")
    @Field(type = FieldType.Text, store = true)
    private String fixBlngAdmdvs;
    @ApiModelProperty(value = "开始时间--入院日期")
    @Field(type = FieldType.Date, store = true)
    private Date admDate;
    @ApiModelProperty(value = "结束时间--出院日期")
    @Field(type = FieldType.Date, store = true)
    private Date dscgDate;
    @ApiModelProperty(value = "医疗类别")
    @Field(type = FieldType.Text, store = true)
    private String medType;
    @ApiModelProperty(value = "住院/门诊号")
    @Field(type = FieldType.Text, store = true)
    private String iptOtpNo;
    @ApiModelProperty(value = "病区标识（病区床位）（病房号）")
    @Field(type = FieldType.Text, store = true)
    private String wardAreaBed;
    @ApiModelProperty(value = "入院科室编码")
    @Field(type = FieldType.Text, store = true)
    private String admDeptCodg;
    @ApiModelProperty(value = "入院科室名称")
    @Field(type = FieldType.Text, store = true)
    private String admDeptName;
    @ApiModelProperty(value = "出院科室编码")
    @Field(type = FieldType.Text, store = true)
    private String dscgDeptCodg;
    @ApiModelProperty(value = "出院科室名称")
    @Field(type = FieldType.Text, store = true)
    private String dscgDeptName;
    @ApiModelProperty(value = "住院主诊断代码")
    @Field(type = FieldType.Text, store = true)
    private String dscgMaindiagCode;
    @ApiModelProperty(value = "住院主诊断名称")
    @Field(type = FieldType.Text, store = true)
    private String dscgMaindiagName;
    @ApiModelProperty(value = "医师代码")
    @Field(type = FieldType.Text, store = true)
    private String chfpdrCode;

    @ApiModelProperty(value = "医师名称")
    @Field(type = FieldType.Text, store = true)
    private String chfpdrName;
    @ApiModelProperty(value = "病种编号")
    @Field(type = FieldType.Text, store = true)
    private String diseNo;
    @ApiModelProperty(value = "病种名称")
    @Field(type = FieldType.Text, store = true)
    private String diseName;
    @ApiModelProperty(value = "住院天数")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal iptDays;
    @ApiModelProperty(value = "生育类别（生育状态）")
    @Field(type = FieldType.Text, store = true)
    private String matnType;
    @ApiModelProperty(value = "入院科别")
    @Field(type = FieldType.Text, store = true)
    private String admCaty;
    @ApiModelProperty(value = "出院科别")
    @Field(type = FieldType.Text, store = true)
    private String dscgCaty;
    @ApiModelProperty(value = "病种类型代码【门诊慢性病、门诊特殊疾病、单病种、日间手术病种、其他病种】")
    @Field(type = FieldType.Text, store = true)
    private String diseTypeCode;
    @ApiModelProperty(value = "就诊拓展字段")
    @Field(type = FieldType.Text, store = true)
    private String extendEncounterFields;

    @ApiModelProperty(value = "主院机构编码")
    @Field(type = FieldType.Keyword, store = true)
    private String mainMedinsId;

    @ApiModelProperty(value = "主院机构名称")
    @Field(type = FieldType.Text, store = true)
    private String mainMedinsName;

}
    
