package com.lee.poi.entity;

import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 任务计划配置表
 * </p>
 *
 * @author Lee
 * @since 2021-12-23
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@ApiModel(value = "TaskPlanCfgADTO", description = "任务计划配置表")
public class TaskPlanCfgADTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "任务计划ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String planId;

    @ApiModelProperty(value = "数据唯一记录号")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "计划名称")
    @Size(max = 64, message = "不能超过最大长度：64")
    private String planName;

    @ApiModelProperty(value = "医保区划")
    @Size(max = 6, message = "不能超过最大长度：6")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    @Size(max = 64, message = "不能超过最大长度：64")
    private String admdvsName;

    @ApiModelProperty(value = "执行计划表达式")
    @Size(max = 32, message = "不能超过最大长度：32")
    private String cron;

    @ApiModelProperty(value = "计划类型： 2定时日任务 3定时月任务")
    private Integer planType;

    @ApiModelProperty(value = "筛选条件(页面过滤条件)")
    @Size(max = 65535, message = "不能超过最大长度：65535")
    private String fitrCond;

    @ApiModelProperty(value = "启用标志：0不启用1启用")
    private Integer enabFlag;

    @ApiModelProperty(value = "创建人ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String crterId;

    @ApiModelProperty(value = "创建人姓名")
    @Size(max = 50, message = "不能超过最大长度：50")
    private String crterName;

    @ApiModelProperty(value = "创建机构编号")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String crteOptinsNo;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;
}
