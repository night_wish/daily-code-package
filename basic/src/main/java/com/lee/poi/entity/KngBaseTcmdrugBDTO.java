package com.lee.poi.entity;

import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;
import com.lee.poi.utils.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 知识库维护--中药/中成药知识维护
 * </p>
 *
 * @author Lee
 * @since 2022-06-20
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@ApiModel(value="KngBaseTcmdrugBDTO", description="知识库维护--中药/中成药知识维护")
public class KngBaseTcmdrugBDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "中药/中成药ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String tcmdrugId;

    @ApiModelProperty(value = "数据唯一标识")
    @Size(max=45,message="不能超过最大长度：45")
    private String rid;

    @ApiModelProperty(value = "中药/中成药编码")
    @Size(max=64,message="不能超过最大长度：64")
    private String tcmdrugCode;

    @ApiModelProperty(value = "中药/中成药名称")
    @Size(max=64,message="不能超过最大长度：64")
    private String tcmdrugName;

    @ApiModelProperty(value = "剂型")
    @Size(max=40,message="不能超过最大长度：40")
    private String dosform;

    @ApiModelProperty(value = "规格")
    @Size(max=45,message="不能超过最大长度：45")
    private String spec;

    @ApiModelProperty(value = "知识名称")
    @Size(max=45,message="不能超过最大长度：45")
    private String kngBaseName;

    @ApiModelProperty(value = "药品类型：1-中药、2-中成药")
    @Size(max=3,message="不能超过最大长度：3")
    private String drugType;

    @ApiModelProperty(value = "状态：1-生效、2-废止")
    @Size(max=3,message="不能超过最大长度：3")
    private String tcmdrugStas;

    @ApiModelProperty(value = "创建人ID")
    @Size(max=64,message="不能超过最大长度：64")
    private String crterId;

    @ApiModelProperty(value = "创建人名称")
    @Size(max=45,message="不能超过最大长度：45")
    private String crterName;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;

    @ApiModelProperty(value = "医保区划")
    @Size(max=6,message="不能超过最大长度：6")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    @Size(max=64,message="不能超过最大长度：64")
    private String admdvsName;

    //中药/中成药
    @ApiModelProperty(value = "中药/中成药ID列表")
    private List<String> tcmdrugIdList;


    public String getTcmdrugId() {
        return tcmdrugId;
    }

    public void setTcmdrugId(String tcmdrugId) {
        this.tcmdrugId = tcmdrugId;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getTcmdrugCode() {
        return tcmdrugCode;
    }

    public void setTcmdrugCode(String tcmdrugCode) {
        this.tcmdrugCode = tcmdrugCode;
    }

    public String getTcmdrugName() {
        return tcmdrugName;
    }

    public void setTcmdrugName(String tcmdrugName) {
        this.tcmdrugName = tcmdrugName;
    }

    public String getDosform() {
        return dosform;
    }

    public void setDosform(String dosform) {
        this.dosform = dosform;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getKngBaseName() {
        return kngBaseName;
    }

    public void setKngBaseName(String kngBaseName) {
        this.kngBaseName = kngBaseName;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getTcmdrugStas() {
        return tcmdrugStas;
    }

    public void setTcmdrugStas(String tcmdrugStas) {
        this.tcmdrugStas = tcmdrugStas;
    }

    public String getCrterId() {
        return crterId;
    }

    public void setCrterId(String crterId) {
        this.crterId = crterId;
    }

    public String getCrterName() {
        return crterName;
    }

    public void setCrterName(String crterName) {
        this.crterName = crterName;
    }

    public Date getCrteTime() {
        return crteTime;
    }

    public void setCrteTime(Date crteTime) {
        this.crteTime = crteTime;
    }

    public Date getUpdtTime() {
        return updtTime;
    }

    public void setUpdtTime(Date updtTime) {
        this.updtTime = updtTime;
    }

    public String getAdmdvs() {
        return admdvs;
    }

    public void setAdmdvs(String admdvs) {
        this.admdvs = admdvs;
    }

    public String getAdmdvsName() {
        return admdvsName;
    }

    public void setAdmdvsName(String admdvsName) {
        this.admdvsName = admdvsName;
    }


    @Override
    public String toString() {
        return "KngBaseTcmdrugB{" +
        "tcmdrugId=" + tcmdrugId +
        ", rid=" + rid +
        ", tcmdrugCode=" + tcmdrugCode +
        ", tcmdrugName=" + tcmdrugName +
        ", dosform=" + dosform +
        ", spec=" + spec +
        ", kngBaseName=" + kngBaseName +
        ", drugType=" + drugType +
        ", tcmdrugStas=" + tcmdrugStas +
        ", crterId=" + crterId +
        ", crterName=" + crterName +
        ", crteTime=" + crteTime +
        ", updtTime=" + updtTime +
        ", admdvs=" + admdvs +
        ", admdvsName=" + admdvsName +
        "}";
    }
}
