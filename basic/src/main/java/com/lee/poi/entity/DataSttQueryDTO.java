package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 查询dto
 * </p>
 *
 * @author yehailong
 * @since 2023-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class DataSttQueryDTO{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "审核月份")
    private String auditMonth;

    @ApiModelProperty(value = "医疗机构ID")
    private String medinsId;

    @ApiModelProperty(value = "医疗机构名称")
    private String medinsName;

    @ApiModelProperty(value = "规则ID")
    private String ruleId;

    @ApiModelProperty(value = "规则ID集合")
    private List<String> ruleIds;

    @ApiModelProperty(value = "规则名称")
    private String ruleName;

    @ApiModelProperty(value = "医保区划")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    private String admdvsName;

    @ApiModelProperty(value = "模块编码")
    private String moduCode;

    @ApiModelProperty(value = "单据疑点明细）")
    private List<String> docSusptDetlIds;

    @ApiModelProperty(value = "公用状态字段")
    private String flag;

    private String userId;

    private String userName;

    private Date createDate;

    @ApiModelProperty(value = "医疗机构ID集合")
    private List<String> medinsIds;

    @ApiModelProperty(value = "医保区划集合集合")
    private List<String> admdvsList;

    /**
     * 标记状态
     */
    @ApiModelProperty(value = "数据标记状态")
    private  String markIsuFlag;

    /**
     * 菜单标识 1、数据标记 2、数据下发
     */
    @ApiModelProperty(value = "菜单标识 1、数据标记 2、数据下发")
    private  String menuFlag;

    @ApiModelProperty(value = "模块编码集合")
    private List<String> moduCodeList;

    @ApiModelProperty(value = "任务id")
    private String taskId;

    @ApiModelProperty(value = "docId集合")
    private List<String> docIds;

    @ApiModelProperty(value = "调整金额")
    private BigDecimal adjmAmt;

    @ApiModelProperty(value = "调整类型 1、按输入金额进行调整。    2、 按机审扣款进行调整")
    private String adjmType;

    @ApiModelProperty(value = "Id集合")
    private List<String> ids;



}
