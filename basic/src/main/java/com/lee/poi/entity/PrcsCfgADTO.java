package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 经办流程配置信息表
 * </p>
 *
 * @author Lee
 * @since 2022-01-05
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class PrcsCfgADTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "经办流程配置ID")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String prcsCfgId;

    @ApiModelProperty(value = "数据唯一记录号")
    @Size(max = 40, message = "不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "参数配置类型")
    @Size(max = 30, message = "不能超过最大长度：30")
    private String paraCfgType;

    @ApiModelProperty(value = "配置信息内容")
    @Size(max = 2000, message = "不能超过最大长度：2000")
    private String cfgInfoCont;

    @ApiModelProperty(value = "创建人ID")
    @Size(max = 20, message = "不能超过最大长度：20")
    private String crterId;

    @ApiModelProperty(value = "创建人姓名")
    @Size(max = 50, message = "不能超过最大长度：50")
    private String crterName;

    @ApiModelProperty(value = "数据创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "创建机构编号")
    @Size(max = 20, message = "不能超过最大长度：20")
    private String crteOptinsNo;

    @ApiModelProperty(value = "经办人ID")
    @Size(max = 20, message = "不能超过最大长度：20")
    private String opterId;

    @ApiModelProperty(value = "经办人姓名")
    @Size(max = 50, message = "不能超过最大长度：50")
    private String opterName;

    @ApiModelProperty(value = "经办时间")
    private Date optTime;

    @ApiModelProperty(value = "经办机构编号")
    @Size(max = 20, message = "不能超过最大长度：20")
    private String optinsNo;

    @ApiModelProperty(value = "数据更新时间")
    private Date updtTime;

    @ApiModelProperty(value = "模块编码")
    private String moduCode;


    public String getModuCode() {
        return moduCode;
    }

    public void setModuCode(String moduCode) {
        this.moduCode = moduCode;
    }

    public String getPrcsCfgId() {
        return prcsCfgId;
    }

    public void setPrcsCfgId(String prcsCfgId) {
        this.prcsCfgId = prcsCfgId;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getParaCfgType() {
        return paraCfgType;
    }

    public void setParaCfgType(String paraCfgType) {
        this.paraCfgType = paraCfgType;
    }

    public String getCfgInfoCont() {
        return cfgInfoCont;
    }

    public void setCfgInfoCont(String cfgInfoCont) {
        this.cfgInfoCont = cfgInfoCont;
    }

    public String getCrterId() {
        return crterId;
    }

    public void setCrterId(String crterId) {
        this.crterId = crterId;
    }

    public String getCrterName() {
        return crterName;
    }

    public void setCrterName(String crterName) {
        this.crterName = crterName;
    }

    public Date getCrteTime() {
        return crteTime;
    }

    public void setCrteTime(Date crteTime) {
        this.crteTime = crteTime;
    }

    public String getCrteOptinsNo() {
        return crteOptinsNo;
    }

    public void setCrteOptinsNo(String crteOptinsNo) {
        this.crteOptinsNo = crteOptinsNo;
    }

    public String getOpterId() {
        return opterId;
    }

    public void setOpterId(String opterId) {
        this.opterId = opterId;
    }

    public String getOpterName() {
        return opterName;
    }

    public void setOpterName(String opterName) {
        this.opterName = opterName;
    }

    public Date getOptTime() {
        return optTime;
    }

    public void setOptTime(Date optTime) {
        this.optTime = optTime;
    }

    public String getOptinsNo() {
        return optinsNo;
    }

    public void setOptinsNo(String optinsNo) {
        this.optinsNo = optinsNo;
    }

    public Date getUpdtTime() {
        return updtTime;
    }

    public void setUpdtTime(Date updtTime) {
        this.updtTime = updtTime;
    }


    @Override
    public String toString() {
        return "PrcsCfgA{" +
                "prcsCfgId=" + prcsCfgId +
                ", rid=" + rid +
                ", paraCfgType=" + paraCfgType +
                ", cfgInfoCont=" + cfgInfoCont +
                ", crterId=" + crterId +
                ", crterName=" + crterName +
                ", crteTime=" + crteTime +
                ", crteOptinsNo=" + crteOptinsNo +
                ", opterId=" + opterId +
                ", opterName=" + opterName +
                ", optTime=" + optTime +
                ", optinsNo=" + optinsNo +
                ", updtTime=" + updtTime +
                "}";
    }
}
