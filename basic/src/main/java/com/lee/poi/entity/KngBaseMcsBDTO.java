package com.lee.poi.entity;

import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;
import com.lee.poi.utils.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 知识库维护--医用耗材知识维护
 * </p>
 *
 * @author Lee
 * @since 2022-06-20
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@ApiModel(value="KngBaseMcsBDTO", description="知识库维护--医用耗材知识维护")
public class KngBaseMcsBDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "耗材ID")
    @Size(max=40,message="不能超过最大长度：40")
    private String mcsId;

    @ApiModelProperty(value = "数据唯一标识")
    @Size(max=40,message="不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "耗材编码")
    @Size(max=64,message="不能超过最大长度：64")
    private String mcsCode;

    @ApiModelProperty(value = "耗材名称")
    @Size(max=64,message="不能超过最大长度：64")
    private String mcsName;

    @ApiModelProperty(value = "型号")
    @Size(max=45,message="不能超过最大长度：45")
    private String mol;

    @ApiModelProperty(value = "规格")
    @Size(max=45,message="不能超过最大长度：45")
    private String spec;

    @ApiModelProperty(value = "知识名称")
    @Size(max=64,message="不能超过最大长度：64")
    private String kngBaseName;

    @ApiModelProperty(value = "状态：1-生效、2-废止")
    @Size(max=3,message="不能超过最大长度：3")
    private String mscStas;

    @ApiModelProperty(value = "创建人ID")
    @Size(max=64,message="不能超过最大长度：64")
    private String crterId;

    @ApiModelProperty(value = "创建人名称")
    @Size(max=45,message="不能超过最大长度：45")
    private String crterName;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "更新时间")
    private Date updtTime;

    @ApiModelProperty(value = "医保区划")
    @Size(max=6,message="不能超过最大长度：6")
    private String admdvs;

    @ApiModelProperty(value = "医保区划名称")
    @Size(max=64,message="不能超过最大长度：64")
    private String admdvsName;

    //医用耗材ID列表
    @ApiModelProperty(value = "医用耗材ID列表")
    private List<String> mcsIdList;


    public String getMcsId() {
        return mcsId;
    }

    public void setMcsId(String mcsId) {
        this.mcsId = mcsId;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getMcsCode() {
        return mcsCode;
    }

    public void setMcsCode(String mcsCode) {
        this.mcsCode = mcsCode;
    }

    public String getMcsName() {
        return mcsName;
    }

    public void setMcsName(String mcsName) {
        this.mcsName = mcsName;
    }

    public String getMol() {
        return mol;
    }

    public void setMol(String mol) {
        this.mol = mol;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getKngBaseName() {
        return kngBaseName;
    }

    public void setKngBaseName(String kngBaseName) {
        this.kngBaseName = kngBaseName;
    }

    public String getMscStas() {
        return mscStas;
    }

    public void setMscStas(String mscStas) {
        this.mscStas = mscStas;
    }

    public String getCrterId() {
        return crterId;
    }

    public void setCrterId(String crterId) {
        this.crterId = crterId;
    }

    public String getCrterName() {
        return crterName;
    }

    public void setCrterName(String crterName) {
        this.crterName = crterName;
    }

    public Date getCrteTime() {
        return crteTime;
    }

    public void setCrteTime(Date crteTime) {
        this.crteTime = crteTime;
    }

    public Date getUpdtTime() {
        return updtTime;
    }

    public void setUpdtTime(Date updtTime) {
        this.updtTime = updtTime;
    }

    public String getAdmdvs() {
        return admdvs;
    }

    public void setAdmdvs(String admdvs) {
        this.admdvs = admdvs;
    }

    public String getAdmdvsName() {
        return admdvsName;
    }

    public void setAdmdvsName(String admdvsName) {
        this.admdvsName = admdvsName;
    }


    @Override
    public String toString() {
        return "KngBaseMcsB{" +
        "mcsId=" + mcsId +
        ", rid=" + rid +
        ", mcsCode=" + mcsCode +
        ", mcsName=" + mcsName +
        ", mol=" + mol +
        ", spec=" + spec +
        ", kngBaseName=" + kngBaseName +
        ", mscStas=" + mscStas +
        ", crterId=" + crterId +
        ", crterName=" + crterName +
        ", crterTime=" + crteTime +
        ", updtTime=" + updtTime +
        ", admdvs=" + admdvs +
        ", admdvsName=" + admdvsName +
        "}";
    }
}
