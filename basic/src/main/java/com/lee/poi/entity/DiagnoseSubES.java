package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * @program: hsa-sims-acq
 * @see： cn.hsa.acq.esinit.model.DiagnoseSubESDO
 * @description: [诊断信息]-ES映射对象
 * @author: taominyao
 * @create: 2023-03-15 13:44
 **/
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class DiagnoseSubES {

    @ApiModelProperty(value = "诊断信息ID")
    @Id
    @Field(type = FieldType.Text, store = true)
    private String diagInfoId;
    @ApiModelProperty(value = "就诊ID")
    @Field(type = FieldType.Text, store = true)
    private String mdtrtId;
    @ApiModelProperty(value = "人员编号")
    @Field(type = FieldType.Text, store = true)
    private String psnNo;
    @ApiModelProperty(value = "出入院诊断类别")
    @Field(type = FieldType.Text, store = true)
    private String inoutDiagType;
    @ApiModelProperty(value = "诊断类别")
    @Field(type = FieldType.Text, store = true)
    private String diagType;
    @ApiModelProperty(value = "主诊断标志")
    @Field(type = FieldType.Text, store = true)
    private String maindiagFlag;
    @ApiModelProperty(value = "诊断排序号")
    @Field(type = FieldType.Text, store = true)
    private String diagSrtNo;
    @ApiModelProperty(value = "诊断代码")
    @Field(type = FieldType.Text, store = true)
    private String diagCode;
    @ApiModelProperty(value = "诊断名称")
    @Field(type = FieldType.Text, store = true)
    private String diagName;
    @ApiModelProperty(value = "诊断科室")
    @Field(type = FieldType.Text, store = true)
    private String diagDept;
    @ApiModelProperty(value = "诊断医师代码")
    @Field(type = FieldType.Text, store = true)
    private String diagDrCode;
    @ApiModelProperty(value = "诊断医师姓名")
    @Field(type = FieldType.Text, store = true)
    private String diagDrName;
    @ApiModelProperty(value = "诊断时间")
    @Field(type = FieldType.Date, store = true)
    private Date diagTime;
}
    
