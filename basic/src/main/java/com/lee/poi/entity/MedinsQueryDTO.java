package com.lee.poi.entity;

import com.lee.poi.utils.ApiModel;
import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/9/22 14:28
 */
@Data
@ApiModel("稽核扣款实扣信息DTO")
public class MedinsQueryDTO {

    @ApiModelProperty(value = "医疗机构名称")
    private String medinsName;

    @ApiModelProperty(value = "零售药店名称")
    private String rtalPhacName;

    @ApiModelProperty(value = "机构类型")
    private String medinsType;

    @ApiModelProperty(value = "机构等级")
    private String medinslv;

    @ApiModelProperty(value = "医保区划列表")
    private List<String> admdvsList;

    @ApiModelProperty(value = "医保区划")
    private String admdvs;

    @ApiModelProperty(value = "是否使用权限")
    private boolean usePermission;

    @ApiModelProperty(value = "账户id")
    private String uactId;
}
