package com.lee.poi.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @program: hsa-sims-acq
 * @see： cn.hsa.acq.esinit.model.OrderSubESDO
 * @description: [处方/费用明细、医嘱信息]-ES映射对象
 * @author: taominyao
 * @create: 2023-03-15 13:56
 **/
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class OrderSubES {
    @ApiModelProperty(value = "记账流水号（mdtrtId+bkkpSn=费用明细ID）")
    @Id
    @Field(type = FieldType.Text, store = true)
    private String bkkpSn;
    @ApiModelProperty(value = "费用发生时间")
    @Field(type = FieldType.Date, store = true)
    private Date feeOcurTime;
    @ApiModelProperty(value = "结算ID")
    @Field(type = FieldType.Text, store = true)
    private String setlId;
    @ApiModelProperty(value = "处方/医嘱号")
    @Field(type = FieldType.Text, store = true)
    private String rxDrordNo;
    @ApiModelProperty(value = "数量")
    @Field(type = FieldType.Double, store = true)
    private Double cnt;
    @ApiModelProperty(value = "单价")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal pric;
    @ApiModelProperty(value = "明细项目费用总额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal detItemFeeSumamt;
    @ApiModelProperty(value = "全自费金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal fulamtOwnpayAmt;
    @ApiModelProperty(value = "超限价自费费用")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal overlmtSelfpay;
    @ApiModelProperty(value = "先行自付金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal preselfpayAmt;
    @ApiModelProperty(value = "符合范围金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal inscpAmt;
    @ApiModelProperty(value = "公务员床位费金额")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal cvlservBedfeeAmt;
    @ApiModelProperty(value = "收费项目等级")
    @Field(type = FieldType.Text, store = true)
    private String chrgitmLv;
    @ApiModelProperty(value = "医保目录编码")
    @Field(type = FieldType.Text, store = true)
    private String hilistCode;
    @ApiModelProperty(value = "医保目录名称")
    @Field(type = FieldType.Text, store = true)
    private String hilistName;
    @ApiModelProperty(value = "目录类别")
    @Field(type = FieldType.Text, store = true)
    private String listType;
    @ApiModelProperty(value = "医药机构目录编码")
    @Field(type = FieldType.Text, store = true)
    private String medinsListCodg;
    @ApiModelProperty(value = "医药机构目录名称")
    @Field(type = FieldType.Text, store = true)
    private String medinsListName;
    @ApiModelProperty(value = "医疗收费项目类别")
    @Field(type = FieldType.Text, store = true)
    private String medChrgitmType;
    @ApiModelProperty(value = "规格（例如:0.25g×12片/盒）")
    @Field(type = FieldType.Text, store = true)
    private String spec;
    @ApiModelProperty(value = "计价单位---从 西药中成药、民族药、自制剂等表中获取")
    @Field(type = FieldType.Text, store = true)
    private String specUnit;
    @ApiModelProperty(value = "剂型名称")
    @Field(type = FieldType.Text, store = true)
    private String dosformName;
    @ApiModelProperty(value = "开单科室编码")
    @Field(type = FieldType.Text, store = true)
    private String bilgDeptCodg;
    @ApiModelProperty(value = "开单科室名称")
    @Field(type = FieldType.Text, store = true)
    private String bilgDeptName;
    @ApiModelProperty(value = "开单医师代码")
    @Field(type = FieldType.Text, store = true)
    private String bilgDrCode;
    @ApiModelProperty(value = "开单医师姓名")
    @Field(type = FieldType.Text, store = true)
    private String bilgDrName;
    @ApiModelProperty(value = "出院带药标志 0否 1是 （医嘱行为）")
    @Field(type = FieldType.Text, store = true)
    private String dscgTkdrugFlag;
    @ApiModelProperty(value = "医嘱类别---1临时医嘱 2长期医嘱 3备用医嘱 - 医嘱表")
    @Field(type = FieldType.Text, store = true)
    private String drordType;
    @ApiModelProperty(value = "医嘱开始日期 - 医嘱表")
    @Field(type = FieldType.Date, store = true)
    private Date drordBegntime;
    @ApiModelProperty(value = "医嘱停止日期 - 医嘱表")
    @Field(type = FieldType.Date, store = true)
    private Date drordEndtime;
    @ApiModelProperty(value = "（下达）医嘱的科室标识 - 医嘱表")
    @Field(type = FieldType.Text, store = true)
    private String drordDeptCodg;
    @ApiModelProperty(value = "（下达）医嘱科室名称 - 嘱表")
    @Field(type = FieldType.Text, store = true)
    private String drordDeptName;
    @ApiModelProperty(value = "开处方(医嘱)医生标识 - 医嘱表")
    @Field(type = FieldType.Text, store = true)
    private String drordDrCode;
    @ApiModelProperty(value = "开处方(医嘱)医生姓名")
    @Field(type = FieldType.Text, store = true)
    private String drordDrName;
    @ApiModelProperty(value = "处方拓展字段")
    @Field(type = FieldType.Text, store = true)
    private String extendOrderFields;

    @ApiModelProperty(value = "实际数量")
    @Field(type = FieldType.Double, store = true)
    private Double realityCnt;

    @ApiModelProperty(value = "实际交易费用")
    @Field(type = FieldType.Double, store = true)
    private BigDecimal realityAmt;

    @ApiModelProperty(value = "退费标识 0-非退费 1-退费")
    @Field(type = FieldType.Text, store = true)
    private String refundFlag;
}
    
