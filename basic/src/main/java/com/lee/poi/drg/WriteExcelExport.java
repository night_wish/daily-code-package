package com.lee.poi.drg;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.math.BigDecimal;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/3/12 13:55
 */
public class WriteExcelExport {

    public static void main(String[] args) throws IOException {
        InputStream excelStream = WriteExcelExport.class.getResourceAsStream("/excelTemplates/drgMonAloSttTemplate.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(excelStream);
        XSSFSheet sheet = workbook.getSheetAt(0);
        sheet.getRow(0).getCell(0).setCellValue("测试基本医疗保险住院医疗费(DRG)拨付表");//表头
        sheet.getRow(2).getCell(2).setCellValue("20240312");//拨付年月
        sheet.getRow(2).getCell(8).setCellValue("职工基本医疗保险");//基金类型拨付类型
        sheet.getRow(3).getCell(2).setCellValue("测试医疗机构");//医疗机构
        sheet.getRow(3).getCell(8).setCellValue("fixMedinsCode001");//医疗机构编码
        sheet.getRow(2).getCell(10).setCellValue("123456789");//月每积分费用

        //合计月度预付费用
        sheet.getRow(6).getCell(3).setCellValue("63");//合计分组病例数
        sheet.getRow(6).getCell(4).setCellValue("64");//合计月度点数
        sheet.getRow(6).getCell(7).setCellValue("67");//合计预付总费用

        //本月正常分组月度预付费用
        sheet.getRow(7).getCell(3).setCellValue("73");//本月正常分组病例数
        sheet.getRow(7).getCell(4).setCellValue("74");//本月正常月度点数
        sheet.getRow(7).getCell(7).setCellValue("77");//本月正常预付总费用

        //按床日月度预付费用
        sheet.getRow(8).getCell(3).setCellValue("83");//按床日分组病例数
        sheet.getRow(8).getCell(4).setCellValue("84");//按床日月度点数
        sheet.getRow(8).getCell(7).setCellValue("87");//按床日预付总费用

        //新技术分组月度预付费用
        sheet.getRow(9).getCell(3).setCellValue("93");//新技术分组病例数
        sheet.getRow(9).getCell(4).setCellValue("94");//新技术月度点数
        sheet.getRow(9).getCell(7).setCellValue("97");//新技术预付总费用

        //疗效价值付费月度预付费用
        sheet.getRow(10).getCell(3).setCellValue("103");//疗效价值付费分组病例数
        sheet.getRow(10).getCell(4).setCellValue("104");//疗效价值付费月度点数
        sheet.getRow(10).getCell(7).setCellValue("107");//疗效价值付费预付总费用

        //跨月退费月度预付费用
        sheet.getRow(11).getCell(3).setCellValue("113");//跨月退费分组病例数
        sheet.getRow(11).getCell(4).setCellValue("114");//跨月退费月度点数
        sheet.getRow(11).getCell(7).setCellValue("117");//跨月退费预付总费用

        sheet.getRow(14).getCell(3).setCellValue("143");//特病单议分组病例数
        sheet.getRow(14).getCell(4).setCellValue("144");//特病单议追加点数
        sheet.getRow(14).getCell(7).setCellValue("147");//特病单议追加费用

        sheet.getRow(17).getCell(3).setCellValue("173");//审核扣款费用-合计
        sheet.getRow(18).getCell(3).setCellValue("183");//审核扣款
        sheet.getRow(19).getCell(3).setCellValue("193");//调整事项
        sheet.getRow(20).getCell(3).setCellValue("203");//其他事项

        sheet.getRow(23).getCell(4).setCellValue("234");//医疗总费用
        sheet.getRow(24).getCell(4).setCellValue("244");//医疗保障基金支付合计
        sheet.getRow(26).getCell(4).setCellValue("264");//统筹基金支出
        sheet.getRow(27).getCell(4).setCellValue("274");//个人账户支出
        sheet.getRow(28).getCell(4).setCellValue("284");//公务员医疗补助资金支出
        sheet.getRow(29).getCell(4).setCellValue("294");//大额医疗补助基金支出
        sheet.getRow(30).getCell(4).setCellValue("304");//大病补充医疗保险基金支出
        sheet.getRow(31).getCell(4).setCellValue("314");//补充医疗保险基金支出
        sheet.getRow(32).getCell(4).setCellValue("324");//伤残人员医疗保障基金支出
        sheet.getRow(33).getCell(4).setCellValue("334");//医疗救助基金支出
        sheet.getRow(34).getCell(4).setCellValue("344");//其它基金支付
        sheet.getRow(35).getCell(4).setCellValue("354");//个人现金支付
        sheet.getRow(36).getCell(4).setCellValue("364");//其他基金应支付
        sheet.getRow(37).getCell(4).setCellValue("374");//其他基金非月度拨付
        sheet.getRow(38).getCell(4).setCellValue("384");//其他基金月度拨付

        sheet.getRow(23).getCell(9).setCellValue("239");//医保月度预付金额
        sheet.getRow(24).getCell(9).setCellValue("249");//预付比例
        sheet.getRow(25).getCell(9).setCellValue("259");//本月统筹基金应预付金额(含核减或追加、扣款)
        sheet.getRow(26).getCell(9).setCellValue("269");//本月其他基金月度拨付
        sheet.getRow(27).getCell(9).setCellValue("279");//本月医保基金应预付金额(统筹+其他基金月度拨付
        sheet.getRow(28).getCell(9).setCellValue("289");//本月统筹基金
        sheet.getRow(31).getCell(9).setCellValue("319");//个人账户支出
        sheet.getRow(32).getCell(9).setCellValue("329");//公务员医疗补助资金支出
        sheet.getRow(33).getCell(9).setCellValue("339");//大额医疗补助基金支出
        sheet.getRow(34).getCell(9).setCellValue("349");//大病补充医疗保险基金支出
        sheet.getRow(35).getCell(9).setCellValue("359");//补充医疗保险基金支出
        sheet.getRow(36).getCell(9).setCellValue("369");//伤残人员医疗保障基金支出
        sheet.getRow(37).getCell(9).setCellValue("379");//医疗救助基金支出
        sheet.getRow(38).getCell(9).setCellValue("389");//其它基金支付
        sheet.getRow(39).getCell(9).setCellValue("399");//特病单议统筹基金

        try (OutputStream fileOut = new FileOutputStream("C:\\Users\\james\\Desktop\\1.xlsx")) {  //获取文件流
            workbook.write(fileOut);   //将workbook写入文件流
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
