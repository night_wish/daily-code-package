package com.lee.poi.jiangxi;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.NumberFormat;
import java.util.*;

/**
 * @author Lee
 * @version 1.0
 * @description 江西对比两个excel的内容
 * @date 2024/7/8 18:12
 */
@Slf4j
public class CompareExcelDataUtils {

    //9769
    private static final String sourcePath = "C:\\Users\\james\\Desktop\\aft.xlsx";

    private static final String sourceSheetName = "Sheet1";

    //5178
    private static final String destPath = "C:\\Users\\james\\Desktop\\5177有关联项.xlsx";

    private static final String destSheetName = "Sheet1";

    public static void main(String[] args) throws IOException {
        Set<String> sourceDocIds = readExcel(sourcePath, sourceSheetName);
        Set<String> descDocIds = readExcel(destPath, destSheetName);
        log.info("source size:{}",sourceDocIds.size());
        log.info("desc size:{}",descDocIds.size());
        sourceDocIds.removeAll(descDocIds);
        log.info("finished size:{}",sourceDocIds.size());
        sourceDocIds.forEach(t->{
            System.out.println("'"+t+"',");
        });

    }

    public static Set<String> readExcel(String path, String sheetName) throws IOException {
        Set<String> resSet = new HashSet<>();
        //创建输入流
        FileInputStream fileInputStream = new FileInputStream(path);
        //获得文档
        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
        //根据name获取sheet表
        XSSFSheet sheet = workbook.getSheet(sheetName);

        int lastRow = sheet.getLastRowNum();//获得行数，下标从0开始
        XSSFRow row = sheet.getRow(0);//获取第二行（第一行一般是标题）
        int lastCell = row.getLastCellNum();//获得列数，下标从1开始

        for (int i = 0; i <= lastRow; i++) { //从第二行开始读
            row = sheet.getRow(i);
            if (row != null) {
                String docId = row.getCell(0).getStringCellValue();
                resSet.add(docId);
            }
        }
        return resSet;
    }
}
