package com.lee.poi;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/5/9 16:33
 */
@Slf4j
public class InportExcelToString {

    public static void main(String[] args) throws IOException {
        String filePath = "C:\\Users\\le'e\\Desktop\\菜单权限编码.xlsx";
        FileInputStream fis = new FileInputStream(filePath);

        Workbook wb = new XSSFWorkbook(fis);

        Sheet sheet = wb.getSheetAt(15);
        int lastRowNum = sheet.getLastRowNum();
        StringBuffer sb = new StringBuffer();
        for (int i=2;i<=lastRowNum;i++){
            Row row = sheet.getRow(i);
            Cell cell = row.getCell(0);
            log.info(cell.getStringCellValue());
            sb.append(",").append(cell.getStringCellValue());
        }
        log.info(sb.toString());
    }
}
