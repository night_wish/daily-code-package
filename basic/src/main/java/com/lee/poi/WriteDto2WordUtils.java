package com.lee.poi;

import com.lee.poi.entity.*;
import com.lee.poi.utils.ApiModelProperty;
import com.lee.poi.utils.Size;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.xwpf.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 详细设计 生成工具
 *
 * @author Lee
 * @version 1.0
 * @description dto转word
 * @date 2022/8/12 9:45
 */
public class WriteDto2WordUtils {

    //不需要转的字段
    private static Map<String, String> filterMap = new HashMap<>();

    public static void main(String[] args) throws IOException {
        String inputUrl = "G:\\AdvcdSpace\\daily-code-package\\basic\\src\\main\\resources\\tmplate\\详细设计说明模板.docx";
        String outputUrl = "G:\\AdvcdSpace\\daily-code-package\\basic\\src\\main\\resources\\tmplate\\new.docx";

        //占位符替换
        String moduleName = "医保管理类规则库";

        Map<String, String> textMap = new HashMap<>();
        textMap.put("${pageTitle}", moduleName);
        textMap.put("${pageCode}", "S010101");
        textMap.put("${pageTitleAbbr}", moduleName + "是对" + moduleName + "的描述，包括对" + moduleName + "的查询、添加、修改、删除等操作");
        textMap.put("${pageEntity}", "RuleTmplRslt");
        textMap.put("${packagePath}", "kng.kngMgt.kngbase");

        //表格处理
        List<RowObj> rowObjList = getDeclaredFieldsInfo(TaskAuditBtchDDTO.class);

        changWord(inputUrl, outputUrl, textMap, rowObjList);
    }


    /**
     * 根据模板生成新word文档
     * 判断表格是需要替换还是需要插入，判断逻辑有$为替换，表格无$为插入
     *
     * @param inputUrl   模板存放地址
     * @param outputUrl  新文档存放地址
     * @param textMap    需要替换的信息集合
     * @param rowObjList 需要插入的表格信息集合
     * @return 成功返回true, 失败返回false
     */
    public static boolean changWord(String inputUrl, String outputUrl, Map<String, String> textMap, List<RowObj> rowObjList) {
        //模板转换默认成功
        boolean changeFlag = true;
        try {
            //获取docx解析对象
            XWPFDocument document = new XWPFDocument(POIXMLDocument.openPackage(inputUrl));
            //解析替换文本段落对象
            changeText(document, textMap);
            //解析替换表格对象
            changeTable(document, textMap);
            //解析替换表格对象
            XWPFTable entityTable1 = document.getTables().get(24);
            insertTable(entityTable1, rowObjList);

            //生成新的word
            File file = new File(outputUrl);
            FileOutputStream stream = new FileOutputStream(file);
            document.write(stream);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
            changeFlag = false;
        }
        return changeFlag;
    }

    //全局替换占位符
    public static void changeText(XWPFDocument document, Map<String, String> textMap) {
        Iterator<XWPFParagraph> iterator = document.getParagraphsIterator();
        XWPFParagraph para;
        while (iterator.hasNext()) {
            para = iterator.next();
            if (!StringUtils.isEmpty(para.getParagraphText())) {
                replaceInPara(para, textMap);
            }
        }
    }


    /**
     * 替换段落中的占位符
     *
     * @param para
     */
    public static void replaceInPara(XWPFParagraph para, Map<String, String> params) {
        // 获取当前段落的文本
        String sourceText = para.getParagraphText();
        // 控制变量
        boolean replace = false;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            if (sourceText.indexOf(key) != -1) {
                Object value = entry.getValue();
                if (value instanceof String) {
                    // 替换文本占位符
                    sourceText = sourceText.replace(key, value.toString());
                    replace = true;
                }
            }
        }
        if (replace) {
            // 获取段落中的行数
            List<XWPFRun> runList = para.getRuns();
            for (int i = runList.size(); i >= 0; i--) {
                // 删除之前的行
                para.removeRun(i);
            }
            // 创建一个新的文本并设置为替换后的值 这样操作之后之前文本的样式就没有了，待改进
            para.createRun().setText(sourceText);
        }
    }

    /**
     * 替换表格中的占位符
     *
     * @param doc
     * @param params
     */
    public static void changeTable(XWPFDocument doc, Map<String, String> params) {
        // 获取文档中所有的表格
        Iterator<XWPFTable> iterator = doc.getTablesIterator();
        XWPFTable table;
        List<XWPFTableRow> rows;
        List<XWPFTableCell> cells;
        List<XWPFParagraph> paras;
        while (iterator.hasNext()) {
            table = iterator.next();
            if (table.getRows().size() > 1) {
                //判断表格是需要替换还是需要插入，判断逻辑有${为替换，
                if (matcher(table.getText()).find()) {
                    rows = table.getRows();
                    for (XWPFTableRow row : rows) {
                        cells = row.getTableCells();
                        for (XWPFTableCell cell : cells) {
                            paras = cell.getParagraphs();
                            for (XWPFParagraph para : paras) {
                                replaceInPara(para, params);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 正则匹配字符串
     *
     * @param str
     * @return
     */
    private static Matcher matcher(String str) {
        Pattern pattern = Pattern.compile("\\$\\{(.+?)\\}", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);
        return matcher;
    }

    //获取实体的注释
    public static List<RowObj> getDeclaredFieldsInfo(Class<?> clazz) {
        List<RowObj> rowObjList = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            if (!filterMap.containsValue(fields[i].getName())) {
                boolean isApiExists = fields[i].isAnnotationPresent(ApiModelProperty.class);
                if (isApiExists) {
                    RowObj rowObj = new RowObj();
                    rowObj.setRowNo(Integer.toString(i));//行号
                    // 获取注解字段名称
                    String name = fields[i].getAnnotation(ApiModelProperty.class).value();
                    rowObj.setChiName(name.trim());//中文名
                    rowObj.setEngName(fields[i].getName());//英文名
                    rowObj.setType(fields[i].getType().getSimpleName().trim());//类型
                    boolean isSizeExists = fields[i].isAnnotationPresent(Size.class);
                    if (isSizeExists) {
                        int size = fields[i].getAnnotation(Size.class).max();
                        rowObj.setRange(Integer.toString(size));//值范围
                    }
                    rowObj.setIsMust("否");
                    rowObj.setDescr("");
                    rowObjList.add(rowObj);
                }
            }
        }
        return rowObjList;
    }


    /**
     * 为表格插入行数，此处不处理表头，所以从第二行开始
     *
     * @param table     需要插入数据的表格
     * @param tableList 插入数据集合
     */
    private static void insertTable(XWPFTable table, List<RowObj> tableList) {
        System.out.println(tableList.size());
        //创建与数据一致的行数
        for (int i = 0; i < tableList.size(); i++) {
            table.createRow();
        }
        int length = table.getRows().size();
        System.out.println(length);
        for (int i = 1; i < length - 1; i++) {
            XWPFTableRow newRow = table.getRow(i);
            List<XWPFTableCell> cells = newRow.getTableCells();
            cells.get(0).setVerticalAlignment(XWPFTableCell.XWPFVertAlign.BOTH);
            cells.get(0).setText(tableList.get(i - 1).getRowNo());
            cells.get(1).setText(tableList.get(i - 1).getChiName());
            cells.get(2).setText(tableList.get(i - 1).getEngName());
            cells.get(3).setText(tableList.get(i - 1).getType());
            cells.get(4).setText(tableList.get(i - 1).getRange());
            cells.get(5).setText(tableList.get(i - 1).getIsMust());
            cells.get(6).setText(tableList.get(i - 1).getDescr());
        }
    }
}
