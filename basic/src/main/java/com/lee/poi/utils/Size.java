package com.lee.poi.utils;

import com.nimbusds.jose.Payload;

import java.lang.annotation.*;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/8/11 14:30
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Size {
    String message() default "{javax.validation.constraints.Size.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int min() default 0;

    int max() default 2147483647;

}
