package com.lee.poi.utils;

import java.lang.annotation.*;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/8/11 14:09
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ApiModel {
    String value() default "";

    String description() default "";

    Class<?> parent() default Void.class;

    String discriminator() default "";

    Class<?>[] subTypes() default {};

    String reference() default "";
}
