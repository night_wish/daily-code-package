package com.lee.poi.easyexcel.controller;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/12/3 15:28
 */

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import com.lee.json.WrapperResponse;
import com.lee.mybatis.mapper.DrugListMapper;
import com.lee.poi.easyexcel.dto.DrugListDto;
import com.lee.poi.easyexcel.dto.KbmsDrugImportDTO;
import com.lee.utils.IDUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/12/3 11:21
 */
@RestController
@RequestMapping("/web/knowledgeFile")
public class DrugKnowledgeFileController {


    @Resource
    private DrugListMapper drugListDAO;

    private static final Integer batchSize = 500;

    /**
     * 插入药品说明书
     */
    @PostMapping("/insertDurgKng")
    public WrapperResponse updateKnowledgeData(@RequestParam("excel") MultipartFile excel) throws IOException, InterruptedException {
        if (excel == null) {
            return WrapperResponse.fail("请上传excel文件", null);
        }
        InputStream is = excel.getInputStream();
        Date currentDate = new Date();
        EasyExcel.read(is, KbmsDrugImportDTO.class, new PageReadListener<KbmsDrugImportDTO>(dataList -> {
            for (KbmsDrugImportDTO dto : dataList) {
                dto.setRid(UUID.randomUUID().toString().replace("-",""));
                dto.setDrugListId(UUID.randomUUID().toString().replace("-",""));
                dto.setAdmdvs("420000");
                dto.setCrteTime(currentDate);
                dto.setKngFlag("标准");
                //未上报
                dto.setRpupStas("0");
                // 修改人
                dto.setModier("system");
            }
            drugListDAO.batchSaveDrugList(dataList);
        },batchSize)).sheet().headRowNumber(1).doRead();
        is.close();
        return WrapperResponse.success("成功",null);
    }


}
