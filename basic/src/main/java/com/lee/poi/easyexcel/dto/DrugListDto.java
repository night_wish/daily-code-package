package com.lee.poi.easyexcel.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * <p>
 * 药品说明书(B014)
 * </p>
 *
 * @author Wang.FM Create on 2020/11/12 14:25
 * @version 1.0
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DrugListDto {

    /**
     * 大类名称
     */
    private String genCategoryName;

    /**
     * 药品代码
     */
    private String drugCode;

    /**
     * 药品通用名(注册名称)
     */
    private String genericName;

    /**
     * DR编码
     */
    private String drCode;

    /**
     * 商品名称
     */
    private String tradName;

    /**
     * 注册剂型
     */
    private String dosageForm;

    /**
     * 注册规格
     */
    private String specification;

    /**
     * 成分
     */
    private String component;

    /**
     * 适应症
     */
    private String indication;

    /**
     * 用法用量
     */
    private String usageDosage;

    /**
     * 不良反应
     */
    private String adverseReactions;

    /**
     * 禁忌
     */
    private String taboo;


    /**
     * 注意事项
     */
    private String careMatter;

    /**
     * 孕妇及哺乳期妇女用药
     */
    private String womanMedicine;

    /**
     * 儿童用药
     */
    private String childrenMedicine;

    /**
     * 老年人用药
     */
    private String agednessMedicine;

    /**
     * 药物相互作用
     */
    private String medicineInteracts;

    /**
     * 药物过量
     */
    private String medicineBellyful;

    /**
     * 药理毒理
     */
    private String pharmacologyPoisons;

    /**
     * 药代动力学
     */
    private String pharmacokinetics;

    /**
     * 药品企业
     */
    private String drugEnterprise;

    /**
     * 批准文号
     */
    private String approveDocNo;

    /**
     * 知识标识 0：标准； 1：自定义
     */
    private String kngFlag;

    /**
     * 上报状态 0：未上报；1：已上报
     */
    private String rpupStas;

    /**
     * 医保区划
     */
    private String admdvs;


    /**
     * 药品不良反应
     */
    private String sideEffect;

    /**
     * 药品配伍禁忌
     */
    private String injectionVitroCompatibility;
    /**
     * 修改人
     */
    private String modier;

    private String updateBy;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime = new Date();

    private Integer createBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private String id;

    private String drugReactions;

    private String drugCompaTaboos;

    private String drugListId;
}
