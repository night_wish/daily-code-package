package com.lee.poi.easyexcel.dto;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.lee.poi.easyexcel.StringLengthLimitConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/12/3 11:36
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class KbmsDrugImportDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelIgnore
    private String drugListId;

    @ExcelProperty(value = "大类名称", index = 1, converter = StringLengthLimitConverter.class)
    private String majcls;

    @ExcelProperty(value = "药品代码", index = 2, converter = StringLengthLimitConverter.class)
    private String drugCode;

    @ExcelProperty(value = "注册名称", index = 3, converter = StringLengthLimitConverter.class)
    private String drugGenname;

    @ExcelProperty(value = "DR编码", index = 4, converter = StringLengthLimitConverter.class)
    private String drCode;

    @ExcelProperty(value = "商品名称", index = 5, converter = StringLengthLimitConverter.class)
    private String prodname;

    @ExcelProperty(value = "注册剂型", index = 6, converter = StringLengthLimitConverter.class)
    private String regDosform;

    @ExcelProperty(value = "注册规格", index = 7, converter = StringLengthLimitConverter.class)
    private String spec;

    @ExcelProperty(value = "成分", index = 8, converter = StringLengthLimitConverter.class)
    private String ing;

    @ExcelProperty(value = "适应症", index = 9, converter = StringLengthLimitConverter.class)
    private String inda;

    @ExcelProperty(value = "用法用量", index = 10, converter = StringLengthLimitConverter.class)
    private String usedUseamt;

    @ExcelProperty(value = "不良反应", index = 11, converter = StringLengthLimitConverter.class)
    private String defs;

    @ExcelProperty(value = "禁忌", index = 12, converter = StringLengthLimitConverter.class)
    private String tabo;

    @ExcelProperty(value = "注意事项", index = 13, converter = StringLengthLimitConverter.class)
    private String mnan;

    @ExcelProperty(value = "孕妇及哺乳期妇女用药", index = 14, converter = StringLengthLimitConverter.class)
    private String womMedc;

    @ExcelProperty(value = "儿童用药", index = 15, converter = StringLengthLimitConverter.class)
    private String chldMedc;

    @ExcelProperty(value = "老年人用药", index = 16, converter = StringLengthLimitConverter.class)
    private String eldPatnMedc;

    @ExcelProperty(value = "药物相互作用", index = 17, converter = StringLengthLimitConverter.class)
    private String mednItrcEfft;

    @ExcelProperty(value = "药物过量", index = 18, converter = StringLengthLimitConverter.class)
    private String overQunt;

    @ExcelProperty(value = "药理毒理", index = 19, converter = StringLengthLimitConverter.class)
    private String phamPosn;

    @ExcelProperty(value = "药代动力学", index = 20, converter = StringLengthLimitConverter.class)
    private String phamDscr;


    //修改日期 21

    //包装材质 22

    //最小制剂单位 23

    //最小包装单位  24


    @ExcelProperty(value = "药品企业", index = 25, converter = StringLengthLimitConverter.class)
    private String drugEntp;


    //上市许可持有人 26

    @ExcelProperty(value = "批准文号", index = 27, converter = StringLengthLimitConverter.class)
    private String aprvno;

    //药品本位码 28

    //市场状态 29

    //医保药品名称 30

    //甲乙丙类标识 31

    //医保剂型 32

    //备注 33

    //协议期内谈判药品标识 34

    //谈判药品协议有效期起始日期  35

    //谈判药品协议有效期截止日期  36


    /**
     * 审核日期
     */
    @ExcelIgnore
    private String chkDate;

    /**
     * 审核人
     */
    @ExcelIgnore
    private String chker;

    /**
     * 启用状态
     */
    @ExcelIgnore
    private String enabStas;

    /**
     * 修改人
     */
    @ExcelIgnore
    private String modier;

    /**
     * 知识标识
     */
    @ExcelIgnore
    private String kngFlag;

    /**
     * 上报状态
     */
    @ExcelIgnore
    private String rpupStas;

    /**
     * 医保区划
     */
    @ExcelIgnore
    private String admdvs;

    /**
     * 药品不良反应
     */
    @ExcelIgnore
    private String drugDefs;

    /**
     * 药品配伍禁忌
     */
    @ExcelIgnore
    private String drugTabo;

    /**
     * 数据唯一记录号
     */
    @ExcelIgnore
    private String rid;

    /**
     * 数据创建时间
     */
    @ExcelIgnore
    private Date crteTime;

    /**
     * 数据更新时间
     */
    @ExcelIgnore
    private Date updtTime;
}
