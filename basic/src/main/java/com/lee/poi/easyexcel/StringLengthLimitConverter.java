package com.lee.poi.easyexcel;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/12/4 10:31
 */
public class StringLengthLimitConverter implements Converter<String> {

    @Override
    public String convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        if (cellData == null || StringUtils.isBlank(cellData.getStringValue())) {
            return null;
        }
        String value = cellData.getStringValue();
        if(value.length()>1500){
            value = value.substring(0,1500)+"......";
        }
        return value;
    }

    @Override
    public WriteCellData<?> convertToExcelData(String value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        if (value == null) {
            return null;
        }
        // 创建 WriteCellData 对象
        WriteCellData<String> writeCellData = new WriteCellData<>(value);

        return writeCellData;
    }
}
