package com.lee.poi.nanjing;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023-12-09 10:52
 */
public class CompareTwoExcel {

    private static final String sourcePath = "C:\\Users\\le'e\\Desktop\\266机构带区划_231208.xlsx";

    private static final String destPath = "C:\\Users\\le'e\\Desktop\\266机构带区划_231208.xlsx";

    public static void main(String[] args) throws IOException {
        Map<String, String> sourceMap = readExcel(sourcePath, "Sheet1");

        compareExcel(destPath,"Sheet2",sourceMap);

    }

    //判5改9
    public static void compareExcel(String path, String sheetName,Map<String, String> sourceMap) throws IOException {
        //创建输入流
        FileInputStream fileInputStream = new FileInputStream(path);
        //获得文档
        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
        //根据name获取sheet表
        XSSFSheet sheet = workbook.getSheet(sheetName);

        int lastRow = sheet.getLastRowNum();//获得行数，下标从0开始
        XSSFRow titleRow = sheet.getRow(1);//获取第一行（第一行一般是标题）
        XSSFRow row = sheet.getRow(2);//获取第二行（第一行一般是标题）
        int lastCell = row.getLastCellNum();//获得列数，下标从1开始
        int total = 0;
        for (int i = 1; i <= lastRow; i++) { //从第二行开始写
            row = sheet.getRow(i);
            if (row != null) {
                String hospCode = getCellValue(row.getCell(1)).trim();
                String name = getCellValue(row.getCell(2)).trim();
                if(sourceMap.containsKey(hospCode)){
                    continue;
                }else{
                    total++;
                    System.out.println(hospCode+" 编码不存在============医院名称:"+name+"  total:"+total);
                }
            }
        }
        workbook.close();
    }


    public static Map<String, String> readExcel(String path, String sheetName) throws IOException {
        List<Map<String, String>> resList = new ArrayList<>();
        //创建输入流
        FileInputStream fileInputStream = new FileInputStream(path);
        //获得文档
        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
        //根据name获取sheet表
        XSSFSheet sheet = workbook.getSheet(sheetName);

        int lastRow = sheet.getLastRowNum();//获得行数，下标从0开始
        XSSFRow titleRow = sheet.getRow(1);//获取第一行（第一行一般是标题）
        XSSFRow row = sheet.getRow(2);//获取第二行（第一行一般是标题）
        int lastCell = row.getLastCellNum();//获得列数，下标从1开始

        Map<String, String> rowMap = new HashMap<>();
        for (int i = 1; i <= lastRow; i++) { //从第二行开始读
            row = sheet.getRow(i);
            if (row != null) {
                String content = getCellValue(row.getCell(1));
                rowMap.put(getCellValue(row.getCell(0)).trim(),content);
                System.out.println("编码:"+getCellValue(row.getCell(0))+"  名称："+content);
            }
        }
        System.out.println("总共："+rowMap.size()+"条记录");
        return rowMap;
    }

    private static String getCellValue(Cell cell) {
        String cellValue = "";
        // 以下是判断数据的类型
        if(cell==null){
            return cellValue;
        }
        switch (cell.getCellTypeEnum()) {
            case NUMERIC: // 数字
                if (org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    cellValue = sdf.format(org.apache.poi.ss.usermodel.DateUtil.getJavaDate(cell.getNumericCellValue())).toString();
                } else {
                    DataFormatter dataFormatter = new DataFormatter();
                    cellValue = dataFormatter.formatCellValue(cell);
                }
                break;
            case STRING: // 字符串
                cellValue = cell.getStringCellValue();
                break;
            case BOOLEAN: // Boolean
                cellValue = cell.getBooleanCellValue() + "";
                break;
            case FORMULA: // 公式
                cellValue = cell.getCellFormula() + "";
                break;
            case BLANK: // 空值
                cellValue = "";
                break;
            case ERROR: // 故障
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue;
    }

}
