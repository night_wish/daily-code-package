package com.lee.hadoop.mapReduce.wordCount;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * @description 单词计数Reducer
 * @author Lee
 * @date 2024/4/8 16:57
 * @version 1.0
 * @入参：
 * KEYIN, Mapper的输入Key, 这里是 输入的文字，Text
 * VALUEIN, Mapper的输入value，这里是输入文字的数量，IntWritable
 * KEYOUT,  Mapper的输出Key，这里是 输出的文字 Text
 * VALUEOUT  Mapper的输出value，这里是输出文件的数 IntWritable
 */
public class WordCountReducer extends Reducer<Text, IntWritable,Text, IntWritable> {
    private IntWritable outValue = new IntWritable();
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        int count = 0;
        for (IntWritable value : values) {
            count = count + value.get();
        }
        outValue = new IntWritable(count);
        //写出
        context.write(key,outValue);
    }
}
