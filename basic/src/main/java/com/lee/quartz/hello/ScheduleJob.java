package com.lee.quartz.hello;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Date;

/**
 * 定时任务测试类
 */
public class ScheduleJob {

    public static void main(String[] args) throws Exception {
        //创建调度器(scheduler)： new StdSchedulerFactory().getScheduler()
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        //创建任务实例(jobDetail)
        JobDetail jobDetail = JobBuilder.newJob(HelloJob.class)//加载任务类
                .withIdentity("helloJob","defaultJobGroup")//指明任务名称+任务组名称
                .build();

        //创建触发器(Trigger)
        SimpleTrigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("HelloTrigger", "defaultTriggerGroup") //指明触发器名称+触发器组名称
                .startNow()//马上开启触发器  或startAt()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(5).repeatForever())//每5秒执行一次 SimpleTrigger
                .build();

        //让调度器关联  任务+触发器
        scheduler.scheduleJob(jobDetail, trigger);
        //启动
        scheduler.start();
    }

}
