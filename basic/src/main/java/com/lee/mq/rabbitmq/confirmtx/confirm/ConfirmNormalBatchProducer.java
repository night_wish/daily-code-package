package com.lee.mq.rabbitmq.confirmtx.confirm;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.AMQP.BasicProperties.Builder;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * confirm-普通批量模式：消息生产者(保证生产者和消息队列之间的信息安全)
 *
 * 普通批量 confirm 模式：每发送一批消息后，调用 waitForConfirms()方法，等待服务器端 confirm。
 * （多条消息发送同时监听，假如多条消息中有成功的和失败的，那么这多条消息都会返回重发！）
 */
@Slf4j
public class ConfirmNormalBatchProducer {

    private static final String QUEUE_NAME = "confirm_queue";

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        //创建链接
        Connection conn = MqConnectUtils.getMqConnection();
        //创建通道
        Channel channel = conn.createChannel();
        //创建消息队列--消息队列持久化 durable设为true（可以不持久化，跟confirm没关系）
        channel.queueDeclare(QUEUE_NAME,true,false,false,null);
        //开启confirm模式
        channel.confirmSelect();


        for (int i = 0; i <10 ; i++) {
            //发送消息
            String msg = "this is my eighth message";
            //消息持久化(可以不持久化，跟confirm没关系)
            Builder builder = new Builder();
            builder.deliveryMode(2);
            BasicProperties properties = builder.build();

            channel.basicPublish("",QUEUE_NAME,properties,msg.getBytes());
            log.info("producer send msg : {}",msg);
        }


        //confirm普通模式：判断消息是否发送成功
        if(!channel.waitForConfirms()){
            log.info("msg send fail, try agin!");
        }else{
            log.info("msg send success!");
        }


        //关闭链接
        channel.close();
        conn.close();
    }
}
