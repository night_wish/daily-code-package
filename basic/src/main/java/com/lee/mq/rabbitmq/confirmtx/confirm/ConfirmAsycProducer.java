package com.lee.mq.rabbitmq.confirmtx.confirm;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.AMQP.BasicProperties.*;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmListener;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.TimeoutException;

/**
 * confirm-异步模式：消息生产者(保证生产者和消息队列之间的信息安全)
 *
 * 异步 confirm 模式：提供一个回调方法，服务端 confirm 了一条或者多条消息后 Client 端会回调这个方法。
 *
 * broker回传给生产者的确认消息中deliver-tag域包含了确认消息的序列号，此外broker也可以设置basic.ack的multiple域，
 * 表示这个序列号之前的所有消息都已经得到了处理
 */
@Slf4j
public class ConfirmAsycProducer {

    private static final String QUEUE_NAME = "confirm_queue";

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        //创建链接
        Connection conn = MqConnectUtils.getMqConnection();
        //创建通道
        Channel channel = conn.createChannel();
        //创建消息队列--消息队列持久化 durable设为true（可以不持久化，跟confirm没关系）
        channel.queueDeclare(QUEUE_NAME,true,false,false,null);
        //开启confirm模式
        channel.confirmSelect();

        //创建一个有序集合用于保存每次消息的deliver-tag
        SortedSet<Long> confirmSortedSet = new TreeSet<>();

        //设置监听事件，异步监听每条消息的成功与失败
        channel.addConfirmListener(new ConfirmListener() {

            /**
             * 成功时回调
             *
             * 每次回调handleAck方法，就从confirmSortedSet中删除
             * 一条（muiltiple=false）或多条（muiltiple=true）记录
             */
            @Override
            public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                if(multiple){
                    //当发送多条消息时，他返回可能多条消息的接受情况，也可能返回单条消息的情况
                    log.info("producer handleAck method multiple = true");
                    confirmSortedSet.headSet(deliveryTag+1).clear();
                }else {
                    log.info("producer handleAck method multiple = false");
                    confirmSortedSet.remove(deliveryTag);
                }
            }

            /**
             * 失败时回调
             *
             * 每次回调handleAck方法，就从confirmSortedSet中删除
             * 一条（muiltiple=false）或多条（muiltiple=true）记录
             */
            @Override
            public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                if(multiple){
                    //当发送多条消息时，他返回可能多条消息的接受情况，也可能返回单条消息的情况
                    log.info("producer handleNack method multiple = true");
                    confirmSortedSet.headSet(deliveryTag+1).clear();
                }else {
                    log.info("producer handleNack method multiple = false");
                    confirmSortedSet.remove(deliveryTag);
                }
            }
        });


        //消息持久化
        Builder builder = new Builder();
        builder.deliveryMode(2);
        BasicProperties properties = builder.build();

        //发送消息
        for (int i = 0; i <50 ; i++) {
            Thread.sleep(3000);
            String msg = "this is my ninth message "+ i;
            //获取下一条消息的tag编号,并将其添加进
            long nextPublishSeqNo = channel.getNextPublishSeqNo();
            channel.basicPublish("",QUEUE_NAME,properties,msg.getBytes());
            confirmSortedSet.add(nextPublishSeqNo);
        }


    }
}
