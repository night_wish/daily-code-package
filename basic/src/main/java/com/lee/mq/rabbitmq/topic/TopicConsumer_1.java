package com.lee.mq.rabbitmq.topic;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * topic主题模式：消息生产者
 *
 * 我们之前写了fanout方式进行消息订阅转发；
 * 使用Direct携带routingKey的方式，实现按照指定的routingKey推送消息。
 *
 *
 * 当我们采取exchange的direct方式进行消息推送的时候，我们却有几项难题：
 * 1、测试发现，当指定具体的key后，只有当交换机exchange的routingKey和消息消费者与交换机绑定的routingKey一致时，
 *    对应的消息消费者才能够获取到对应的消息。
 * 2、每个消费者都需要采取channel.queueBind的形式进行对应的routingKey的绑定，如果有多个routingKey则需要在代码中
 *    添加多个channel.queueBind，相比较而言不利于代码的维护。
 *    channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, ROUTING_KEY);
 *
 *  为了减少上面的疑虑，rabbitmq给我们推出了topics方式：
 *  就像数据库sql查询的模糊匹配一样，
 *  " # " 匹配一个或者多个词。
 *  " * "匹配一个词。
 */
@Slf4j
public class TopicConsumer_1 {

    private static final String EXCHANGE_NAME = "exchange_topic";

    private static final String QUEUE_NAME = "queue_topic_1";

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建连接
        Connection conn = MqConnectUtils.getMqConnection();
        //创建通道
        Channel channel = conn.createChannel();
        //创建消息队列
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        //绑定消息队列到交换机：消息队列、交换机、路由键
        channel.queueBind(QUEUE_NAME,EXCHANGE_NAME,"imgs.#");

        //创建消费者
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
            @SneakyThrows
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body,"UTF-8");
                log.info("consumer 1 received msg : {}",msg);

                Thread.sleep(1000);
            }
        };

        //消费者监听
        channel.basicConsume(QUEUE_NAME,true,defaultConsumer);

    }

}
