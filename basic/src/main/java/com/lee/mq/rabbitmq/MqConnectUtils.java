package com.lee.mq.rabbitmq;


import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * mq链接工具
 */
public class MqConnectUtils {

    public static Connection getMqConnection() throws IOException, TimeoutException {
        //1 创建一个链接工厂，并设置链接信息
        ConnectionFactory factory = new ConnectionFactory();

        //地址
        factory.setHost("60.205.254.48");
        //端口
        factory.setPort(5672);
        //虚拟消息服务器，想象成一个数据库
        factory.setVirtualHost("/myvhost");
        //用户名和密码
        factory.setUsername("lee");
        factory.setPassword("ren");

        //2 创建链接
        return factory.newConnection();
    }
}
