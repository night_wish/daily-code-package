package com.lee.mq.rabbitmq.workqueue;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 工作队列：消息消费者 （轮询）
 * simple queue 是 消息消费者和消息生产者一一对应的关系，在实际开发中，生产者发送消息是毫不费力的，
 * 而消费者一般是要跟业务相结合的，消费者收到消息就需要处理。可能需要花费时间，这时候队列中就会积压过多的消息。
 *
 * 现象：
 * 1、消费者一延时 1000ms，消费者二延时 2000ms，但消息数一致。
 * 2、效益一为偶数，消息二为基数。
 * 结论：
 * 这种消息机制叫轮询分发，两者的延迟时间并不会影响到获取到的消息数目。
 * 消息消费为分发处理----模。消息数%2、消息数%n
 */
@Slf4j
public class WorkQueueConsumer_1 {

    private static final String QUEUE_NAME = "work_queue";

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建连接
        Connection conn = MqConnectUtils.getMqConnection();
        //创建通道
        Channel channel = conn.createChannel();
        //声明队列
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);

        //创建消费者
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
            @SneakyThrows
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body,"UTF-8");
                log.info("consumer 1  received msg : {}",msg);

                Thread.sleep(1000);
            }
        };

        //创建监听
        channel.basicConsume(QUEUE_NAME,true,defaultConsumer);

    }
}
