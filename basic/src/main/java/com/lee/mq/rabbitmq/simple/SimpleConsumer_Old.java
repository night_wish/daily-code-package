package com.lee.mq.rabbitmq.simple;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.QueueingConsumer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消息消费者---旧的写法
 */
@Slf4j
public class SimpleConsumer_Old {

    private static final String QUEUE_NAME = "simple_queue";

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        //创建一个链接
        Connection conn = MqConnectUtils.getMqConnection();

        //创建一个通道
        Channel channel = conn.createChannel();

        //定义队列的消费者
        QueueingConsumer queueingConsumer = new QueueingConsumer(channel);

        //消费者监听--队列名称、是否自动确认、消费者
        //autoAck设置为true时，消息队列可以不用在意消息消费者是否处理完消息，一直发送全部消息。
        //当 autoAck设置为true时，也就是自动确认模式，一旦消息队列将消息发送给消息消费者后，就会从内存中将这个消息删除。
        //当autoAck设置为false时，也就是手动模式，如果此时的有一个消费者宕机，消息队列就会将这条消息继续发送给其他的消费者，
        //    这样数据在消息消费者集群的环境下，也就算是不丢失了。
        channel.basicConsume(QUEUE_NAME,true,queueingConsumer);

        //获取数据
        while (true){
            QueueingConsumer.Delivery delivery = queueingConsumer.nextDelivery();
            String msg = new String(delivery.getBody(), "UTF-8");
            log.info("comsumer received msg : {}",msg);
        }
    }
}
