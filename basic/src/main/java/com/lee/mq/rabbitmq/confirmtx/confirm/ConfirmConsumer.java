package com.lee.mq.rabbitmq.confirmtx.confirm;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * confirm：消息消费者(保证生产者和消息队列之间的信息安全)
 */
@Slf4j
public class ConfirmConsumer {

    private static final String QUEUE_NAME = "confirm_queue";

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        //创建链接
        Connection conn = MqConnectUtils.getMqConnection();
        //创建通道
        Channel channel = conn.createChannel();
        //创建消息队列--生产者和消费者必须设置一致
        channel.queueDeclare(QUEUE_NAME,true,false,false,null);

        //创建消费者
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body,"UTF-8");
                log.info("consumer received msg : {}",msg);
            }
        };

        //创建监听
        channel.basicConsume(QUEUE_NAME,true,defaultConsumer);
    }
}
