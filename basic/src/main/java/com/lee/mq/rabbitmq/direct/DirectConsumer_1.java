package com.lee.mq.rabbitmq.direct;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Routing路由模式：消息生产者
 *
 * 之前我们实现消息发布订阅模式时，采用的是fanout。
 * 采取FANOUT设置时，表示不处理路由键，只需要将队列绑定到交换机，发送消息到交换机就会被转发到该与交换机绑定的所有队列。
 *
 * 需要按照指定的routingKey实现向指定的queue中推送消息，就要使用direct方式了
 * 1、消息生产者生产消息，发送至消息交换机，并携带一个routingKey信息。
 * 2、当消息生产者推送消息并携带routingkey至交换机后，此时的交换机只会推送消息至绑定对应routingkey的消息队列中。
 *
 * 消息生产者：
 * 1、消息生产者，生产消息并推送消息至消息交换机，同时设置对应的路由键
 *
 * 消息消费者：
 * 1、消费者，绑定消息队列到对应的消息交换机，同时设置自己的路由键
 * 2、消息消费者可以绑定多个routingKey信息
 *
 * 缺点：
 * 1、测试发现，当指定具体的key后，只有当交换机exchange的routingKey和消息消费者与交换机绑定的routingKey一致时，
 * 对应的消息消费者才能够获取到对应的消息。
 * 2、每个消费者都需要采取channel.queueBind的形式进行对应的routingKey的绑定，如果有多个routingKey则需要在代码中
 * 添加多个channel.queueBind，相比较而言不利于代码的维护。
 *
 * 我们使用topic模型来解决
 */
@Slf4j
public class DirectConsumer_1 {

    private static final String EXCHANGE_NAME = "exchange_direct";

    private static final String QUEUE_NAME = "queue_direct_1";

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建连接
        Connection conn = MqConnectUtils.getMqConnection();
        //创建通道
        Channel channel = conn.createChannel();
        //创建消息队列
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        //绑定消息队列到交换机
        channel.queueBind(QUEUE_NAME,EXCHANGE_NAME,"hebei.handan");

        //创建消费者
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
            /**
             * No-op implementation of {@link Consumer#handleDelivery}.
             *
             * @param consumerTag
             * @param envelope
             * @param properties
             * @param body
             */
            @SneakyThrows
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body,"UTF-8");
                log.info("consumer 1 received msg : {}",msg);

                Thread.sleep(1000);
            }
        };

        //消费者监听
        channel.basicConsume(QUEUE_NAME,true,defaultConsumer);

    }
}
