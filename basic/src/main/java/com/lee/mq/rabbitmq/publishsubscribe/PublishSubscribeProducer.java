package com.lee.mq.rabbitmq.publishsubscribe;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 发布订阅模式：消息生产者
 *
 * 应用场景：注册验证码–发送邮件、短信
 *
 * 在之前的操作中，生产者生产的消息每次在被其中一个消费者消费后，都会删除掉该信息数据，
 * 如果说我想要我一条消息让多个消费者都可以使用呢？所以订阅模式应运而生。
 *
 * 1、一个生产者，多个消费者
 * 2、之前的生产者产生消息是直接发送给消息队列，由消息队列发送至各个消费者；
 *    现在的模式是生产者将消息发送给 交换机 ，再通过 交换机 发送给各项 消息队列。
 * 3、以前是一个消息队列，并且一个消息队列发送消息给多个消费者；现在是一个消费者一个消息队列。
 *
 * 为什么要多一个交换机exchange呢？
 * 消息队列中的消息是保存在内存中的，每当消息队列确认消费者收到消息(autoAck = false)或者消息队列发送消息给消费者(autoAck = true)后，
 * 会将消息从消息队列中移除，如果要保证多个消费者都能够收到相同的消息，就需要给每个消费者分配一个消息队列，每个消息队列中的消息都一样，
 * 所以需要采取 交换机(exchange) 实现功能。
 *
 * fanout
 *
 * 之前我们是将消息发送至消息队列中，现在我们是将消息推送至交换机中
 *
 * 在rabbitmq中，消息转发器exchange并没有消息的保存能力，只有消息队列才有存储能力。
 * 如果没有消息队列绑定到这个消息转发器上，消息会丢失。
 *
 * 消费者需要将 消息队列绑定到交换机上
 */
@Slf4j
public class PublishSubscribeProducer {

    private static final String EXCHANGE_NAME = "exchange_publish_subscribe_fanout";

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建链接
        Connection conn = MqConnectUtils.getMqConnection();
        //创建通道
        Channel channel = conn.createChannel();
        //创建交换机--无法使用builtinExchangeType.FANOUT了
        //之前是声明 队列，现在是声明交换机
        //交换机没有保存消息的能力，队列才有。如果没有消息队列绑定到这个交换机上，消息会丢失
        channel.exchangeDeclare(EXCHANGE_NAME,"fanout");

        String msg = "this is my fourth msg.";
        //发送消息
        channel.basicPublish(EXCHANGE_NAME,"",null,msg.getBytes());
        log.info("producer send msg : {}",msg);

        //关闭
        channel.close();
        conn.close();
    }

}
