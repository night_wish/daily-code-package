package com.lee.mq.rabbitmq.simple;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 简单消息的生产者
 *
 * 简单队列存在的不足：
 * 耦合性过高。
 * 1、生产者和消费者必须保证一一对应，如果需要实现多个消费者共同消费同一个消息生产者生产的消息时，则会出问题。(消息一旦消费就没有了)
 * 2、生产者的队列名称发生变更后，消息消费者的队列名也需要同时进行变更操作。
 */
@Slf4j
public class SimpleProducer {

    private static final String QUEUE_NAME = "simple_queue";

    public static void main(String[] args) throws IOException, TimeoutException {

        //创建一个链接
        Connection conn = MqConnectUtils.getMqConnection();

        //给链接设置一个通道
        Channel channel = conn.createChannel();

        //给通道指明一个队列--队列名称、是否持久化、是否独占队列、是否自动删除、参数
        //exclusive：是否独占队列：创建者独占队列，连接断开后自动删除
        //autoDelete：当所有消费者链接断开时是否自动删除队列
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);

        String msg = "this is my first msg";

        //发送消息--交换机（没有指定使用默认的default exchange）、路由键routing-key、参数、消息体
        channel.basicPublish("",QUEUE_NAME,null,msg.getBytes());
        log.info("producer msg : {}",msg);

        //关闭链接和通道
        channel.close();
        conn.close();
    }
}
