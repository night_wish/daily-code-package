package com.lee.mq.rabbitmq.confirmtx.tx;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 事务模式：消息生产者(保证生产者和消息队列之间的信息安全)
 *
 * 消息生产者：
 * txSelect()开启事务，将当前channel设置为transaction模式
 * txCommit()提交事务
 * txRollback()回滚事务
 */
@Slf4j
public class TxProducer {

    private static final String QUEUE_NAME = "tx_queue";

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建链接
        Connection conn = MqConnectUtils.getMqConnection();
        //创建通道
        Channel channel = conn.createChannel();
        //创建消息队列
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);

        //发送消息
        try{
            //开启事务
            channel.txSelect();
            Thread.sleep(5000);
            String msg = "this is my seventh message";
//            int a = 10/0;
            channel.basicPublish("",QUEUE_NAME,null,msg.getBytes());
            log.info("producer send msg : {}",msg);
            //事务提交
            channel.txCommit();
        }catch (Exception e){
            e.printStackTrace();
            //事务回滚
            channel.txRollback();
        }

        //关闭链接
        channel.close();
        conn.close();
    }
}
