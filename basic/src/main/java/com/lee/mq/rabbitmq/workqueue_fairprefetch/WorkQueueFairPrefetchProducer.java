package com.lee.mq.rabbitmq.workqueue_fairprefetch;


import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消息生产者 （公平分发）
 * 需求：
 * 消费者一(1000ms) 和 消费者二(2000ms) 设置了不同的延迟效果。运行后，却出现了两个消费者处理的消息数完全一致。
 * 但我们从实际的项目需求来说，消费者一处理速度快，就需要多分配消息处理到消费者一中，消费者二处理慢，就少分配消息至消费者二中，
 * 这样能够提升消息处理的效率。
 *
 *
 * 公平分发：谁做的快谁就多做！
 * 只有在消息消费者成功消费消息，发送消费成功的指令给队列后，消息队列才会继续向该消费者发送下一条消息指令。
 *
 * channel.basicQos(1);//消费者也需要增加
 * 此处的 1 表示 限制发送给每个消费者每次最大的消息数。
 *
 * 在消息队列收到消息消费者发送来的“消息已消费”的消费"回执"信息前，不会向该消费者继续发送消息，只有收到了回执信息，才会继续向该消息消费者发送下一条消息。
 * 例如：我有A、B、C 三个消息消费者，消息队列最初给三个消息消费者都发送了消息，A、C已经处理完毕，但B还在处理，
 * 消息队列收到A、C的回执信息，但还未收到B的回执信息，此时的消息队列只会给消息消费者A、C继续发送下一条消息，但不会给B继续发送消息。
 *
 * 消息消费者要实现 “公平分发” 的操作，需要关闭自动应答操作
 * 同时，在处理完消息后，需要向消息队列做“消费完成”的应答
 *
 * 结果：
 * 两个消费者全部消费完信息后，看消费的消息数目，发现：
 * 消费者一比消费者处理的消息要多很多
 */
@Slf4j
public class WorkQueueFairPrefetchProducer {

    private static final String QUEUE_NAME = "fair_prefetch_work_queue";

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        //创建连接
        Connection conn = MqConnectUtils.getMqConnection();
        //创建通道
        Channel channel = conn.createChannel();
        //创建队列
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);

        //限制每次发送给每个消费者的最大消息数（消费完前）
        channel.basicQos(1);

        //发送消息
        for (int i = 0; i <50 ; i++) {
            String msg = "this is my third msg : "+i;
            channel.basicPublish("",QUEUE_NAME,null,msg.getBytes());
            log.info("producer send msg : {}",msg);

            Thread.sleep(50);
        }

        //关闭链接
        channel.close();
        conn.close();

    }

}
