package com.lee.mq.rabbitmq.simple;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消息消费者--现有写法
 *
 * 简单队列存在的不足：
 * 耦合性过高。
 * 1、生产者和消费者必须保证一一对应，如果需要实现多个消费者共同消费同一个消息生产者生产的消息时，则会出问题。(消息一旦消费就没有了)
 * 2、生产者的队列名称发生变更后，消息消费者的队列名也需要同时进行变更操作。
 */
@Slf4j
public class SimpleConsumer_New {

    private static final String QUEUE_NAME = "simple_queue";

    public static void main(String[] args) throws IOException, TimeoutException {
        //获取链接
        Connection mqConnection = MqConnectUtils.getMqConnection();

        //设置通道
        Channel channel = mqConnection.createChannel();

        //指明消费者
        DefaultConsumer callbck = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body,"UTF-8");
                log.info("consumer received msg : {}",msg);
            }
        };

        //监听队列: 队列名称、ack、callback
        channel.basicConsume(QUEUE_NAME,true,callbck);

    }
}
