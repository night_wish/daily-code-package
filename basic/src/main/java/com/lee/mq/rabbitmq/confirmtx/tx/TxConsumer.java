package com.lee.mq.rabbitmq.confirmtx.tx;

import com.lee.mq.rabbitmq.MqConnectUtils;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消息确认机制和事务：消息生产者(保证生产者和消息队列之间的信息安全)
 *
 * 消息生产者：
 * txSelect()开启事务，将当前channel设置为transaction模式
 * txCommit()提交事务
 * txRollback()回滚事务
 */
@Slf4j
public class TxConsumer {

    private static final String QUEUE_NAME = "tx_queue";

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建链接
        Connection conn = MqConnectUtils.getMqConnection();
        //创建通道
        Channel channel = conn.createChannel();
        //创建消息队列
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);

        //创建消费者
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body,"UTF-8");
                log.info("consumer received msg : {}",msg);

            }
        };

        //创建监听
        channel.basicConsume(QUEUE_NAME,true,defaultConsumer);
    }
}
