package com.lee.io.test;

import java.io.PipedOutputStream;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/2/29 13:31
 */
public class PipedSend implements Runnable{

    private PipedOutputStream pipedOutputStream = null;

    public PipedSend() {
        this.pipedOutputStream = new PipedOutputStream();
    }

    public PipedOutputStream getPipedOutputStream() {
        return pipedOutputStream;
    }

    @Override
    public void run() {
        String str = "Hello World >> Napalm-Lee";
        try {
            pipedOutputStream.write(str.getBytes());
            pipedOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
