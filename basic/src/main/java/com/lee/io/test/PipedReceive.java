package com.lee.io.test;

import java.io.PipedInputStream;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/2/29 13:43
 */
public class PipedReceive implements Runnable{

    private PipedInputStream pipedInputStream;

    public PipedReceive() {
        this.pipedInputStream = new PipedInputStream();
    }

    public PipedInputStream getPipedInputStream() {
        return pipedInputStream;
    }

    @Override
    public void run() {
        byte[] bytes = new byte[1024];
        int len = 0;
        try {
            len = pipedInputStream.read(bytes);
            pipedInputStream.close();
            System.out.println(new String(bytes, 0, len));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
