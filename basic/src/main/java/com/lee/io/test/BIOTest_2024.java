package com.lee.io.test;

import cn.hutool.core.thread.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/2/26 16:00
 */
@Slf4j
public class BIOTest_2024 {

    /**
     * 案例1：创建一个新文件
     * 存在则返回false,不存在则创建.
     * 创建文件用createNewFile，创建文件夹用mkdir--->案例5
     */
    @Test
    public void test_1_CreateNewFile(){
        File file = new File("D:\\hello.txt");
        try {
            boolean res = file.createNewFile();
            log.debug("res:{}",res);
        } catch (IOException e) {
            log.debug("创建文件失败");
            throw new RuntimeException(e);
        }
    }

    /**
     * 案例2：File常用的的常量
     */
    @Test
    public void test_2_showFileCont(){
        log.debug(File.separator);
        log.debug(String.valueOf(File.separatorChar));

        log.debug(File.pathSeparator);
        log.debug(String.valueOf(File.pathSeparatorChar));
    }

    /**
     * 案例3：用常量改写上面代码
     * 在则返回false,不存在则创建
     */
    @Test
    public void test_3_CreateNewFile(){
        String fileName = "D:"+File.separator+"hello.txt";
        File file = new File(fileName);
        try {
            boolean res = file.createNewFile();
            log.debug("res:{}",res);
        } catch (IOException e) {
            log.debug("创建文件失败.");
            throw new RuntimeException(e);
        }
    }

    /**
     * 案例4：删除一个文件
     */
    @Test
    public void test_4_deleteFile(){
        String fileName = "D:"+File.separator+"hello2.txt";
        File file = new File(fileName);
        if(file.exists()){
            boolean delete = file.delete();
            log.debug("res : {}",delete);
        }else{
            log.debug("文件不存在");
        }

    }

    /**
     * 案例5：创建一个文件夹
     * 存在则返回false,不存在则创建.
     * 创建文件用createNewFile，创建文件夹用mkdir--->案例5
     */
    @Test
    public void test_5_mkdirFile(){
        String fileName = "D:"+File.separator+"myFile";
        File file = new File(fileName);
        if(!file.exists()){
            boolean res = file.mkdir();
            log.debug("res : {}",res);
        }else{
            log.debug("文件已经存在");
        }
    }

    /**
     * 案例6：列出文件夹下的所有文件（包括隐藏文件）
     * 展示的是【文件名】不含路径
     */
    @Test
    public void test_6_listAllFile(){
        String fileName =  "D:"+File.separator;
        File file = new File(fileName);
        if(file.exists()){
            String[] list = file.list();
            for (String s : list) {
                log.debug(s);
            }
        }else{
            log.debug("文件不存在");
        }
    }

    /**
     * 案例7：列出文件夹下的所有文件（包括隐藏文件）
     *  含全路径
     */
    @Test
    public void test_7_listAllFileAndPath(){
        String fileName =  "D:"+File.separator;
        File file = new File(fileName);
        if(file.exists()){
            File[] files = file.listFiles();
            for (File file1 : files) {
                System.out.println(file1.getAbsolutePath());
            }
        }
    }

    /**
     * 案例8：判断一个指定路径 是 文件夹 还是 文件
     */
    @Test
    public void test_8_pathIsDirectoryOrFile(){
        String fileName = "D:"+File.separator+"myFile";
        File file = new File(fileName);
        if(file.isDirectory()){
            log.debug("是一个目录");
        }else if(file.isFile()){
            log.debug("是一个文件");
        }
    }

    /**
     * 案例9：列出执行路径下的全部文件
     */
    @Test
    public void test_9_listAllFileByPath(){
        String fileName = "D:"+File.separator+"utils"+File.separator+"壁纸";
        File file = new File(fileName);
        printAllFile(file);
    }

    private void printAllFile(File file) {
        if(file.exists()){
            if(file.isDirectory()){
                File[] files = file.listFiles();
                if(files!=null && files.length>0){
                    for (File chilFile : files) {
                        printAllFile(chilFile);
                    }
                }
            }else{
                log.debug(file.getAbsolutePath());
            }
        }
    }


    /**
     * 案例10：向指定文件写入内容
     * @throws IOException
     */
    @Test
    public void test_10_writeStr2File() throws IOException {
        String fileName = "D:"+File.separator+"myFile"+File.separator+"hello.txt";
        File file = new File(fileName);
        if(!file.exists()){
            boolean res = file.createNewFile();
            log.debug("创建文件成功");
        }

        String word = "你好,";
        OutputStream ops = new FileOutputStream(file);//写，再次运行会覆盖掉原来的
        ops.write(word.getBytes(StandardCharsets.UTF_8));
        ops.close();
    }


    /**
     * 案例11：向指定文件写入内容
     * @throws IOException
     */
    @Test
    public void test_11_appendStr2File() throws IOException {
        String fileName = "D:"+File.separator+"myFile"+File.separator+"hello.txt";
        File file = new File(fileName);
        String word = "Napalm-Lee";
        OutputStream ops = new FileOutputStream(file,true);//追加，在原来的基础上追加内容
        ops.write(word.getBytes(StandardCharsets.UTF_8));
        ops.close();
    }

    /**
     * 案例12：读取文件内容
     * @throws IOException
     */
    @Test
    public void test_12_readFileContent() throws IOException {
        String fileName = "D:"+File.separator+"myFile"+File.separator+"hello.txt";
        File file = new File(fileName);
        InputStream is = new FileInputStream(file);
        byte[] content = new byte[1024];
        is.read(content);
        is.close();
        log.debug("文件内容:{}",new String(content));
    }


    /**
     * 案例12：读取文件内容（优化数组大小）
     * @throws IOException
     */
    @Test
    public void test_12_newReadFileContent() throws IOException {
        String fileName = "D:"+File.separator+"myFile"+File.separator+"hello.txt";
        File file = new File(fileName);
        InputStream is = new FileInputStream(file);
        byte[] content = new byte[(int)file.length()];
        log.debug("文件长度:{}",file.length());
        is.read(content);
        is.close();
        log.debug("文件内容:{}",new String(content));
    }


    /**
     * 案例12：读取文件内容（未知文件大小）
     * 当读到文件末尾的时候会返回-1.正常情况下是不会返回-1的
     */
    @Test
    public void test_13_unknowReadFileSize() throws IOException {
        String fileName = "D:"+File.separator+"myFile"+File.separator+"hello.txt";
        File file = new File(fileName);
        InputStream is = new FileInputStream(file);
        byte[] content = new byte[1024];
        int count = 0;
        int temp = 0;
        while((temp=is.read())!=-1){
            content[count++] = (byte) temp;
        }
        is.close();
        log.debug("文件内容:{}",new String(content));
    }

    /**
     * 方案14：向文件中写入字符串
     */
    @Test
    public void test_14_writeStr2File() throws IOException {
        String fileName = "D:"+File.separator+"myFile"+File.separator+"hello.txt";
        File file = new File(fileName);
        Writer writer = new FileWriter(file);//写，再次运行会覆盖掉原来的。
        //Writer writer = new FileWriter(file,true);//追加，在原来的基础上追加内容
        String content = "今天是2024-02-28,天气晴朗.\r\n你好,Napalm-Lee";
        writer.write(content);
        writer.close();
    }

    /**
     * 方案15：读取文件内容
     */
    @Test
    public void test_15_readFileContent() throws IOException {
        String fileName = "D:"+File.separator+"myFile"+File.separator+"hello.txt";
        File file = new File(fileName);
        Reader reader = new FileReader(file);
        char[] content = new char[1024];
        int read = reader.read(content);
        reader.close();
        log.debug("内容长度:{}",read);
        log.debug("内容:{}",new String(content, 0, read));
    }

    /**
     * 方案15：读取文件内容
     * 当然最好采用循环读取的方式，因为我们有时候不知道文件到底有多大。
     */
    @Test
    public void test_15_readFileContent_loop() throws IOException {
        String fileName = "D:"+File.separator+"myFile"+File.separator+"hello.txt";
        File file = new File(fileName);
        Reader reader = new FileReader(file);
        char[] content = new char[256];
        int counter = 0;
        int temp = 0;
        while((temp=reader.read())!=-1){
            content[counter++] = (char) temp;
        }
        reader.close();
        log.debug("内容长度:{}",counter);
        log.debug("内容:{}",new String(content, 0, counter));
    }

    /**
     * 案例16：将字节输出流转换为字符输出流
     * @throws IOException
     */
    @Test
    public void test_16_convertStream2CharWriter() throws IOException {
        String fileName = "D:" + File.separator + "myFile" + File.separator + "hello.txt";
        File file = new File(fileName);
        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write("hello\r\nMr.Lee");
        writer.close();
    }

    /**
     * 案例16：将字节输入流转换为字符输入流
     */
    @Test
    public void test_16_convertStream2CharReader() throws IOException {
        String fileName = "D:" + File.separator + "myFile" + File.separator + "hello.txt";
        File file = new File(fileName);
        Reader reader = new InputStreamReader(new FileInputStream(file));
        char[] content = new char[256];
        int counter = 0;
        int temp = 0;
        while((temp=reader.read())!=-1){
            content[counter++] = (char) temp;
        }
        reader.close();
        log.debug("内容长度:{}",counter);
        log.debug("内容:{}",new String(content, 0, counter));
    }


    /**
     * 案例17：使用内存操作流将一个大写字母转化为小写字母
     * 内容操作流一般使用来生成一些临时信息采用的，这样可以避免删除的麻烦。
     */
    @Test
    public void test_17_convertUpperCase2LowerCase() throws IOException {
        String content = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(content.getBytes());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int temp = 0;
        while((temp=byteArrayInputStream.read())!=-1){
            byteArrayOutputStream.write(Character.toLowerCase((char) temp));
        }
        byteArrayInputStream.close();
        byteArrayOutputStream.close();
        log.debug("内容:{}",byteArrayOutputStream.toString());
    }


    /**
     * 案例18：管道流主要可以进行两个线程之间的通信
     */
    @Test
    public void test_18_pipedStreamMsg(){
        PipedSend pipedSender = new PipedSend();
        PipedReceive pipedReceiver = new PipedReceive();
        try {
            pipedSender.getPipedOutputStream().connect(pipedReceiver.getPipedInputStream());//管道连接
        } catch (IOException e) {
            e.printStackTrace();
        }
        new Thread(pipedSender).start();
        new Thread(pipedReceiver).start();
    }

    /**
     *  案例19：使用缓冲区从键盘上读入内容
     */
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("请输入内容：");
        try {
            String str = reader.readLine();
            System.out.println("您输入的内容是:  "+str);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
