package com.lee.io.test;

import com.alibaba.fastjson.JSON;
import com.lee.base.commons.DocD;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.*;

@Slf4j
public class BIOTest_2021 {


    /**
     * 文件符号
     * ;
     * \
     */
    @Test
    public void fileSymbol() {
        log.info("file.PathSeparator: {}", File.pathSeparator);
        log.info("file.separator : {}", File.separator);
    }

    /**
     * 创建文件
     */
    @Test
    public void createFile() throws IOException {
        File file = new File("d:" + File.separator + "b.txt");
        file.createNewFile();
    }

    /**
     * 创建文件夹
     */
    @Test
    public void createDic() {
        File file = new File("d:" + File.separator + "b");
        if (!file.exists()) {
            file.mkdir();
            log.info("~~~文件不存在，创建");
        }
    }

    /**
     * 迭代创建文件夹
     */
    @Test
    public void createDics() {
        File file = new File("d:" + File.separator + "b" + File.separator + "a");
        if (!file.exists()) {
            file.mkdirs();
            log.info("~~~文件s不存在，创建");
        }
    }

    /**
     * 判断指定路径是否为一个目录，而不是文件
     */
    @Test
    public void isDicOrFile() {
        File file = new File("d:" + File.separator + "a.txt");
        if (file.isDirectory()) {
            log.info("~~~is directory~~~");
        }

        if (file.isFile()) {
            log.info("~~~is file~~~");
        }
    }

    /**
     * 文件名称遍历
     */
    @Test
    public void listFile1() {
        File file = new File("d:");
        String[] list = file.list();
        for (String fileName : list) {
            log.info("~~fileName: {}", fileName);
        }
    }

    /**
     * 文件遍历+全路径
     */
    @Test
    public void listFile2() {
        File file = new File("d:");
        File[] files = file.listFiles();
        for (File f : files) {
            log.info("~~file : {}", f);
        }
    }


    /**
     * 迭代遍历目录下的全部文件
     */
    @Test
    public void listDicUnderFile() {
        File file = new File("d:" + File.separator + "tools" + File.separator + "CentOS");
        if (file.isDirectory()) {
            printFile(file);
        }
    }

    public void printFile(File file) {
        if (file != null) {
            if (file.isDirectory()) {
                log.info("~~~~>dic : {} ", file);
                File[] childFiles = file.listFiles();
                if (childFiles != null && childFiles.length > 0) {
                    for (File f : childFiles) {
                        printFile(f);
                    }
                }
            } else {
                log.info("~~~~>file : {}", file);
            }
        }
    }


    /**
     * 字节流读
     * 取文件内容
     */
    @Test
    public void readFile1() throws IOException {
        File file = new File("d:" + File.separator + "a.txt");
        InputStream in = new FileInputStream(file);
        byte[] bytes = new byte[1024];
        in.read(bytes);
        in.close();
        log.info(new String(bytes));
    }

    /**
     * 字节流读
     * 该示例中由于b字节数组长度为1024，如果文件较小，则会有大量填充空格。我们可以利用in.read(b);的返回值来设计程序
     */
    @Test
    public void readFile2() throws IOException {
        File file = new File("d:" + File.separator + "a.txt");
        InputStream in = new FileInputStream(file);
        byte[] bytes = new byte[1024];
        int len = in.read(bytes);
        in.close();
        log.info(new String(bytes, 0, len));
    }


    /**
     * 字节流读
     * 上面的例子可以看出，我们预先申请了一个指定大小的空间，但是有时候这个空间可能太小，有时候可能太大，我们需要准确的大小，这样节省空间，那么我们可以这样做：
     */
    @Test
    public void readFile3() throws IOException {
        File file = new File("d:" + File.separator + "a.txt");
        InputStream in = new FileInputStream(file);
        byte[] bytes = new byte[(int) file.length()];
        in.read(bytes);
        in.close();
        log.info(new String(bytes));
    }

    /**
     * 一个一个读
     */
    @Test
    public void readFile4() throws IOException {
        File file = new File("d:" + File.separator + "a.txt");
        InputStream in = new FileInputStream(file);
        byte[] bytes = new byte[(int) file.length()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) in.read();
        }
        in.close();
        log.info(new String(bytes));
    }

    @Test
    public void readFile5() throws IOException {
        File file = new File("d:" + File.separator + "a.txt");
        InputStream in = new FileInputStream(file);
        byte[] bytes = new byte[1024];
        int count = 0;
        int temp = 0;
        while ((temp = in.read()) != -1) {
            bytes[count++] = (byte) temp;
        }
        in.close();
        log.info(new String(bytes, 0, count));
    }


    /**
     * 字节流：写文件
     */
    @Test
    public void writeFile1() throws IOException {
        File file = new File("d:" + File.separator + "a.txt");
        if (file.isFile()) {
            OutputStream os = new FileOutputStream(file);
            byte[] content = "你好".getBytes();
            os.write(content);
            os.close();
        }
    }


    /**
     * 字节流：追加写文件
     */
    @Test
    public void appendWriteFile2() throws IOException {
        File file = new File("d:" + File.separator + "a.txt");
        if (file.isFile()) {
            OutputStream os = new FileOutputStream(file, true);
            byte[] content = "你在干嘛呢".getBytes();
            os.write(content);
            os.close();
        }
    }


    /**
     * 一行一行读取文件
     */
    @Test
    public void readFileByLine() throws IOException {
        List<String> list = new ArrayList<String>();
        File file = new File("d:" + File.separator + "B.txt");
        if (file.isFile()) {
            // 防止路径乱码   如果utf-8 乱码  改GBK     eclipse里创建的txt  用UTF-8，在电脑上自己创建的txt  用GBK
            InputStreamReader isr = new InputStreamReader(new FileInputStream("d:" + File.separator + "B.txt"), "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            while ((line = br.readLine()) != null) {
                // 如果 t x t文件里的路径 不包含---字符串       这里是对里面的内容进行一个筛选
                if (line.lastIndexOf("---") < 0) {
                    list.add(line);
                }
            }
            br.close();
            isr.close();

            for (String s : list) {
                //System.out.print("d."+s+",");
                //System.out.print("\"" + s + "\",");
                System.out.print(s + " ");
            }

        }
    }


    /**
     * 复制文件
     */
    @Test
    public void copyFile() {
        File sourceFile = new File("D:/A.txt");
        File targetFile = new File("E:/A.txt");

        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream(sourceFile);
            fos = new FileOutputStream(targetFile);
            byte[] buf = new byte[10];
            while (fis.read(buf) != -1) {
                fos.write(buf);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 断点续传
     */
    @Test
    public void breakAndContinueCopyFile() {
        int position = -1;//记录中断的位置

        File sourceFile = new File("D:/A.txt");//源文件
        File targetFile = new File("E:/A.txt");//目标文件

        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream(sourceFile);
            fos = new FileOutputStream(targetFile);
            byte[] buf = new byte[10];
            while (fis.read(buf) != -1) {
                fos.write(buf);
                System.out.println(targetFile.length());
                if (targetFile.length() == 100) {
                    position = 100;
                    throw new IOException("长度打到100  网络中");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("  长度达到100  网络中断，开始断点续传");
            breakAndContinue(sourceFile, targetFile, position);
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void breakAndContinue(File sourceFile, File targetFile, int position) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        RandomAccessFile readFile = null;
        RandomAccessFile writeFile = null;
        try {
            readFile = new RandomAccessFile(sourceFile, "r");
            writeFile = new RandomAccessFile(targetFile, "rw");

            readFile.seek(position);
            writeFile.seek(position);

            // 数据缓冲区
            byte[] buf = new byte[10];
            // 数据读写
            while (readFile.read(buf) != -1) {
                writeFile.write(buf);
            }

            System.out.println("断点续传完毕");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("断点续传发生意外");
        } finally {
            try {
                if (readFile != null) {
                    readFile.close();
                }
                if (writeFile != null) {
                    writeFile.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Test
    public void test() {
        /*String a = "abc";
        String b = null;
        a.concat(b);*/

        /*String uuid = UUID.randomUUID().toString().replace("-","");
        System.out.println(uuid);
        System.out.println(uuid.length());*/

        DocD docD = new DocD();
        docD.setDocId("111");
        initDoc(docD);
        log.info("---->docD:{}", JSON.toJSONString(docD));
    }


    public void initDoc(DocD docD) {
        docD.setPatnId("patn111");
        docD.setDocName("单据名称");
    }

    @Test
    public void sqlTest() {
        String sql = "select * from a $1where group by $2groupBy ";
        sql = genSql(sql);
        log.info("----sql:{}", sql);
    }

    public String genSql(String sql) {
        return genSql2(sql);
    }

    public String genSql2(String sql) {
        sql = sql.replace("$1where", "where 1=1 ");
        sql = sql.replace("$2groupBy", "id desc");
        return sql;
    }

    public static void main(String[] args) {
        List<TT> list = new ArrayList<>();
        list.add(new TT(1,"a1"));
        list.add(new TT(2,"a2"));
        list.add(new TT(3,"a3"));
        list.add(new TT(4,"a4"));
        list.add(new TT(5,"a5"));
        list.add(new TT(6,"a6"));
        list.add(new TT(7,"a7"));
        list.add(new TT(8,"a8"));
        list.add(new TT(9,"a9"));
        list.add(new TT(10,"a10"));
        /*for (int i=list.size()-1;i>=0;i--) {
            if(list.get(i).getNum()%2==0){
                list.remove(list.get(i));
            }
        }*/
        ListIterator<TT> itr = list.listIterator();
        while (itr.hasNext()) {
            TT next = itr.next();
            if (next.getNum() % 2 == 0) {
                itr.remove();
            }else{
                next.setName("aa"+next.getNum());
                itr.set(next);
            }
        }
        log.info("list:{}",JSON.toJSONString(list));

    }





}

@Data
@Accessors(chain = true)
@AllArgsConstructor
class TT{
    private Integer num;
    private String name;
}
