package com.lee.io.utils;

import java.io.*;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/9/27 16:08
 */
public class SplitFile {

    public static void main(String []args)throws Exception{

        File fileNovel = new File("d:"+ File.separator+"神墓.txt");
        BufferedReader bufReader = new BufferedReader(new FileReader(fileNovel));
        String novelLine;
        String charpterUrl;
        File fileNovelCharpter = null;
        int charpterCount=0;
        while((novelLine = bufReader.readLine())!=null){

            //如果包含"章"，"节"，“集”，则创建相关的文件
            if((novelLine.contains("第")&&novelLine.contains("章")) || (novelLine.contains("第")&&novelLine.contains("卷"))){

                charpterCount++;
                //new File("d:"+File.separator+novelLine+".txt").createNewFile();
                new File("d:"+File.separator+"神墓"+File.separator+charpterCount+"、"+novelLine+".txt").createNewFile();

            }else{
                BufferedWriter bufWriter = new BufferedWriter(new FileWriter(fileNovelCharpter,true));
                bufWriter.write(novelLine);
                bufWriter.newLine();
                bufWriter.flush();

                continue;
            }

            charpterUrl ="d:"+File.separator+"神墓"+File.separator+novelLine+".txt";
            fileNovelCharpter = new File("d:"+File.separator+"神墓"+File.separator+charpterCount+"、"+novelLine+".txt");
        }

        bufReader.close();
    }
}
