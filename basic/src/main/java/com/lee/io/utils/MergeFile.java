package com.lee.io.utils;

import java.io.*;

/**
 * @author Lee
 * @version 1.0
 * @description 合并文件
 * @date 2022/2/27 17:49
 */
public class MergeFile {

    public static void main(String[] args) throws IOException {
        //定义输出目录
        String FileOut="C:\\Users\\le'e\\Desktop\\merge.txt";
        BufferedWriter bw=new BufferedWriter(new FileWriter(FileOut));

        //读取目录下的每个文件或者文件夹，并读取文件的内容写到目标文字中去
        File[] list = new File("C:\\Users\\le'e\\Desktop\\a").listFiles();
        int fileCount = 0;
        int folderConut= 0;
        for(File file : list)
        {
            if(file.isFile())
            {
                fileCount++;
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                while((line=br.readLine())!=null) {
                    bw.write(line);
                    bw.newLine();
                }
                br.close();
            }else {
                folderConut++;
            }
        }
        bw.close();
        System.out.println("输入目录下文件个数为"+fileCount);
        System.out.println("输入目录下文件夹个数为"+folderConut);

    }
}
