package com.lee.io.utils;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.lee.poi.entity.DocDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.pegdown.PegDownProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/1/19 16:55
 */
@SpringBootTest
@Slf4j
@Component
public class MarkDownToHtml {


    @Value("${sequence.rid.admdvs}")
    private String aaa;

    @Value("${spring.profiles.active")
    private String profileActive;

    public static void main(String[] args) {

        String join = String.join(",", new ArrayList<>());
        System.out.println("=="+join);
    }

    @Test
   public void test(){
        DocDTO d = new DocDTO();
        List<String> strings = new ArrayList<>();
        strings.add("1");
        strings.add("2");
        strings.add("3");
        strings.add("4");
        d.setTaskIds(strings);
        d.getTaskIds().remove("2");
        System.out.println(JSON.toJSONString(d.getTaskIds()));
   }

    /**
     * markdown转html
     *
     * @throws IOException
     */
    @Test
    public void convertMarkDownFileToHtml() throws IOException {
        BufferedReader br = null;
        log.info("----------->profileActive:{}",profileActive);
        if("local".equals(profileActive)){
            //本地
            File file = ResourceUtils.getFile("classpath:markdown-tmpl/tmpl.html");
            FileInputStream fileInputStream = new FileInputStream(file);
            br = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
        }else{
            //打包后
            ClassPathResource classPathResource = new ClassPathResource("markdown-tmpl/tmpl.html");
            br = new BufferedReader(new InputStreamReader(classPathResource.getInputStream(), "UTF-8"));
        }

        StringBuffer sb = new StringBuffer();
        String str = "";
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();
        String res = sb.toString();


        File mdFile = new File("G:\\AdvcdSpace\\daily-code-notes\\基础进阶笔记\\02-01、SpringBoot从入门到放弃，第一章.md");
        br = new BufferedReader(new InputStreamReader(new FileInputStream(mdFile), "UTF-8"));
        String line = null;
        String mdContent = "";
        while ((line = br.readLine()) != null) {
            mdContent += line + "\r\n";
        }
        PegDownProcessor pdp = new PegDownProcessor(Integer.MAX_VALUE);
        String htmlContent = pdp.markdownToHtml(mdContent);
        //System.out.println(htmlContent);
        br.close();

        String replace = res.replace("$articleTitle", "02-01、SpringBoot从入门到放弃，第一章.md").replace("$articleContent", htmlContent);
        System.out.println(replace);
    }

}
