package com.lee.base.reflect;

import com.lee.base.commons.Animals;
import com.lee.base.commons.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

@Slf4j
public class ReflectTest {


    @Test
    public void testMethod() throws ClassNotFoundException {
        //获取class的三种方法
        Class<?> aClass = Class.forName("com.lee.base.commons.Animals");
        log.info("====>{}",aClass);
        Class<Animals> animalsClass = Animals.class;
        log.info("====>{}",animalsClass);
    }

    /**
     * 两种创建 实例的方式
     */
    @Test
    public void testNewInstance() throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Class<User> aClass = (Class<User>) Class.forName("com.lee.base.commons.User");
        User user0 = aClass.newInstance();
        log.info("new instance : {}",user0);

        Constructor<User> declaredConstructor = aClass.getDeclaredConstructor(Integer.class, String.class, int.class);
        //如果全参构造是私有的，设置可以访问
        //declaredConstructor.setAccessible(true);
        User user1 = declaredConstructor.newInstance(1, "lee", 18);
        log.info("new instance : {}",user1);
    }

    /**
     *  调用方法
     */
    @Test
    public void testInvoke() throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Class<User> aClass = (Class<User>) Class.forName("com.lee.base.commons.User");
        String name = aClass.getName();
        log.info(" name : {}",name);
        String simpleName = aClass.getSimpleName();
        log.info(" simple name : {}",simpleName);
        User user = aClass.newInstance();
        String sayHi = user.sayHi("lee");
        log.info("sayHi : {}",sayHi);

        Method method1 = aClass.getDeclaredMethod("setId", Integer.class);
        method1.invoke(user,1);
        Method method2 = aClass.getDeclaredMethod("setName", String.class);
        method2.invoke(user,"lee");
        Method method3 = aClass.getDeclaredMethod("setAge", int.class);
        method3.invoke(user,18);

        Method sayHi1 = aClass.getDeclaredMethod("sayHi", String.class);
        String res = (String)sayHi1.invoke(user, "ren ya ling");
        log.info("res : {}",res);

        log.info("current user : {}",user);
    }



    /**
     * 1、获取字节码Class对象
     * Class.forName("全类名")：将字节码文件加载进内存，返回Class对象
     * 类名.class: 通过类名的属性class获取Class对象
     * 对象.getClass(): getClass()方法在Object类中定义获取Class对象
     */
    @Test
    public void getClassTest() throws ClassNotFoundException {
        //source源码阶段
        Class<?> cls1 = Class.forName("com.lee.base.commons.Animals");
        log.info("====> {}",cls1);
        //Class类对象阶段
        Class cls2 = Animals.class;
        log.info("====> {}",cls2);
        //Runtime运行时阶段
        Animals animals = new Animals();
        Class cls3 = animals.getClass();
        log.info("====> {}",cls3);
    }


    /**
     * 获取Field和Method
     * @throws ClassNotFoundException
     */
    @Test
    public void getDeclareTest() throws ClassNotFoundException {
        //获取Class
        Class<?> cls = Class.forName("com.lee.base.commons.Animals");
        //获取Field
        log.info("====> {}","--------------------");
        Field[] fields = cls.getFields();
        Arrays.stream(fields).forEach(f->{
            log.info("====> {}",f);
        });
        log.info("====> {}","--------------------");
        Field[] fields2 = cls.getDeclaredFields();
        Arrays.stream(fields2).forEach(f->{
            log.info("====> {}",f);
        });
        log.info("====> {}","--------------------");
        Method[] methods = cls.getMethods();
        Arrays.stream(methods).forEach(m->{
            log.info("====> {}",m);
        });
        log.info("====> {}","--------------------");
    }





}
