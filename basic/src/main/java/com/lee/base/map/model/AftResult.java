package com.lee.base.map.model;

import com.lee.base.commons.MenuInfo;
import com.lee.base.commons.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/4/7 13:25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AftResult {

    private Map<String, User> userMap = new ConcurrentHashMap<>();

    private Map<String, MenuInfo> menuInfoMap = new ConcurrentHashMap<>();
}
