package com.lee.base.map;

import com.alibaba.fastjson.JSON;
import com.lee.base.commons.MenuInfo;
import com.lee.base.commons.User;
import com.lee.base.map.model.AftResult;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/4/7 13:27
 */
public class MapTest {

    public static void main(String[] args) {
//        MapTest t1 = new MapTest();
//        AftResult result = new AftResult();
//
//        AftResult result1 = t1.generateChild1();
//        result.getUserMap().putAll(result1.getUserMap());
//        result.getMenuInfoMap().putAll(result1.getMenuInfoMap());
//
//        AftResult result2 = t1.generateChild2();
//        result.getUserMap().putAll(result2.getUserMap());
//        result.getMenuInfoMap().putAll(result2.getMenuInfoMap());
//
//        System.out.println(JSON.toJSONString(result));

        for (int i=0;i<10;i++){
            for(int j= i+1;j<10;j++){
                System.out.print(j+" ");
            }
            System.out.println("=="+i+"== ");
        }
    }


    public AftResult generateChild1(){
        AftResult result = new AftResult();
        Map<String, User> userMap = new ConcurrentHashMap<>();
        userMap.put("u1",User.builder().id(1).age(21).name("uname1").build());
        userMap.put("u2",User.builder().id(2).age(22).name("uname2").build());
        userMap.put("u3",User.builder().id(3).age(23).name("uname3").build());
        result.setUserMap(userMap);

        Map<String, MenuInfo> menuInfoMap = new ConcurrentHashMap<>();
        menuInfoMap.put("m1",MenuInfo.builder().menuCode("m1").menuName("menu1").build());
        menuInfoMap.put("m2",MenuInfo.builder().menuCode("m2").menuName("menu2").build());
        menuInfoMap.put("m3",MenuInfo.builder().menuCode("m3").menuName("menu3").build());
        result.setMenuInfoMap(menuInfoMap);
        return result;
    }

    public AftResult generateChild2(){
        AftResult result = new AftResult();
        Map<String, User> userMap = new ConcurrentHashMap<>();
        userMap.put("u1",User.builder().id(1).age(21).name("uname4").build());
        userMap.put("u5",User.builder().id(2).age(22).name("uname5").build());
        userMap.put("u6",User.builder().id(3).age(23).name("uname6").build());
        result.setUserMap(userMap);

        Map<String, MenuInfo> menuInfoMap = new ConcurrentHashMap<>();
        menuInfoMap.put("m4",MenuInfo.builder().menuCode("m4").menuName("menu4").build());
        menuInfoMap.put("m5",MenuInfo.builder().menuCode("m5").menuName("menu5").build());
        menuInfoMap.put("m6",MenuInfo.builder().menuCode("m6").menuName("menu6").build());
        result.setMenuInfoMap(menuInfoMap);
        return result;
    }

}
