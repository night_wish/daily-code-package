package com.lee.base.commons;

import com.lee.poi.utils.ApiModelProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description 菜单信息
 * @date 2022/8/23 16:00
 */
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MenuInfo {

    private static final long serialVersionUID = -2281280257595155589L;

    @ApiModelProperty(value = "菜单编码")
    private String menuCode;

    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @ApiModelProperty(value = "父菜单编码")
    private String prntCode;

    @ApiModelProperty(value = "子菜单")
    private  List<MenuInfo> children = new ArrayList<>();

    public MenuInfo(String menuCode, String menuName, String prntCode) {
        this.menuCode = menuCode;
        this.menuName = menuName;
        this.prntCode = prntCode;
    }
}
