package com.lee.base.commons;

import lombok.Data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class Employee implements Serializable {

    private static final long serialVersionUID = -6698507152795989083L;


    private Integer id;

    private String name;

    private int age;

    private Double salary;

    private Status status;

    private Date createTime;

    private String gender;

    public enum Status{
        FREE,BUSY,VACATION
    }

    public Employee() {
    }

    public Employee(Integer id) {
        this.id = id;
    }

    public Employee(Integer id, String name, Date createTime) {
        this.id = id;
        this.name = name;
        this.createTime = createTime;
    }

    public Employee(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Employee(Integer id, String name, int age, Double salary, Status status) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.status = status;
    }


    public Employee(Integer id, String name, int age, Double salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public Employee(Integer id, String name, int age,String gender, Double salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
    }


    @Override
    public String toString() {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", gender=" + gender +
                ", status=" + status +
//                ", createTime=" + s.format(createTime) +
                '}';
    }
}
