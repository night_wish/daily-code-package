package com.lee.base.commons;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/7/7 14:22
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DocD {

    private String docId;
    private String patnId;
    private String docName;
    private List<String> docIdList;


}
