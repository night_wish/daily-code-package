package com.lee.base.commons;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/7/20 10:20
 */
@Data
@ToString
public class LoginUser implements Serializable {

    private static final long serialVersionUID = 246018794335376518L;

    public String username;

    public transient String password;

    public static String gender;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static String getGender() {
        return gender;
    }

    public static void setGender(String gender) {
        LoginUser.gender = gender;
    }
}
