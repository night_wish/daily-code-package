package com.lee.base.commons;

import lombok.*;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
//@ToString
public class User implements Serializable {

    private static final long serialVersionUID = 7415483516195697752L;

    private Integer id;

    private String name;

    private int age;


    public String sayHi(String name){
      return name+" say Hi to you !";
    }

}
