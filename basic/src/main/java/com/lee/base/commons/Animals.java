package com.lee.base.commons;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.ToString;

import java.io.PrintWriter;
import java.io.StringWriter;

@Data
@ToString
public class Animals {

    public String uuid;

    protected String type;

    int age;

    private String color;

    public static void main(String[] args) {
        try{
            int a = 1/0;
        } catch (Exception ex) {
            ex.printStackTrace();
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            String errCont = sw.toString();
            System.out.println(errCont);
        }
    }

    public Animals() {
    }

    public Animals(String type, String color) {
        this.type = type;
        this.color = color;
    }

    public Animals(String uuid, String type, int age, String color) {
        this.uuid = uuid;
        this.type = type;
        this.age = age;
        this.color = color;
    }


    public void say(String voice){
        System.out.println("say  O(∩_∩)O哈哈~ "+voice);
    }

    private void said(String voice){
        System.out.println("said O(∩_∩)O哈哈~ "+voice);
    }
}
