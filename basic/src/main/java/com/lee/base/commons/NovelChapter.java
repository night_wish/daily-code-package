package com.lee.base.commons;

import lombok.*;

/**
 * @author Lee
 * @version 1.0
 * @description 小说章节表
 * @date 2022/10/28
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
@Builder
public class NovelChapter {
    private static final long serialVersionUID = 774589739952130506L;

    private String chapterName;

    private String content;

}
