package com.lee.base.stream;

import com.lee.base.commons.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/7/5 15:56
 */
@Slf4j
public class StreamAndParallel {


    public static void main(String[] args) {
        List<User> userList = new ArrayList<>();
        for(int i=0;i<10000000;i++){
            User user = new User(i, "name:" + i, i);
            userList.add(user);
        }

        streamTest(userList);
        parallelStreamTest(userList);
        //1657008305644ms
        //1657008308631ms
    }

    public static void streamTest(List<User> userList){
        long startT = System.currentTimeMillis();
        log.info("stream start : 开始：{}ms",startT);

        userList.stream().forEach(t->{
            t.setName("姓名"+t.getName());
        });


        long endT = System.currentTimeMillis();
        log.info("stream start : 结束：{}ms",endT);
        log.info("stream 总消耗：{}ms",endT-startT);
    }

    public static void parallelStreamTest(List<User> userList){
        long startT = System.currentTimeMillis();
        log.info("parallelStream start : 开始：{}ms",startT);

        userList.parallelStream().forEach(t->{
            t.setName("姓名"+t.getName());
        });


        long endT = System.currentTimeMillis();
        log.info("parallelStream start : 结束：{}ms",endT);
        log.info("parallelStream 总消耗：{}ms",endT-startT);
    }


    private static final List<String> array = Arrays.asList("1","2","3","4","5","6","7","8","9");

    /**
     * stream处理 list 按顺序处理
     * 只有一个main线程
     */
    @Test
    public void streamPrint(){
        array.stream().forEach(t->{
            log.info("stream print   : {}",t);
        });
    }

    /**
     * parallelStream处理 list会有 顺序 的问题，
     * 一个main线程 + 其他多个线程
     */
    @Test
    public void parallelStreamPrint(){
        array.parallelStream().forEach(t->{
            log.info("parallelStream print   : {}",t);
        });
    }

}
