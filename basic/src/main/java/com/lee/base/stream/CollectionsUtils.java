package com.lee.base.stream;

import com.lee.base.commons.Employee;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.cglib.beans.BeanCopier;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * collections 测试
 */
@Slf4j
public class CollectionsUtils {


    @Test
    public void testReduce(){
        List<Employee> employeeList = Arrays.asList(
              new Employee(1,"张三",29,10.00),
              new Employee(1,"张三",29,20.00),
              new Employee(1,"张三",29,30.00),
              new Employee(2,"李四",35,190.00),
              new Employee(3,"王五",43,40.00),
              new Employee(3,"王五",42,5.00),
              new Employee(3,"王五",43,6.00),
              new Employee(4,"赵六",37,60.00),
              new Employee(5,"刘七",28,11.00),
              new Employee(5,"刘七",29,9.00)
        );
        Map<String, Employee> collect = employeeList.stream().collect(Collectors.toMap(e -> e.getId() + "-" + e.getName(), a -> a, (o1, o2) -> {
            o1.setSalary(o1.getSalary() + o2.getSalary());
            return o1;
        }));
        log.info("res====>{}",collect);
    }

    @Test
    public void testGroupBy() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<Employee> employeeList = Arrays.asList(
              new Employee(1,"张三",sdf.parse("2020-01-01")),
              new Employee(1,"张三",sdf.parse("2020-01-01")),
              new Employee(1,"张三",sdf.parse("2020-01-01")),
              new Employee(2,"李四",sdf.parse("2020-01-07")),
              new Employee(3,"王五",sdf.parse("2020-01-02")),
              new Employee(3,"王五",sdf.parse("2020-01-02")),
              new Employee(3,"王五",sdf.parse("2020-01-02")),
              new Employee(4,"赵六",sdf.parse("2020-01-03")),
              new Employee(5,"刘七",sdf.parse("2020-01-06")),
              new Employee(5,"刘七",sdf.parse("2020-01-06"))
        );

        Map<Integer, List<Employee>> collect = employeeList.stream().collect(Collectors.groupingBy(e -> e.getId()));

        log.info("res====>{}",collect);

    }


    @Test
    public void testStreamOf(){
        List<String> collect = Stream.of("1", "2", "3", "4").collect(Collectors.toList());
        collect.stream().forEach(t->{
            log.info("new value : {}",t);
        });
    }


    @Test
    public void testArrays2(){
        List<Integer> arrys = new ArrayList<>();
        arrys.add(1);
        arrys.add(2);
        arrys.add(3);
        arrys.add(4);
        List<Integer> collect = arrys.stream().map(t -> t + 1).collect(Collectors.toList());
        collect.stream().forEach(t->{
            log.info("new value : {}",t);
        });
    }

    @Test
    public void testAnyMatch() throws ParseException {
       /* List<String> arrys = new ArrayList<>();
        arrys.add("1");
        arrys.add("2");
        arrys.add("3");
        arrys.add("4");
        boolean b = arrys.stream().anyMatch(t -> "5".equals(t) || "3".equals(t));
        log.info("b==>{}",b);*/

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<Employee> employeeList = Arrays.asList(
              new Employee(1,"张三",sdf.parse("2020-01-01")),
              new Employee(1,"张三",sdf.parse("2020-01-01")),
              new Employee(1,"张三",sdf.parse("2020-01-01")),
              new Employee(2,"李四",sdf.parse("2020-01-07")),
              new Employee(3,"王五",sdf.parse("2020-01-02")),
              new Employee(3,"王五",sdf.parse("2020-01-02")),
              new Employee(3,"王五",sdf.parse("2020-01-02")),
              new Employee(4,"赵六",sdf.parse("2020-01-03")),
              new Employee(5,"刘七",sdf.parse("2020-01-06")),
              new Employee(5,"刘七",sdf.parse("2020-01-06"))
        );

        boolean b = employeeList.stream().anyMatch(t -> "张三".equals(t.getName()) || "刘七".equals(t.getName()));
        log.info("b===>{}",b);
    }


}
