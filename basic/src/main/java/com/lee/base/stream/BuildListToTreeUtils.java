package com.lee.base.stream;

import com.alibaba.fastjson.JSON;
import com.lee.base.commons.MenuInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/9/28 17:47
 */
@Slf4j
public class BuildListToTreeUtils {

    public static void main(String[] args) {
        MenuInfo m1 = new MenuInfo("c1","菜单1","0");
        MenuInfo m2 = new MenuInfo("c1-1","菜单1-1","c1");
        MenuInfo m3 = new MenuInfo("c2","菜单2","0");
        MenuInfo m4 = new MenuInfo("c2-1","菜单2-1","c2");
        MenuInfo m5 = new MenuInfo("c1-2","菜单1-2","c1");
        MenuInfo m6 = new MenuInfo("c1-3","菜单1-3","c1");
        MenuInfo m7 = new MenuInfo("c2-2","菜单2-2","c2");
        MenuInfo m8 = new MenuInfo("c3","菜单3","0");
        MenuInfo m9 = new MenuInfo("c3-1","菜单3-1","c3");
        List<MenuInfo> list = new ArrayList<>();
        list.add(m1);
        list.add(m2);
        list.add(m3);
        list.add(m4);
        list.add(m5);
        list.add(m6);
        list.add(m7);
        list.add(m8);
        list.add(m9);
        List<MenuInfo> menuInfos = buildListToTree(list, "0");
        log.info("---->{}", JSON.toJSONString(menuInfos));
    }

    public static List<MenuInfo> buildListToTree(List<MenuInfo> menuInfoList, String prntCode) {
        List<MenuInfo> children = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(menuInfoList) && StringUtils.isNotBlank(prntCode)) {
            for (MenuInfo menuInfo : menuInfoList) {
                if (prntCode.equals(menuInfo.getPrntCode())) {
                    buildChildren(menuInfoList, menuInfo);
                    children.add(menuInfo);
                }
            }
        }
        return children;
    }

    public static void buildChildren(List<MenuInfo> menuInfoList, MenuInfo dest) {
        if (CollectionUtils.isNotEmpty(menuInfoList) && dest != null) {
            for (MenuInfo menuInfo : menuInfoList) {
                if (dest.getMenuCode().equals(menuInfo.getPrntCode())) {
                    buildChildren(menuInfoList, menuInfo);
                    List<MenuInfo> children = dest.getChildren();
                    children.add(menuInfo);
                    dest.setChildren(children);
                }
            }
        }
    }
}
