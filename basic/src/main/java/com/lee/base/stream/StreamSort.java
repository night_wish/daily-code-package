package com.lee.base.stream;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.lee.base.commons.Employee;
import com.lee.base.commons.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class StreamSort {

    @Test
    public void testDistinct() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<Employee> employeeList = Arrays.asList(
                new Employee(1,"张三",sdf.parse("2020-01-01")),
                new Employee(1,"张三",sdf.parse("2020-01-01")),
                new Employee(1,"张三",sdf.parse("2020-01-01")),
                new Employee(2,"李四",sdf.parse("2020-01-07")),
                new Employee(3,"王五",sdf.parse("2020-01-02")),
                new Employee(3,"王五",sdf.parse("2020-01-02")),
                new Employee(3,"王五",sdf.parse("2020-01-02")),
                new Employee(4,"赵六",sdf.parse("2020-01-03")),
                new Employee(5,"刘七",sdf.parse("2020-01-06")),
                new Employee(5,"刘七",sdf.parse("2020-01-06"))
        );

        employeeList = employeeList.stream().distinct().collect(Collectors.toList());
        for(Employee e : employeeList){
            System.out.println(e);
        }
    }


    @Test
    public void testSorted() throws ParseException {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

      List<Employee> employeeList = Arrays.asList(
              new Employee(1,"张三",sdf.parse("2020-01-01")),
              new Employee(2,"李四",sdf.parse("2020-01-07")),
              new Employee(3,"王五",sdf.parse("2020-01-02")),
              new Employee(4,"赵六",sdf.parse("2020-01-03")),
              new Employee(5,"刘七",sdf.parse("2020-01-06"))
      );

      List<Employee> collect = employeeList.stream().sorted((e1, e2) -> {
          return e1.getCreateTime().compareTo(e2.getCreateTime());
      }).collect(Collectors.toList());

      for(Employee e : collect){
          System.out.println(e);
      }
    }


    @Test
    public void testFilter(){
        Employee e1 = new Employee(1,"张三",25,"male",17000d);
        Employee e2 = new Employee(2,"李四",18,"female",19000d);
        Employee e3 = new Employee(3,"王五",55,"male",7000d);
        Employee e4 = new Employee(4,"赵六",39,"female",12000d);
        Employee e5 = new Employee(5, "刘七", 28, "male", 14000d);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(e1);
        employeeList.add(e2);
        employeeList.add(e3);
        employeeList.add(e4);
        employeeList.add(e5);

        List<Employee> list = employeeList.stream().filter(e -> e.getGender().contains("female")).collect(Collectors.toList());
//        employeeList.removeIf(e -> e.getGender().contains("female"));
        employeeList.removeAll(list);
        for(Employee e : employeeList){
            System.out.println(e);
        }

    }


    @Test
    public void testMap(){
        Employee e1 = new Employee(1,"张三",25,"male",17000d);
        Employee e2 = new Employee(2,"李四",18,null,19000d);
        Employee e3 = new Employee(3,"王五",55,null,7000d);
        Employee e4 = new Employee(4,"赵六",39,"female",12000d);
        Employee e5 = new Employee(5, "刘七", 28, "male", 14000d);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(e1);
        employeeList.add(e2);
        employeeList.add(e3);
        employeeList.add(e4);
        employeeList.add(e5);

        List<String> collect = employeeList.stream().filter(e-> !StringUtils.isEmpty(e.getGender())).map(e -> e.getGender()).collect(Collectors.toList());
        for(String e : collect){
            System.out.println(e);
        }
    }


    @Test
    public void testDistinct2(){
        Employee e1 = new Employee(1,"张三",25,"male",17000d);
        Employee e2 = new Employee(2,"李四",18,null,19000d);
        Employee e3 = new Employee(3,"王五",55,null,7000d);
        Employee e4 = new Employee(4,"赵六",39,"female",12000d);
        Employee e5 = new Employee(5, "刘七", 28, "male", 14000d);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(e1);
        employeeList.add(e2);
        employeeList.add(e3);
        employeeList.add(e4);
        employeeList.add(e5);

        List<User> list = employeeList.stream().map(e->{
            return new User(1,"aa",2);
        }).collect(Collectors.toList());

        for (User u : list){
            System.out.println("l :"+u);
        }

        List<User> collect = list.stream().distinct().collect(Collectors.toList());

        for (User u : collect){
            System.out.println("c : "+u);
        }

        List<User> nullList = new ArrayList<>();

        List<User> collect2 = nullList.stream().distinct().collect(Collectors.toList());
        for (User u : collect2){
            System.out.println("c : "+u);
        }
    }


    @Test
    public void testToMap(){
        Employee e1 = new Employee(1,"张三",25,"male",17000d);
        Employee e6 = new Employee(1,"张三",25,"male",17000d);
        Employee e2 = new Employee(2,"李四",18,null,19000d);
        Employee e3 = new Employee(3,"王五",55,null,7000d);
        Employee e4 = new Employee(4,"赵六",39,"female",12000d);
        Employee e5 = new Employee(5, "刘七", 28, "male", 14000d);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(e1);
        employeeList.add(e2);
        employeeList.add(e3);
        employeeList.add(e4);
        employeeList.add(e5);
        employeeList.add(e6);

        Map<String, Integer> collect = employeeList.stream().collect(Collectors.toMap(Employee -> Employee.getName() + Employee.getGender(), Employee -> Employee.getId(),(oldData,newData)->newData));
        System.out.println(collect);

        Map<String, Employee> collect1 = employeeList.stream().collect(Collectors.toMap(Employee::getName, o -> o,(oldData,newData)->newData));
        log.info("====>{}", JSON.toJSONString(collect1));

        Map<String, Employee> collect2 = employeeList.stream().collect(Collectors.toMap(Employee::getName, Function.identity(),(oldData,newData)->newData));
        log.info("====>{}", JSON.toJSONString(collect2));

        //分组1
        Map<String, List<Employee>> collect5 = employeeList.stream().collect(Collectors.groupingBy(e -> e.getGender()));
        log.info("====>{}",collect5);

        //分组2
        Map<String, List<Employee>> collect6 = employeeList.stream().collect(Collectors.toMap(t->t.getGender(),o-> Lists.newArrayList(o),(o, n) ->{ o.addAll(n);return o;}));
        log.info("====>{}",collect6);
    }


    @Test
    public void testList(){
        Employee e1 = new Employee(1,"张三",25,"male",17000d);
        Employee e6 = new Employee(1,"张三",25,"male",17000d);
        Employee e2 = new Employee(2,"李四",18,"female",19000d);
        Employee e3 = new Employee(3,"王五",55,"male",7000d);
        Employee e4 = new Employee(4,"赵六",39,"female",12000d);
        Employee e5 = new Employee(5, "刘七", 28, "male", 14000d);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(e1);
        employeeList.add(e2);
        employeeList.add(e3);
        employeeList.add(e4);
        employeeList.add(e5);
        employeeList.add(e6);

        List<String> collect3 = employeeList.stream().map(t -> t.getName()).collect(Collectors.toList());
        log.info("====>{}", JSON.toJSONString(collect3));

        List<String> collect4 = employeeList.stream().map(Employee::getName).collect(Collectors.toList());
        log.info("====>{}", JSON.toJSONString(collect4));



    }



}
