package com.lee.base.json;

import com.alibaba.fastjson.JSONObject;
import com.lee.base.commons.Employee;
import com.lee.base.commons.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 项目名称：basic
 * 类 名 称：FastJsonTest
 * 类 描 述：
 * 创建时间：2021-08-10 10:11
 * 创 建 人：Lee
 */
@Slf4j
public class FastJsonTest {

    //单行诊断字节数
    private static final Long DISE_VOLUME = 63L;
    //单行就诊字节数
    private static final Long MDTRT_VOLUME = 306L;
    //单行处方字节数
    private static final Long RX_VOLUME = 264L;

    @Value("${engine.ali.dwd:dwd.}")
    private String dwd;

    //a.FIXMEDINS_OPTINS in (select admdvs from dwd.dwd_oth_admdvs_dim_a where city_admdvs in ('420700') or admdvs in ('420700'))
    //a.FIXMEDINS_OPTINS in (select ada.admdvs from dwd.dwd_oth_admdvs_dim_a as ada where ada.city_admdvs in ('001','002','003')  or ada.admdvs in ('001','002','003'))
//    a.FIXMEDINS_OPTINS in (
//            select ada.admdvs from dwd.dwd_oth_admdvs_dim_a as ada where (
//                    ada.city_admdvs in ('001','002','003')  or ada.admdvs in ('001','002','003')
//            )
//                    )
    @Test
    public void testSQL(){
        List<String> list = new ArrayList<>();
        list.add("001");
        list.add("002");
        list.add("003");
//        String dwd = "dwd.";
        String admdvsTableName = "dwd_oth_admdvs_dim_a";
        String str = "";
        StringBuilder sql = new StringBuilder();
        if(null!=list&&0<list.size()) {
            str= StringUtils.join(list.toArray(), "','");

            log.info("========>{}",str);
            sql.append(" a.FIXMEDINS_OPTINS in (select ada.admdvs from ").append(dwd).append(admdvsTableName).append(" as ada where (ada.city_admdvs in ('")
                    .append(str).append("')  or ada.admdvs in ('").append(str).append("')))");
        }

        log.info("sql : {}",sql.toString());
    }


    @Test
    public void testJSONList(){
//        User u1 = new User(1,"lee",20);
//        User u2 = new User(2,"ren",20);
//        List<User> list = new ArrayList<>();
//        list.add(u1);
//        list.add(u2);
//        log.info("result:{}",list);
        Double volume = (RX_VOLUME + DISE_VOLUME + MDTRT_VOLUME) * Double.valueOf(20000000) / 1024 / 1024 / 1024d;
        Double bucks = Math.ceil(volume / 3);
        log.info("ceils:{}",bucks.intValue());
    }


    @Test
    public void testFastJson(){
        User user =  new User(1,"aa",92);
        Map<String, Object> params = (Map<String, Object>) JSONObject.toJSON(user);
        log.info("---->params : {}"+params);
    }
}
