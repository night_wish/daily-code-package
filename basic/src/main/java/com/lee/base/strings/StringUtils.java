package com.lee.base.strings;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/3/4 17:07
 */
@Slf4j
public class StringUtils {

    @Test
    public void testReplace1(){
        List<String> medinsLv = Arrays.asList("1","2","3","9");
        List<String> newMedinsLv = new ArrayList<>();
        if(medinsLv.contains("1")){
            newMedinsLv.add("08");
            newMedinsLv.add("09");
            newMedinsLv.add("10");
        }

        if(medinsLv.contains("2")){
            newMedinsLv.add("05");
            newMedinsLv.add("06");
            newMedinsLv.add("07");
        }

        if(medinsLv.contains("3")){
            newMedinsLv.add("01");
            newMedinsLv.add("02");
            newMedinsLv.add("03");
            newMedinsLv.add("04");
        }

        if(medinsLv.contains("9")){
            newMedinsLv.add("11");
        }
        newMedinsLv.stream().forEach(System.out::println);
    }

    @Test
    public void testFormat(){
        String format = String.format("%080d", 20220304);
        log.info("====结果是====={},length={}",format,format.length());
    }

    public static void main(String[] args) {
        String s1 = "adshfalwieuhewewnvflkjdslqqqqqqqqqqqqqqqqqqqqqqqqqqq";
        String s2 = "ewfdsknjlifdsiauewi";
        Long hash = Math.abs(Long.parseLong(String.valueOf(Objects.hash(s1, s2))));
        String format = String.format("%09d", hash);
        if (format.length() > 9) {
            format =  format.substring(0, 19);
        }
        log.info("====result : {}",format);
    }
}
