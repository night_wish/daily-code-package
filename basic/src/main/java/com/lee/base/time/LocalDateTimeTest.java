package com.lee.base.time;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.time.*;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/7/28 9:56
 */
@Slf4j
public class LocalDateTimeTest {

    public static void main(String[] args) {
        for (int i=0;i<10;i++){
            System.out.println("====>"+i);
            if(i==5){
                try {
                    int a = 10/0;
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        }
    }

    @Test
    public void string2LocalDateTime(){
        String dateStr = "2022-08-26 16:26:38";
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.parse(dateStr,format);
        log.info("localDateTime: {}",localDateTime);
    }

    @Test
    public void testDateToLocalDateTime(){
        Date date = new Date();
        ZonedDateTime zonedDateTime = date.toInstant().atZone(ZoneId.systemDefault());
        LocalDateTime localDateTime = zonedDateTime.toLocalDateTime();
        log.info("localDateTime: {}",localDateTime);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String res = localDateTime.format(format);
        log.info("format : {}",res);
    }

    @Test
    public void testLocalDateTime2Date(){
        LocalDateTime localDateTime = LocalDateTime.of(LocalDate.now(),LocalTime.of(15,56,01));
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        Date from = Date.from(instant);
        log.info(" date : {}",from);
    }


//    public static void main(String[] args) {
//        LocalDateTimeTest t = new LocalDateTimeTest();
//        long sleepDur = t.calculSleepDur(0, 0, 1);
//        System.out.println(sleepDur);
//    }


    public Boolean ifNeedSleep(){
        LocalDateTime nowDateTime = LocalDateTime.now();
        LocalDateTime workStartTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(10,0,0));
        LocalDateTime workEndTime = LocalDateTime.of(LocalDate.now(),LocalTime.of(21,0,0));
        if(nowDateTime.isAfter(workStartTime) && nowDateTime.isBefore(workEndTime)){
            return false;
        }
        return true;
    }


    public long calculSleepDur(int hours,int minutes,int seconds){
        LocalTime nowTime = LocalTime.of(hours,minutes,seconds);
        LocalTime dayStartTime = LocalTime.of(0,0,0);
        LocalTime dayEndTime = LocalTime.of(23,59,59);
        LocalTime workEndTime = LocalTime.of(21,0,0);
        LocalTime workStartTime = LocalTime.of(10,0,0);
        if(nowTime.compareTo(workEndTime)>0 && nowTime.compareTo(dayEndTime)<=0){
            //21点之后,0点之前
            return Duration.between(nowTime, dayEndTime).getSeconds()+Duration.between(dayStartTime, workStartTime).getSeconds();
        }else if(nowTime.compareTo(dayStartTime)>=0 && nowTime.compareTo(workStartTime)<0){
            //0点之后,10点之前
            return Duration.between(nowTime, workStartTime).getSeconds();
        }else{
            return 0;
        }
    }


}
