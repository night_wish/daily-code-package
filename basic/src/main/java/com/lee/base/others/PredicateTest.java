package com.lee.base.others;

import com.lee.base.commons.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/8/19 13:26
 */
@Slf4j
public class PredicateTest {

    @Test
    public void predicateEqualsTest(){
        //5、equal 相等操作
        User u = new User(1,"lee",20);
        Predicate<User> predicate = Predicate.isEqual(u);

        log.info("----->{}",predicate.test(new User(1,"ren",20)));//false
        log.info("----->{}",predicate.test(new User(1,"lee",20)));//true
        log.info("----->{}",predicate.test(new User(2,"lee",20)));//false
    }

    @Test
    public void predicateOrTest() {
        //4、or或操作
        Predicate<String> predicate = x -> x.equals("A");
        predicate = predicate.or(x->x.equals("B"));

        log.info("---->{}",predicate.test("A"));//true
        log.info("---->{}",predicate.test("B"));//true
        log.info("---->{}",predicate.test("C"));//false
        log.info("---->{}",predicate.test("D"));//false
    }

    @Test
    public void predicateNagateTest() {
        //3、非操作
        Predicate<Integer> predicate = x -> x > 7;
        predicate = predicate.and(x -> x % 2 == 0);
        predicate = predicate.negate();//将之前的判断 做非操作
        log.info("---->{}", predicate.test(6));//true
        log.info("---->{}", predicate.test(8));//false
        log.info("---->{}", predicate.test(9));//true
        log.info("---->{}", predicate.test(10));//false
    }

    @Test
    public void predicateAndTest() {
        //2、判断数字是否大于7且是偶数
        Predicate<Integer> predicate = x -> x > 7;
        predicate = predicate.and(x -> x % 2 == 0);

        log.info("--->{}", predicate.test(6));//false
        log.info("--->{}", predicate.test(8));//true
        log.info("--->{}", predicate.test(9));//fasle
        log.info("--->{}", predicate.test(10));//true
    }

    @Test
    public void predicateTest() {
        //1、判断数字是否大于7
        Predicate<Integer> predicate = x -> x > 7;
        log.info("---->{}", predicate.test(6));//false
        log.info("---->{}", predicate.test(7));//false
        log.info("---->{}", predicate.test(8));//true
    }
}
