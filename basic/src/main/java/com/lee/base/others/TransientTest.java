package com.lee.base.others;

import com.lee.base.commons.LoginUser;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.util.StringUtils;

import java.io.*;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/7/20 10:20
 */
@Slf4j
public class TransientTest {

    @Test
    public void transientTest() throws IOException, ClassNotFoundException {
        //序列化
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername("lee");
        loginUser.setPassword("123");
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("D:/User.txt"));
        oos.writeObject(loginUser);
        oos.flush();
        oos.close();

        //反序列化
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("D:/User.txt"));
        LoginUser o = (LoginUser) ois.readObject();
        ois.close();
        log.info("username : {}",o.getUsername());
        log.info("password : {}",o.getPassword());
        log.info("gender : {}", LoginUser.getGender());
    }

    @Test
    public void staticTest() throws IOException, ClassNotFoundException {
        //序列化
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername("lee");
        loginUser.setPassword("123");
        LoginUser.gender = "male";
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("D:/User.txt"));
        oos.writeObject(loginUser);
        oos.flush();
        oos.close();

        //反序列化
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("D:/User.txt"));
        LoginUser o = (LoginUser) ois.readObject();
        ois.close();
        log.info("username : {}",o.getUsername());
        log.info("password : {}",o.getPassword());
        log.info("gender : {}", LoginUser.getGender());
    }

    public static void main(String[] args) {
        System.out.println(checkKngGroup2ItemWithHospLV("L2",""));
    }

    private static boolean checkKngGroup2ItemWithHospLV(String kngGroup2Item, String medinsLV) {
        //todo:校验医疗机构等级
        if(!StringUtils.isEmpty(medinsLV.trim())){
            Integer medinsLvTmp = Integer.valueOf(medinsLV.trim());
            Integer kngGroup2ItemVal = Integer.valueOf(kngGroup2Item.substring(1));
            if(kngGroup2Item.startsWith("H")){
                if(medinsLvTmp.compareTo(kngGroup2ItemVal)>0){
                    return true;
                }
            }else{
                if(medinsLvTmp.compareTo(kngGroup2ItemVal)<0){
                    return true;
                }
            }
        }
        return false;
    }
}
