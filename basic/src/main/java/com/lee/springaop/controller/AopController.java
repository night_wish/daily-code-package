package com.lee.springaop.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/aopController")
public class AopController {

    @GetMapping(value = "/Curry")
    public void Curry(){
        log.info("库里上场打球了！！");
    }

    @GetMapping(value = "/Harden")
    public void Harden(){
        log.info("哈登上场打球了！！");
    }

    @GetMapping(value = "/Antetokounmpo")
    public void Antetokounmpo(){
        log.info("字母哥上场打球了！！");
    }


    @GetMapping(value = "/Jokic")
    public void Jokic(){
        log.info("约基奇上场打球了！！");
    }

    @GetMapping(value = "/Durant/{point}")
    public void Durant(@PathVariable("point")  int point){
        log.info("杜兰特上场打球了！！");
    }

    @RequestMapping("/testTaskOrgStt/{taskId}")
    public String testTaskOrgStt(@PathVariable("taskId")String taskId){
        log.info("====================,{}",taskId);
        return "success";
    }

}
