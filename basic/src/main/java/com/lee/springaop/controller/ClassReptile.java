package com.lee.springaop.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/2/24 11:06
 */
@RestController
public class ClassReptile {

    @GetMapping("/restTest/{courseId}")
    public void test(@PathVariable("courseId")String courseId) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        headers.add("cookie", "NTESSTUDYSI=5129a676cc7a45288c82ed0c253ee041; EDUWEBDEVICE=da0c6586487b4e49a057f45d594c0aff; __yadk_uid=Wi3Xk3Rr2Qy6CN54WAhbSnBnpUTqSVTY; WM_TID=Vlf/rEOpdlNEUUAFEBYu7lkjRLPfV/kR; Hm_lvt_77dc9a9d49448cf5e629e5bebaa5500b=1645594134; WM_NI=/++O9xpoDPI/+TFlEvV+nu+RNb7vHu7rXO0AnTDsD2U0PYXZ76ntyeLWeBXcuCzYIdWrIGd441AmiGhi+n24vzVztKNctcDLIuxNVs18eQdtyb6HPRSJsy//hYEGp7lKQjk=; WM_NIKE=9ca17ae2e6ffcda170e2e6ee8fee65fced008fd5698feb8bb7d15f838b8fafaa7ea9b289d1e5349ae8968ffc2af0fea7c3b92aedf5e5b0bc74b4b18cb1cc25e98c8a8dc87e96f5a296aa5a8ebef997cb7ff5ae98b0b34bae8c8c94c73a97f0a0badb52f5eaa295fc6f9089aba3db69b79db6bacc7095ae87a8e740f8ef8f8eec4dacb7e58fb140b6a69a9bf7418ebdbc84c7488babb8afd579f3e8abb2e780bbbefaa9d35a9196bd9aae25ac878ba8f33db0959f8bea37e2a3; NTES_YD_SESS=IzTiGYx5no7eENw4t7sJLte_bd9ofJvoN.d7omufc7nvYtclYUOmhkqIkWt6gpssGS73hzbOgKv.cYb_5QYl6yex_Ekd76Ogj5RybaztJH1XwuYPru7O1yrdH5PCvDC2gHrRHpfhXwMDIpNcvfcZwVXH3Fj1gLzjHcipcmWMt28HT5J1Y0dtUMYfXPsDtTEs7scdMxqybk3N3lIlLVdVKlmpBrquI2clAVNkp4.d533sD; NTES_YD_PASSPORT=3Y9J6zk4.LnL72Q29V0CLZkzTcIXbtXgmpx0qesJafPxl.DwlvG8Q0iZ0V.HaRnn4rEXQktGaLctjDE.sdgjkFEkLNvg2k0SfIsr9WEZ6H4qU4yUMW2AQOxMfIoC26vbAjR01BGKpzb2zeTKDqjsMdsdKPHZyjPHYuYAmXTz2hQA8EkMLjMWp3vRUJYj.PQM1WG5Pt4GJAFFuFHfL4zpMpwE_; S_INFO=1645671286|0|0&60##|18310103915; P_INFO=18310103915|1645671286|1|imooc|00&99|null&null&null#bej&null#10#0|&0|null|18310103915; st_autoregister=1; STUDY_SESS=\"8kLuLZkDV+q/P3hLEWPnlUCc77UxGbdrj3ualCQfkqx0TgHcn9hJwtBUWU1vkClHSLC8Ox/RYbPhnV5X7v04pH1uVPx07weGFJkRGGRmMwqdTif/21t0pBeAzs+RQMo+DPOx9No8WgWfZqAFdlhs60xN/58IszT8VYprM9DWSDJ0ECcYCppL/GPTsECRQ39VTI8+lZKyHhiycNQo+g+/oA==\"; STUDY_INFO=\"yd.db8c89d161534e81a@163.com|8|1510883346|1645671297246\"; NETEASE_WDA_UID=1510883346#|#1645671297062; hasVolume=true; videoVolume=0.8; Hm_lpvt_77dc9a9d49448cf5e629e5bebaa5500b=1645671380");
        headers.add("origin", "https://www.icourse163.org");
        headers.add("referer", "https://www.icourse163.org/course/YCTC-1207453802");

        MediaType type = MediaType.parseMediaType("application/x-www-form-urlencoded");
        headers.setContentType(type);
        for (int i=1;i<68;i++) {
            String url = "https://www.icourse163.org/web/j/mocCourseV2RpcBean.getCourseEvaluatePaginationByCourseIdOrTermId.rpc?csrfKey=5129a676cc7a45288c82ed0c253ee041";
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("courseId", courseId);
            params.add("pageIndex", String.valueOf(i));
            params.add("pageSize", "50");
            params.add("orderBy", "3");
            HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<ClassResp> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, ClassResp.class);

            ClassResp body = response.getBody();
            List<Content> list = body.getResult().getList();
            for (Content content : list) {
                System.out.println(content.getContent());
            }
        }
    }
}
