package com.lee.springaop.controller;

import lombok.Data;

import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/2/24 11:26
 */
@Data
public class ClassResp {

    private String code;

    private String message;

    private String traceId;

    private Boolean sampled;

    private Result result;

}

@Data
class Result{
    private List<Content> list;
}

@Data
class Content{
    private String content;
}
