package com.lee.springaop.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class CustomAspect {


    //定义切入点
    @Pointcut("execution(public * com.lee.springaop.controller.AopController.*(..)))")
    public void CustomAspect(){

    }


    @Pointcut("execution(public * com.lee.springaop.controller.AopController.Durant(..))")
    public void CustomAroundAspect(){

    }

    @Pointcut("execution(public * com.lee.springaop.controller.AopController.Durant(int)) && args(int))")
    public void CustomAroundAspect2(){

    }

    //前置通知
    @Before("CustomAspect()")
    public void doBeforeGame(){
        log.info("before : 经纪人正在处理球星赛前事务！");
    }

    //后置通知
    @After("CustomAspect()")
    public void doAfterGame(){
        log.info("after : 经纪人为球星表现疯狂鼓掌！");
    }

    //后置通知--正常返回
    @AfterReturning("CustomAspect()")
    public void doAfterReturningGame(){
        log.info("after returning : 返回通知：经纪人为球星表现疯狂鼓掌！");
    }

    //后置通知--发生异常
    @AfterThrowing("CustomAspect()")
    public void doAfterThrowingGame(){
        log.info("after throwing : 异常通知：球迷要求退票！");
    }


    //环绕通知
    @Around("CustomAroundAspect()")
    public void doAroundGame(ProceedingJoinPoint joinPoint) {
        try {

            log.info("Around before 1: 经纪人正在处理球星赛前事务");

            //参数
            Object[] args = joinPoint.getArgs();
            for (Object o : args){
                log.info("params 1: "+o.toString());
            }

            //target
            Object target = joinPoint.getTarget();
            log.info("target 1: "+target.toString());

            //进行下一个advice或目标方法调用
            joinPoint.proceed();

            log.info("Around after 1 : 返回通知：经纪人为球星表现疯狂鼓掌");
        } catch (Throwable throwable) {
            log.info("Aroung throwing 1: 异常通知：球迷要求退票");
        }
    }


    //环绕通知
    @Around("CustomAroundAspect2() && args(point)")
    public void doAroundGame2(ProceedingJoinPoint joinPoint,int point) {
        try {

            log.info("Around before 2: 经纪人正在处理球星赛前事务");


            //进行下一个advice或目标方法调用
            joinPoint.proceed();

            log.info("球星本场得到 2:" + point + "分" );

            log.info("Around after 2 : 返回通知：经纪人为球星表现疯狂鼓掌");
        } catch (Throwable throwable) {
            log.info("Aroung throwing 2 : 异常通知：球迷要求退票");
        }
    }


}
