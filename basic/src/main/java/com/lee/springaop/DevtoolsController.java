package com.lee.springaop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class DevtoolsController {


    @Value("${mydev.test}")
    private String test;

    @GetMapping("/sayhi")
    public String sayHi(){
        String flag = "dsadsad6233ew";
        log.info("=====>flag:{},  test:{}",flag,test);
        return "ok";
    }
}
