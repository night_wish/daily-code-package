package com.lee.spark;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF2;
import org.apache.spark.sql.types.DataTypes;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/8/26 16:17
 */
@Slf4j
public class SparkTest {

    @Test
    public void testReadCsv() {
        Map<String, String> docSusptMap = new HashMap<>();
        SparkSession sparkSession = SparkSession.builder().master("local").appName("app").getOrCreate();

        sparkSession.udf().register("resetSusptDetlDocId", new ResetSusptDetlDocId(), DataTypes.StringType);


        Properties prop = new Properties();
        prop.setProperty("user", "root");
        prop.setProperty("password", "admin123");
        Dataset<Row> docSusptDataSet = sparkSession.read().jdbc("jdbc:mysql://192.168.73.107:13306/intlgsupn_hblk_db", "(select * from doc_suspt_d) tt", prop);

        Dataset<Row> docSusptDetlDataSet = sparkSession.read().jdbc("jdbc:mysql://192.168.73.107:13306/intlgsupn_hblk_db", "(select * from doc_suspt_detl_d) tt", prop);

        List<Row> rows = docSusptDataSet.collectAsList();
        for (Row row : rows) {
            docSusptMap.put(row.getAs("DOC_SUSPT_ID"), row.getAs("DOC_ID"));
        }
        docSusptDataSet.show();
        docSusptDetlDataSet.show();


//        docSusptDetlDataSet.

//        Dataset<Row> newDocSusptDetlDataSet = docSusptDetlDataSet.withColumn("doc_id", functions.callUDF("resetSusptDetlDocId", docSusptDetlDataSet.col("doc_suspt_id")));
//        newDocSusptDetlDataSet.show();

        log.info("读取数据 : docSusptMap:{}", JSON.toJSONString(docSusptMap));
    }

}

class ResetSusptDetlDocId implements UDF2<String, Map<String, String>, String> {
    @Override
    public String call(String s, Map<String, String> docSusptMap) throws Exception {
        if (docSusptMap != null && StringUtils.isNotBlank(s) && docSusptMap.containsKey(s)) {
            return docSusptMap.get(s);
        }
        return null;
    }
}