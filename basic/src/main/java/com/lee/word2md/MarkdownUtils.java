package com.lee.word2md;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024-01-15 18:02
 */
public class MarkdownUtils {


    private static final Pattern BOLD_PATTERN = Pattern.compile("(\\*\\*[^\\*]+\\*\\*)");
    private static final Pattern ITALIC_PATTERN = Pattern.compile("(\\_\\_[^\\_]+\\_\\_)");
    private static final Pattern CODE_BLOCK_PATTERN = Pattern.compile("(\\`\\`\\`[^\\`]+\\`\\`\\`)");
    private static final Pattern LINK_PATTERN = Pattern.compile("(\\[(.+?)\\]\\((.+?)\\))");

    public static String toMarkdown(String text) {
        String result = text;

        if(!StringUtils.isEmpty(result)){
            result = BOLD_PATTERN.matcher(result).replaceAll("**$1**");
            result = ITALIC_PATTERN.matcher(result).replaceAll("__$1__");
            result = CODE_BLOCK_PATTERN.matcher(result).replaceAll("```$1```");
            result = LINK_PATTERN.matcher(result).replaceAll("[$1]($2)");
        }

        return result;
    }
}
