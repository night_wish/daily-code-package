package com.lee.word2md;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFPictureData;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024-01-15 18:01
 */
public class Doc2Md {

    public static void main(String[] args) throws IOException {
        // 读取 docx 文件
        FileInputStream inputStream = new FileInputStream(new File("C:\\Users\\le'e\\Downloads\\html5.docx"));
        XWPFDocument document = new XWPFDocument(inputStream);

        // 获取所有段落
        List<XWPFParagraph> paragraphs = document.getParagraphs();

        // 遍历所有段落
        for (XWPFParagraph paragraph : paragraphs) {
            // 获取所有文本运行
            List<XWPFRun> runs = paragraph.getRuns();

            // 遍历所有文本运行
            for (XWPFRun run : runs) {
                // 获取文本内容
                String text = StringUtils.isEmpty(run.getText(0)) ? "": new String(run.getText(0).getBytes(StandardCharsets.UTF_8));


                // 将文本内容转换为 Markdown 格式
                text = MarkdownUtils.toMarkdown(text);

                // 设置文本内容
                run.setText(text, 0); // 设置运行中的第一个文本
            }
        }

        // 获取所有图片
        List<XWPFPictureData> pictures = document.getAllPictures();

        // 遍历所有图片
        for (XWPFPictureData picture : pictures) {
            // 获取图片的数据
            byte[] data = picture.getData();

            // 获取图片的文件名
            String fileName = picture.getFileName();

            // 将图片保存到本地
            FileOutputStream outputStream = new FileOutputStream(new File("C:\\Users\\le'e\\Downloads\\imgs\\html+css\\"+fileName));
            outputStream.write(data);
            outputStream.close();
        }

        // 输出 Markdown 文件
        FileOutputStream outputStream = new FileOutputStream(new File("C:\\Users\\le'e\\Downloads\\html5.md"));
        document.write(outputStream);
        outputStream.close();

    }
}
