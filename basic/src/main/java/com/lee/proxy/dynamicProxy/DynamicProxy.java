package com.lee.proxy.dynamicProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DynamicProxy implements InvocationHandler {

    private Object factory;

    public DynamicProxy(Object factory) {
        this.factory = factory;
    }


    //通过Proxy动态代理的对象
    public Object getProxyInstance(){
        return Proxy.newProxyInstance(factory.getClass().getClassLoader(),factory.getClass().getInterfaces(),this);
    }


    //通过动态代理对目标方法进行增强
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        askCustomer();
        Object invoke = method.invoke(factory, args);
        dealProduct();
        return invoke;
    }


    public void askCustomer(){
        System.out.println("before 询问客户需要什么样的商品");
    }

    public void dealProduct(){
        System.out.println("after 包装好后将商品交给客户");
    }
}
