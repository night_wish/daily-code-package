package com.lee.proxy.dynamicProxy;

/**
 * 测试：动态代理
 */
public class DynamicTest {

    public static void main(String[] args) {
        CFactory cFactory = new CFactoryImpl();
        DynamicProxy proxy_1 = new DynamicProxy(cFactory);
        CFactory proxy_1Instance = (CFactory) proxy_1.getProxyInstance();
        proxy_1Instance.produceShoes("蓝色");

        System.out.println("-----------------------------------");

        DFactory dFactory = new DFactoryImpl();
        DynamicProxy proxy_2 = new DynamicProxy(dFactory);
        DFactory proxy_2Instance = (DFactory) proxy_2.getProxyInstance();
        proxy_2Instance.producePants("休闲");
    }

}
