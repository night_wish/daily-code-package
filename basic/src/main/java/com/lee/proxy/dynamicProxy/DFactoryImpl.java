package com.lee.proxy.dynamicProxy;

/**
 * D工厂，生产裤子
 */
public class DFactoryImpl implements DFactory {


    public void producePants(String type){
        System.out.println("DFactory 生产了 "+type+" 裤子");
    }

}
