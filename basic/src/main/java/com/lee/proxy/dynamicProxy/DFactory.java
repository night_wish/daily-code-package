package com.lee.proxy.dynamicProxy;
/**
 * D工厂，生产裤子
 */
public interface DFactory {

    public void producePants(String type);
}
