package com.lee.proxy.dynamicProxy;
/**
 * C工厂，生产鞋子
 */
public interface CFactory {

    public void produceShoes(String type);
}
