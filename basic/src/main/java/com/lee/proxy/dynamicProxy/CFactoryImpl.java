package com.lee.proxy.dynamicProxy;

/**
 * C工厂，生产鞋子
 */
public class CFactoryImpl implements CFactory {


    public void produceShoes(String type){
        System.out.println("CFactory 生产了 "+type+" 鞋子");
    }

}
