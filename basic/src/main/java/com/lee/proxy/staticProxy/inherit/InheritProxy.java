package com.lee.proxy.staticProxy.inherit;

/**
 * A工厂的代理
 */
public class InheritProxy extends AFactory {

    public void produceKeyBoard(String type){
        askCustomer();
        System.out.println("AFactory 生产了 "+type+" 键盘");
        dealProduct();
    }


    public void askCustomer(){
        System.out.println("before 询问客户需要什么样的键盘");
    }

    public void dealProduct(){
        System.out.println("after 包装好后将键盘交给客户");
    }

}
