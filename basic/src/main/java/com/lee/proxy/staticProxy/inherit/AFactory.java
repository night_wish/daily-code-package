package com.lee.proxy.staticProxy.inherit;

/**
 * A工厂，生产键盘
 */
public class AFactory {


    public void produceKeyBoard(String type){
        System.out.println("AFactory 生产了 "+type+" 键盘");
    }

}
