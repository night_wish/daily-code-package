package com.lee.proxy.staticProxy.aggregation;

/**
 * 测试：java可以实现多个接口，如果我们需要C工厂的鞋子，
 *       只要代理类实现C工厂的接口就可以了
 *
 *       但是代码冗余，只要我们需求不断增加代理类就得不断修改
 */
public class AggregationTest {

    public static void main(String[] args) {
        BFactory factory = new BFactoryImpl();
        AggregationProxy proxy = new AggregationProxy(factory);
        proxy.produceClothes("红色");
    }
}
