package com.lee.proxy.staticProxy.inherit;

/**
 * 测试
 *   由于java只能extends父类，如果我想要B工厂的衣服怎么办？
 *   再给B工厂也写一个代理？
 *
 */
public class InheritTest {

    public static void main(String[] args) {
        AFactory factory = new InheritProxy();
        factory.produceKeyBoard("青轴");
    }

}
