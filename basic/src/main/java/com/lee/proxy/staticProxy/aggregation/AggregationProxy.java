package com.lee.proxy.staticProxy.aggregation;

/**
 * B工厂的代理
 */
public class AggregationProxy implements BFactory {

    private BFactory factory;

    public AggregationProxy(BFactory factory) {
        this.factory = factory;
    }

    @Override
    public void produceClothes(String type) {
        askCustomer();
        factory.produceClothes(type);
        dealProduct();
    }

    public void askCustomer(){
        System.out.println("before 询问客户需要什么样的衣服");
    }

    public void dealProduct(){
        System.out.println("after 包装好后将衣服交给客户");
    }


}
