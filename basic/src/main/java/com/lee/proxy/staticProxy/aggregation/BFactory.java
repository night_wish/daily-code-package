package com.lee.proxy.staticProxy.aggregation;
/**
 * B工厂，生产衣服
 */
public interface BFactory {

    public void produceClothes(String type);
}
