package com.lee.proxy.staticProxy.aggregation;

/**
 * B工厂，生产衣服
 */
public class BFactoryImpl implements BFactory{


    public void produceClothes(String type){
        System.out.println("BFactory 生产了 "+type+" 衣服");
    }

}
