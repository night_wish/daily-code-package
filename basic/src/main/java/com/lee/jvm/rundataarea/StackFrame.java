package com.lee.jvm.rundataarea;

import java.util.ArrayList;
import java.util.List;

public class StackFrame {

    public static void main(String[] args) {
        Byte[] b = new Byte[100*1024*1024];
        List<Byte[]> list = new ArrayList<>();
        list.add(b);
    }

    public void methodA(){
        int i = 10;
        int j = 20;
        methodB();
    }

    public void methodB(){
        int k = 30;
        int l = 40;
    }
}
