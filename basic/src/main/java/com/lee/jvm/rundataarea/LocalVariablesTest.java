package com.lee.jvm.rundataarea;

public class LocalVariablesTest {

    public static void main(String[] args) {
        LocalVariablesTest test = new LocalVariablesTest();
        int i=0;
        test.methods1();

        try {
            Thread.sleep(1000000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public  int methods1(){
        int j=10;
        double k = 2.0;
        for(int f=0;f<10;f++){
            System.out.println("----");
        }
        int m = 2;
        System.out.println("this is method 1");
        return j;
    }
}
