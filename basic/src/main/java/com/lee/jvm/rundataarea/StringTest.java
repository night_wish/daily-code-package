package com.lee.jvm.rundataarea;

public class StringTest {

    public static void main(String[] args) {
        String s1 = new String("77");//s1指向堆中的地址，且常量池中有77
        s1.intern();//指向了常量池中已有的77,但没有把 这个引用赋值给s1,它是不同于s1=s1.intern()的
        String s2 = new String("77").intern();//s2指向了s1在常量池中创建的77的地址
        String s3 = "77";//s3也指向了s1在常量池中创建的77的地址
        System.out.println(s1==s2);//false
        System.out.println(s1==s3);//false
        System.out.println(s2==s3);//true

        String s4 = new String("1")+new String("2");//s4指向了自己再堆中的地址，且在常量池中创建了1和2,但没有创建12
        s4.intern();//在常量池中创建了77,常量池中的77指向了s4在堆中的地址
        String s5 = "12";//s5指向了常量池中77,相当于指向了s4的堆中的引用
        System.out.println(s4==s5);//true

        String s6 = new String("5")+new String("6");//s6指向了自己在堆中的地址，且在常量池中创建了5和6,但没有创建56
        String s7 = "56";//在常量池中创建了56
        String s8 = s6.intern();//intern由于56已在常量池中创建，因此s8指向了s7在常量池中创建的56
        System.out.println(s6==s7);//false
        System.out.println(s6==s8);//false
        System.out.println(s7==s8);//true
    }

}
