package com.lee.mybatis.entity;

import lombok.Data;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/11/8 14:38
 */
@Data
public class RuleParams {

    private String sourceTmplCode;

    private String destTmplCode;
}
