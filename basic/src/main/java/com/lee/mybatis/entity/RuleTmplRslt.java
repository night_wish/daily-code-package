package com.lee.mybatis.entity;

import com.lee.poi.utils.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/11/8 14:08
 */
@Data
@ToString
public class RuleTmplRslt {

    @ApiModelProperty(value = "唯一标识")
    private String rid;

    @ApiModelProperty(value = "模板编码")
    private String tmplCode;

    @ApiModelProperty(value = "模板名称")
    private String tmplName;

    @ApiModelProperty(value = "模板类别（ims:智能监管 drg:DRG dip:DIP）")
    private String tmplCat;

    @ApiModelProperty(value = "模板类型 0:基础模板类型1:医保管理类 2:临床诊断类 3:药品类 4:诊疗项目类 5:临床路径类 6:手术操作类 99:其他类")
    private String tmplType;

    @ApiModelProperty(value = "模板选项名称")
    private String tmplItemName;

    @ApiModelProperty(value = "模板选项编码")
    private String tmplItemCode;

    @ApiModelProperty(value = "父级模板选项编码")
    private String tmplItemCodePrnt;

    @ApiModelProperty(value = "模板选项类型0:表头显示1:字典复选(查接口)2:字典复选(使用DEF_VAL值)3:带添加的输入框4:字典单选(使用DEF_VAL值)5:使用表达式生成的文本输入框6:{}占位符样式8:文本输入框10:开关类型12:下拉框类型 13:配置项类型")
    private String tmplItemType;

    @ApiModelProperty(value = "模板选项排序")
    private Integer tmplItemOrder;

    @ApiModelProperty(value = "模板选项默认值")
    private String tmplItemDefVal;

    @ApiModelProperty(value = "模板选项等级")
    private Integer tmplItemLv;

    @ApiModelProperty(value = "模板选项选择值")
    private String tmplItemVal;

    @ApiModelProperty(value = "提示")
    private String tmplItemWarn;

    @ApiModelProperty(value = "模板选项显隐(0:隐藏 1:显示 )")
    private Integer tmplItemDisp;

    @ApiModelProperty(value = "有效标识(0:无效 1:有效)")
    private String itemValiFlag;

    @ApiModelProperty(value = "模板描述")
    private String tmplDscr;

    @ApiModelProperty(value = "程序编码")
    private String tmplProgCode;

    @ApiModelProperty(value = "模板版本")
    private String tmplVer;

    @ApiModelProperty(value = "创建人ID")
    private String crterId;

    @ApiModelProperty(value = "创建人姓名")
    private String crterName;

    @ApiModelProperty(value = "数据创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "数据更新时间")
    private Date updtTime;
}
