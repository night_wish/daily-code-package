package com.lee.mybatis.mapper;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/11/8 14:10
 */

import com.lee.mybatis.entity.RuleTmplRslt;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RuleTmplRsltMapper{

    List<String> selectOtherRuleTmpl(@Param("dto") RuleTmplRslt ruleTmplRslt);

    List<RuleTmplRslt> selectRuleTmpl(@Param("dto") RuleTmplRslt ruleTmplRslt);

    int deleteRuleTmpl(@Param("tmplCode") String tmplCode);

    int insertRuleTmpl(RuleTmplRslt ruleTmplRslt);
}
