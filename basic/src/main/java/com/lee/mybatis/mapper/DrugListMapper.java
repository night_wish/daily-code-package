package com.lee.mybatis.mapper;

import com.lee.poi.easyexcel.dto.KbmsDrugImportDTO;
import com.lee.work.tce.entity.WmTcmpatInfoLibDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description:
 * @author: Wzx
 * @create: 2022-09-02 10:01
 **/
@Mapper
public interface DrugListMapper {

    int batchSaveDrugList(@Param("lists") List<KbmsDrugImportDTO> drugListDtos);

    int batchSaveTcmpatList(@Param("lists") List<WmTcmpatInfoLibDO> wmTcmpatInfoLibDOList);

}
