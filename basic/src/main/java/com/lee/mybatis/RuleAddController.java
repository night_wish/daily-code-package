//package com.lee.mybatis;
//
//import com.lee.mybatis.entity.RuleParams;
//import com.lee.mybatis.entity.RuleTmplRslt;
//import com.lee.mybatis.mapper.RuleTmplRsltMapper;
//import com.lee.utils.DateUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.support.atomic.RedisAtomicLong;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
///**
// * @author Lee
// * @version 1.0
// * @description
// * @date 2022/11/8 14:06
// */
//@Slf4j
//@RestController
//public class RuleAddController {
//
//    @Autowired
//    private RuleTmplRsltMapper ruleTmplRsltMapper;
//
//    @Autowired
//    private RedisTemplate<String, String> redisTemplate;
//
//    @Value("${sequence.rid.admdvs}")
//    private String admdvs;
//
//    @PostMapping("/ruleInfo")
//    public List<RuleTmplRslt> ruleInfo(@RequestBody RuleTmplRslt ruleTmplRslt){
//        List<RuleTmplRslt> sList = ruleTmplRsltMapper.selectRuleTmpl(ruleTmplRslt);
//        return sList;
//    }
//
//    @PostMapping("/addRuleTmpl")
//    public String add(@RequestBody RuleParams params) {
//        //查询源模板属性
//        RuleTmplRslt ruleTmplRslt1 = new RuleTmplRslt();
//        ruleTmplRslt1.setTmplCode(params.getSourceTmplCode());
//        List<RuleTmplRslt> sList = ruleTmplRsltMapper.selectRuleTmpl(ruleTmplRslt1);
//
//        //查询同类别下的规则模板
//        RuleTmplRslt ruleTmplRslt3 = new RuleTmplRslt();
//        ruleTmplRslt3.setTmplCode(params.getSourceTmplCode());
//        ruleTmplRslt3.setTmplCat(sList.get(0).getTmplCat());
//        List<String> tmplCodes = ruleTmplRsltMapper.selectOtherRuleTmpl(ruleTmplRslt3);
//
//        for(String tmplCode : tmplCodes){
//            //查询原来的模板
//            RuleTmplRslt ruleTmplRslt2 = new RuleTmplRslt();
//            ruleTmplRslt2.setTmplCode(tmplCode);
//            List<RuleTmplRslt> dList = ruleTmplRsltMapper.selectRuleTmpl(ruleTmplRslt2);
//            RuleTmplRslt drslt = dList.get(0);
//
//            //删除模板属性
//            ruleTmplRsltMapper.deleteRuleTmpl(tmplCode);
//            //新增模板属性
//            for (RuleTmplRslt rslt : sList) {
//                RuleTmplRslt dist = new RuleTmplRslt();
//                BeanUtils.copyProperties(rslt, dist);
//
//                dist.setRid(getRid());
//                dist.setTmplCode(tmplCode);
//                dist.setTmplName(drslt.getTmplName());
//                dist.setTmplCat(drslt.getTmplCat());
//                dist.setTmplType(drslt.getTmplType());
//                dist.setTmplDscr(drslt.getTmplDscr());
//                dist.setTmplProgCode(drslt.getTmplProgCode());
//
//                ruleTmplRsltMapper.insertRuleTmpl(dist);
//            }
//        }
//        return "success";
//    }
//
//    public String getRid() {
//        RedisAtomicLong counter = new RedisAtomicLong("RuleTMPL", redisTemplate.getConnectionFactory());
//        counter.expire(24, TimeUnit.HOURS);
//        long num = counter.incrementAndGet();
//        String format = String.format("%06d", num);
//        String number = Integer.toString((int) ((Math.random() * 9 + 1) * 1000));
//        return admdvs + DateUtils.getCurrentDateTimeNum() + format + number;
//    }
//}
