package com.lee.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.parser.Feature;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/7/13 10:20
 */
@Slf4j
public class JsonParse {


    public static void main(String[] args) {
//        WrapperResponse<List<SetlChkDetEvtCDTO>> setlData = null;
//        String response = "{\"code\":0,\"type\":\"success\",\"message\":\"成功\",\"data\":[{\"pageNum\":null,\"pageSize\":null,\"taskID\":\"42000020220606150407136014\",\"setlChkDetId\":\"4200002022071309492512002\",\"evtsn\":\"420000202207130949251000012002\",\"servMattInstId\":\"420000202206061548031002198247\",\"servMattNodeInstId\":\"42000020220606154803100219824701\",\"evtInstId\":\"4200002022071309492518002\",\"evtType\":\"01\",\"mdtrtId\":\"429900G9000017828794\",\"setlId\":\"429900G9000034647904\",\"fixmedinsCode\":\"H42900500011\",\"fixmedinsName\":\"湖北江汉油田总医院\",\"clrType\":\"21\",\"fixBlngAdmdvs\":\"429005\",\"medfeeSumamt\":34920.92,\"fulamtOwnpayAmt\":null,\"overlmtSelfpay\":null,\"preselfpayAmt\":null,\"inscpAmt\":null,\"chkDetAmt\":770.50,\"chkDetSouc\":\"2\",\"chkDetRea\":\"扣款\",\"chkDetDscr\":\"扣款\",\"valiFlag\":\"1\",\"rchkFlag\":\"0\",\"rid\":\"420000202207130949251431145743\",\"updtTime\":1657676965729,\"crterId\":\"1628734438774001294\",\"crterName\":\"国新测试\",\"crteTime\":1657676965729,\"crteOptinsNo\":\"1584537072224072980\",\"opterId\":\"1628734438774001294\",\"opterName\":\"国新测试\",\"optTime\":1657676965729,\"optinsNo\":\"1584537072224072980\",\"poolareaNo\":\"429900\",\"feedetlChkDetEvtCDTOList\":[{\"pageNum\":null,\"pageSize\":null,\"feedetlChkDetId\":\"4200002022071309492512006\",\"evtsn\":\"420000202207130949251000012027\",\"servMattInstId\":\"420000202206061548031002198247\",\"feedetlChkDetEvtId\":\"420000202207130949251000012028\",\"fixmedinsCode\":\"H42900500011\",\"fixmedinsName\":\"湖北江汉油田总医院\",\"mdtrtId\":\"429900G9000017828794\",\"setlId\":\"429900G9000034647904\",\"bkkpSn\":\"100811071646\",\"hilistCode\":\"XB01AFH075B002010180081\",\"hilistName\":\"磺达肝癸钠注射液\",\"listType\":\"101\",\"medinslistCodg\":\"6667125\",\"medinsListName\":\"磺达肝癸钠注射液\",\"detItemFeeSumamt\":0,\"fulamtOwnpayAmt\":0,\"overlmtSelfpay\":0,\"preselfpayAmt\":0,\"inscpAmt\":0,\"chkDetAmt\":308.20,\"chkDetSouc\":\"2\",\"chkDetRea\":\"磺达肝癸钠注射液/磺达肝癸钠注射液限下肢关节置换术使用，该患者本次就诊不符合开具条件\",\"chkDetDscr\":\"磺达肝癸钠注射液/磺达肝癸钠注射液限下肢关节置换术使用，该患者本次就诊不符合开具条件\",\"valiFlag\":\"1\",\"chkStas\":null,\"rid\":\"420000202207130949251431145739\",\"updtTime\":1657676965729,\"crterId\":\"1628734438774001294\",\"crterName\":\"国新测试\",\"crteTime\":1657676965729,\"crteOptinsNo\":\"1584537072224072980\",\"opterId\":\"1628734438774001294\",\"opterName\":\"国新测试\",\"optTime\":1657676965729,\"optinsNo\":\"1584537072224072980\",\"poolareaNo\":\"429900\",\"setlChkDetId\":\"4200002022071309492512002\"},{\"pageNum\":null,\"pageSize\":null,\"feedetlChkDetId\":\"4200002022071309492512007\",\"evtsn\":\"420000202207130949251000012030\",\"servMattInstId\":\"420000202206061548031002198247\",\"feedetlChkDetEvtId\":\"420000202207130949251000012031\",\"fixmedinsCode\":\"H42900500011\",\"fixmedinsName\":\"湖北江汉油田总医院\",\"mdtrtId\":\"429900G9000017828794\",\"setlId\":\"429900G9000034647904\",\"bkkpSn\":\"100811071776\",\"hilistCode\":\"XB01AFH075B002010180081\",\"hilistName\":\"磺达肝癸钠注射液\",\"listType\":\"101\",\"medinslistCodg\":\"6667125\",\"medinsListName\":\"磺达肝癸钠注射液\",\"detItemFeeSumamt\":0,\"fulamtOwnpayAmt\":0,\"overlmtSelfpay\":0,\"preselfpayAmt\":0,\"inscpAmt\":0,\"chkDetAmt\":154.10,\"chkDetSouc\":\"2\",\"chkDetRea\":\"磺达肝癸钠注射液/磺达肝癸钠注射液限下肢关节置换术使用，该患者本次就诊不符合开具条件\",\"chkDetDscr\":\"磺达肝癸钠注射液/磺达肝癸钠注射液限下肢关节置换术使用，该患者本次就诊不符合开具条件\",\"valiFlag\":\"1\",\"chkStas\":null,\"rid\":\"420000202207130949251431145740\",\"updtTime\":1657676965729,\"crterId\":\"1628734438774001294\",\"crterName\":\"国新测试\",\"crteTime\":1657676965729,\"crteOptinsNo\":\"1584537072224072980\",\"opterId\":\"1628734438774001294\",\"opterName\":\"国新测试\",\"optTime\":1657676965729,\"optinsNo\":\"1584537072224072980\",\"poolareaNo\":\"429900\",\"setlChkDetId\":\"4200002022071309492512002\"},{\"pageNum\":null,\"pageSize\":null,\"feedetlChkDetId\":\"4200002022071309492512008\",\"evtsn\":\"420000202207130949251000012033\",\"servMattInstId\":\"420000202206061548031002198247\",\"feedetlChkDetEvtId\":\"420000202207130949251000012034\",\"fixmedinsCode\":\"H42900500011\",\"fixmedinsName\":\"湖北江汉油田总医院\",\"mdtrtId\":\"429900G9000017828794\",\"setlId\":\"429900G9000034647904\",\"bkkpSn\":\"100811071823\",\"hilistCode\":\"XB01AFH075B002010180081\",\"hilistName\":\"磺达肝癸钠注射液\",\"listType\":\"101\",\"medinslistCodg\":\"6667125\",\"medinsListName\":\"磺达肝癸钠注射液\",\"detItemFeeSumamt\":0,\"fulamtOwnpayAmt\":0,\"overlmtSelfpay\":0,\"preselfpayAmt\":0,\"inscpAmt\":0,\"chkDetAmt\":154.10,\"chkDetSouc\":\"2\",\"chkDetRea\":\"磺达肝癸钠注射液/磺达肝癸钠注射液限下肢关节置换术使用，该患者本次就诊不符合开具条件\",\"chkDetDscr\":\"磺达肝癸钠注射液/磺达肝癸钠注射液限下肢关节置换术使用，该患者本次就诊不符合开具条件\",\"valiFlag\":\"1\",\"chkStas\":null,\"rid\":\"420000202207130949251431145741\",\"updtTime\":1657676965729,\"crterId\":\"1628734438774001294\",\"crterName\":\"国新测试\",\"crteTime\":1657676965729,\"crteOptinsNo\":\"1584537072224072980\",\"opterId\":\"1628734438774001294\",\"opterName\":\"国新测试\",\"optTime\":1657676965729,\"optinsNo\":\"1584537072224072980\",\"poolareaNo\":\"429900\",\"setlChkDetId\":\"4200002022071309492512002\"},{\"pageNum\":null,\"pageSize\":null,\"feedetlChkDetId\":\"4200002022071309492512009\",\"evtsn\":\"420000202207130949251000012036\",\"servMattInstId\":\"420000202206061548031002198247\",\"feedetlChkDetEvtId\":\"420000202207130949251000012037\",\"fixmedinsCode\":\"H42900500011\",\"fixmedinsName\":\"湖北江汉油田总医院\",\"mdtrtId\":\"429900G9000017828794\",\"setlId\":\"429900G9000034647904\",\"bkkpSn\":\"100811071715\",\"hilistCode\":\"XB01AFH075B002010180081\",\"hilistName\":\"磺达肝癸钠注射液\",\"listType\":\"101\",\"medinslistCodg\":\"6667125\",\"medinsListName\":\"磺达肝癸钠注射液\",\"detItemFeeSumamt\":0,\"fulamtOwnpayAmt\":0,\"overlmtSelfpay\":0,\"preselfpayAmt\":0,\"inscpAmt\":0,\"chkDetAmt\":154.10,\"chkDetSouc\":\"2\",\"chkDetRea\":\"磺达肝癸钠注射液/磺达肝癸钠注射液限下肢关节置换术使用，该患者本次就诊不符合开具条件\",\"chkDetDscr\":\"磺达肝癸钠注射液/磺达肝癸钠注射液限下肢关节置换术使用，该患者本次就诊不符合开具条件\",\"valiFlag\":\"1\",\"chkStas\":null,\"rid\":\"420000202207130949251431145742\",\"updtTime\":1657676965729,\"crterId\":\"1628734438774001294\",\"crterName\":\"国新测试\",\"crteTime\":1657676965729,\"crteOptinsNo\":\"1584537072224072980\",\"opterId\":\"1628734438774001294\",\"opterName\":\"国新测试\",\"optTime\":1657676965729,\"optinsNo\":\"1584537072224072980\",\"poolareaNo\":\"429900\",\"setlChkDetId\":\"4200002022071309492512002\"}]}]}";
//        //setlData = JSON.parseObject(response,WrapperResponse.class,Feature.OrderedField,Feature.IgnoreNotMatch);
//
//        setlData = (WrapperResponse<List<SetlChkDetEvtCDTO>>)JSON.parseObject(response, new TypeReference<WrapperResponse<List<SetlChkDetEvtCDTO>>>() {
//            },  new com.alibaba.fastjson.parser.Feature[0]);
//
//        log.info("---setlData---:{}",JSON.toJSONString(setlData));

        System.out.println("=====================");
        for(int j =0;j<3;j++){
            System.out.println(" niupi");
            for(int i=0;i<5;i++){
                if(i==3){
                    System.out.println("等于3啦");
                    return;
                }
                System.out.println(" i = "+i);
            }
            System.out.println("niupi 222");
        }


        System.out.println("~~~~~~~~~~~~~~~~~~~~~");


    }
}
