package com.lee.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/8/29 18:07
 */
@Data
@ToString
public class DictEntity {
    private String code;

    private String label;

    public DictEntity() {
    }

    public DictEntity(String code, String label) {
        this.code = code;
        this.label = label;
    }
}
