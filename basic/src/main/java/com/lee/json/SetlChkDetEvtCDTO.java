package com.lee.json;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class SetlChkDetEvtCDTO implements Serializable {
    private String pageNum;
    private String pageSize;
    private String taskID;
    private String setlChkDetId;
    private String evtsn;
    private String servMattInstId;
    private String servMattNodeInstId;
    private String evtInstId;
    private String evtType;
    private String mdtrtId;
    private String setlId;
    private String fixmedinsCode;
    private String fixmedinsName;
    private String clrType;
    private String fixBlngAdmdvs;
    private BigDecimal medfeeSumamt;
    private BigDecimal fulamtOwnpayAmt;
    private BigDecimal overlmtSelfpay;
    private BigDecimal preselfpayAmt;
    private BigDecimal inscpAmt;
    private BigDecimal chkDetAmt;
    private String chkDetSouc;
    private String chkDetRea;
    private String chkDetDscr;
    private String valiFlag;
    private String rchkFlag;
    private String rid;
    private Date updtTime;
    private String crterId;
    private String crterName;
    private Date crteTime;
    private String crteOptinsNo;
    private String opterId;
    private String opterName;
    private Date optTime;
    private String optinsNo;
    private String poolareaNo;
    private List<FeedetlChkDetEvtCDTO> FeedetlChkDetEvtCDTOList;

    public SetlChkDetEvtCDTO() {
    }

    public String getPageNum() {
        return this.pageNum;
    }

    public String getPageSize() {
        return this.pageSize;
    }

    public String getTaskID() {
        return this.taskID;
    }

    public String getSetlChkDetId() {
        return this.setlChkDetId;
    }

    public String getEvtsn() {
        return this.evtsn;
    }

    public String getServMattInstId() {
        return this.servMattInstId;
    }

    public String getServMattNodeInstId() {
        return this.servMattNodeInstId;
    }

    public String getEvtInstId() {
        return this.evtInstId;
    }

    public String getEvtType() {
        return this.evtType;
    }

    public String getMdtrtId() {
        return this.mdtrtId;
    }

    public String getSetlId() {
        return this.setlId;
    }

    public String getFixmedinsCode() {
        return this.fixmedinsCode;
    }

    public String getFixmedinsName() {
        return this.fixmedinsName;
    }

    public String getClrType() {
        return this.clrType;
    }

    public String getFixBlngAdmdvs() {
        return this.fixBlngAdmdvs;
    }

    public BigDecimal getMedfeeSumamt() {
        return this.medfeeSumamt;
    }

    public BigDecimal getFulamtOwnpayAmt() {
        return this.fulamtOwnpayAmt;
    }

    public BigDecimal getOverlmtSelfpay() {
        return this.overlmtSelfpay;
    }

    public BigDecimal getPreselfpayAmt() {
        return this.preselfpayAmt;
    }

    public BigDecimal getInscpAmt() {
        return this.inscpAmt;
    }

    public BigDecimal getChkDetAmt() {
        return this.chkDetAmt;
    }

    public String getChkDetSouc() {
        return this.chkDetSouc;
    }

    public String getChkDetRea() {
        return this.chkDetRea;
    }

    public String getChkDetDscr() {
        return this.chkDetDscr;
    }

    public String getValiFlag() {
        return this.valiFlag;
    }

    public String getRchkFlag() {
        return this.rchkFlag;
    }

    public String getRid() {
        return this.rid;
    }

    public Date getUpdtTime() {
        return this.updtTime;
    }

    public String getCrterId() {
        return this.crterId;
    }

    public String getCrterName() {
        return this.crterName;
    }

    public Date getCrteTime() {
        return this.crteTime;
    }

    public String getCrteOptinsNo() {
        return this.crteOptinsNo;
    }

    public String getOpterId() {
        return this.opterId;
    }

    public String getOpterName() {
        return this.opterName;
    }

    public Date getOptTime() {
        return this.optTime;
    }

    public String getOptinsNo() {
        return this.optinsNo;
    }

    public String getPoolareaNo() {
        return this.poolareaNo;
    }

    public List<FeedetlChkDetEvtCDTO> getFeedetlChkDetEvtCDTOList() {
        return this.FeedetlChkDetEvtCDTOList;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }

    public void setSetlChkDetId(String setlChkDetId) {
        this.setlChkDetId = setlChkDetId;
    }

    public void setEvtsn(String evtsn) {
        this.evtsn = evtsn;
    }

    public void setServMattInstId(String servMattInstId) {
        this.servMattInstId = servMattInstId;
    }

    public void setServMattNodeInstId(String servMattNodeInstId) {
        this.servMattNodeInstId = servMattNodeInstId;
    }

    public void setEvtInstId(String evtInstId) {
        this.evtInstId = evtInstId;
    }

    public void setEvtType(String evtType) {
        this.evtType = evtType;
    }

    public void setMdtrtId(String mdtrtId) {
        this.mdtrtId = mdtrtId;
    }

    public void setSetlId(String setlId) {
        this.setlId = setlId;
    }

    public void setFixmedinsCode(String fixmedinsCode) {
        this.fixmedinsCode = fixmedinsCode;
    }

    public void setFixmedinsName(String fixmedinsName) {
        this.fixmedinsName = fixmedinsName;
    }

    public void setClrType(String clrType) {
        this.clrType = clrType;
    }

    public void setFixBlngAdmdvs(String fixBlngAdmdvs) {
        this.fixBlngAdmdvs = fixBlngAdmdvs;
    }

    public void setMedfeeSumamt(BigDecimal medfeeSumamt) {
        this.medfeeSumamt = medfeeSumamt;
    }

    public void setFulamtOwnpayAmt(BigDecimal fulamtOwnpayAmt) {
        this.fulamtOwnpayAmt = fulamtOwnpayAmt;
    }

    public void setOverlmtSelfpay(BigDecimal overlmtSelfpay) {
        this.overlmtSelfpay = overlmtSelfpay;
    }

    public void setPreselfpayAmt(BigDecimal preselfpayAmt) {
        this.preselfpayAmt = preselfpayAmt;
    }

    public void setInscpAmt(BigDecimal inscpAmt) {
        this.inscpAmt = inscpAmt;
    }

    public void setChkDetAmt(BigDecimal chkDetAmt) {
        this.chkDetAmt = chkDetAmt;
    }

    public void setChkDetSouc(String chkDetSouc) {
        this.chkDetSouc = chkDetSouc;
    }

    public void setChkDetRea(String chkDetRea) {
        this.chkDetRea = chkDetRea;
    }

    public void setChkDetDscr(String chkDetDscr) {
        this.chkDetDscr = chkDetDscr;
    }

    public void setValiFlag(String valiFlag) {
        this.valiFlag = valiFlag;
    }

    public void setRchkFlag(String rchkFlag) {
        this.rchkFlag = rchkFlag;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public void setUpdtTime(Date updtTime) {
        this.updtTime = updtTime;
    }

    public void setCrterId(String crterId) {
        this.crterId = crterId;
    }

    public void setCrterName(String crterName) {
        this.crterName = crterName;
    }

    public void setCrteTime(Date crteTime) {
        this.crteTime = crteTime;
    }

    public void setCrteOptinsNo(String crteOptinsNo) {
        this.crteOptinsNo = crteOptinsNo;
    }

    public void setOpterId(String opterId) {
        this.opterId = opterId;
    }

    public void setOpterName(String opterName) {
        this.opterName = opterName;
    }

    public void setOptTime(Date optTime) {
        this.optTime = optTime;
    }

    public void setOptinsNo(String optinsNo) {
        this.optinsNo = optinsNo;
    }

    public void setPoolareaNo(String poolareaNo) {
        this.poolareaNo = poolareaNo;
    }

    public void setFeedetlChkDetEvtCDTOList(List<FeedetlChkDetEvtCDTO> FeedetlChkDetEvtCDTOList) {
        this.FeedetlChkDetEvtCDTOList = FeedetlChkDetEvtCDTOList;
    }
}
