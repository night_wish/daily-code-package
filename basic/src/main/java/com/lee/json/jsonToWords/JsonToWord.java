package com.lee.json.jsonToWords;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/8/8 17:33
 */
@Slf4j
public class JsonToWord {

    //接口描述
    public static final String interfaceDscr = "支持监管部门对药品配伍禁忌进行维护";
    //方法
    public static final String className = "kngBaseDrugDspeTaboB";
    public static final String classDscr = "药品配伍禁忌";
    public static final String packageName = "cn.hsa.kng.kngMgt.kngbase."+className+".";
    public static final String methods = "queryPage";


    public static void main(String[] args) throws IOException {
//        File file = ResourceUtils.getFile("classpath:json/a.json");
        File file = new File("C:\\Users\\le'e\\Desktop\\10.17.128.167.json");
        String jsonStr = readJsonFile(file);
        JSONObject jsonObject = JSONObject.parseObject(jsonStr);
        ResultObj resultObj = jsonObject.toJavaObject(ResultObj.class);
        Entry entry = resultObj.getLog().getEntries().get(0);
        //入参
        List<QueryObj> enterParams = entry.getRequest().getQueryString();
        StringBuilder enterParamsStr = new StringBuilder("{\n");
        for(QueryObj obj : enterParams){
            enterParamsStr.append("     \"").append(obj.getName()).append("\"").append(":")
                  .append("\"").append(obj.getValue()).append("\",\n");
        }
        String enterParamsRes =  enterParamsStr.toString();
        enterParamsRes = enterParamsRes.substring(0,enterParamsRes.length()-2);
        enterParamsRes = enterParamsRes+"\n}";
        System.out.println(enterParamsRes);



        Map<String,String> map1 = new HashMap<>();
        map1.put("interfaceDscr",interfaceDscr);
//        buildDocs("classpath:word/wordTmplates.doc",map,"D:/AAA.doc");

        //模板文件地址
        String inputUrl = "D:\\wordTmplates.docx";
        //新生产的模板文件
        String outputUrl = "D:\\new.docx";

        //获取docx解析对象
        XWPFDocument document = new XWPFDocument(POIXMLDocument.openPackage(inputUrl));
        //处理第一个：接口描述
        WordTemplateUtil.changeText(document,map1);
        //处理第二个表格：入参JSON
        XWPFTable table1 = document.getTables().get(0);
        Map<String,String> map2 = new HashMap<>();
        map2.put("enterParams",enterParamsRes);
        WordTemplateUtil.changeTable(table1,map2,null);

        //处理第三个表格：入参功能输入项
//        XWPFTable table2 = document.getTables().get(1);
//        List<String[]> testList2 = new ArrayList<>();
//        for(int i=1;i<=enterParams.size();i++){
//            QueryObj obj = enterParams.get(i-1);
//            if("pageNumber".equals(obj.getName())){
//                testList2.add(new String[]{i+"","页码",obj.getName(),"Integer","   "});
//            }else if("pageSize".equals(obj.getName())){
//                testList2.add(new String[]{i+"","每页数量",obj.getName(),"Integer","   "});
//            }else if("crterId".equals(obj.getName())){
//                testList2.add(new String[]{i+"","创建人ID",obj.getName(),"String","   "});
//            }else if("crterName".equals(obj.getName())){
//                testList2.add(new String[]{i+"","创建人姓名",obj.getName(),"String","   "});
//            }else if("crterTime".equals(obj.getName())){
//                testList2.add(new String[]{i+"","创建时间",obj.getName(),"datetime","   "});
//            }else if("updtTime".equals(obj.getName())){
//                testList2.add(new String[]{i+"","更新时间",obj.getName(),"datetime","   "});
//            }else {
//                testList2.add(new String[]{i+"",obj.getName(),obj.getValue()});
//            }
//        }
//        WordTemplateUtil.changeTable(table2,null,testList2);

        //处理第四个表格
        XWPFTable table3 = document.getTables().get(2);
        Map<String,String> map3 = new HashMap<>();
        map3.put("ServiceName",className+"Service");
        map3.put("ServiceDscr",classDscr+" Service");
        map3.put("ServicePackage",packageName+"service");
        map3.put("ServiceMethods",methods);
        WordTemplateUtil.changeTable(table3,map3,null);

        //处理第五个表格
        XWPFTable table4 = document.getTables().get(3);
        Map<String,String> map4 = new HashMap<>();
        map4.put("BoName",className+"BO");
        map4.put("BoDscr",classDscr+" BO");
        map4.put("BoPackage",packageName+"bo");
        map4.put("BoMethos",methods);
        WordTemplateUtil.changeTable(table4,map4,null);


        //处理第六个表格
        String text = entry.getResponse().getContent().getText();//出参
        JSONObject jsonObject1 = JSONObject.parseObject(text);
        Text text1 = jsonObject1.toJavaObject(Text.class);
        System.out.println("------------>"+text.toString());


//        XWPFTable table5 = document.getTables().get(4);
//        Map<String,String> map5 = new HashMap<>();
//        map5.put("outParams",text.toString());
//        WordTemplateUtil.changeTable(table5,map5,null);


        //处理第七个表格
//        XWPFTable table6 = document.getTables().get(5);
//        List<String[]> testList6 = new ArrayList<String[]>();
//        testList6.add(new String[]{"1", "1AA", "1BB", "1CC","1DD"});
//        testList6.add(new String[]{"2", "2AA", "2BB", "2CC","2DD"});
//        testList6.add(new String[]{"3", "3AA", "3BB", "3CC","3DD"});
//        testList6.add(new String[]{"4", "4AA", "4BB", "4CC","4DD"});
//        WordTemplateUtil.changeTable(table6,null,testList6);


        //生成新的word
        File file2 = new File(outputUrl);
        FileOutputStream stream = new FileOutputStream(file2);
        document.write(stream);
        stream.close();
    }




    public static void buildDocs(String  tmpFile, Map<String, String> contentMap, String exportFile) throws FileNotFoundException {
        File file = ResourceUtils.getFile(tmpFile);
        InputStream inputStream = new FileInputStream(file);
        HWPFDocument document = null;
        try {
            document = new HWPFDocument(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 读取文本内容
        Range bodyRange = document.getRange();
        // 替换内容
        for (Map.Entry<String, String> entry : contentMap.entrySet()) {
            bodyRange.replaceText("${" + entry.getKey() + "}", entry.getValue());
        }

        //导出到文件
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            document.write(byteArrayOutputStream);
            OutputStream outputStream = new FileOutputStream(exportFile);
            outputStream.write(byteArrayOutputStream.toByteArray());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 读取JSON文件
     * @param jsonFile
     * @return
     */
    public static String readJsonFile(File jsonFile) {
        String jsonStr = "";
        log.info("开始读取" + jsonFile.getPath() + "文件");
        try {
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            log.info("读取" + jsonFile.getPath() + "文件结束!");
            return jsonStr;
        } catch (Exception e) {
            log.info("读取" + jsonFile.getPath() + "文件出现异常，读取失败!");
            e.printStackTrace();
            return null;
        }
    }

}
