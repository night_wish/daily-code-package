package com.lee.json.jsonToWords;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/8/8 17:52
 */
@Data
@ToString
public class ResultObj implements Serializable {

    private static final long serialVersionUID = 1L;

    private MyLog log;


}


@Data
@ToString
class MyLog implements Serializable{
    private static final long serialVersionUID = 1L;


    private List<Entry> entries;
}

@Data
@ToString
class Entry  implements Serializable {

    private static final long serialVersionUID = 1L;

    private Request request;

    private Response response;
}

@Data
@ToString
class Request  implements Serializable {

    private static final long serialVersionUID = 1L;
    //GET POST DELETE PUT
    private String method;

    private String url;

    //入参
    private List<QueryObj> queryString;


}

@Data
@ToString
class Response  implements Serializable {

    private static final long serialVersionUID = 1L;
    private Content content;
}

@Data
@ToString
class Content  implements Serializable {

    private static final long serialVersionUID = 1L;
    private String size;
    private String mimeType;
    private String text;
}

@Data
@ToString
class Text implements Serializable{
    private static final long serialVersionUID = 1L;
    private String code;
    private String type;
    private String message;
    //private String data;
}

@Data
@ToString
class QueryObj  implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;

    private String value;
}







