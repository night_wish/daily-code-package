package com.lee.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lee.work.rule.Dic;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/8/29 18:06
 */
@Slf4j
public class DictEnumConvert {

    public static void main(String[] args) {
        File file = new File("G:\\AdvcdSpace\\daily-code-package\\basic\\src\\main\\resources\\json\\enum.json");
        String jsonStr = readJsonFile(file);
        List<DictEntity> dictEntities = JSON.parseArray(jsonStr, DictEntity.class);
        //List inoutDiagType = jsonObject.getObject("inoutDiagType", List.class);
        for(DictEntity e : dictEntities){
            System.out.println(e.getLabel());
        }
        for(DictEntity e : dictEntities){
            System.out.println(e.getCode());
        }
    }

    /**
     * 读取JSON文件
     * @param jsonFile
     * @return
     */
    public static String readJsonFile(File jsonFile) {
        String jsonStr = "";
        log.info("开始读取" + jsonFile.getPath() + "文件");
        try {
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            log.info("读取" + jsonFile.getPath() + "文件结束!");
            return jsonStr;
        } catch (Exception e) {
            log.info("读取" + jsonFile.getPath() + "文件出现异常，读取失败!");
            e.printStackTrace();
            return null;
        }
    }
}
