package com.lee.json.polcent;

import cn.hutool.core.date.DateTime;
import com.alibaba.fastjson.JSONObject;
import com.lee.poi.WriteExcelUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024-01-05 17:10
 */
@Slf4j
public class ConvertJson2SQL {

    public static void main(String[] args) throws IOException {
        File file = new File("G:\\AdvcdSpace\\daily-code-package\\basic\\src\\main\\resources\\json\\政策中心\\NatDataDicList.json");
        String jsonStr = readJsonFile(file);
        JSONObject jsonObject = JSONObject.parseObject(jsonStr);
        WrapperResponse resultObj = jsonObject.toJavaObject(WrapperResponse.class);
        List<NatDataDicA> resList = resultObj.getData();
//        log.info("content:{}",JSON.toJSONString(resultObj));

        //生成txt文件
        genSql2Txt(resList);

        //生成Excel文档
        genContent2Excel(resList);
    }


    public static void genContent2Excel(List<NatDataDicA> resList) {
        List<List<String>> ret = new ArrayList<>();
        for (int i = 0; i < resList.size(); i++) {
            List<String> obj2List = resList.get(i).convertObj2List();
            ret.add(obj2List);
        }
        WriteExcelUtils.createWorkBook("字典", Arrays.asList(new String[]{"NAT_DATA_DIC_ID", "NAT_DIC_VAL_CODE", "NAT_DIC_VAL_NAME", "PRNT_DIC_VAL_CODE", "DIC_TYPE_CODE"
              , "EXT_FLAG", "DIC_SOUC_ADMDVS", "PINYIN", "WUBI", "SEQ", "MEMO", "VALI_FLAG", "RID", "CRTE_TIME", "UPDT_TIME", "CRTER_ID", "CRTER_NAME"
              , "CRTE_OPTINS_NO", "OPTER_ID", "OPTER_NAME", "OPT_TIME", "OPTINS_NO", "VER"}), ret, "C:\\Users\\le'e\\Desktop\\dic.xlsx");
    }


    /**
     * 生成txt文件
     */
    public static void genSql2Txt(List<NatDataDicA> resList) throws IOException {
        String insertSQL = "INSERT INTO `intlgsupn_hb_db`.`nat_data_dic_a` (`NAT_DATA_DIC_ID`, `NAT_DIC_VAL_CODE`, `NAT_DIC_VAL_NAME`, `PRNT_DIC_VAL_CODE`, `DIC_TYPE_CODE`, `EXT_FLAG`, `DIC_SOUC_ADMDVS`, `PINYIN`, `WUBI`, `SEQ`, `MEMO`, `VALI_FLAG`, `RID`, `CRTE_TIME`, `UPDT_TIME`, `CRTER_ID`, `CRTER_NAME`, `CRTE_OPTINS_NO`, `OPTER_ID`, `OPTER_NAME`, `OPT_TIME`, `OPTINS_NO`, `VER`) VALUES ('${1}', '${2}', '${3}', '${4}', '${5}', '${6}', '${7}', '${8}', '${9}', ${10}, '${11}', '${12}', '${13}', '${14}', '${15}', '${16}', '${17}', '${18}', '${19}', '${20}', '${21}', '${22}', '${23}');";
        StringBuffer sb = new StringBuffer();
        for (NatDataDicA dic : resList) {
            String replace = insertSQL.replace("${1}", dic.getNatDataDicId()).replace("${2}", dic.getNatDicValCode() + "").replace("${3}", dic.getNatDicValName() + "")
                                   .replace("${4}", dic.getPrntDicValCode() + "").replace("${5}", dic.getDicTypeCode() + "").replace("${6}", dic.getExtFlag() + "")
                                   .replace("${7}", dic.getDicSoucAdmdvs() + "").replace("${8}", dic.getPinyin() + "").replace("${9}", dic.getWubi() + "")
                                   .replace("${10}", dic.getSeq() + "").replace("${11}", dic.getMemo() + "").replace("${12}", dic.getValiFlag() + "")
                                   .replace("${13}", dic.getRid() + "").replace("${14}", new DateTime(dic.getCrteTime()) + "").replace("${15}", new DateTime(dic.getUpdtTime()) + "")
                                   .replace("${16}", dic.getCrterId() + "").replace("${17}", dic.getCrterName() + "").replace("${18}", dic.getCrteOptinsNo() + "").replace("${19}", dic.getOpterId() + "")
                                   .replace("${20}", dic.getOpterName() + "").replace("${21}", new DateTime(dic.getOptTime()) + "").replace("${22}", dic.getOptinsNo() + "").replace("${23}", dic.getVer() + "");

            sb.append(replace).append("\n");
        }
        log.info("sql:{}", sb);
        String filePath = "C:\\Users\\le'e\\Desktop\\dic.sql";
        File desFile = new File(filePath);
        if (!desFile.exists()) {
            desFile.createNewFile();
        }
        FileWriter writer = new FileWriter(desFile);
        writer.write(sb.toString());
        writer.close();
    }


    /**
     * 读取JSON文件
     *
     * @param jsonFile
     * @return
     */
    public static String readJsonFile(File jsonFile) {
        String jsonStr = "";
        log.info("开始读取" + jsonFile.getPath() + "文件");
        try {
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            log.info("读取" + jsonFile.getPath() + "文件结束!");
            return jsonStr;
        } catch (Exception e) {
            log.info("读取" + jsonFile.getPath() + "文件出现异常，读取失败!");
            e.printStackTrace();
            return null;
        }
    }
}
