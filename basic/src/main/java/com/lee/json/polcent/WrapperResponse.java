package com.lee.json.polcent;

import lombok.Data;
import org.elasticsearch.action.search.SearchResponse;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024-01-05 17:20
 */
@Data
public class WrapperResponse implements Serializable {

    private int code;

    private String type;

    private String message;

    private List<NatDataDicA> data;
}
