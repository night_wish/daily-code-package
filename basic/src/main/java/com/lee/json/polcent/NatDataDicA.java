package com.lee.json.polcent;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024-01-05 17:12
 */
@Data
public class NatDataDicA implements Serializable {

    private String natDataDicId;
    private String natDicValCode;
    private String natDicValName;
    private String prntDicValCode;
    private String dicTypeCode;
    private String extFlag;
    private String dicSoucAdmdvs;
    private String pinyin;
    private String wubi;
    private Integer seq;
    private String memo;
    private String valiFlag;
    private String rid;
    private long crteTime;
    private long updtTime;
    private String crterId;
    private String crterName;
    private String crteOptinsNo;
    private String opterId;
    private String opterName;
    private long optTime;
    private String optinsNo;
    private String ver;


    public List<String> convertObj2List() {
        List<String> ret = new ArrayList<>();
        ret.add(this.natDataDicId + "");
        ret.add(this.natDicValCode);
        ret.add(this.natDicValName + "");
        ret.add(this.dicTypeCode);
        ret.add(this.extFlag);
        ret.add(this.dicSoucAdmdvs);
        ret.add(this.pinyin);
        ret.add(this.wubi);
        ret.add(this.seq+"");
        ret.add(this.memo);
        ret.add(this.valiFlag);
        ret.add(this.rid);
        ret.add(this.crteTime+"");
        ret.add(this.updtTime+"");
        ret.add(this.crterId);
        ret.add(this.crterName);
        ret.add(this.crteOptinsNo);
        ret.add(this.opterId);
        ret.add(this.opterName);
        ret.add(this.optTime+"");
        ret.add(this.optinsNo);
        ret.add(this.ver);
        return ret;
    }
}
