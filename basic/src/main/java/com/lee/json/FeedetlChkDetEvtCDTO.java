package com.lee.json;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class FeedetlChkDetEvtCDTO implements Serializable {
    private String pageNum;
    private String pageSize;
    private String feedetlChkDetId;
    private String SetlChkDetId;
    private String evtsn;
    private String servMattInstId;
    private String feedetlChkDetEvtId;
    private String fixmedinsCode;
    private String fixmedinsName;
    private String mdtrtId;
    private String setlId;
    private String bkkpSn;
    private String hilistCode;
    private String hilistName;
    private String listType;
    private String medinslistCodg;
    private String medinsListName;
    private BigDecimal detItemFeeSumamt;
    private BigDecimal fulamtOwnpayAmt;
    private BigDecimal overlmtSelfpay;
    private BigDecimal preselfpayAmt;
    private BigDecimal inscpAmt;
    private BigDecimal chkDetAmt;
    private String chkDetSouc;
    private String chkDetRea;
    private String chkDetDscr;
    private String valiFlag;
    private String rid;
    private Date updtTime;
    private String crterId;
    private String crterName;
    private Date crteTime;
    private String crteOptinsNo;
    private String opterId;
    private String opterName;
    private Date optTime;
    private String optinsNo;
    private String poolareaNo;

    public FeedetlChkDetEvtCDTO() {
    }

    public String getPageNum() {
        return this.pageNum;
    }

    public String getPageSize() {
        return this.pageSize;
    }

    public String getFeedetlChkDetId() {
        return this.feedetlChkDetId;
    }

    public String getSetlChkDetId() {
        return this.SetlChkDetId;
    }

    public String getEvtsn() {
        return this.evtsn;
    }

    public String getServMattInstId() {
        return this.servMattInstId;
    }

    public String getFeedetlChkDetEvtId() {
        return this.feedetlChkDetEvtId;
    }

    public String getFixmedinsCode() {
        return this.fixmedinsCode;
    }

    public String getFixmedinsName() {
        return this.fixmedinsName;
    }

    public String getMdtrtId() {
        return this.mdtrtId;
    }

    public String getSetlId() {
        return this.setlId;
    }

    public String getBkkpSn() {
        return this.bkkpSn;
    }

    public String getHilistCode() {
        return this.hilistCode;
    }

    public String getHilistName() {
        return this.hilistName;
    }

    public String getListType() {
        return this.listType;
    }

    public String getMedinslistCodg() {
        return this.medinslistCodg;
    }

    public String getMedinsListName() {
        return this.medinsListName;
    }

    public BigDecimal getDetItemFeeSumamt() {
        return this.detItemFeeSumamt;
    }

    public BigDecimal getFulamtOwnpayAmt() {
        return this.fulamtOwnpayAmt;
    }

    public BigDecimal getOverlmtSelfpay() {
        return this.overlmtSelfpay;
    }

    public BigDecimal getPreselfpayAmt() {
        return this.preselfpayAmt;
    }

    public BigDecimal getInscpAmt() {
        return this.inscpAmt;
    }

    public BigDecimal getChkDetAmt() {
        return this.chkDetAmt;
    }

    public String getChkDetSouc() {
        return this.chkDetSouc;
    }

    public String getChkDetRea() {
        return this.chkDetRea;
    }

    public String getChkDetDscr() {
        return this.chkDetDscr;
    }

    public String getValiFlag() {
        return this.valiFlag;
    }

    public String getRid() {
        return this.rid;
    }

    public Date getUpdtTime() {
        return this.updtTime;
    }

    public String getCrterId() {
        return this.crterId;
    }

    public String getCrterName() {
        return this.crterName;
    }

    public Date getCrteTime() {
        return this.crteTime;
    }

    public String getCrteOptinsNo() {
        return this.crteOptinsNo;
    }

    public String getOpterId() {
        return this.opterId;
    }

    public String getOpterName() {
        return this.opterName;
    }

    public Date getOptTime() {
        return this.optTime;
    }

    public String getOptinsNo() {
        return this.optinsNo;
    }

    public String getPoolareaNo() {
        return this.poolareaNo;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public void setFeedetlChkDetId(String feedetlChkDetId) {
        this.feedetlChkDetId = feedetlChkDetId;
    }

    public void setSetlChkDetId(String SetlChkDetId) {
        this.SetlChkDetId = SetlChkDetId;
    }

    public void setEvtsn(String evtsn) {
        this.evtsn = evtsn;
    }

    public void setServMattInstId(String servMattInstId) {
        this.servMattInstId = servMattInstId;
    }

    public void setFeedetlChkDetEvtId(String feedetlChkDetEvtId) {
        this.feedetlChkDetEvtId = feedetlChkDetEvtId;
    }

    public void setFixmedinsCode(String fixmedinsCode) {
        this.fixmedinsCode = fixmedinsCode;
    }

    public void setFixmedinsName(String fixmedinsName) {
        this.fixmedinsName = fixmedinsName;
    }

    public void setMdtrtId(String mdtrtId) {
        this.mdtrtId = mdtrtId;
    }

    public void setSetlId(String setlId) {
        this.setlId = setlId;
    }

    public void setBkkpSn(String bkkpSn) {
        this.bkkpSn = bkkpSn;
    }

    public void setHilistCode(String hilistCode) {
        this.hilistCode = hilistCode;
    }

    public void setHilistName(String hilistName) {
        this.hilistName = hilistName;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public void setMedinslistCodg(String medinslistCodg) {
        this.medinslistCodg = medinslistCodg;
    }

    public void setMedinsListName(String medinsListName) {
        this.medinsListName = medinsListName;
    }

    public void setDetItemFeeSumamt(BigDecimal detItemFeeSumamt) {
        this.detItemFeeSumamt = detItemFeeSumamt;
    }

    public void setFulamtOwnpayAmt(BigDecimal fulamtOwnpayAmt) {
        this.fulamtOwnpayAmt = fulamtOwnpayAmt;
    }

    public void setOverlmtSelfpay(BigDecimal overlmtSelfpay) {
        this.overlmtSelfpay = overlmtSelfpay;
    }

    public void setPreselfpayAmt(BigDecimal preselfpayAmt) {
        this.preselfpayAmt = preselfpayAmt;
    }

    public void setInscpAmt(BigDecimal inscpAmt) {
        this.inscpAmt = inscpAmt;
    }

    public void setChkDetAmt(BigDecimal chkDetAmt) {
        this.chkDetAmt = chkDetAmt;
    }

    public void setChkDetSouc(String chkDetSouc) {
        this.chkDetSouc = chkDetSouc;
    }

    public void setChkDetRea(String chkDetRea) {
        this.chkDetRea = chkDetRea;
    }

    public void setChkDetDscr(String chkDetDscr) {
        this.chkDetDscr = chkDetDscr;
    }

    public void setValiFlag(String valiFlag) {
        this.valiFlag = valiFlag;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public void setUpdtTime(Date updtTime) {
        this.updtTime = updtTime;
    }

    public void setCrterId(String crterId) {
        this.crterId = crterId;
    }

    public void setCrterName(String crterName) {
        this.crterName = crterName;
    }

    public void setCrteTime(Date crteTime) {
        this.crteTime = crteTime;
    }

    public void setCrteOptinsNo(String crteOptinsNo) {
        this.crteOptinsNo = crteOptinsNo;
    }

    public void setOpterId(String opterId) {
        this.opterId = opterId;
    }

    public void setOpterName(String opterName) {
        this.opterName = opterName;
    }

    public void setOptTime(Date optTime) {
        this.optTime = optTime;
    }

    public void setOptinsNo(String optinsNo) {
        this.optinsNo = optinsNo;
    }

    public void setPoolareaNo(String poolareaNo) {
        this.poolareaNo = poolareaNo;
    }
}
