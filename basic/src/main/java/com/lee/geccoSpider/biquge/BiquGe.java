package com.lee.geccoSpider.biquge;

import com.geccocrawler.gecco.annotation.*;
import com.geccocrawler.gecco.request.HttpRequest;
import com.geccocrawler.gecco.spider.SpiderBean;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023-12-04 14:14
 */
@Data
@ToString
@Gecco(matchUrl = "https://www.bqu9.cc/book/1398/", pipelines = "biqugePipeline")
public class BiquGe implements SpiderBean {

    /**
     * 向指定URL发送GET方法的请求
     */
    @Request
    private HttpRequest request;

    @RequestParameter("bookId")
    private String bookId;

    @Text
    @HtmlField(cssPath = ".info > h1:first-child")
    private String novelName;

    @Text
    @HtmlField(cssPath = ".small > span:first-child")
    private String authorName;

    @Text
    @HtmlField(cssPath = ".cover > img['src']")
    private String coverUrl;

    @Text
    @HtmlField(cssPath = ".intro > dl > dd:first-child")
    private String synopsis;

    @Text
    @HtmlField(cssPath = ".listmain > dl > dd > a['href']")
    private List<String> charpterUrlList;

    @Text
    @HtmlField(cssPath = ".listmain > dl  dd > a")
    private List<String> charpterNameList;

      @Text
      @HtmlField(cssPath = "body div.listmain")
      private String content;
}
