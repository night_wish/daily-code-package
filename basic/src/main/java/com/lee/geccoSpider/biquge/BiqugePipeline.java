package com.lee.geccoSpider.biquge;

import com.alibaba.fastjson.JSON;
import com.geccocrawler.gecco.annotation.PipelineName;
import com.geccocrawler.gecco.pipeline.Pipeline;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023-12-11 11:31
 */
@Slf4j
@PipelineName(value = "biqugePipeline")
public class BiqugePipeline implements Pipeline<BiquGe> {

    /**
     * 将抓取到的内容进行处理  这里是打印在控制台
     */
    @Override
    public void process(BiquGe biquGe) {
        System.out.println("=======start=====");
        System.out.println("novelName" + biquGe.getNovelName());
        System.out.println("authorName:" + biquGe.getAuthorName() + "  coverUrl" + biquGe.getCoverUrl());
        System.out.println("synopsis:" + biquGe.getSynopsis());
        System.out.println(JSON.toJSON(biquGe.getCharpterUrlList()));
        System.out.println(JSON.toJSON(biquGe.getCharpterNameList()));
        System.out.println(biquGe.getContent());
        System.out.println("=======end=====");
    }
}
