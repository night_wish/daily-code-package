package com.lee.geccoSpider.biquge;

import com.geccocrawler.gecco.GeccoEngine;
import com.geccocrawler.gecco.request.HttpGetRequest;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023-12-11 11:34
 */
public class MainTest {

    public static void main(String[] args) {
//        String url = "https://www.bqu9.cc/book/1398"; //想要爬取的网站的首页地址
//        HttpGetRequest start = new HttpGetRequest(url); //获取网站请求
//        start.setCharset("UTF-8");

        GeccoEngine.create()
              //Gecco搜索的包路径
              .classpath("com.lee.geccoSpider.biquge")
              //开始抓取的页面地址
//              .start(start)
              .thread(10)
              .start("https://www.bqu9.cc/book/1398")
              .retry(10)
              //开启几个爬虫线程
              //单个爬虫每次抓取完一个请求后的间隔时间
              .interval(5)
              //使用pc段userAgent
              .mobile(true)
              //开始运行
              .run();
    }
}
