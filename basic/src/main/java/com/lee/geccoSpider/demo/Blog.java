package com.lee.geccoSpider.demo;

import com.geccocrawler.gecco.annotation.Gecco;
import com.geccocrawler.gecco.annotation.HtmlField;
import com.geccocrawler.gecco.annotation.Request;
import com.geccocrawler.gecco.request.HttpRequest;
import com.geccocrawler.gecco.spider.SpiderBean;
import lombok.Data;

/**
 * @author Lee
 * @version 1.0
 * @description Blog实体类，运行主函数从这里开始解析
 * matchUrl:要抓包的目标地址
 * pipelines:跳转到下个pipelines
 * @date 2023/3/27 9:42
 */
@Data
@Gecco(matchUrl = "http://www.cnblogs.com/boychen/p/7226831.html", pipelines = "blogPipelines")
public class Blog implements SpiderBean {

    /**
     * 向指定URL发送GET方法的请求
     */
    @Request
    private HttpRequest request;

    /**
     * 抓去这个路径下所有的内容
     */
    @HtmlField(cssPath = "body div#cnblogs_post_body")
    private String content;
}
