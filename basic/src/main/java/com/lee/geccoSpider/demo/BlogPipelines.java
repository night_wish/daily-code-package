package com.lee.geccoSpider.demo;

import com.geccocrawler.gecco.GeccoEngine;
import com.geccocrawler.gecco.annotation.PipelineName;
import com.geccocrawler.gecco.pipeline.Pipeline;

/**
 * @author Lee
 * @version 1.0
 * @description 运行完Blog.java 根据@PipelineName 来这里
 * @date 2023/3/27 9:47
 */
@PipelineName(value="blogPipelines")
public class BlogPipelines implements Pipeline<Blog> {

    /**
     * 将抓取到的内容进行处理  这里是打印在控制台
     */
    @Override
    public void process(Blog blog) {
        System.out.println("=======start=====");
        System.out.println(blog.getContent());
        System.out.println("=======end=====");
    }



}
