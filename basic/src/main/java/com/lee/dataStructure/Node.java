package com.lee.dataStructure;

        import lombok.AllArgsConstructor;
        import lombok.Getter;
        import lombok.NoArgsConstructor;
        import lombok.Setter;

/**
 * 节点
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Node<K extends Comparable,V> {
    private K key;
    private V value;
    private Node leftNode;
    private Node rightNode;
    private Node parentNode;
}
