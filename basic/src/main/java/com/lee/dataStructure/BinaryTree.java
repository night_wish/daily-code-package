package com.lee.dataStructure;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 二叉查找树
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BinaryTree<K extends Comparable,V> {

    private Node<K,V> root;

    /**
     * 插入
     */
    public void insert(K key,V value){
        //根节点为null,直接设为根节点
        if(root==null){
            this.root = new Node<>(key,value,null,null,null);
            return;
        }

        //查找要插入的节点的父节点
        Node parentNode = null;
        Node rootNode = this.root;
        int compRes = 0;
        while(rootNode!=null){
            parentNode = rootNode;
            compRes = key.compareTo(rootNode.getKey());
            if(compRes>0){
                //放入右子树
                rootNode = rootNode.getRightNode();
            }else if(compRes==0){
                //相等，替换value
                rootNode.setValue(value);
                return;
            }else{
                //放入左子树
                rootNode = rootNode.getLeftNode();
            }
        }

        //插入
        Node tempNode = parentNode;
        if(compRes>0){
            parentNode.setRightNode(new Node<K,V>(key,value,null,null,tempNode));
        }else{
            parentNode.setLeftNode(new Node<K,V>(key,value,null,null,tempNode));
        }
    }

    /**
     * 查找
     */
    public Node findNode(K key){
        if(this.root==null){
            return null;
        }
        Node rootNode = this.root;
        while(rootNode!=null){
            int compRes = key.compareTo(rootNode.getKey());
            if(compRes<0){
                rootNode = rootNode.getLeftNode();
            }else if(compRes==0){
                return rootNode;
            }else{
                rootNode = rootNode.getRightNode();
            }
        }
        return null;
    }


    /**
     * 前序遍历NLR: 根节点--左子树--右子树
     * @param root
     */
    public void preOrderTraversal(Node root){
        if(root!=null){
            //根节点
            System.out.print(root.getKey()+"  ");
            //左子树
            if(root.getLeftNode()!=null){
                preOrderTraversal(root.getLeftNode());
            }
            //右子树
            if(root.getRightNode()!=null){
                preOrderTraversal(root.getRightNode());
            }
        }
    }


    /**
     * 中序遍历LNR: 左子树--根节点--右子树
     * @param root
     */
    public void inorderTraversal(Node root){
        if(root!=null){
            //左子树
            if(root.getLeftNode()!=null){
                inorderTraversal(root.getLeftNode());
            }
            //根节点
            System.out.print(root.getKey()+"  ");
            //右子树
            if(root.getRightNode()!=null){
                inorderTraversal(root.getRightNode());
            }
        }
    }


    /**
     * 后序遍历LRN: 左子树--右子树--根节点
     * @param root
     */
    public void postorderTraversal(Node root){
        if(root!=null){
            //左子树
            if(root.getLeftNode()!=null){
                postorderTraversal(root.getLeftNode());
            }
            //右子树
            if(root.getRightNode()!=null){
                postorderTraversal(root.getRightNode());
            }
            //根节点
            System.out.print(root.getKey()+"  ");
        }
    }

    /**
     * 层序遍历：一层一层遍历（从上到下 从左到右）
     * @param root
     */
    public void levelOrderTraversal(Node root){
        if(root!=null){
            Queue<Node> queue = new LinkedList();
            queue.add(root);
            while (!queue.isEmpty()){
                Node removeNode = queue.remove();
                System.out.print(removeNode.getKey()+"  ");
                Node leftNode = removeNode.getLeftNode();
                if(leftNode!=null){
                    queue.add(leftNode);
                }
                Node rightNode = removeNode.getRightNode();
                if(rightNode!=null){
                    queue.add(rightNode);
                }
            }
        }
    }

    /**
     * 删除：
     * 1、叶子节点：直接删除
     * 2、节点有一个左子节点：删除该节点，让左子节点替代自己
     * 3、节点有一个右子节点：删除该节点，让右子节点替代自己
     * 4、节点有两个子节点：删除该节点，让该节点左子树中最大的或者右子树中最小的替代自己
     *                  （即让该节点的前驱结点或后继节点来替代自己）
     */
    public void deleteNode(K key){
        Node select = findNode(key);
        if(select!=null){
            if(select.getLeftNode()==null && select.getRightNode()==null){
                //1、删除的是叶子节点
                Node parentNode = select.getParentNode();
                if(parentNode==null){
                    //删除的节点是根节点
                    this.root = null;
                }else{
                    //删除的节点不是根节点
                    if(parentNode.getLeftNode()==select){
                        //删除的节点是其父节点的左节点，将其父节点的左节点置为空
                        parentNode.setLeftNode(null);
                    }else{
                        //删除的节点是其父节点的右节点，将其父节点的右节点置为空
                        parentNode.setRightNode(null);
                    }
                }
            }else if(select.getLeftNode()==null || select.getRightNode()==null){
                //2、删除的节点有一个子节点
                Node parentNode = select.getParentNode();
                Node childrenNode = select.getLeftNode()!=null ? select.getLeftNode() : select.getRightNode();
                if(parentNode==null){
                    //删除的节点是根节点
                    childrenNode.setParentNode(null);
                    this.root = childrenNode;
                }else{
                    //删除的节点不是根节点
                    if(parentNode.getLeftNode()==select){
                        //删除的节点是其父节点的左节点，将其子节点设置为父节点的左节点
                        parentNode.setLeftNode(childrenNode);
                    }else{
                        //删除的节点是其父节点的右节点，将其子节点设置为父节点的右节点
                        parentNode.setRightNode(childrenNode);
                    }
                    //将其子节点的父节点变更为其父节点
                    childrenNode.setParentNode(parentNode);
                }


            }else{
                //3、删除的节点有两个子节点--我们采用将删除节点的后继节点替代自己
                //既然是后继节点那么后继节点要么没有子节点要么只有右节点
                Node parentNode = select.getParentNode();
                //因为删除的节点有两个子节点，所以后继几点肯定不为null
                Node nextNode = findNextNode(select);
                //后继节点的父节点：--后继节点肯定是其父节点的左子节点（后继节点肯定有父节点）
                //后继节点的父节点==删除节点
                //后继节点的父节点!=删除节点
                Node nextNodeParentNode = nextNode.getParentNode();
                //后继节点的子节点(后继节点只能有右子节点或者没有子节点)
                Node nextNodeChildrenNode = nextNode.getRightNode();

                //3.1、后继接点和删除节点互换
                if(parentNode==null){
                    //删除的节点是根节点--后继节点替代自己
                    nextNode.setParentNode(null);
                    this.root = nextNode;
                }else{
                    //删除的节点不是根节点--后继节点替代自己
                    nextNode.setParentNode(parentNode);
                    if(parentNode.getLeftNode()==select){
                        parentNode.setLeftNode(nextNode);
                    }else{
                        parentNode.setRightNode(nextNode);
                    }
                }

                //删除节点的左子树成为后继节点的左子树
                if(select.getLeftNode()!=null){
                    select.getLeftNode().setParentNode(nextNode);
                    nextNode.setLeftNode(select.getLeftNode());
                }

                //3.2、后继节点的父节点和后继节点的子节点挂钩
                if(nextNodeChildrenNode==null){
                    //后继节点没有子节点
                    if(nextNodeParentNode==select){
                        //后继节点的父节点==删除节点(不做任何操作)
                    }else{
                        //后继节点的父节点!=删除节点(设置后继节点的父节点的左子树为null)
                        nextNodeParentNode.setLeftNode(null);
                    }
                }else{
                    //后继节点有子节点
                    if(nextNodeParentNode==select){
                        //后继节点的父节点==删除节点(不做任何操作)
                    }else{
                        //后继节点的父节点!=删除节点
                        //(设置后继节点的父节点的左子树为后继节点的子节点)
                        //(设置后继节点的子节点的父节点为后继节点的父节点)
                        nextNodeParentNode.setLeftNode(nextNodeChildrenNode);
                        nextNodeChildrenNode.setParentNode(nextNodeParentNode);
                    }
                }

                //3.3、删除节点的右子树成为后继节点的右子树
                if(select.getRightNode()!=nextNode){
                    select.getRightNode().setParentNode(nextNode);
                    nextNode.setRightNode(select.getRightNode());
                }

            }
        }
    }

    /**
     * 查询后继节点
     */
    public Node  findNextNode(Node currentNode){
        if(currentNode!=null){
            Node nextNode = currentNode.getRightNode();
            if(nextNode!=null){
                while(nextNode.getLeftNode()!=null){
                    nextNode = nextNode.getLeftNode();
                }
                return nextNode;
            }
        }
        return null;
    }


    /**
     * 查询前驱结点
     */
    public Node findPreNode(Node currentNode){
        if(currentNode!=null){
            Node preNode = currentNode.getLeftNode();
            if(preNode!=null){
                while(preNode.getRightNode()!=null){
                    preNode = preNode.getRightNode();
                }
                return preNode;
            }
        }
        return null;
    }


    public static void main(String[] args) {
        BinaryTree  bt = new BinaryTree();
        bt.insert(8,8);
        bt.insert(5,5);
        bt.insert(11,11);
        bt.insert(3,3);
        bt.insert(2,2);
        bt.insert(4,4);
        bt.insert(7,7);
        bt.insert(6,6);
        bt.insert(9,9);
        bt.insert(12,12);
        System.out.println("**********打印树1***********");
        TreeOperation.show(bt.getRoot());

        System.out.println("**********查找***********");
        Node node = bt.findNode(7);
        System.out.println(node.getKey());
        System.out.println("***********前序遍历**********");
        bt.preOrderTraversal(bt.getRoot());
        System.out.println("\n**********中序遍历***********");
        bt.inorderTraversal(bt.getRoot());
        System.out.println("\n**********后序遍历***********");
        bt.postorderTraversal(bt.getRoot());
        System.out.println("\n**********层序遍历***********");
        bt.levelOrderTraversal(bt.getRoot());

        BinaryTree  bt2 = new BinaryTree();
        bt2.insert(8,8);
        bt2.insert(7,7);
        bt2.insert(4,4);
        bt2.insert(12,12);
        bt2.insert(2,2);
        bt2.insert(5,5);
        bt2.insert(9,9);
        bt2.insert(14,14);
        bt2.insert(1,1);
        bt2.insert(3,3);
        bt2.insert(10,10);
        bt2.insert(13,13);
        bt2.insert(16,16);
        bt2.insert(15,15);
        System.out.println("\n**********打印树2***********");
        TreeOperation.show(bt2.getRoot());
        System.out.println("\n**********删除节点***********");
        bt2.deleteNode(12);
        TreeOperation.show(bt2.getRoot());

    }
}
