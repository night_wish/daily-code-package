package com.lee.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.lee.base.commons.DocD;
import com.lee.base.commons.NovelChapter;
import com.lee.base.commons.User;
import com.lee.utils.BigDecimalUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class JavaTest {

    @Test
    public void testTransaction() {
        System.out.println("=====1==");
        System.out.println("=====2==");
        try {
            int a = 10 / 0;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            System.out.println("=========finaly==========");
        }
        System.out.println("=====3==");
        System.out.println("=====4==");
    }

    @Test
    public void volaRateTest() {
        BigDecimal checkoutPsntime = new BigDecimal("9");
        BigDecimal anaPsntime = new BigDecimal("178102");
        if (checkoutPsntime != null && anaPsntime != null) {
            BigDecimal checkoutPsntimeTmp = BigDecimalUtil.buildBigDecimal(checkoutPsntime).multiply(BigDecimalUtil.buildBigDecimal(100));
            BigDecimal total = BigDecimalUtil.buildBigDecimal(anaPsntime);
            if (!total.equals(BigDecimal.ZERO)) {
                BigDecimal decimal = BigDecimalUtil.divide(checkoutPsntimeTmp, total);
                BigDecimal bigDecimal = decimal.setScale(2, RoundingMode.HALF_UP);
                System.out.println(bigDecimal.toString());
            }
        }
    }


    @Test
    public void ListTest() {
        List<String> list = new ArrayList<>();
        list.add("2");
        list.add("1");
        list.add("3");
        list.add("4");
        String loclPayStr = listToInStr(list);
        System.out.println(loclPayStr);
        System.out.println("==================");
        String twoFixLoclPayStr = loclPayStr;
        if (list.contains("1")) {
            boolean remove = list.remove("1");
            twoFixLoclPayStr = listToInStr(list);
        }
        System.out.println(twoFixLoclPayStr);

    }

    public static String listToInStr(List<String> tList) {
        if (tList != null && tList.size() > 0) {
            String str = tList.toString();
            str = str.replace("[", "('");
            str = str.replace(", ", "','");
            str = str.replace("]", "')");
            return str;
        }
        return "";
    }

    @Test
    public void page() {

        int pageSize = 10;
        int pageNumber = 1;
        List<String> resList = new ArrayList<>();
        resList.add("aa");
        resList.add("bb");

        int totalPage = resList.size() % pageSize == 0 ? resList.size() / pageSize : resList.size() / pageSize + 1;
        int fromIndex = 0;
        if (totalPage != 0) {
            fromIndex = pageNumber >= totalPage ? pageSize * (totalPage - 1) : pageSize * (pageNumber - 1);
        }
        int toIndex = pageSize * pageNumber >= resList.size() ? resList.size() : pageSize * pageNumber;
        System.out.println(totalPage);
        System.out.println(fromIndex);
        System.out.println(toIndex);
    }

    @Test
    public void testStream() {
//        Map<String,String> map = new HashMap<>();
//        map.put("1","a");
//        map.put("2","b");
//        map.put("3","c");
//        map.put("4","d");
//        List<String> l = new ArrayList<>();
//        l.add("a");
//        l.add("b");
//        l.add("c");
//        l.add(" ");
//        String a = l.toString().replaceAll(" ","");
//        System.out.println(a);
//        String ex = "012345678";
//        String exStr = ex.toString().length()>=10?ex.toString().substring(0,10):ex.toString();
//        System.out.println(exStr);
        String admdvs = "431600";
        System.out.println(admdvs.charAt(3));
    }


    @Test
    public void testStringArray() {
        String[] arry = {"1", "2", "3"};
        String s = String.valueOf(arry);
        log.info("----s:{}", s);
    }

    public static void main(String[] args) {
        JavaTest javaTest = new JavaTest();
        List<String> docIds = Arrays.asList("a1βb1", "a2βb2", "a3βb3");
        DocD docD = new DocD();
        docD.setDocIdList(docIds);
        for (String docId : docD.getDocIdList()) {
            docD.setDocId(docId);
            javaTest.method1(docD);
        }
    }

    public void method1(DocD docD) {
        log.info("docd : {}", JSONObject.toJSONString(docD));
        method2(docD);
    }

    public void method2(DocD docD) {
        String docId = docD.getDocId();
        if (docId.contains("β")) {
            String[] arry = docId.split("β");
            docD.setDocId(arry[0]);
            docD.setPatnId(arry[1]);
        }
    }

    @Test
    public void listSplit() {
        List<User> userList = Arrays.asList(new User(1, "a", 11), new User(2, "b", 12), new User(3, "c", 13), new User(4, "d", 14));
        List<List<User>> partition = Lists.partition(userList, 3);
        log.info("partition : {}", JSON.toJSONString(partition));

        List<String> collect = userList.stream().map(t -> t.getName()).collect(Collectors.toList());
        log.info("map function : {}", JSONObject.toJSONString(collect));

    }

    @Test
    public void stringSub() {
        String admdvs = "421213".substring(0, 4) + "00";
        log.info("admdvs:{}", admdvs);

        String docExtendFields = "γγγa";
        String[] γs = docExtendFields.split("γ");
        for (int i = 0; i < γs.length; i++) {
            System.out.println("|" + γs[i] + "|");
        }
    }

    @Test
    public void stringSlice() throws IOException {
        String sliceContent = "第,章η第,节η第,篇η";
        String content1 = "第1章 abc\n" + "abcdefghijklm\n" + "第2篇 123\n" + "123456789\n" + "第3节 a1b2c3\n" + "！@#￥%……&*";
        System.out.println(content1);
        List<List<String>> splitArray = Arrays.stream(sliceContent.split("η")).map(t -> Arrays.stream(t.split(",")).collect(Collectors.toList())).collect(Collectors.toList());

        File file = new File("output.txt");
        FileWriter writer = new FileWriter(file);
        writer.write(content1);
        writer.close();
        List<NovelChapter> chapterList = new ArrayList<>();
        BufferedReader bufReader = new BufferedReader(new FileReader(file));
        String novelLine = "";
        Boolean complChapter = false;
        NovelChapter chapter = null;
        int i = 0;
        while ((novelLine = bufReader.readLine()) != null) {
            String content = novelLine;
            if (StringUtils.isNotBlank(content.trim())) {
                if (splitArray.stream().anyMatch(t -> t.stream().allMatch(content::contains))) {
                    if (!complChapter) {
                        chapter = new NovelChapter();
                        if (i == 0) {
                            complChapter = true;
                        }
                    } else {
                        chapterList.add(chapter);
                    }
                    //章节名称
                    log.info("章节名称===>{}", content);
                    chapter.setChapterName(content.trim());
                } else {
                    //章节内容
                    log.info("章节内容===>{}", content);
                    chapter.setContent(chapter.getContent() + "<br/>" + content.trim());
                }
            }
        }
        bufReader.close();

        log.info("====>{}", JSON.toJSONString(chapterList));
    }

    @Test
    public void testChapter() throws IOException {
        String sliceContent = "第,章η第,节η第,篇η";
        String content1 = "第1章 abc\n" + "abcdefghijklm\n" + "第2篇 123\n" + "123456789\n" + "1a2b3c4d5e\n" + "第3节 a1b2c3\n" + "！@#￥%……&*";
        File file = new File("output.txt");
        FileWriter writer = new FileWriter(file);
        writer.write(content1);
        writer.close();

        List<NovelChapter> novelChapters = assembleNovelChapters(sliceContent, file);

        // 输出结果
        for (NovelChapter chapter : novelChapters) {
            System.out.println("章节名称: " + chapter.getChapterName());
            System.out.println("内容: " + chapter.getContent());
            System.out.println();
        }
    }

    public static List<NovelChapter> assembleNovelChapters(String sliceContent, File file) {
        List<NovelChapter> novelChapters = new ArrayList<>();
        List<List<String>> splitArray = Arrays.stream(sliceContent.split("η")).map(t -> Arrays.stream(t.split(",")).collect(Collectors.toList())).collect(Collectors.toList());
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            String currentChapterName = null;
            StringBuilder currentContent = new StringBuilder();
            while ((line = br.readLine()) != null) {
                boolean isChapterName = false;
                String content = line;
                if (splitArray.stream().anyMatch(t -> t.stream().allMatch(content::contains))) {
                    isChapterName = true;
                }

                if (isChapterName) {
                    if (currentChapterName != null) {
                        NovelChapter chapter = NovelChapter.builder().chapterName(currentChapterName).content(currentContent.toString()).build();
                        novelChapters.add(chapter);
                    }
                    currentChapterName = line;
                    currentContent = new StringBuilder();
                } else {
                    currentContent.append(line).append("<br/>");
                }
            }

            if (currentChapterName != null) {
                NovelChapter chapter = NovelChapter.builder().chapterName(currentChapterName).content(currentContent.toString()).build();
                novelChapters.add(chapter);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return novelChapters;
    }

    @Test
    public void subStr() {
        String projectPrefix = "gxjk_hive_ads.";
        String sqlFormat = "CREATE TABLE gxjk_hive_ads.tmp_fee_mid_43000020231118114433327001 AS SELECT t1.*,tg.CLR_TYPE,tg.CLR_WAY FROM dwd.dwd_fin_fee_list_d t1 INNER JOIN (SELECT t9.CLR_TYPE,t9.CLR_WAY,t9.MDTRT_ID,t10.SETL_ID FROM gxjk_hive_ads.TMP_MDTRT_D_43000020231118114433327001 t9 INNER JOIN dwd.DWD_FIN_SETL_D t10 ON t10.MDTRT_ID = t9.MDTRT_ID WHERE t10.REFD_SETL_FLAG = '0' AND t10.VALI_FLAG='1' AND (t10.acct_used_flag<>'2' or t10.acct_used_flag is null)  AND t10.SETL_TYPE = '2'  ) tg ON t1.MDTRT_ID = tg.MDTRT_ID AND t1.SETL_ID = tg.SETL_ID AND t1.VALI_FLAG = '1'";

        int i = sqlFormat.indexOf(projectPrefix) + projectPrefix.length();
        System.out.println(i);
        int j = sqlFormat.indexOf("as");
        String tableName = sqlFormat.substring(i, j).trim();

        System.out.println(tableName);

    }


}
