package com.lee;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/7/22 10:20
 */
public class MainTest {


    public static String genUUID(String hospitalId, String admissionNumber, Boolean isHisId) {
        String genUUID = "";
        if (StringUtils.isBlank(hospitalId)) {
            hospitalId = "1000";
        }
        if (StringUtils.isBlank(admissionNumber)) {
            admissionNumber = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        }
        if (isHisId) {
            genUUID = hospitalId + "-" + admissionNumber + "-" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("ssmmddHHMMyyyy")) + UUID.randomUUID().toString().substring(0, 15).replace("-", "");
        } else {
            genUUID = hospitalId + admissionNumber + LocalDateTime.now().format(DateTimeFormatter.ofPattern("ssmmddHHMMyyyy")) + UUID.randomUUID().toString().substring(0, 15).replace("-", "");
        }
        return genUUID;
    }

    public static void main(String[] args) throws IOException {
//        String uuid = UUID.randomUUID().toString();
//        System.out.println("===>uuid:{"+uuid+"}");
//        String uuid2 = UUID.randomUUID().toString();
//        System.out.println("===>uuid:{"+uuid2+"}");
//
//        System.out.println(Integer.toString((int) ((Math.random() * 9 + 1) * 1000)));
        /*Set<String> uuidSet = new HashSet<>();//防止UUID重复
        for (int i = 0; i < 10000; i++) {
            String uuid = genUUID(null, null, false);
            while (uuidSet.contains(uuid)) {
                uuid = genUUID(null, null, false);
            }
            System.out.println(uuid);
            uuidSet.add(uuid);
        }
        System.out.println("=========>"+uuidSet.size());*/

        boolean b = validateObjectKey("TestLoaclRule/JB1171000000.jar");
        System.out.println("=====>"+b);
    }
    public static boolean validateObjectKey(String key) {
        if (key == null || key.length() == 0)
            return false;
        byte[] bytes = null;
        try {
            bytes = key.getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            return false;
        }
        char[] keyChars = key.toCharArray();
        char firstChar = keyChars[0];
        if (firstChar == '/' || firstChar == '\\')
            return false;
        return (bytes.length > 0 && bytes.length < 1024);
    }

}

