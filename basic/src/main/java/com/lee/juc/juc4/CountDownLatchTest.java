package com.lee.juc.juc4;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;

/**
 * 倒计数锁：【一个线程等待多个线程】
 *  所有任务完成后 主线程才开始执行：
 *  例：3个学生一起考试，只有3个学生都交完试卷后，老师才可以宣布考试结束
 */
@Slf4j
public class CountDownLatchTest {

    @Test
    public void countDownLatchTest() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(3);
        log.info("老师宣布开始考试啦！");
        new Thread(()->{
            log.info("学生A：我已完成试卷!");
            latch.countDown();
        }).start();
        new Thread(()->{
            log.info("学生B：我已完成试卷!");
            latch.countDown();
        }).start();
        new Thread(()->{
            log.info("学生C：我已完成试卷!");
            latch.countDown();
        }).start();
        latch.await();
        log.info("老师宣布，所有学生都已完成，考试结束!");
    }
}
