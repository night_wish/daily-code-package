package com.lee.juc.juc4;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * 自定义一个不可重入锁
 */
@Slf4j
public class CustomAQS {
    public static void main(String[] args) {
        MyLock lock = new MyLock();

        new Thread(()->{
            lock.lock();
            try {
                log.debug("t1 locking...");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                log.debug("t1 unlock...");
                lock.unlock();
            }
        },"t1").start();

        new Thread(()->{
            lock.lock();
            try {
                log.debug("t2 locking...");
            }finally {
                log.debug("t2 unlock...");
                lock.unlock();
            }
        },"t2").start();
    }
}

//自定义锁（不可重入）
class MyLock implements Lock {

    //自定义同步器类
    class MySync extends AbstractQueuedSynchronizer{

        @Override//尝试获取锁（独占锁）
        protected boolean tryAcquire(int arg) {
            //将资源状态state设置为1--占用
            if(compareAndSetState(0,1)){
                //设置owner线程为当前线程
                setExclusiveOwnerThread(Thread.currentThread());
                return true;
            }
            return false;
        }

        @Override//尝试释放锁 (独占锁)
        protected boolean tryRelease(int arg) {
            //将owner线程设置为空
            //非volatile可见的
            setExclusiveOwnerThread(null);
            //设置资源状态state为0--未被占用
            //state为volatile可见的，有写屏障，因此将owner设置放在其上
            setState(0);
            return true;
        }

        @Override//是否持有独占锁
        protected boolean isHeldExclusively() {
            return getState()==1;
        }

        //条件变量
        public Condition newCondition(){
            return new ConditionObject();
        }
    }

    private MySync mySync = new MySync();

    @Override //加锁（不成功会进入等待队列）
    public void lock() {
        mySync.acquire(1);
    }

    @Override //加锁（可打断的）
    public void lockInterruptibly() throws InterruptedException {
        mySync.acquireInterruptibly(1);
    }

    @Override //尝试加锁（尝试一次）
    public boolean tryLock() {
        return mySync.tryAcquire(1);
    }

    @Override //尝试加锁（带超时的）
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return mySync.tryAcquireNanos(1,unit.toNanos(time));
    }

    @Override //解锁
    public void unlock() {
        //tryRelease不会唤醒其他等待的线程，release会
        mySync.release(0);
    }

    @Override //条件变量
    public Condition newCondition() {
        return mySync.newCondition();
    }
}