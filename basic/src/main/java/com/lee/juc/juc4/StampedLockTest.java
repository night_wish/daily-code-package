package com.lee.juc.juc4;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.StampedLock;

/**
 * 读写锁：乐观读
 */
@Slf4j
public class StampedLockTest {
    public static void main(String[] args) {
        DataContainerStamped containerStamped = new DataContainerStamped("a");
        new Thread(()->{
            containerStamped.read();
        },"t1").start();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()->{
            containerStamped.write("b");
        },"t2").start();
    }
}

@Slf4j
@Data
class DataContainerStamped{
    //共享数据
    private Object Obj;
    //读写锁
    private StampedLock lock = new StampedLock();

    public DataContainerStamped(Object obj) {
        Obj = obj;
    }

    public Object read(){
        //乐观读
        log.debug("乐观读");
        long stamp = lock.tryOptimisticRead();
        log.debug("乐观读 stamp:{}",stamp);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //乐观读验证--是否期间有写操作
        if(lock.validate(stamp)){
            log.debug("乐观读验证成功,期间没有写操作 stamp：{}, 读取数据obj:{}",stamp,getObj());
            return getObj();
        }

        log.debug("乐观读验证失败,锁升级");
        log.debug("尝试获取读锁");
        stamp = lock.readLock();
        log.debug("尝试获取读锁,stamp:{}",stamp);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            log.debug("读取数据");
            log.debug("读取数据 obj：{}",getObj());
            return getObj();
        } finally {
            log.debug("尝试释放读锁 stamp:{}",stamp);
            lock.unlockRead(stamp);
            log.debug("释放读锁成功 stamp:{}",stamp);
        }

    }

    public void write(Object res){
        log.debug("尝试获取写锁");
        long stamp = lock.writeLock();
        log.debug("获取写锁成功 stamp:{}",stamp);
        try {
            log.debug("修改数据"+res);
            setObj(res);
        } finally {
            log.debug("尝试释放写锁 stamp:{}",stamp);
            lock.unlockWrite(stamp);
            log.debug("释放写锁成功 stamp:{}",stamp);
        }

    }

}