package com.lee.juc.juc4;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.util.StringUtils;

import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 多个线程间 相互等待
 */
@Slf4j
public class CyclicBarrierTest {

    /**
     * 五个人一起玩游戏，等所有人准备好后游戏才能开始
     *  在此期间 所有人都在等待其他人
     */
    @Test
    public void playGames() {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(5,()->{
            log.info("所有人都准备好啦，游戏开始!");
        });

        ExecutorService pool = Executors.newFixedThreadPool(5);
        String[] gammer = new String[]{"赤", "橙", "黄", "绿", "青", "蓝", "紫"};
        for (int i = 0; i < 5; i++) {
            int temp = i;
            Future<String> f = pool.submit(() -> {
                log.debug("{} 开始准备", gammer[temp]);
                Thread.sleep(1000);
                log.debug("{} 准备完成", gammer[temp]);
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
                return gammer[temp];
            });
        }

    }


    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7, () -> {
            log.debug("召唤神龙!");
        });

        ExecutorService pool = Executors.newFixedThreadPool(7);
        String[] dragonEge = new String[]{"赤", "橙", "黄", "绿", "青", "蓝", "紫"};
        for (int i = 0; i < 7; i++) {
            int temp = i;
            Future<String> f = pool.submit(() -> {
                log.debug("开始收集 {}色 龙珠", dragonEge[temp]);
                Thread.sleep(1000);
                log.debug("收集 {}色 龙珠成功", dragonEge[temp]);
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
                return dragonEge[temp];
            });
        }

        pool.shutdown();

    }
}
