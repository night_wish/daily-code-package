package com.lee.juc.juc4;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Semaphore;

/**
 * 信号量--限制资源的并发访问数
 */
@Slf4j
public class SemaphoreTest {
    public static void main(String[] args) {
        //信号量--限制资源的并发访问数
        Semaphore semaphore = new Semaphore(3);
        for (int i = 0; i <10 ; i++) {
            new Thread(()->{
                try {
                    //获取信号量(总信号量减1)
                    semaphore.acquire();
                    log.debug("running");
                    Thread.sleep(1000);
                    log.debug("end");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    //释放信号量(总信号量加1)
                    semaphore.release();
                }
            },"t"+i).start();
        }
    }
}
