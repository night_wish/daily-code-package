package com.lee.juc.juc4;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 读写锁
 */
@Slf4j
public class ReadWriteLockTest {
    public static void main(String[] args) {
        DataContainer dContainer = new DataContainer("a");
        new Thread(()->{
            dContainer.read();
        },"t1").start();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()->{
            dContainer.write("b");
        },"t3").start();

    }
}

//数据容器
@Slf4j
@Data
class DataContainer{

    //共享数据
    private Object obj;

    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    //读锁
    private ReentrantReadWriteLock.ReadLock r = lock.readLock();
    //写锁
    private ReentrantReadWriteLock.WriteLock w = lock.writeLock();

    public DataContainer(Object obj) {
        this.obj = obj;
    }

    public Object read(){
        log.debug("尝试获取读锁");
        r.lock();
        log.debug("获取读锁成功");
        try {
            Thread.sleep(5000);
            log.debug("读取数据"+obj);
            return obj;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } finally {
            log.debug("释放读锁");
            r.unlock();
            log.debug("释放读锁成功");
        }
    }

    public void write(Object res){
        log.debug("获取写锁");
        w.lock();
        log.debug("获取写锁成功");
        try {
            log.debug("修改数据"+res);
            setObj(res);
        } finally {
            log.debug("释放写锁");
            w.unlock();
            log.debug("释放写锁成功");
        }
    }


}
