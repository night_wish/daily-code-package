package com.lee.juc.model;

import com.lee.base.commons.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/4/10 9:34
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AftResultDTO {

    private Map<String, User> userMap = new ConcurrentHashMap<>();

    private AnalyzeOutSubDTO analyzeOutSubDTO = new AnalyzeOutSubDTO();
}
