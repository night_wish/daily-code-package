package com.lee.juc.model;

import com.lee.base.commons.MenuInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2023/4/10 9:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnalyzeOutSubDTO {
    private String taskId;

    private List<MenuInfo> menuInfos;
}
