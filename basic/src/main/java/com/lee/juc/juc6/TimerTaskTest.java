package com.lee.juc.juc6;

import cn.hutool.core.thread.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/8/25 10:45
 */
@Slf4j
public class TimerTaskTest {

    public static void main(String[] args) {
//        Random random = new Random();
//        for(int i=0;i<20;i++){
//            String number = Integer.toString(random.nextInt(10000));
//            if(number.length()<4){
//                number = number.
//            }
//            System.out.println();
//        }
        System.out.println(Math.random());
        System.out.println(Math.random()*9);
        System.out.println((int)Math.random()*9+1);
        System.out.println((int)((Math.random()*9+1)*1000));
        System.out.println((int)((Math.random()*9+1)*1000));
        String a = Integer.toString((int)((Math.random()*9+1)*1000));
        System.out.println(a);
    }


    /**
     * 于某个时间点执行,每段时间，执行一次，timer.schedule(任务，时间，时间间隔);
     * 如十秒后执行，然后每五秒执行一次
     */
    @Test
    public void testTimerTask2() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                System.out.print("lee   " + this.scheduledExecutionTime());
                ThreadUtil.sleep(5, TimeUnit.SECONDS);
                System.out.println("  执行完成   ");
            }
        };

        Timer timer = new Timer("我的timer");
        LocalDateTime localDateTime = LocalDateTime.now().plusSeconds(10);
        Date runDate = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        timer.scheduleAtFixedRate(task, runDate, 5000);

        ThreadUtil.sleep(2, TimeUnit.MINUTES);
    }


    /**
     * 于某个时间点执行，timer.schedule(任务，时间);
     * 如十秒后
     */
    @Test
    public void testTimerTask1() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                System.out.println("lee");
            }
        };

        Timer timer = new Timer("我的timer");
        LocalDateTime localDateTime = LocalDateTime.now().plusSeconds(10);
        Date runDate = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        timer.schedule(task, runDate);

        ThreadUtil.sleep(2, TimeUnit.MINUTES);
    }


}
