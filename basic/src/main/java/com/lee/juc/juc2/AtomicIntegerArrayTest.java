package com.lee.juc.juc2;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicIntegerArray;

@Slf4j
public class AtomicIntegerArrayTest {

    public static void main(String[] args) {
        //创建了容量为10个int的AtomicIntegerArray
        AtomicIntegerArray array = new AtomicIntegerArray(10);
        log.debug("第2个值:{}",array.get(1));


        int[] array2 = new int[]{1,2,3,4,5,6,7,8,9,10};
        AtomicIntegerArray array3 = new AtomicIntegerArray(array2);
        log.debug("第2个值:{}",array3.get(1));
        log.debug("第2个值+1:{}",array3.getAndIncrement(1));
        log.debug("第2个值:{}",array3.get(1));
        log.debug("第3个值+5:{}",array3.getAndAdd(2,5));
        log.debug("第3个值:{}",array3.get(2));

        log.debug("第4个值*7：{}",array3.getAndUpdate(3,t->t*7));
        log.debug("第3个值:{}",array3.get(3));

        log.debug("修改第5个值为999");
        array3.set(4,999);
        log.debug("第5个值:{}",array3.get(4));
    }
}
