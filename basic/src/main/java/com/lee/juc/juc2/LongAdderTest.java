package com.lee.juc.juc2;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Slf4j
public class LongAdderTest {
    public static void main(String[] args) {
        for (int i = 0; i <5 ; i++) {
            demo(
                    ()->new AtomicLong(0),
                    (adder)->adder.getAndIncrement()
            );
        }
        log.debug("-------------");
        for (int i = 0; i <5 ; i++) {
            demo(
                    ()->new LongAdder(),
                    (adder)->adder.increment()
            );
        }
    }

    private static <T> void demo(Supplier<T> supplier, Consumer<T> consumer){
        T adder = supplier.get();
        List<Thread> ts = new ArrayList<>();
        //4个线程，每人累加50万
        for (int i=0;i<4;i++){
           ts.add( new Thread(()->{
                for(int j=0;j<500000;j++){
                    consumer.accept(adder);
                }
           }));
        }

        long start = System.nanoTime();
        ts.forEach(t->t.start());
        ts.forEach(t->{
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        long end = System.nanoTime();
        log.debug("adder {}, cost : {} ms",adder,(end-start)/1000_000);

    }
}
