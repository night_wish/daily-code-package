package com.lee.juc.juc2;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicStampedReference;

@Slf4j
public class AtomicStampedReferenceABA02 {

    static AtomicStampedReference<String> asr =new AtomicStampedReference<>("A",0);
    public static void main(String[] args) {
        log.debug("main start....");
        String prev = asr.getReference();
        int stamp = asr.getStamp();
        log.debug("main prev:{} , stamp:{}",prev,stamp);
        other();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.debug("main change A-->C : {}",asr.compareAndSet(prev,"C",stamp,stamp+1));
    }

    private static void other(){
        new Thread(()->{
            String prev = asr.getReference();
            int stamp = asr.getStamp();
            log.debug("t1 prev:{} , stamp:{}",prev,stamp);
            log.debug("t1 change A-->B : {}",asr.compareAndSet(prev,"B",stamp,stamp+1));
        },"t1").start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(()->{
            String prev = asr.getReference();
            int stamp = asr.getStamp();
            log.debug("t2 prev:{} , stamp:{}",prev,stamp);
            log.debug("t2 change B-->A : {}",asr.compareAndSet(prev,"A",stamp,stamp+1));
        },"t2").start();
    }
}
