package com.lee.juc.juc2;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test01 {

    static boolean runFlag = true;
    public static void main(String[] args) {
        new Thread(()->{
            while (runFlag){
                //
                System.out.println();
            }
        },"t1").start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.debug("main 线程修改flag");
        runFlag = false;
    }
}
