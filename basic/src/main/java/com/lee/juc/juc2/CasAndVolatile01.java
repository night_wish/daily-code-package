package com.lee.juc.juc2;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class CasAndVolatile01 {

    public static void main(String[] args) {

        //unsafe实现
        Account account1 = new AccountUnsafe(10000);
        Account.demo(account1);

        //加锁实现
        Account account2 = new AccountLock(10000);
        Account.demo(account2);

        //无锁实现
        Account account3 = new AccountCas(10000);
        Account.demo(account3);


    }
}

//无锁实现
class AccountCas implements Account{

    AtomicInteger balance;

    public AccountCas(Integer balance) {
        this.balance = new AtomicInteger(balance);
    }

    @Override
    public Integer getBalance() {
        return balance.get(); //底层是volatile
    }

    @Override
    public void withDraw(Integer amount) {
        while(true){
            //余额最新值
            Integer prev = balance.get();
            //修改后的余额
            Integer next = prev - amount;
            //同步到主存
            if(balance.compareAndSet(prev,next)){
                break;
            }
        }
    }
}

//加锁实现
class AccountLock implements Account{

    Integer balance;

    public AccountLock(Integer balance) {
        this.balance = balance;
    }

    @Override
    public Integer getBalance() {
        synchronized (this){
            return this.balance;
        }
    }

    @Override
    public void withDraw(Integer amount) {
        synchronized (this){
            this.balance -= amount;
        }
    }
}

//错误实现
class AccountUnsafe implements Account{

    Integer balance;

    public AccountUnsafe(Integer balance) {
        this.balance = balance;
    }

    @Override
    public Integer getBalance() {
        return this.balance;
    }

    @Override
    public void withDraw(Integer amount) {
        this.balance -= amount;
    }
}

interface Account{

    //获取余额
    Integer getBalance();

    //取款
    void withDraw(Integer amount);

    //方法内启动1000个线程，每个线程-10元
    //如果总额为10000，那么余额将会为0
    static void demo(Account account){
        List<Thread> ts = new ArrayList<>();
        for(int i=0;i<1000;i++){
            ts.add(new Thread(()->{
                account.withDraw(10);
            }));
        }
        //计算起始时间
        long start = System.nanoTime();
        ts.forEach(Thread::start);
        ts.forEach(t->{
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        long end = System.nanoTime();
        System.out.println(account.getBalance()
        +"  cost : "+(end-start)/1000_000+"ms");

    }
}
