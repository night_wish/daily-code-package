package com.lee.juc.juc2;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class AtomicReferenceABA01 {

    static AtomicReference<String> at = new AtomicReference("A");
    public static void main(String[] args) {
        log.debug("main start...");
        String prev = at.get();
        other();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.debug("main change A-->C : {}",at.compareAndSet(prev,"C"));
    }

    private static void other(){
        new Thread(()->{
            log.debug("t1 change A-->B : {}",at.compareAndSet("A","B"));
        },"t1").start();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()->{
            log.debug("t1 change B-->A : {}",at.compareAndSet("B","A"));
        },"t2").start();
    }
}


