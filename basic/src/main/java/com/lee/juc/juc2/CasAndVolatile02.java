package com.lee.juc.juc2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class CasAndVolatile02 {

    public static void main(String[] args) {
        DecimalAccount account = new BigDecimalAccountCas(new BigDecimal("10000"));
        DecimalAccount.demo(account);
    }
}

class BigDecimalAccountCas implements DecimalAccount{

    private AtomicReference<BigDecimal> balance;

    public BigDecimalAccountCas(BigDecimal balance) {
        this.balance = new AtomicReference<>(balance);
    }

    @Override
    public BigDecimal getBalance() {
        return balance.get();
    }

    @Override
    public void withDraw(BigDecimal amount) {
        while (true){
            BigDecimal prev = balance.get();
            BigDecimal next = prev.subtract(amount);
            if(balance.compareAndSet(prev,next)){
                break;
            }
        }
    }
}

interface DecimalAccount{

    //获取余额
    BigDecimal getBalance();

    //取款
    void withDraw(BigDecimal amount);

    //方法内启动1000个线程，每个线程-10元
    //如果总额为10000，那么余额将会为0
    static void demo(DecimalAccount account2){
        List<Thread> ts = new ArrayList<>();
        for(int i=0;i<1000;i++){
            ts.add(new Thread(()->{
                account2.withDraw(BigDecimal.TEN);
            }));
        }
        //计算起始时间
        long start = System.nanoTime();
        ts.forEach(Thread::start);
        ts.forEach(t->{
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        long end = System.nanoTime();
        System.out.println(account2.getBalance()
                +"  cost : "+(end-start)/1000_000+"ms");

    }
}
