package com.lee.juc.juc2;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class AtomicIntegerTest {
    public static void main(String[] args) {
        AtomicInteger i = new AtomicInteger(0);

        System.out.println(i.incrementAndGet());//++i
        System.out.println(i.getAndIncrement());//i++

        System.out.println(i.addAndGet(5));//先+5然后get
        System.out.println(i.getAndAdd(5));//先get然后+5

        System.out.println(i.updateAndGet(x->x*5));//先乘以5后get
        System.out.println(i.getAndUpdate(x->x*5));//先get然后乘以5

        System.out.println(i.get());
    }
}
