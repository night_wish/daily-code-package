package com.lee.juc.juc2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

@Slf4j
public class AtomicReferenceFieldUpdaterTest {
    public static void main(String[] args) {
        Student st = new Student("李四");

        //参数：类，要更新字段的类型，要更新字段的名称
        AtomicReferenceFieldUpdater updater = AtomicReferenceFieldUpdater.newUpdater(Student.class,String.class,"name");

        log.debug("更新名称为张三:{}",updater.compareAndSet(st,"李四","张三"));
        log.debug("st : {}",st);

    }
}

@Data
@ToString
@AllArgsConstructor
class Student{
    volatile String name;
}
