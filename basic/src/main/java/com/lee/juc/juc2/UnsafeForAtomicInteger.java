package com.lee.juc.juc2;

import lombok.extern.slf4j.Slf4j;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class UnsafeForAtomicInteger {

    public static void main(String[] args) {
        MyAtomicInteger mat = new MyAtomicInteger(10000);

        List<Thread> ts = new ArrayList<>();
        //1000个线程，每个线程减去10
        for(int i=0;i<1000;i++){
            ts.add(new Thread(()->{
                mat.decrement(10);
            }));
        }

        ts.forEach(t->t.start());
        ts.forEach(t->{
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        log.debug("mat : {}",mat.get());
    }
}

class MyAtomicInteger{

    private volatile Integer value;
    private static final long valueOffset;
    private static final Unsafe UNSAFE;

    static{
        try {
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            UNSAFE = (Unsafe) theUnsafe.get(null);

            valueOffset = UNSAFE.objectFieldOffset(MyAtomicInteger.class.getDeclaredField("value"));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            throw new RuntimeException();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    public MyAtomicInteger(Integer value) {
        this.value = value;
    }

    public Integer get(){
        return value;
    }

    public void decrement(Integer num){
        while (true){
            Integer prev = this.value;
            Integer next = prev - num;
            if(UNSAFE.compareAndSwapObject(this,valueOffset,prev,next)){
                break;
            }
        }
    }
}
