package com.lee.juc.juc2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicMarkableReference;

@Slf4j
public class AtomicMarkableReferenceTest {

    public static void main(String[] args) {
        GarbageBag bag = new GarbageBag("一个满了的垃圾袋");
        AtomicMarkableReference<GarbageBag> amr = new AtomicMarkableReference<>(bag,true);
        log.debug("start...");
        GarbageBag prev = amr.getReference();
        log.debug("main prev:{}",prev);

        new Thread(()->{
            log.debug("清洁阿姨 更换垃圾袋: {}",amr.compareAndSet(prev,new GarbageBag("清洁阿姨放了一个新的垃圾袋"),true,false));
        },"清洁阿姨").start();


        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.debug("main 更换垃圾袋: {}",amr.compareAndSet(prev,new GarbageBag("main放了一个新的垃圾袋"),true,false));

    }

}

@Data
@ToString
@AllArgsConstructor
class GarbageBag{
    private String desc;
}
