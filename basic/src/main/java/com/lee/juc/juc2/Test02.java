package com.lee.juc.juc2;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test02 {

    int num = 0;
    boolean ready = false;

    public void actor_1(int r){
        if(ready){
            r = num + num;
            log.info("r : {}",r);
        }else{
            r = 1;
            log.info("r : {}",r);
        }
    }

    public void actor_2(){
        num = 2;
        ready = true;
    }

}
