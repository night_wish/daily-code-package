package com.lee.juc.juc2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
/**
 * 使用Unsafe线程安全的操作Teacher对象的成员变量
 * (之前使用AtomicReferenceFieldUpdater)
 */
@Slf4j
public class UnsafeAccessor {

    public static void main(String[] args) {
        try {
            //获取Unsafe
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            Unsafe unsafe = (Unsafe) theUnsafe.get(null);
            log.debug("unsafe:{}",unsafe);

            Teacher tc = new Teacher(1,"张三");
            //获取域的偏移地址
            long idOffset = unsafe.objectFieldOffset(Teacher.class.getDeclaredField("id"));
            long nameOffset = unsafe.objectFieldOffset(Teacher.class.getDeclaredField("name"));

            //执行CAS操作
            unsafe.compareAndSwapInt(tc,idOffset,1,2);
            unsafe.compareAndSwapObject(tc,nameOffset,"张三","李四");

            //检验
            log.debug("tc : {}",tc);

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}

@Data
@ToString
@AllArgsConstructor
class Teacher{
    volatile int id;
    volatile String name;
}
