package com.lee.juc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 项目名称：hsa-ims-engine-crhms
 * 类 名 称：ThreadPoolConfig
 * 类 描 述：
 * 创建时间：2021-08-13 09:28
 * 创 建 人：Lee
 */
@Configuration
public class ThreadPoolConfig {

    /**
     * 线程池维护线程的最少数量，即使没有任务需要执行，也会一直存活
     */
    @Value("${poolconfig.corePoolSize:1}")
    private int corePoolSize;

    /**
     * 线程池维护线程的最大数量
     */
    @Value("${poolconfig.maxPoolSize:1}")
    private int maxPoolSize;

    /**
     * 缓存队列（阻塞队列）当核心线程数达到最大时，新任务会放在队列中排队等待执行
     */
    @Value("${poolconfig.queueCapacity:10}")
    private int queueCapacity;

    /**
     * 允许的空闲时间，当线程空闲时间达到keepAliveTime时，线程会退出，直到线程数量=corePoolSize
     */
    @Value("${poolconfig.keepAlive:300}")
    private int keepAlive;


    @Bean(name="imsNationTaskExecutor")
    public TaskExecutor taskExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //设置核心线程数
        executor.setCorePoolSize(corePoolSize);
        // 设置最大线程数
        executor.setMaxPoolSize(maxPoolSize);
        // 设置队列容量
        executor.setQueueCapacity(queueCapacity);
        // 设置允许的空闲时间（秒）
        executor.setKeepAliveSeconds(keepAlive);
        // 设置默认线程名称
        executor.setThreadNamePrefix("thread-bigdata-");
        // 设置拒绝策略rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALL_RUNS:不在新线程中执行任务，二十由调用者所在的线程来执行
        //executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());

        //队列满了之后 抛弃存在时间最长的任务  然后新任务进去，不会抛异常
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardOldestPolicy());
        // 等待所有任务结束后在关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        return executor;
    }

    @Bean(name="imsNationTaskExecutor2")
    public ThreadPoolTaskExecutor taskExecutor2(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //设置核心线程数
        executor.setCorePoolSize(corePoolSize);
        // 设置最大线程数
        executor.setMaxPoolSize(maxPoolSize);
        // 设置队列容量
        executor.setQueueCapacity(queueCapacity);
        // 设置允许的空闲时间（秒）
        executor.setKeepAliveSeconds(keepAlive);
        // 设置默认线程名称
        executor.setThreadNamePrefix("thread-bigdata-");
        // 设置拒绝策略rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALL_RUNS:不在新线程中执行任务，二十由调用者所在的线程来执行
        //executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());

        //队列满了之后 抛弃存在时间最长的任务  然后新任务进去，不会抛异常
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardOldestPolicy());
        // 等待所有任务结束后在关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        return executor;
    }
}
