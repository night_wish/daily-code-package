package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.LockSupport;

@Slf4j
public class ParkInterrupt {

    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            log.debug("park.......");
            LockSupport.park();
            log.debug("unpark.......");
            log.debug("打断标记为:{}",Thread.currentThread().isInterrupted());
        },"t1");

        t1.start();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t1.interrupt();
    }
}
