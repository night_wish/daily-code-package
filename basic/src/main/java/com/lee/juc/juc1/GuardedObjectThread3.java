package com.lee.juc.juc1;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

@Slf4j
public class GuardedObjectThread3 {

    public static void main(String[] args) throws InterruptedException {
        for (int i=0;i<3;i++){
            new Consignee().start();
        }
        Thread.sleep(1000);
        for (int id : MailBox.getIds()){
            new Postman(id,"内容"+id).start();
        }
    }
}

//收件人
@Slf4j
class Consignee extends Thread{
    @Override
    public void run() {
        //收信--我收信的信件ID是xxx
        GurdedObject3 go = MailBox.createGurdedObject();
        log.info("开始收信 id : {}",go.getId());
        Object mail = go.get(5000);
        log.info("收到信 id: {},内容:{}",go.getId(),mail);
    }
}

//邮递员
@Slf4j
class Postman extends Thread{
    private int id;
    private String mail;

    public Postman(int id,String mail){
        this.id = id;
        this.mail = mail;
    }

    @Override
    public void run() {
        //送信--我送信的信件ID是xxx
        GurdedObject3 go = MailBox.getGurdedObject(id);
        log.info("送信 id: {},内容:{}",go.getId(),mail);
        go.produce(mail);
    }
}

//邮箱
class MailBox{

    //int为 gurdedObject id，hashtable是线程安全的
    private static Map<Integer,GurdedObject3> boxes = new Hashtable<>();

    //生产唯一ID
    private static int id = 1;
    private static synchronized int generatorId(){
        return id++;
    }

    public static GurdedObject3 createGurdedObject(){
        //创建信件
        GurdedObject3 go = new GurdedObject3(generatorId());
        //放入信箱
        boxes.put(go.getId(),go);
        return go;
    }

    public static GurdedObject3 getGurdedObject(int id){
        //收到信件后移除从信箱中移除信件
        return boxes.remove(id);
    }

    public static Set<Integer> getIds(){
        return boxes.keySet();
    }


}

//邮件
@Data
class GurdedObject3{

    //唯一标识
    private int id;

    //结果
    private Object response;

    //获取结果
    public Object get(long timeout){
        synchronized (this){
            //开始时间
            long beginTime = System.currentTimeMillis();
            //等待的时间
            long passedTime = 0;
            while(response==null){
                if(passedTime>=timeout){
                    break;
                }
                try {
                    this.wait(timeout-passedTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                passedTime = System.currentTimeMillis() - beginTime;
            }
        }
        return response;
    }

    //生产结果
    public void produce(Object response){
        synchronized (this){
            this.response = response;
            this.notifyAll();
        }
    }

    //构造函数
    public GurdedObject3(int id){
        this.id = id;
    }
}