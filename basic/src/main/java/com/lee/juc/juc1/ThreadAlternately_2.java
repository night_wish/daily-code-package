package com.lee.juc.juc1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class ThreadAlternately_2 {

    public static void main(String[] args) {
        AwaitNotify an = new AwaitNotify(5);
        Condition aCond = an.newCondition();
        Condition bCond = an.newCondition();
        Condition cCond = an.newCondition();
        new Thread(()->{
            an.print("a",aCond,bCond);
        },"t1").start();

        new Thread(()->{
            an.print("b",bCond,cCond);
        },"t2").start();

        new Thread(()->{
            an.print("c",cCond,aCond);
        },"t3").start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //主线程唤醒t1
        an.lock();
        try {
            aCond.signal();
        }finally {
            an.unlock();
        }

    }
}

@Slf4j
@Data
@AllArgsConstructor
class AwaitNotify extends ReentrantLock {

    private int loopNum;//循环次数

    //str打印内容 currentCond当前休息室 nextCond下一个休息室
    public void print(String str, Condition currentCond,Condition nextCond){
        for (int i = 0; i <loopNum ; i++) {
            this.lock();
            try{
                currentCond.await();
                //被唤醒后打印 内容
                log.info(str);
                //唤醒另一个线程
                nextCond.signal();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                this.unlock();
            }
        }
    }
}
