package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class MultiRoomThread {

    public static void main(String[] args) {
        BigRoom2 br = new BigRoom2();
        new Thread(()->{
            try {
                br.sleep();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"小南").start();

        new Thread(()->{
            try {
                br.study();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"小女").start();
    }
}

@Slf4j
class BigRoom2{

    private final Object sleepRoom = new Object();
    private final Object studyRoom = new Object();

    public void sleep() throws InterruptedException {
        synchronized (sleepRoom){
            log.info("sleep 2个小时");
            Thread.sleep(2000);
        }
    }

    public void study() throws InterruptedException {
        synchronized (studyRoom){
            log.info("study 1个小时");
            Thread.sleep(1000);
        }
    }
}
