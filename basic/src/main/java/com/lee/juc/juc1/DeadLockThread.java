package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

/**
 * 死锁
 */
@Slf4j
public class DeadLockThread {

    public static void main(String[] args) {
        Object A = new Object();
        Object B = new Object();

        new Thread(()->{
            synchronized (A){
                log.info("lock A");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (B){
                    log.info("lock A");
                    log.info("操作.....");
                }
            }
        },"A").start();

        new Thread(()->{
            synchronized (B){
                log.info("lock A");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (A){
                    log.info("lock A");
                    log.info("操作.....");
                }
            }
        },"B").start();
    }
}
