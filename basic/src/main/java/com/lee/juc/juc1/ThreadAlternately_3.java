package com.lee.juc.juc1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.LockSupport;

@Slf4j
public class ThreadAlternately_3 {

    static Thread t1,t2,t3;
    public static void main(String[] args) {
        ParkUnpark pu = new ParkUnpark(5);

        t1 = new Thread(()->{
            pu.print("a",t2);
        },"t1");

        t2 = new Thread(()->{
            pu.print("b",t3);
        },"t2");

        t3 = new Thread(()->{
            pu.print("c",t1);
        },"t3");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t1.start();
        t2.start();
        t3.start();
        LockSupport.unpark(t1);
    }
}

@Slf4j
@Data
@AllArgsConstructor
class ParkUnpark{

    private int loopNum;//循环次数

    //str打印的内容 t被唤醒的线程
    public void print(String str,Thread t){
        for (int i = 0; i <loopNum ; i++) {
            LockSupport.park();
            log.info(str);
            LockSupport.unpark(t);
        }
    }
}
