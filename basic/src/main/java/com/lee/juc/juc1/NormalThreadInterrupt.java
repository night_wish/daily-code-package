package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NormalThreadInterrupt {

    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            log.debug("sleep start....");
            while(true){
                boolean interrupted = Thread.currentThread().isInterrupted();
                if(interrupted){
                    log.debug("t1 线程被打断了,且打断标记为：{}",interrupted);
                    break;
                }
            }
        },"t1");
        t1.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.debug("interrupt");
        t1.interrupt();
        log.debug("main 打断标记：{}"+t1.isInterrupted());
    }
}
