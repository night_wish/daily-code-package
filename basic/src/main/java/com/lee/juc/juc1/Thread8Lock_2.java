package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;
/**
 * 线程八锁二：结果是一秒后1,2或者先2然后一秒后1
 *
 * 代码中的synchronized表示都是当前对象n2
 *
 * 12:03:41.580 [t2] DEBUG com.lee.juc.Thread8Lock_2 - begin
 * 12:03:41.580 [t1] DEBUG com.lee.juc.Thread8Lock_2 - begin
 * 12:03:41.583 [t2] DEBUG com.lee.juc.Number2 - 2
 * 12:03:42.584 [t1] DEBUG com.lee.juc.Number2 - 1
 */
@Slf4j
public class Thread8Lock_2 {

    public static void main(String[] args) {

        Number2 n2 = new Number2();

        new Thread(()->{
            log.debug("begin");
            try {
                n2.a();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t1").start();

        new Thread(()->{
            log.debug("begin");
            n2.b();
        },"t2").start();


    }
}

@Slf4j
class Number2{

    public synchronized void a() throws InterruptedException {
        Thread.sleep(1000);
        log.debug("1");
    }

    public synchronized void b(){
        log.debug("2");
    }
}
