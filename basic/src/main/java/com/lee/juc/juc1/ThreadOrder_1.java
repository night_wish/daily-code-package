package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadOrder_1 {

    static final Object lock = new Object();//锁对象
    static boolean if2runned = false;//线程2是否执行

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            synchronized (lock) {
                while (!if2runned) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                log.info("1");
            }
        }, "t1");

        Thread t2 = new Thread(() -> {
            synchronized (lock) {
                log.info("2");
                if2runned = true;
                lock.notify();
            }
        }, "t2");

        t1.start();
        t2.start();
    }
}
