package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StartAndRun {

    public static void main(String[] args) {
        Thread t1 = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(3000);
                    log.debug("thread running....");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
//        t1.start();
        t1.run();
        log.debug("main running");
    }
}
