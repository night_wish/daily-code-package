package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

/**
 * 保护性暂停模式：
 * 线程1等待线程2的结果
 * 扩展：增加等待超时
 */
@Slf4j
public class GuardedObjectThread2 {

    public static void main(String[] args) {

        GuardedObject2 guardedObject = new GuardedObject2();

        //等待结果线程
        new Thread(()->{
            log.info("等待结果...");
            String resp = (String) guardedObject.get(2000);
            log.info("结果为：{}",resp);
        },"t1").start();

        //生产结果线程
        new Thread(()->{
            log.info("执行生产...");
            try {
                log.info("先休息一会再忙");
                Thread.sleep(3000);
                guardedObject.produce("生产了一颗芒果");
                log.info("休息好了");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t2").start();

    }
}

class GuardedObject2{
    //结果
    private Object response;

    //获取结果
    public Object get(long timeout){
        synchronized (this){
            //开始时间
            long beginTime = System.currentTimeMillis();
            //等待的时间
            long passedTime = 0;
            while(response==null){
                if(passedTime>=timeout){
                    break;
                }
                try {
                    this.wait(timeout-passedTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                passedTime = System.currentTimeMillis() - beginTime;
            }
        }
        return response;
    }

    //生产结果
    public void produce(Object response){
        synchronized (this){
            this.response = response;
            this.notifyAll();
        }
    }
}

