package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

/**
 * 小南和小女在一间屋子干活，小南没烟罢工，小女没外卖罢工
 */
@Slf4j
public class WaitNotifyUseThread3 {

    static final Object room  = new Object();//屋子
    static boolean hasCigarette = false;//是否有烟
    static boolean hasTakeOut = false;//是否有外卖

    public static void main(String[] args) {
        //小南
        new Thread(()->{
            synchronized (room){
                log.info("有烟没：[{}]",hasCigarette);
                if(!hasCigarette){
                    log.info("没烟,歇会儿！");
                    try {
                        room.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                log.info("现在有烟了没[{}]",hasCigarette);
                if(hasCigarette){
                    log.info("可以开始干活了");
                }else{
                    log.info("今天活是干不成喽");
                }
            }
        },"小南").start();

        //小女
        new Thread(()->{
            synchronized (room){
                log.info("有外卖没：[{}]",hasTakeOut);
                if(!hasTakeOut){
                    log.info("没外卖,歇会儿！");
                    try {
                        room.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                log.info("现在有外卖了没[{}]",hasTakeOut);
                if(hasTakeOut){
                    log.info("可以开始干活了");
                }else{
                    log.info("今天活是干不成喽");
                }
            }
        },"小女").start();

        //主线程休息1s
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //送外卖的到了
        new Thread(()->{
            synchronized (room){
                hasTakeOut = true;
                log.info("外卖到了");
                room.notify();
            }
        },"送外卖的").start();

        //送烟的到了
        new Thread(()->{
            synchronized (room){
                hasCigarette = true;
                log.info("烟到了");
                room.notify();
            }
        },"送烟的").start();
    }

}