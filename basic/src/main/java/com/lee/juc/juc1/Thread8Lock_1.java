package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

/**
 * 线程八锁一：结果是1,2或者2,1
 *
 * 代码中的synchronized表示都是当前对象n1
 *
 * 11:59:14.288 [t1] DEBUG com.lee.juc.Thread8Lock_1 - begin
 * 11:59:14.288 [t2] DEBUG com.lee.juc.Thread8Lock_1 - begin
 * 11:59:14.290 [t1] DEBUG com.lee.juc.Number - 1
 * 11:59:14.290 [t2] DEBUG com.lee.juc.Number - 2
 */
@Slf4j
public class Thread8Lock_1 {

    public static void main(String[] args) {

        Number n1 = new Number();

        new Thread(()->{
            log.debug("begin");
            n1.a();
        },"t1").start();

        new Thread(()->{
            log.debug("begin");
            n1.b();
        },"t2").start();


    }
}

@Slf4j
class Number{

    public synchronized void a(){
        log.debug("1");
    }

    public synchronized void b(){
        log.debug("2");
    }
}

