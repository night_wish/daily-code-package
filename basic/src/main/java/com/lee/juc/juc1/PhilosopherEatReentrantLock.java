package com.lee.juc.juc1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.ReentrantLock;

/**
 * reentrantLock解决哲学家就餐问题
 */
public class PhilosopherEatReentrantLock {

    public static void main(String[] args) {
        Chopstick2 c1 = new Chopstick2("1");
        Chopstick2 c2 = new Chopstick2("2");
        Chopstick2 c3 = new Chopstick2("3");
        Chopstick2 c4 = new Chopstick2("4");
        Chopstick2 c5 = new Chopstick2("5");

        new Philosopher2("苏格拉底",c1,c2).start();
        new Philosopher2("柏拉图",c2,c3).start();
        new Philosopher2("亚里士多德",c3,c4).start();
        new Philosopher2("赫拉克利特",c4,c5).start();
        new Philosopher2("阿基米德",c5,c1).start();
    }

}


@Slf4j
class Philosopher2 extends Thread{
    //左边的筷子
    private Chopstick2 left;
    //右边的筷子
    private Chopstick2 right;

    public Philosopher2(String name,Chopstick2 left,Chopstick2 right){
        super(name);
        this.left = left;
        this.right = right;
    }

    @Override
    public void run() {
        while (true){
            //尝试获取左手边的筷子
            if(left.tryLock()){
                try{
                    //尝试获取右手边的筷子
                    if(right.tryLock()){
                        try {
                            //吃饭
                            eat();
                        }finally {
                            right.unlock();
                        }
                    }
                }finally {
                    left.unlock();
                }
            }

        }
    }

    public void eat(){
        log.info("eat.....");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

//筷子集成ReentrantLock
@Data
@AllArgsConstructor
class Chopstick2 extends ReentrantLock {

    private String name;

    @Override
    public String toString() {
        return "筷子{ "+name+"}";
    }
}
