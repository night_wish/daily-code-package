package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

/**
 * 保护性暂停模式：
 * 线程1等待线程2的结果
 */
@Slf4j
public class GuardedObjectThread1 {

    public static void main(String[] args) {

        GuardedObject guardedObject = new GuardedObject();

        //等待结果线程
        new Thread(()->{
            log.info("等待结果...");
            String resp = (String) guardedObject.get();
            log.info("结果为：{}",resp);
        },"t1").start();

        //生产结果线程
        new Thread(()->{
            log.info("执行生产...");
            guardedObject.produce("生产了一颗芒果");
            try {
                log.info("刚生忙了一会休息休息");
                Thread.sleep(3000);
                log.info("休息好了");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t2").start();

    }
}

class GuardedObject{
    //结果
    private Object response;

    //获取结果
    public Object get(){
        synchronized (this){
            while(response==null){
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    //生产结果
    public void produce(Object response){
        synchronized (this){
            this.response = response;
            this.notifyAll();
        }
    }
}
