package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

//守护线程
@Slf4j
public class DaemonThread {

    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            while(true){
                log.debug("daemon thread start");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.debug("daemon thread stop");
            }
        },"t1");

        //设置t1为守护线程
        t1.setDaemon(true);
        t1.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.debug("main thread stop");
    }
}
