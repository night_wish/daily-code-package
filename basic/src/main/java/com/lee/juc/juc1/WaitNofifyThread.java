package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WaitNofifyThread {

    final static Object obj =  new Object();

    public static void main(String[] args) throws InterruptedException {
        new Thread(()->{
            synchronized (obj){
                log.info("执行代码...");
                try {
                    obj.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("其他代码...");
            }
        },"t1").start();

        new Thread(()->{
            synchronized (obj){
                log.info("执行代码...");
                try {
                    obj.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("其他代码...");
            }
        },"t2").start();

        Thread.sleep(2000);
        log.info("唤醒obj上其他线程");
        synchronized (obj){
            obj.notify();//唤醒waitset中的一个线程
//            obj.notifyAll();//唤醒waitset中的所有线程
        }
    }
}
