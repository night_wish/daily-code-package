package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

@Slf4j
public class CreateThread {

    @Test
    public void threadTest(){
        //创建线程对象
        Thread t1 = new Thread(){
            @Override
            public void run() {
                log.debug("t1 running");
            }
        };
        //启动线程
        t1.start();
    }

    @Test
    public void threadLambdaTest(){
        //创建线程对象
        Thread t1 = new Thread(() -> log.debug("t1 running"));
        //启动线程
        t1.start();
    }

    @Test
    public void runnableTest(){
        //创建任务对象
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                log.debug("r1 running");
            }
        };

        //创建线程对象
        Thread t2 = new Thread(r1);

        //启动线程
        t2.start();
    }

    @Test
    public void runnableLambdaTest(){
        //创建任务对象
        Runnable r1 = () -> log.debug("r1 running");

        //创建线程对象
        Thread t2 = new Thread(r1);

        //启动线程
        t2.start();
    }

    @Test
    public void futureTaskTest() throws ExecutionException, InterruptedException {
        //创建任务对象 : FutureTask实现了RunnableTask,RunnableTask实现了Runnable,Future
        FutureTask<Integer> f1 = new FutureTask<>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                log.debug("callable...");
                return 100;
            }
        });

        //创建线程对象:futureTask实现了runnable接口，所以可以当做Thread的target
        Thread t3 = new Thread(f1);
        //开启线程
        t3.start();

        //接收执行结果 ： 主线程阻塞
        Integer res = f1.get();
        log.debug("结果：{}",res);
    }


}
