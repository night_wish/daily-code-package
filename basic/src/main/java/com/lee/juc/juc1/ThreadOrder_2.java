package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.LockSupport;

@Slf4j
public class ThreadOrder_2 {

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            LockSupport.park();//暂停t1线程，等待t2线程唤醒
            log.info("1");
        }, "t1");
        t1.start();

        new Thread(()->{
            log.info("2");
            LockSupport.unpark(t1);
        },"t2").start();

    }
}
