package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.LockSupport;

@Slf4j
public class ParkAndUnpark {

    public static void main(String[] args){
        Thread t1 = new Thread(()->{
            log.info("start...");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("park...");
            LockSupport.park();
            log.info("resume...");
        },"t1");
        t1.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("unpark");
        LockSupport.unpark(t1);
    }

}
