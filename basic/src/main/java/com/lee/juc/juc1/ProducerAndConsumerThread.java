package com.lee.juc.juc1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;

/**
 * 生产者-消费者模式
 */
@Slf4j
public class ProducerAndConsumerThread {
    public static void main(String[] args) {

        MessageQueue mq = new MessageQueue(2);
        //生产者
        for (int i = 0; i <3 ; i++) {
            int id = i;
            new Thread(()->{
                mq.put(new Message(id,"值"+id));
            },"生产者"+i).start();
        }

        //消费者
        new Thread(()->{
            while(true){
                try {
                    Thread.sleep(1000);
                    mq.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"消费者").start();

    }
}

//消息队列，java线程间通信
@Slf4j
class MessageQueue{

    //消息的队列集合
    private LinkedList<Message> list = new LinkedList<>();
    //队列容量
    private int capcity;

    public MessageQueue(int capcity) {
        this.capcity = capcity;
    }

    //获取消息
    public Message take(){
        synchronized (list){
            while(list.isEmpty()){
                try {
                    log.info("队列为空,消费者等待");
                    list.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //将消息从队头移除
            Message message = list.removeFirst();
            log.info("已消费消息 {}",message);
            list.notifyAll();
            return message;
        }
    }

    //添加消息
    public void put(Message message){
        synchronized (list){
            while(list.size()==capcity){
                try {
                    log.info("队列已满,生产者等待");
                    list.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //将消息添加到队尾
            list.addLast(message);
            log.info("已生产消息 {}",message);
            list.notifyAll();
        }
    }

}

//消息
@Data
@ToString
@AllArgsConstructor
final class Message{
    private int id;
    private Object value;
}