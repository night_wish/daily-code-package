package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShareDataSynchronized {

    private static int counter = 0;

    private static Object obj = new Object();

    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            for(int i=0;i<5000;i++){
                synchronized (obj){
                    counter++;
                }
            }
        },"t1");

        Thread t2 = new Thread(()->{
            for(int i=0;i<5000;i++){
                synchronized (obj){
                    counter--;
                }
            }
        },"t2");

        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.debug("counter : {}",counter);
    }

}
