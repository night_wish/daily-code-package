package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class ReentrantLockTryLockTest {

    private static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            try{
                log.info("t1尝试获取锁");
                if(!lock.tryLock(2, TimeUnit.SECONDS)){
                    log.info("t1获取锁失败");
                    return;
                }
            } catch (InterruptedException e) {
                log.info("t1获取锁失败");
                e.printStackTrace();
                return;
            }

            try{
                log.info("t1成功获取锁");
            }finally {
                lock.unlock();
            }

        }, "t1");

        log.info("main线程获取锁");
        lock.lock();
        t1.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("main线程释放锁");
        lock.unlock();

    }
}
