package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WaitNotifyUseThread2 {

    static final Object room  = new Object();//屋子
    static boolean hasCigarette = false;//是否有烟

    public static void main(String[] args) throws InterruptedException {
        //小南
        new Thread(()->{
            synchronized (room){
                log.info("有烟没：[{}]",hasCigarette);
                if(!hasCigarette){
                    log.info("没烟,歇会儿！");
                    try {
                        room.wait(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                log.info("现在有烟了没[{}]",hasCigarette);
                if(hasCigarette){
                    log.info("可以开始干活了");
                }
            }
        },"小南").start();

        //其他五个人
        for (int i=0;i<5;i++){
            new Thread(()->{
                synchronized (room){
                    log.info("可以开始干活了!");
                }
            },"其他人").start();
        }


        //主线程休息1秒
        Thread.sleep(1000);

        //送烟的
        new Thread(()->{
            synchronized (room){
                hasCigarette = true;
                log.info("烟到了");
                room.notify();
            }
        },"送烟的").start();
    }


}
