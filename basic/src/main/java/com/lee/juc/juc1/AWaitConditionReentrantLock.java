package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 小南和小女在一间屋子干活，小南没烟罢工，小女没外卖罢工
 */
@Slf4j
public class AWaitConditionReentrantLock {

    static ReentrantLock ROOM = new ReentrantLock();//房间
    static Condition cigaretteCondition = ROOM.newCondition();//等烟的休息室
    static Condition takeOutCondition = ROOM.newCondition();//等外卖的休息室
    static boolean hasCigarette = false;//是否有烟
    static boolean hasTakeOut = false;//是否有外卖

    public static void main(String[] args) {
        //小南
        new Thread(()->{
            ROOM.lock();
            try{
                log.info("有烟没：[{}]",hasCigarette);
                while(true){
                    if(!hasCigarette){
                        log.info("没烟，歇会儿！");
                        try {
                            cigaretteCondition.await();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }else{
                        log.info("可以开始干活了");
                        break;
                    }
                }
            }finally {
                ROOM.unlock();
            }
        },"小南").start();

        //小女
        new Thread(()->{
            ROOM.lock();
            try{
                log.info("有外卖没：[{}]",hasTakeOut);
                while(true){
                    if(!hasTakeOut){
                        log.info("没外卖，歇会儿！");
                        try {
                            takeOutCondition.await();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }else{
                        log.info("可以开始干活了");
                        break;
                    }
                }
            }finally {
                ROOM.unlock();
            }
        },"小女").start();

        //主线程休息1s
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //送外卖的到了
        new Thread(()->{
            ROOM.lock();
            try{
                hasTakeOut = true;
                log.info("外卖到了");
                takeOutCondition.signal();
            }finally {
                ROOM.unlock();
            }
        },"送外卖的").start();

        //送烟的到了
        new Thread(()->{
            ROOM.lock();
            try {
                hasCigarette = true;
                log.info("烟到了");
                cigaretteCondition.signal();
            }finally {
                ROOM.unlock();
            }
        },"送烟的").start();
    }

}
