package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SteepTea {
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            try {
                log.debug("洗水壶...");
                Thread.sleep(1000);
                log.debug("拿水壶烧水...");
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"老李");


        Thread t2 = new Thread(()->{
            try {
                log.debug("洗茶壶...");
                Thread.sleep(1000);
                log.debug("洗茶杯...");
                Thread.sleep(1000);
                log.debug("拿茶叶...");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                t1.join();//等待水烧开
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.debug("泡茶...");
        },"小李");

        t1.start();
        t2.start();

    }
}
