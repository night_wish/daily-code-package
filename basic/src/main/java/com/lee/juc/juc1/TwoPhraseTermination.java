package com.lee.juc.juc1;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TwoPhraseTermination {
    public static void main(String[] args) throws InterruptedException {
        MyThread t1 = new MyThread();
        t1.start();

        Thread.sleep(3000);
        t1.stop();
    }
}

@Slf4j
class MyThread{
    private Thread monitorThread;

    //启动线程
    public void start(){
        monitorThread = new Thread(()->{
            while(true){
                Thread currentThread = Thread.currentThread();
                if(currentThread.isInterrupted()){
                    log.debug("t1线程被终止后，处理后续业务");
                    break;
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    log.debug("t1线程 休眠 被异常中断,重置 中断标记为true");
                    currentThread.interrupt();
                }
                log.debug("t1线程正在处理业务");
            }
        },"t1");

        monitorThread.start();
    }

    //终止线程
    public void stop(){
        monitorThread.interrupt();
    }


}
