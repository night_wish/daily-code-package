package com.lee.juc.juc1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadAlternately_1 {

    public static void main(String[] args) {
        WaitNotify waitNotify = new WaitNotify(1, 5);
        new Thread(()->{
            waitNotify.print("a",1,2);
        },"t1").start();

        new Thread(()->{
            waitNotify.print("b",2,3);
        },"t2").start();

        new Thread(()->{
            waitNotify.print("c",3,1);
        },"t3").start();
    }
}

/**
 * 输出内容     等待标记flag   下一个标记flag
 *   a             1               2
 *   b             2               3
 *   c             3               1
 */
@Slf4j
@Data
@AllArgsConstructor
class WaitNotify{
    //等待标记
    private int flag;

    //循环次数
    private int loopNum;

    //打印
    public void print(String str,int waitFlag,int nextFlag){
        for (int i = 0; i <5 ; i++) {
            synchronized (this){
                while(flag!=waitFlag){
                    try {
                        this.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                log.info(str);
                flag = nextFlag;
                this.notifyAll();
            }
        }
    }
}
