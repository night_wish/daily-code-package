package com.lee.juc.juc1;


import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 可打断锁
 */
@Slf4j
public class ReentrantLockInterruptTest {

    private static ReentrantLock lock = new ReentrantLock();
    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(() -> {
            try {
                log.info("t1尝试获取锁");
                lock.lockInterruptibly();
            } catch (InterruptedException e) {
                log.info("t1获取锁失败");
                e.printStackTrace();
                return;
            }
            try {
                log.info("t1获取锁成功");
            }finally {
                lock.unlock();
            }
        },"t1");

        log.info("main线程获取锁");
        lock.lock();
        t1.start();

        //main睡1s
        Thread.sleep(1000);
        //主线程打断t1
        log.info("mian线程打断t1");
        t1.interrupt();
    }
}
