package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

/**
 * 活锁
 */
@Slf4j
public class LiveLockThread {

    static volatile int count = 10;

    public static void main(String[] args) {
        new Thread(()->{
            //期望count减到0停止
            while(count>0){
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                count--;
                log.info("count : {}"+count);
            }
        },"t1").start();


        new Thread(()->{
            //期望count加到0停止
            while(count<20){
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                count++;
                log.info("count : {}"+count);
            }
        },"t2").start();
    }
}
