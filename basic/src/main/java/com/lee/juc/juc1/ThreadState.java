package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadState {

    public static void main(String[] args) {
        //NEW
        Thread t1 = new Thread(()->{
            log.debug("running");
        },"t1");

        //RUNNING
        Thread t2 = new Thread(()->{
            while (true){

            }
        },"t2");
        t2.start();

        //TERMINATED
        Thread t3 = new Thread(()->{
            log.debug("running...");
        },"t3");
        t3.start();

        //TIMED_WATING
        Thread t4 = new Thread(()->{
            synchronized (ThreadState.class){ //t4比t6抢险占据了 锁
                try {
                    Thread.sleep(1000000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"t4");
        t4.start();

        //WAITING
        Thread t5 = new Thread(()->{
            try {
                t2.join();//等待t2的执行结束
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t5");
        t5.start();


        //BLOCKED
        Thread t6 = new Thread(()->{
            synchronized (ThreadState.class){ //t4比t6抢险占据了 锁
                try {
                    Thread.sleep(1000000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"t6");
        t6.start();


        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.debug("t1 state : {}",t1.getState());
        log.debug("t2 state : {}",t2.getState());
        log.debug("t3 state : {}",t3.getState());
        log.debug("t4 state : {}",t4.getState());
        log.debug("t5 state : {}",t5.getState());
        log.debug("t6 state : {}",t6.getState());


    }
}
