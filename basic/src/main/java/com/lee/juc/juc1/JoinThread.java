package com.lee.juc.juc1;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class JoinThread {
    static int r = 0;

    public static void main(String[] args) {
        test1();
    }

    public static void test1(){
        log.debug("开始");
        Thread t1 = new Thread(()->{
            log.debug("进入t1");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.debug("wake up");
            r=10;
            log.debug("退出t1");
        },"t1");

        t1.start();
//        try {
//            t1.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        log.debug("r的值为:{}",r);
        log.debug("结束");
    }
}
