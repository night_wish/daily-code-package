package com.lee.juc.juc3;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

@Slf4j
public class ThreadPoolExecutorTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //execute
//        execute();
        //submit
//        submit();
        //invokeAll
//        invokeAll();
        //invokeAny
        invokeAny();
    }

    private static void execute(){
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(()->{
            log.debug("execute ...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    private static void submit() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Future<String> future = executorService.submit(()->{
            log.debug("submit...");
            Thread.sleep(1000);
            return "ok";
        });
        log.debug("future:{}",future.get());
    }

    private static void invokeAll() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        List<Future<Object>> futureList = executorService.invokeAll(Arrays.asList(
                () -> {
                    log.debug("begin 1");
                    Thread.sleep(1000);
                    return "ok 1";
                },
                () -> {
                    log.debug("begin 2");
                    Thread.sleep(3000);
                    return "ok 2";
                },
                () -> {
                    log.debug("begin 3");
                    Thread.sleep(500);
                    return "ok 3";
                }
        ));
        futureList.forEach(t->{
            try {
                log.debug("result : {}",t.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
    }

    private static void invokeAny() throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        String res = executorService.invokeAny(Arrays.asList(
                () -> {
                    log.debug("begin 1");
                    Thread.sleep(1000);
                    return "ok 1";
                },
                () -> {
                    log.debug("begin 2");
                    Thread.sleep(3000);
                    return "ok 2";
                },
                () -> {
                    log.debug("begin 3");
                    Thread.sleep(500);
                    return "ok 3";
                }
        ));

        log.debug("result : {}",res);
    }
}
