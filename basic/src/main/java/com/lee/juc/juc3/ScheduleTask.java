package com.lee.juc.juc3;

import lombok.extern.slf4j.Slf4j;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 定时任务：每周四 18:00:00执行任务
 */
@Slf4j
public class ScheduleTask {

    public static void main(String[] args) {
        //获取当前时间
        LocalDateTime nowTime = LocalDateTime.now();
        //获取周四时间
        LocalDateTime thursdayTime = nowTime.withHour(18).withMinute(0).withSecond(0).withNano(0).with(DayOfWeek.THURSDAY);
        //如果当前时间大于周四获取下周四
        if(nowTime.compareTo(thursdayTime)>0){
            thursdayTime = thursdayTime.plusWeeks(1);//增加一个星期
        }
        //获取时间差转化成毫秒
        long initialDelay = Duration.between(nowTime, thursdayTime).toMillis();
        long period = 1000*60*60*24*7;
        
        //定时任务
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(1);
        pool.scheduleWithFixedDelay(() -> {
            try {
                log.debug("定时任务");
            } catch (Exception e) {
                log.error("error:{}",e);
            }
        },initialDelay,period, TimeUnit.MILLISECONDS);
    }
}
