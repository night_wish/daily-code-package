package com.lee.juc.juc3;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

@Slf4j
public class ForkJoinTest {
    public static void main(String[] args) {
        ForkJoinPool pool = new ForkJoinPool();
        Integer res = pool.invoke(new AddTask(5));
        log.debug("res:{}",res);

        //拆分
        //AddTask(5)=5+AddTask(4)
        //AddTask(4)=4+AddTask(3)
        //...
        //AddTask(2)=2+AddTask(1)
        //AddTask(1)=1
    }
}

//计算1+2+3~+N
@Slf4j
class AddTask extends RecursiveTask<Integer>{

    private Integer n;

    public AddTask(Integer n) {
        this.n = n;
    }

    @Override
    protected Integer compute() {
        if(n==1){
            return 1;
        }

        AddTask addTask = new AddTask(n - 1);
        addTask.fork();//将addTask任务交给新的线程

        Integer res = n+addTask.join();//获取任务结果
        return res;
    }
}