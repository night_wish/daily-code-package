package com.lee.juc.juc3;

import lombok.extern.slf4j.Slf4j;

import java.util.Timer;
import java.util.TimerTask;

@Slf4j
public class TimerScheduleTask {
    public static void main(String[] args) {
        Timer timer = new Timer();

        TimerTask task1 = new TimerTask() {
            @Override
            public void run() {
                log.debug("任务1");
                int i = 1/0;
            }
        };

        TimerTask task2 = new TimerTask() {
            @Override
            public void run() {
                log.debug("任务2");
            }
        };

        timer.schedule(task1,1000);
        timer.schedule(task2,1000);
    }
}
