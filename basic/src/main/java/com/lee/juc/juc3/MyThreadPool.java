package com.lee.juc.juc3;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 自定义线程池
 */
@Slf4j
public class MyThreadPool {
    public static void main(String[] args) {
        //ThreadPool pool = new ThreadPool(2,1000,TimeUnit.MILLISECONDS,3);
        ThreadPool pool = new ThreadPool(2,1000,TimeUnit.MILLISECONDS,3,(queue,task)->{
            //1）死等
            //queue.put(task);
            //2）带超时的等待
            //queue.add(task,1000,TimeUnit.MILLISECONDS);
            //3）让调用者放弃任务执行
            //log.debug("自动放弃任务,task{}",task);
            //4）让调用者直接抛出异常
            //throw new RuntimeException("直接抛出异常,task:"+task);
            //5）让调用者自己执行任务
            task.run();
        });
        for (int i=0;i<7;i++){
            int j = i;
            pool.execute(()->{
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.debug("任务内容:{}",j);
            });
        }
    }
}

//拒绝策略
@FunctionalInterface
interface RejectPolicy<T>{
    void reject(BlockingQueue<T> queue,T task);
}

//线程池
@Slf4j
class ThreadPool{
    //任务队列
    private BlockingQueue<Runnable> taskQueue;
    //线程集合
    private HashSet<Worker> workers = new HashSet<>();
    //核心线程数
    private int coreSize;
    //任务超时时间
    private long timeout;
    //超时时间单位
    private TimeUnit timeUnit;

    //决绝策略
    private RejectPolicy<Runnable> rejectPolicy;

    public ThreadPool(int coreSize, long timeout, TimeUnit timeUnit, int queueCapacity) {
        this.coreSize = coreSize;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
        taskQueue = new BlockingQueue<>(queueCapacity);
    }

    public ThreadPool(int coreSize, long timeout, TimeUnit timeUnit, int queueCapacity,RejectPolicy<Runnable> rejectPolicy) {
        this.coreSize = coreSize;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
        taskQueue = new BlockingQueue<>(queueCapacity);
        this.rejectPolicy = rejectPolicy;
    }



    //执行任务
    public void execute(Runnable task){
        //当任务数没有超过coreSize时，直接执行
        //当任务数超过coreSize时，存储到任务队列taskQueue
        synchronized (workers){
            if(workers.size()<coreSize){
                Worker woker = new Worker(task);
                log.debug("新增任务，task:{}, worker:{}",task,woker);
                workers.add(woker);
                woker.start();
            }else{
                //死等
                //taskQueue.put(task);
                //决绝策略
                taskQueue.tryPut(rejectPolicy,task);
            }
        }
    }

    class Worker extends Thread{
        private Runnable task;

        public Worker(Runnable task) {
            this.task = task;
        }

        @Override
        public void run() {
            //当任务不为空时，执行任务
            //当任务为空时，从阻塞队列拿出任务执行
//            while (task!=null || (task = taskQueue.take())!=null){
            while (task!=null || (task = taskQueue.poll(timeout,timeUnit))!=null){
                try{
                    log.debug("执行任务,task:{}",task);
                    task.run();
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    task = null;
                }
            }
            synchronized (this){
                log.debug("移除任务,task:{}",this);
                workers.remove(this);
            }
        }
    }
}

//阻塞队列
@Slf4j
class BlockingQueue<T>{

    //任务队列
    private Deque<T> queue = new ArrayDeque<>();
    //锁
    private ReentrantLock lock = new ReentrantLock();
    //生产者条件变量
    private Condition fullWaitSet = lock.newCondition();
    //消费者条件变量
    private Condition emptyWaitSet = lock.newCondition();
    //队列容量
    private int capacity;

    public BlockingQueue(int capacity) {
        this.capacity = capacity;
    }

    //阻塞获取--带超时的
    public T poll(long timeout, TimeUnit unit){
        lock.lock();
        try {
            long nanos = unit.toNanos(timeout);
            while(queue.isEmpty()){
                try {
                    if(nanos<=0){
                        return null;
                    }
                    nanos = emptyWaitSet.awaitNanos(nanos);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            T t = queue.removeFirst();
            fullWaitSet.signal();
            return t;
        }finally {
            lock.unlock();
        }
    }


    //阻塞获取
    public T take(){
        lock.lock();
        try {
            while(queue.isEmpty()){
                try {
                    emptyWaitSet.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            T t = queue.removeFirst();
            fullWaitSet.signal();
            return t;
        }finally {
            lock.unlock();
        }
    }

    //阻塞添加
    public void put(T t){
        lock.lock();
        try {
            while(queue.size()==capacity){
                try {
                    log.debug("等待...新增任务到阻塞队列,task:{}",t);
                    fullWaitSet.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            log.debug("新增任务到阻塞队列,task:{}",t);
            queue.addLast(t);
            emptyWaitSet.signal();
        }finally {
            lock.unlock();
        }
    }

    //带超时时间的阻塞添加
    public boolean add(T t,long timeout,TimeUnit unit){
        lock.lock();
        try {
            long nanos = unit.toNanos(timeout);
            while(queue.size()==capacity){
                try {
                    if(nanos<0){
                        return false;
                    }
                    log.debug("等待...新增任务到阻塞队列,task:{}",t);
                    nanos = fullWaitSet.awaitNanos(nanos);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            log.debug("新增任务到阻塞队列,task:{}",t);
            queue.addLast(t);
            emptyWaitSet.signal();
            return true;
        }finally {
            lock.unlock();
        }
    }

    //阻塞添加--拒绝策略
    public void tryPut(RejectPolicy<T> rejectPolicy, T task) {
        lock.lock();
        try {
            //队列已满
            if(queue.size()==capacity){
                rejectPolicy.reject(this,task);
            }else{
                //队列未满直接新增
                queue.addLast(task);
            }
        }finally {
            lock.unlock();
        }
    }

    //获取容量大小
    public int size(){
        lock.lock();
        try {
            return queue.size();
        }finally {
            lock.unlock();
        }
    }


}
