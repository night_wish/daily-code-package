package com.lee.juc.juc3;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Slf4j
public class ThreadPoolHungry {

    public static void main(String[] args) {
        //两个服务员--又点餐又做菜
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        //服务员1--服务客人1
        executorService.execute(()->{
            log.debug("1号客人点餐：宫保鸡丁");
            Future<String> future = executorService.submit(() -> {
                log.debug("为1号客人做菜");
                return "宫保鸡丁";
            });
            try {
                log.debug("为1号客人上菜:{}",future.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });


        //服务员2--服务客人2
        executorService.execute(()->{
            log.debug("2号客人点餐：糖醋里脊");
            Future<String> future = executorService.submit(() -> {
                log.debug("为2号客人做菜");
                return "糖醋里脊";
            });
            try {
                log.debug("为2号客人上菜:{}",future.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });
    }
}
