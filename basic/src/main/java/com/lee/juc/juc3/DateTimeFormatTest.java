package com.lee.juc.juc3;

import lombok.extern.slf4j.Slf4j;

import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

@Slf4j
public class DateTimeFormatTest {
    public static void main(String[] args) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        for (int i = 0; i <10 ; i++) {
            new Thread(()->{
                TemporalAccessor accessor = dtf.parse("2021-03-09");
                log.debug("accessor : {}",accessor);
            }).start();
        }
    }
}
