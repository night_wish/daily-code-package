package com.lee.juc.juc3;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ScheduledThreadPool {
    public static void main(String[] args) {
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(1);

        pool.schedule(()->{
            log.debug("任务1");
            int i=1/0;
        },1, TimeUnit.SECONDS);

        pool.schedule(()->{
            log.debug("任务2");
        },1, TimeUnit.SECONDS);
    }
}
