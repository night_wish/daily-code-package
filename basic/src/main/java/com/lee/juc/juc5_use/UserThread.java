package com.lee.juc.juc5_use;

import com.lee.base.commons.User;

import java.util.concurrent.Callable;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/7/25 14:32
 */
public class UserThread implements Callable<User> {

    private Integer age;

    public UserThread(Integer age){
        this.age = age;
    }

    @Override
    public User call() throws Exception {
        User user = new User();
        user.setId(1);
        user.setName("lee");
        user.setAge(age);
        return user;
    }
}
