package com.lee.juc.juc5_use;

import com.lee.base.commons.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2022/7/15 16:18
 */
@Slf4j
@RestController
public class JUCController {

    @Autowired
    @Qualifier("imsNationTaskExecutor")
    private TaskExecutor taskExecutor;

    @Autowired
    @Qualifier("imsNationTaskExecutor2")
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;


    private static int clickCounter = 1;
    private static int enterCounter = 1;

    @GetMapping("/juc/test")
    public String test1(){
        log.info("====>clickCounter:{}",clickCounter++);
        taskExecutor.execute(()->{
            log.info("=============>进来了：{}",Thread.currentThread().getName());
            try {
                log.info("====>enterCounter : {}",enterCounter++);
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("=============>出去了：{}",Thread.currentThread().getName());
        });
        return "测试...............";
    }

    @GetMapping("/juc/test2")
    public String test2() throws ExecutionException, InterruptedException {
        Future<?> submit = threadPoolTaskExecutor.submit(new UserThread(18));
        User user = (User) submit.get();
        return user.toString();
    }

}
