package com.lee.hutool;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeUtil;
import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lee
 * @version 1.0
 * @description 糊涂工具测试
 * @date 2023/6/26 9:27
 */
@Slf4j
public class HutoolUtils {

    /**
     * TreeUtil 构造树
     */
    @Test
    public void buildDeptTreeTest(){
        List<Dept> deptList = new ArrayList<>();
        deptList.add(new Dept("1","部门1",null,10,"2000"));
        deptList.add(new Dept("2","部门2",null,20,"2001"));
        deptList.add(new Dept("3","部门1-1","1",5,"2010"));
        deptList.add(new Dept("4","部门1-2","1",7,"2011"));
        deptList.add(new Dept("5","部门2-1","2",19,"2010"));
        deptList.add(new Dept("6","部门2-1-1","5",90,"2014"));

        /*TreeNodeConfig config = new TreeNodeConfig();
        config.setIdKey("id");
        config.setNameKey("deptName");
        config.setParentIdKey("prtnId");*/
        List<Tree<String>> deptTreeList = TreeUtil.build(deptList, null, null, (obj, tree) -> {
            tree.setId(obj.getId());
            tree.setName(obj.getDeptName());
            tree.setParentId(obj.getPrtnId());
            tree.putExtra("deptNumber", obj.getDeptNumber());
            tree.putExtra("buildYear", obj.getBuildYear());
        });
        log.info(JSON.toJSONString(deptTreeList));
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    class Dept{
        private String id;
        private String deptName;
        private String prtnId;
        private Integer deptNumber;
        private String buildYear;
    }
}
