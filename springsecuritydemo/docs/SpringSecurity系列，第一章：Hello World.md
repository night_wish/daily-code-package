# SpringSecurity系列，第一章：Hello World

之前项目一直在用shiro,对SpringSecurity的了解比较少。正好这段时间项目中用到了SpringSecurity和CAS。做此整理：



**下面实例中使用springboot2.3.4最新版本，其内置的SpringSecurity版本为5.3.4**

## 一、依赖导入

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>


<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>

<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>

<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.3</version>
</dependency>


<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
</dependency>
```

## 二、创建数据库

一般权限控制分为三层：用户 : 角色 : 权限。用户和角色为多对多，角色和权限也是多对多的关系。

我们暂时先只 考虑  用户  和 角色：

```sql
/**用户表**/
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(45) NOT NULL COMMENT '用户名',
  `password` varchar(45) NOT NULL COMMENT '用户密码',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

/**角色表**/
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(45) NOT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

/**用户和角色关联表**/
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和角色关联表';
```

插入数据：

```sql
INSERT INTO `sys_user` VALUES (1,'admin','123'),(2,'test','456');

INSERT INTO `sys_role` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');

INSERT INTO `sys_user_role` VALUES (1,1,1),(2,2,2);
```

**注意：这里的ROLE_ADMIN和ROLR_USER是springsecurity规定的，暂时不要乱改**

## 三、准备工作

### 3.1、前端页面

在resource/static目录下创建：登录页login.html和登录成功也home.html

login.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录页面</title>
</head>
<body>
    
    <h1>登陆</h1>
    <form method="post" action="/login">
        <div>
            用户名：<input type="text" name="username">
        </div>
        <div>
            密码：<input type="password" name="password">
        </div>
        <div>
            <button type="submit">登陆</button>
            <button type="reset">重置</button>
        </div>
    </form>
    
</body>
</html>
```

**注意：Spring Security登录认证的默认请求路径为 /login，用户名默认字段为username，密码默认字段为password**

home.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>首页</title>
</head>
<body>

    <h1>登陆成功</h1>
    <a href="/admin">检测ROLE_ADMIN角色</a>
    <a href="/user">检测ROLE_USER角色</a>
    <button onclick="window.location.href='/logout'">退出登录</button>

</body>
</html>
```

### 3.2、配置文件

```yml
server:
  port: 8081

##链接数据库
spring:
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/springsecuritydemo?useUnicode=true&characterEncoding=utf-8&useSSL=true
    username: root
    password: admin123


##下划线转驼峰
##mapper文件位置
##别名
mybatis:
  configuration:
    map-underscore-to-camel-case: true
  mapper-locations: classpath:mapper/*xml
  type-aliases-package: com.lee.entity


##打印日志
logging:
  level:
    com.lee: debug
    org.springframework: warn
```

### 3.3、实体类

```java
/**
 * 用户
 */
@Data
public class SysUser implements Serializable {
    private static final long serialVersionUID = -4875468867515766700L;

    private Integer id;

    private String name;

    private String password;
}

/**
 * 角色
 */
@Data
public class SysRole implements Serializable {
    private static final long serialVersionUID = 4010892493931066572L;

    private Integer id;

    private String name;
}


/**
 * 用户和角色关联
 */
@Data
public class SysUserRole  implements Serializable {
    private static final long serialVersionUID = 8397057707441550264L;

    private Integer id;

    private Integer userId;

    private Integer roleId;
}
```

### 3.4、Mapper.java

```java
public interface SysUserMapper {

    SysUser selectUserById(Integer id);

    SysUser selectUserByName(String name);
}


public interface SysRoleMapper {

    SysRole selectRoleById(Integer id);
}

public interface SysUserRoleMapper {

    List<SysUserRole> selectRoleByUserId(Integer userId);
}
```



### 3.5、Mapper.xml

```XML
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.lee.mapper.SysUserMapper" >


    <select id="selectUserById" parameterType="java.lang.Integer" resultType="com.lee.entity.SysUser">
        select * from sys_user where id = #{id}
    </select>

    <select id="selectUserByName" parameterType="java.lang.String" resultType="com.lee.entity.SysUser">
        select * from sys_user where name = #{name}
    </select>

</mapper>



<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.lee.mapper.SysRoleMapper" >

    <select id="selectRoleById" parameterType="java.lang.Integer" resultType="com.lee.entity.SysRole">
        select * from sys_role where id = #{id}
    </select>

</mapper>



<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.lee.mapper.SysUserRoleMapper" >

    <select id="selectRoleByUserId" parameterType="java.lang.Integer" resultType="com.lee.entity.SysUserRole">
        select * from sys_user_role where user_id = #{userId}
    </select>

</mapper>
```

### 3.6、service

```java
public interface SysUserService {

    SysUser selectUserById(Integer id);

    SysUser selectUserByName(String name);
}

@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;


    @Override
    public SysUser selectUserById(Integer id) {
        return sysUserMapper.selectUserById(id);
    }

    @Override
    public SysUser selectUserByName(String name) {
        return sysUserMapper.selectUserByName(name);
    }
}





public interface SysRoleService {

    SysRole selectRoleById(Integer id);
}

@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public SysRole selectRoleById(Integer id) {
        return sysRoleMapper.selectRoleById(id);
    }
}




public interface SysUserRoleService {

    List<SysUserRole> selectRoleByUserId(Integer userId);
}


@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public List<SysUserRole> selectRoleByUserId(Integer userId) {
        return sysUserRoleMapper.selectRoleByUserId(userId);
    }
}
```

### 3.7、Controller

```java
@Slf4j
@Controller
public class LoginController {

    @PostMapping("/login")
    public String showLogin() {
        return "login.html";
    }

    @GetMapping("/")
    public String showHome() {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        log.info("当前登陆用户：" + name);
        return "home.html";
    }


    @ResponseBody
    @GetMapping("/admin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String printAdmin() {
        return "如果你看见这句话，说明你有ROLE_ADMIN角色";
    }

    @ResponseBody
    @GetMapping("/user")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String printUser() {
        return "如果你看见这句话，说明你有ROLE_USER角色";
    }

}
```

### 3.8、启动类

```java
@SpringBootApplication
@MapperScan("com.lee.mapper")//扫描mapper文件
public class SpringsecuritydemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringsecuritydemoApplication.class, args);
    }

}
```

## 四、配置SpringSecurity

### 4.1、自定义UserDetailsService

我们需要自定义UserDetailsService将用户的**信息**和**权限**注入进来。

```java
@Slf4j
@Service
public class CustomUserdetailService implements UserDetailsService {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //加载用户信息
        SysUser sysUser = sysUserService.selectUserByName(username);
        if(sysUser==null){
            log.info("用户名：{} 不存在",username);
            throw new UsernameNotFoundException("用户名不存在!");
        }

        //加载用户权限
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        List<SysUserRole> sysUserRoles =  sysUserRoleService.selectRoleByUserId(sysUser.getId());
        if(sysUserRoles!=null && sysUserRoles.size()>0){
            for(SysUserRole userRole : sysUserRoles){
                SysRole sysRole = sysRoleService.selectRoleById(userRole.getRoleId());
                authorities.add(new SimpleGrantedAuthority(sysRole.getName()));
            }
        }

        //返回userDetails的实现类：里边包含了用户信息和权限信息
        return new User(username,sysUser.getPassword(),authorities);
    }
}
```

### 4.2、WebSecurityConfig配置类

WebSecurityConfig是Spring Security的配置类。

其包含了三个注解：

```xml
@Configuration  该类是个注解类
@EnableWebSecurity 开启Security服务
```

**1）、首先将我们自定义的CustomUserDetailsService注入进去**

**2）、指明加密方式**

**3）、指明认证或过滤规则**

```java
@Configuration
@EnableWebSecurity//开启security服务
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private CustomUserdetailService customUserdetailService;

    //指明userDetailsService，自定义加密方法（不加密）
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserdetailService).passwordEncoder(new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return rawPassword.toString();
            }
            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return encodedPassword.equals(rawPassword.toString());
            }
        });
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().authenticated()
                .and()

                .formLogin()
                    .loginPage("/login.html") //用户未登录时，访问资源都跳转到该页面，登录页面
                    .loginProcessingUrl("/login")//登录表单，form中的action地址，也就是处理认证请求的路径
                    .usernameParameter("username")//登录表单，form中用户名input框的name名，默认是username
                    .passwordParameter("password")//登录表单，form中密码input框的password名，默认是password
                    .defaultSuccessUrl("/")//登录成功后，默认跳转的路径
                    .permitAll()

                .and()
                .logout().permitAll();

        //关闭csrf跨域
        http.csrf().disable();
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        //对静态文件放行
        web.ignoring().mvcMatchers("/css/**","/imgs/**","/js/**");
    }
}
```

## 五、测试

```xml
浏览器访问：http://127.0.0.1:8081/

admin用户登录 密码123
test用户登录 密码456

test用户点击：检测ROLE_ADMIN角色 检测ROLE_USER角色

发现我们都能进去
@PreAuthorize("hasRole('XXX')") 注解不起作用了
```

![](imgs\springsecurity\1.png)



![](imgs\springsecurity\2.png)

![](imgs\springsecurity\3.png)

![](imgs\springsecurity\4.png)



**解决方案：**

```XML
在我们的配置类中WebSecurityConfig注解上添加：

@EnableGlobalMethodSecurity(prePostEnabled = true)

Spring Security默认是禁用注解的，要想开启注解，需要在继承WebSecurityConfigurerAdapter的类上加@EnableGlobalMethodSecurity注解。
```

![](imgs\springsecurity\5.png)