# SpringSecurity系列，第三章：权限控制

前面写到一般的权限都是通过用户：角色：权限三层划分的，用户和角色为多对多关系，角色和权限也是多对多的关系。

之前通过在方法上增加@PreAuthorize("hasRole('XXX')")注解，依据用户是否是某个角色来控制用户能否访问该方法。

下面我会记录下通过hasPermission，用户是否拥有某个权限来更细粒度的控制用户对方法的访问权限。

## 一、创建数据库

```sql
/**权限表**/
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '权限ID',
  `name` varchar(45) DEFAULT NULL COMMENT '权限名称',
  `perms` varchar(45) DEFAULT NULL COMMENT '权限标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限表';


/**用户和权限关联表**/
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `permission_id` int(11) NOT NULL COMMENT '权限ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色和权限关联表';


/**添加数据**/
INSERT INTO `sys_permission` VALUES (1,'用户新增','sys:user:add'),(2,'用户修改','sys:user:edit'),('3', '角色添加', 'sys:role:add'
),('4', '角色修改', 'sys:role:edit');
INSERT INTO `sys_role_permission` VALUES (1,1,1),(2,1,2),(3,2,3),(4,2,4);
```

## 二、Entity

```java
@Data
public class SysPermission implements Serializable {
    private static final long serialVersionUID = -2255788597545068005L;

    private Integer id;

    private String name;

    private String perms;
}



@Data
public class SysRolePermission implements Serializable {
    private static final long serialVersionUID = 5210775380322066314L;

    private Integer id;

    private Integer roleId;

    private Integer permissionId;

}
```

## 三、Mapper.java

```java
public interface SysPermissionMapper {

    SysPermission selectPermissionById(Integer id);
}


public interface SysRolePermissionMapper {

    List<SysPermission> selectPermissionByRoleId(Integer roleId);

}
```

## 四、Mapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.lee.mapper.SysPermissionMapper" >


    <select id="selectPermissionById" parameterType="java.lang.Integer" resultType="com.lee.entity.SysPermission">
        select * from sys_permission where id = #{id}
    </select>

</mapper>



<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.lee.mapper.SysRolePermissionMapper" >

    <select id="selectPermissionByRoleId" parameterType="java.lang.Integer" resultType="com.lee.entity.SysPermission">
        select p.*
        from sys_role_permission rp
        left join sys_permission p on p.id = rp.permission_id
        where rp.role_id = #{roleId}
    </select>

</mapper>
```

## 五、service

```java
public interface SysPermissionService {

    SysPermission selectPermissionById(Integer id);
}

@Service
public class SysPermissionServiceImpl implements SysPermissionService {

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Override
    public SysPermission selectPermissionById(Integer id) {
        return sysPermissionMapper.selectPermissionById(id);
    }
}




public interface SysRolePermissionService {

    List<SysPermission> selectPermissionByRoleId(Integer roleId);
}


@Service
public class SysRolePermissionServiceImpl implements SysRolePermissionService {

    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;


    @Override
    public List<SysPermission> selectPermissionByRoleId(Integer roleId) {
        return sysRolePermissionMapper.selectPermissionByRoleId(roleId);
    }
}

```

## 六、Controller

```java
@ResponseBody
@GetMapping("/user/add")
@PreAuthorize("hasPermission('/user/add','sys:user:add')")
public String printAdminR() {
    return "如果你看见这句话，说明你访问/user/add路径具有sys:user:add权限";
}

@ResponseBody
@GetMapping("/role/edit")
@PreAuthorize("hasPermission('/role/edit','sys:role:edit')")
public String printAdminC() {
    return "如果你看见这句话，说明你访问/role/edit路径具有sys:role:edit权限";
}
```

## 七、修改CustomUserdetailService

```java
@Slf4j
@Service
public class CustomUserdetailService implements UserDetailsService {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysRolePermissionService sysRolePermissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //加载用户信息
        SysUser sysUser = sysUserService.selectUserByName(username);
        if(sysUser==null){
            log.info("用户名：{} 不存在",username);
            throw new UsernameNotFoundException("用户名不存在!");
        }

        //加载用户角色
//        Collection<GrantedAuthority> authorities = new ArrayList<>();
//        List<SysUserRole> sysUserRoles =  sysUserRoleService.selectRoleByUserId(sysUser.getId());
//        if(sysUserRoles!=null && sysUserRoles.size()>0){
//            for(SysUserRole userRole : sysUserRoles){
//                SysRole sysRole = sysRoleService.selectRoleById(userRole.getRoleId());
//                authorities.add(new SimpleGrantedAuthority(sysRole.getName()));
//            }
//        }


        //加载用户权限信息
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        List<SysUserRole> sysUserRoles = sysUserRoleService.selectRoleByUserId(sysUser.getId());
        if(sysUserRoles!=null && sysUserRoles.size()>0){
            for (SysUserRole userRole : sysUserRoles){
                List<SysPermission> sysPermissions = sysRolePermissionService.selectPermissionByRoleId(userRole.getRoleId());
                if(sysPermissions!=null && sysPermissions.size()>0){
                    for(SysPermission p : sysPermissions){
                        authorities.add(new SimpleGrantedAuthority(p.getPerms()));
                    }
                }
            }
        }


        //返回userDetails的实现类：里边包含了用户信息和权限信息
        return new User(username,sysUser.getPassword(),authorities);
    }
}
```

## 八、自定义PermissionEvaluator

我们需要自定义对hasPermission的处理，只要实现PermissionEvaluator接口即可

```java
@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        
        //获取当前用户
        User user = (User) authentication.getPrincipal();
        
        //获取当前用户权限
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        if(authorities!=null && authorities.size()>0){
            for (GrantedAuthority a : authorities){
                if(permission.equals(a.getAuthority())){
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        return false;
    }

}
```

## 九、加入WebSecurityConfig

将定义的CustomPermissionEvaluator加入WebSecurityConfig:

```java
@Bean
public DefaultWebSecurityExpressionHandler defaultWebSecurityExpressionHandler(){
    DefaultWebSecurityExpressionHandler handler = new DefaultWebSecurityExpressionHandler();
    handler.setPermissionEvaluator(new CustomPermissionEvaluator());
    return handler;
}
```

## 十、页面新增

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>首页</title>
</head>
<body>

    <h1>登陆成功</h1>
    <a href="/admin">检测ROLE_ADMIN角色</a><br/>
    <a href="/user">检测ROLE_USER角色</a><br/>
    <a href="/user/add">添加用户</a><br/>
    <a href="/role/edit">角色编辑</a><br/>
    <button onclick="window.location.href='/logout'">退出登录</button>

</body>
</html>
```

## 十一、测试

```xml
浏览器访问：http://127.0.0.1:8081/login.html
用admin登录
访问：添加用户  和  角色编辑
```

![](imgs\springsecurity\16.png)



![](imgs\springsecurity\17.png)



