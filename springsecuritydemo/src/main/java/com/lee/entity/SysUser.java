package com.lee.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户
 */
@Data
public class SysUser implements Serializable {
    private static final long serialVersionUID = -4875468867515766700L;

    private Integer id;

    private String name;

    private String password;
}