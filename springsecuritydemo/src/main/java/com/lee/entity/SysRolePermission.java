package com.lee.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysRolePermission implements Serializable {
    private static final long serialVersionUID = 5210775380322066314L;

    private Integer id;

    private Integer roleId;

    private Integer permissionId;

}