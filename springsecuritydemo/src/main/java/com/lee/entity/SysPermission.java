package com.lee.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysPermission implements Serializable {
    private static final long serialVersionUID = -2255788597545068005L;

    private Integer id;

    private String name;

    private String perms;
}