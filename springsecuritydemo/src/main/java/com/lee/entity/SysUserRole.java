package com.lee.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户和角色关联
 */
@Data
public class SysUserRole  implements Serializable {
    private static final long serialVersionUID = 8397057707441550264L;

    private Integer id;

    private Integer userId;

    private Integer roleId;
}