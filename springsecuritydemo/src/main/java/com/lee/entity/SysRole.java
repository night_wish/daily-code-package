package com.lee.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 角色
 */
@Data
public class SysRole implements Serializable {
    private static final long serialVersionUID = 4010892493931066572L;

    private Integer id;

    private String name;
}