package com.lee.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity//开启security服务
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private CustomUserdetailService customUserdetailService;

    @Autowired
    private DataSource dataSource;

    @Bean
    public DefaultWebSecurityExpressionHandler defaultWebSecurityExpressionHandler(){
        DefaultWebSecurityExpressionHandler handler = new DefaultWebSecurityExpressionHandler();
        handler.setPermissionEvaluator(new CustomPermissionEvaluator());
        return handler;
    }


    //RememberMe 用户信息存储进数据库
    @Bean
    public PersistentTokenRepository persistentTokenRepository(){
        JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
        tokenRepository.setDataSource(dataSource);

        return tokenRepository;
    }

    //指明userDetailsService，自定义加密方法（不加密）
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserdetailService).passwordEncoder(new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return rawPassword.toString();
            }
            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return encodedPassword.equals(rawPassword.toString());
            }
        });
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().authenticated()
                .and()

                .formLogin()
                    .loginPage("/login.html") //用户未登录时，访问资源都跳转到该页面，登录页面
                    .loginProcessingUrl("/login")//登录表单，form中的action地址，也就是处理认证请求的路径
                    .usernameParameter("username")//登录表单，form中用户名input框的name名，默认是username
                    .passwordParameter("password")//登录表单，form中密码input框的password名，默认是password
                    .defaultSuccessUrl("/")//登录成功后，默认跳转的路径
                    .failureUrl("/login/error")//登录失败URL
                    .permitAll()

                .and()
                .logout().permitAll()

                .and()
                .rememberMe()//记住账号和密码
                .tokenRepository(persistentTokenRepository());//tokenRepositoryBean 去数据库中查找RememberMe信息

        //关闭csrf跨域
        http.csrf().disable();

    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        //对静态文件放行
        web.ignoring().mvcMatchers("/css/**","/imgs/**","/js/**");
    }
}
