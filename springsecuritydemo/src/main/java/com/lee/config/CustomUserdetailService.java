package com.lee.config;

import com.lee.entity.SysPermission;
import com.lee.entity.SysUser;
import com.lee.entity.SysUserRole;
import com.lee.service.SysRolePermissionService;
import com.lee.service.SysRoleService;
import com.lee.service.SysUserRoleService;
import com.lee.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@Service
public class CustomUserdetailService implements UserDetailsService {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysRolePermissionService sysRolePermissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //加载用户信息
        SysUser sysUser = sysUserService.selectUserByName(username);
        if(sysUser==null){
            log.info("用户名：{} 不存在",username);
            throw new UsernameNotFoundException("用户名不存在!");
        }

        //加载用户角色
//        Collection<GrantedAuthority> authorities = new ArrayList<>();
//        List<SysUserRole> sysUserRoles =  sysUserRoleService.selectRoleByUserId(sysUser.getId());
//        if(sysUserRoles!=null && sysUserRoles.size()>0){
//            for(SysUserRole userRole : sysUserRoles){
//                SysRole sysRole = sysRoleService.selectRoleById(userRole.getRoleId());
//                authorities.add(new SimpleGrantedAuthority(sysRole.getName()));
//            }
//        }


        //加载用户权限信息
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        List<SysUserRole> sysUserRoles = sysUserRoleService.selectRoleByUserId(sysUser.getId());
        if(sysUserRoles!=null && sysUserRoles.size()>0){
            for (SysUserRole userRole : sysUserRoles){
                List<SysPermission> sysPermissions = sysRolePermissionService.selectPermissionByRoleId(userRole.getRoleId());
                if(sysPermissions!=null && sysPermissions.size()>0){
                    for(SysPermission p : sysPermissions){
                        authorities.add(new SimpleGrantedAuthority(p.getPerms()));
                    }
                }
            }
        }


        //返回userDetails的实现类：里边包含了用户信息和权限信息
        return new User(username,sysUser.getPassword(),authorities);
    }
}
