package com.lee.config;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Collection;

@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        
        //获取当前用户
        User user = (User) authentication.getPrincipal();
        
        //获取当前用户权限
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        if(authorities!=null && authorities.size()>0){
            for (GrantedAuthority a : authorities){
                if(permission.equals(a.getAuthority())){
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        return false;
    }

}
