package com.lee.service.impl;

import com.lee.entity.SysUserRole;
import com.lee.mapper.SysUserRoleMapper;
import com.lee.service.SysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public List<SysUserRole> selectRoleByUserId(Integer userId) {
        return sysUserRoleMapper.selectRoleByUserId(userId);
    }
}
