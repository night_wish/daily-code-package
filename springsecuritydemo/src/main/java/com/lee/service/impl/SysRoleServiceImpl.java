package com.lee.service.impl;

import com.lee.entity.SysRole;
import com.lee.mapper.SysRoleMapper;
import com.lee.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public SysRole selectRoleById(Integer id) {
        return sysRoleMapper.selectRoleById(id);
    }
}
