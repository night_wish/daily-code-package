package com.lee.service.impl;

import com.lee.entity.SysPermission;
import com.lee.mapper.SysPermissionMapper;
import com.lee.service.SysPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysPermissionServiceImpl implements SysPermissionService {

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Override
    public SysPermission selectPermissionById(Integer id) {
        return sysPermissionMapper.selectPermissionById(id);
    }
}
