package com.lee.service.impl;

import com.lee.entity.SysPermission;
import com.lee.mapper.SysRolePermissionMapper;
import com.lee.service.SysRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysRolePermissionServiceImpl implements SysRolePermissionService {

    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;


    @Override
    public List<SysPermission> selectPermissionByRoleId(Integer roleId) {
        return sysRolePermissionMapper.selectPermissionByRoleId(roleId);
    }
}
