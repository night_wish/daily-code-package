package com.lee.service.impl;

import com.lee.entity.SysUser;
import com.lee.mapper.SysUserMapper;
import com.lee.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;


    @Override
    public SysUser selectUserById(Integer id) {
        return sysUserMapper.selectUserById(id);
    }

    @Override
    public SysUser selectUserByName(String name) {
        return sysUserMapper.selectUserByName(name);
    }
}
