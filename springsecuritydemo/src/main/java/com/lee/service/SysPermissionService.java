package com.lee.service;

import com.lee.entity.SysPermission;

public interface SysPermissionService {

    SysPermission selectPermissionById(Integer id);
}
