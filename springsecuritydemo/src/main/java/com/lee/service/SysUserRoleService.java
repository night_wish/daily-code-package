package com.lee.service;

import com.lee.entity.SysUserRole;

import java.util.List;

public interface SysUserRoleService {

    List<SysUserRole> selectRoleByUserId(Integer userId);
}
