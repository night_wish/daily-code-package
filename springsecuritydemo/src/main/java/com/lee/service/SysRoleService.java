package com.lee.service;

import com.lee.entity.SysRole;

public interface SysRoleService {

    SysRole selectRoleById(Integer id);
}
