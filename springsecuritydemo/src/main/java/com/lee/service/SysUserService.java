package com.lee.service;

import com.lee.entity.SysUser;

public interface SysUserService {

    SysUser selectUserById(Integer id);

    SysUser selectUserByName(String name);
}
