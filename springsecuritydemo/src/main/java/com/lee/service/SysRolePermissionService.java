package com.lee.service;

import com.lee.entity.SysPermission;

import java.util.List;

public interface SysRolePermissionService {

    List<SysPermission> selectPermissionByRoleId(Integer roleId);
}
