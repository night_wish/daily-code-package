package com.lee.mapper;


import com.lee.entity.SysPermission;

public interface SysPermissionMapper {

    SysPermission selectPermissionById(Integer id);
}