package com.lee.mapper;

import com.lee.entity.SysUser;

public interface SysUserMapper {

    SysUser selectUserById(Integer id);

    SysUser selectUserByName(String name);

}