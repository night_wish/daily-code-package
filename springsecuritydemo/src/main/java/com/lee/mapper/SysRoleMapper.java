package com.lee.mapper;

import com.lee.entity.SysRole;

public interface SysRoleMapper {

    SysRole selectRoleById(Integer id);
}