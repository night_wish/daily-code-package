package com.lee.mapper;

import com.lee.entity.SysUserRole;

import java.util.List;

public interface SysUserRoleMapper {

    List<SysUserRole> selectRoleByUserId(Integer userId);

}