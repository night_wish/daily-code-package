package com.lee.mapper;


import com.lee.entity.SysPermission;

import java.util.List;

public interface SysRolePermissionMapper {

    List<SysPermission> selectPermissionByRoleId(Integer roleId);

}