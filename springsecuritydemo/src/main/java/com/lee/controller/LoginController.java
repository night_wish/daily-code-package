package com.lee.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
public class LoginController {

    @PostMapping("/login")
    public String showLogin() {
        return "login.html";
    }

    @GetMapping("/")
    public String showHome() {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        log.info("当前登陆用户：" + name);
        return "home.html";
    }


    @ResponseBody
    @GetMapping("/admin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String printAdmin() {
        return "如果你看见这句话，说明你有ROLE_ADMIN角色";
    }

    @ResponseBody
    @GetMapping("/user")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String printUser() {
        return "如果你看见这句话，说明你有ROLE_USER角色";
    }


    @ResponseBody
    @GetMapping("/login/error")
    public String loginError(HttpServletRequest req, HttpServletResponse resp){
        AuthenticationException excp = (AuthenticationException) req.getSession().getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
        return "异常信息为："+excp.getMessage();
    }

    @ResponseBody
    @GetMapping("/user/add")
    @PreAuthorize("hasPermission('/user/add','sys:user:add')")
    public String printAdminR() {
        return "如果你看见这句话，说明你访问/user/add路径具有sys:user:add权限";
    }

    @ResponseBody
    @GetMapping("/role/edit")
    @PreAuthorize("hasPermission('/role/edit','sys:role:edit')")
    public String printAdminC() {
        return "如果你看见这句话，说明你访问/role/edit路径具有sys:role:edit权限";
    }

}
