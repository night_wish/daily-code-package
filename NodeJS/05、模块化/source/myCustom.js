//外部文件导入此模块的时候自动执行
console.log("这是一个自定义模块");

//通过在module.export对象上挂载属性对外暴露属性
module.exports.username = 'lee';

//通过在module.export对象上挂载方法对外暴露方法
module.exports.sayHi = function () {
  console.log("hi.");
}

let age = 18;
module.exports.myAge = age;


//如果module.exports指向一个对象，那个这个对象会把上面挂载的内容冲掉
// module.exports = {
//   nickName: "ren",
//   sayHello: function () {
//     console.log(this.sayHello);
//   }
// }

console.log(module);