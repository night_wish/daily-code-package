const path = require('path');
const fs = require('fs');

const pathStr = path.join("/a", "/b/c", "../d", "/e");
console.log(pathStr); //输出结果为 /a/b/d/e


//读取文件
fs.readFile(path.join(__dirname, "/..", "/01、初识NodeJS", "/hello.js"), 'utf8', function (err, fileContent) {
  //1、文件读取成功err为null 
  //2、文件读取失败err为错误对象，fileContent为undefined
  if (err != null) {
    console.log("文件读取失败" + err.message);
  } else {
    console.log("文件内容为" + fileContent);
  }
});
