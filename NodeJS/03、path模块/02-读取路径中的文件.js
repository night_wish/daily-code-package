const path = require('path');

let pathStr = "/a/b/c/hello.txt";
const fileName1 = path.basename(pathStr);
console.log(fileName1); //输出hello.txt

const fileName2 = path.basename(pathStr, ".txt");
console.log(fileName2); //输出hello

let extName = path.extname(pathStr);
console.log(extName); //输出.txt
