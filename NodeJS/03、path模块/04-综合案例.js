const fs = require("fs");
const path = require("path");

//正则表达式 最外层前后//表示这是个正则表达式 /s表示空字符 /S表示非空字符 *表示1次或多次
const regStyle = /<style>[\s\S]*<\/style>/;
const regJs = /<script>[\s\S]*<\/script>/;

fs.readFile(path.join(__dirname, "./source/素材.html"), "utf8", function (err, content) {
  if (err) {
    console.log("读取文件失败" + err.message);
    return;
  }
  //处理style
  dealStyle(content);
  //处理script
  dealScript(content);
  //处理html
  dealHtml(content);
});

//处理style
function dealStyle(htmlContent) {
  let styleContent = regStyle.exec(htmlContent);
  styleContent = styleContent[0].replace("<style>", "").replace("</style>", "");
  fs.writeFile(path.join(__dirname, "./source/index.css"), styleContent, "utf8", function (err) {
    if (err) {
      console.log("写入css内容出错" + err.message);
      return;
    }
    console.log("写入css内容成功.");
  });
};

//处理script
function dealScript(htmlContent) {
  let scriptContent = regJs.exec(htmlContent);
  scriptContent = scriptContent[0].replace("<script>", "").replace("</script>", "");
  fs.writeFile(path.join(__dirname, "./source/index.js"), scriptContent, "utf8", function (err) {
    if (err) {
      console.log("写入js内容出错" + err.message);
      return;
    }
    console.log("写入js内容成功.");
  });
}

//处理html
function dealHtml(htmlContent) {
  //替换style//替换js
  htmlContent = htmlContent.replace(regStyle, '<link rel="stylesheet" href="./index.css">').replace(regJs, '<script src="./index.js"></script>');
  fs.writeFile(path.join(__dirname, "./source/index.html"), htmlContent, "utf8", function (err) {
    if (err) {
      console.log("写入html内容出错" + err.message);
      return;
    }
    console.log("写入html成功");
  });
}