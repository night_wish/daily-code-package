function dateFormat(dateStr) {
  let dt = new Date(dateStr);

  let y = padZero(dt.getFullYear());
  let m = padZero(dt.getMonth());
  let d = padZero(dt.getDay());

  let h = padZero(dt.getHours());
  let mi = padZero(dt.getMinutes());
  let s = padZero(dt.getSeconds());

  return `${y}-${m}-${d} ${h}-${mi}-${s}`;
}

function padZero(num) {
  return num > 9 ? num : '0' + num;
}

//对外暴露方法
module.exports = {
  dateFormat
};