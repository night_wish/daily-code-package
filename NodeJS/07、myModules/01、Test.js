const express = require("express");

const app = express();



//中间件处理请求参数
const myBodyPaser = require("./my-custom-body-parser");
app.use(myBodyPaser);


//监听user请求
app.post("/user", (req, res) => {
  res.send(req.body);
});


app.listen("80", () => {
  console.log("this server is running on http://127.0.0.1:80");
});