const qs = require("querystring");

const myBodyPaser = (req, res, next) => {
  //1、req如果请求数据量比较大，无法一次性发送完毕，则客户端会把数据切割后，分批发送到服务器。
  //需要监听req 对象的 data 事件
  let reqStr = "";
  req.on("data", (chunk) => {
    reqStr += chunk;
  });

  //2、请求数据接收完后会自动触发end事件，我们就在end事件里处理入参
  req.on("end", () => {
    console.log(reqStr);
    let myBody = qs.parse(reqStr);
    console.log(myBody);

    //3、将myBody挂载到req上
    req.body = myBody;
    next();
  });

}

//将模块暴露出去
module.exports = myBodyPaser;