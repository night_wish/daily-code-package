//导入fs模块
const fs = require('fs');
console.log(__dirname);

//读取文件
fs.readFile("../01、初识NodeJS/hello.js", 'utf8', function (err, fileContent) {
  //1、文件读取成功err为null 
  //2、文件读取失败err为错误对象，fileContent为undefined
  if (err != null) {
    console.log("文件读取失败" + err.message);
  } else {
    console.log("文件内容为" + fileContent);
  }
});
