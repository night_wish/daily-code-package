const fs = require('fs');

fs.writeFile("../01、初识NodeJS2/hellotoo.js", "这就是我想写入的内容", "utf8", function (err) {

  //路径不会自动生成
  //文件会自动生成
  //写入成功err为null,写入失败err为失败对象
  if (err != null) {
    console.log("写入文件错误:" + err.message);
  } else {
    console.log("写入文件成功");
  }
});