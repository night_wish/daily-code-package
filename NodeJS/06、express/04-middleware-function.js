const express = require("express");

const app = express();

//全局中间件，给req添加一个时间参数
app.use((req, res, next) => {
  req.startTime = Date.now();
  next();
});

app.get("/user", (req, res) => {
  res.send(`this query time is ${req.startTime}`);
})



app.listen(80, () => {
  console.log("this express server running at http://localhost:80");
})