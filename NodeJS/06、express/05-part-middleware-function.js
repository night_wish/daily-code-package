const express = require("express");

const app = express();



//定义一个局部生效的中间件
const pmw1 = function (req, res, next) {
  console.log("this is my first part middleware function");
  next();
}

const pmw2 = function (req, res, next) {
  console.log("this is my second part middleware function");
  next();
}

//全局中间件要使用app.use(pmw);  局部局部中间件不需要

app.get("/a", pmw1, pmw2, (req, res) => {
  res.send("hehe");
})

app.get("/b", (req, res) => {
  res.send("heihei");
})


app.listen(80, () => {
  console.log("this express server running at http://localhost:80");
})