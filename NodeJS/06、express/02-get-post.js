//引入express
const express = require("express");

//创建app
const app = express();

//监听get请求
app.get("/user/list", (req, res) => {
  res.send(req.query);
});

app.get("/user/:id/:name", (req, res) => {
  res.send(req.params);
});

//监听post请求
// app.use(express.json());
// app.post("/user/add", (req, res) => {
//   let id = req.body.id;
//   let name = req.body.name;
//   let age = req.body.age;
//   //res.send(`this post params is id=${id},name=${age},age=${age}`);
//   res.send(req.body);
// });

app.use(express.urlencoded({ extended: true }));
app.post("/animal/add", (req, res) => {
  res.send(req.body);
})

//监听80端口
app.listen(80, () => {
  console.log("the express server running at http://localhost:80");
})