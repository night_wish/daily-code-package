const express = require("express");

const app = express();

// const mw = function (req, res, next) {
//   console.log("this is a simple middleware");
//   next();
// }

// //使中间件全局生效
// app.use(mw);

app.use(function (req, res, next) {
  console.log("this is my first middleware");
  next();
});

app.get("/user/list", function (req, res) {
  console.log("this is a get query");
  res.send("user list page");
});

app.listen(80, () => {
  console.log("the express server running at http://localhost:80");
})