const http = require('http');

//创建一个服务器
const server = http.createServer();

//绑定request事件
server.on('request', (req, res) => {
  let url = req.url;
  let method = req.method;

  //想要使用${}的时候 前后必须用``包住，不能使用""
  let resStr = `我返回的内容：URL请求地址:${url},请求方式:${method}`;
  res.setHeader("Content-Type", "text/html; charset=utf-8");
  //向客户端发送指定内容，并结束这次请求的处理过程
  res.end(resStr);
});

//启动服务并绑定80端口
server.listen(80, () => {
  console.log("this server is running.  url=>http://127.0.0.1:80");
})