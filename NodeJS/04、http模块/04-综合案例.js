const http = require("http");
const fs = require("fs");
const path = require("path");

const server = http.createServer();

server.on('request', (req, res) => {
  let url = req.url;
  let sourcePath = path.join(__dirname, "../03、path模块/source", url);
  fs.readFile(sourcePath, "utf-8", (err, content) => {
    if (err) {
      console.log("文件读取失败" + sourcePath);
      return;
    }
    res.end(content);
  });
});

server.listen(80, function () {
  console.log("server online, port:80,  url:http:127.0.0.1");
})