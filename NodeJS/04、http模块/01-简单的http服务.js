const http = require('http');

//创建一个http服务
const server = http.createServer();

//绑定request事件
server.on('request', (req, res) => {
  console.log("someone is visited this server");
});

//服务监听80端口
server.listen('8080', () => {
  console.log("port 8080.  server running :http://127.0.0.1:8080");
});