const http = require('http');

//创建服务
const server = http.createServer();

//绑定request事件
server.on('request', (req, res) => {
  let url = req.url;

  let content = "";
  if (url === "/" || url === "/index" || url === "/index.html") {
    content = `<h1>首页</h1>`;
  } else if (url === "/about" || url === "/about.html") {
    content = `<h1>相关页面</h1>`;
  } else {
    content = `<h1>404</h1>`;
  }

  // //设置头部信息
  res.setHeader('Content-Type', "text/html; charset=utf-8");
  // //向客户端发送指定内容，并结束这次请求的处理过程
  res.end(content);
});

//监听80端口
server.listen(8081, () => {
  console.log("this server is running on port 80,  url: http://127.0.0.1:8081");
});