const express = require("express");
const cors = require("cors");

const app = express();

//注意这个要写在外层而不是路由模块里
//解决跨域问题
app.use(cors());
app.use(express.json());
//绑定路由
const myRouter = require("./myRouter")
app.use("/api", myRouter);


app.listen("80", () => {
  console.log("this server is running at http://127.0.0.1:80");
})