const express = require("express");
const router = express.Router();


router.get("/user/queryInfoById", (req, res) => {
  let data = req.query;
  res.send({
    "code": "200",
    "message": "GET请求成功",
    "data": data
  });
});


router.post("/user/add", (req, res) => {
  let data = req.body;
  console.log(data);
  res.send({
    "code": "200",
    "message": "POST请求成功",
    "data": data
  });
})

module.exports = router;