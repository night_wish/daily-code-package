package com.lee.hadoop.hdfs;


import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/3/15 16:58
 * 注意：
 * 客户端去操作 HDFS 时，是有一个用户身份的。默认情况下，HDFS 客户端 API 会从采用 Windows 默认用户访问 HDFS，会报权限异常错误。所以在访问 HDFS 时，一定要配置用户。
 */
@Slf4j
public class HdfsClientTest {

    private FileSystem fileSystem;

    /**
     * 文件系统相关的配置在 Hadoop 中可以包括以下一些常见参数：
     *
     * 1. fs.defaultFS：默认文件系统的 URI。
     * 2. dfs.replication：文件的副本数量。
     * 3. dfs.namenode.name.dir：NameNode 存储元数据的目录。
     * 4. dfs.datanode.data.dir：DataNode 存储数据块的目录。
     * 5. dfs.permissions.enabled：是否启用权限检查。
     * 6. dfs.blocksize：数据块的大小。
     * 7. dfs.namenode.handler.count：NameNode 处理请求的线程数。
     * 8. dfs.replication.max：文件的最大副本数量。
     * 9. dfs.client.read.shortcircuit：是否启用客户端短路读取等。
     */
    public HdfsClientTest() throws URISyntaxException, IOException, InterruptedException {
        //连接集群的nn地址
        URI uri = new URI("hdfs://localhost:9000");
        //创建一个配置文件
        Configuration configuration = new Configuration();
        //设置默认副本数 客户端代码中设置的值 > ClassPath 下的用户自定义配置文件 > 然后是服务器的自定义配置 （xxx-site.xml） > 服务器的默认配置 （xxx-default.xml）
        //configuration.set("dfs.replication","1");

        //用户---默认是windows用户
        String user = "james";
        // 1 获取客户端对象
        fileSystem = FileSystem.get(uri, configuration, user);
    }

    /**
     * 关闭
     * @throws IOException
     */
    @Test
    public void close() throws IOException {
        // 3 关闭资源
        fileSystem.close();
    }


    /**
     * 1、创建文件夹
     */
    @Test
    public void testMkdir() throws IOException {
        Path path = new Path("/hebei");
        fileSystem.mkdirs(path);
        close();
    }

    /**
     * 2、上传
     */
    @Test
    public void testCopyFromLocal() throws IOException {
        //参数一：delSrc 是否删除原数据
        //参数二：是否允许覆盖
        //参数三： 原数据路径
        //参数四：目的地路径
        Path sPath = new Path("C:\\Users\\james\\Desktop\\WorkTmp.txt");
        Path dPath = new Path("/hebei");
        fileSystem.copyFromLocalFile(false,true,sPath,dPath);
        close();
    }

    /**
     * 2、设置副本数(默认是3)
     *  或者 通过  configuration.set("dfs.replication","2");  设置
     */
    @Test
    public void editReplication() throws IOException {
        Path sPath = new Path("/hebei/WorkTmp.txt");
        fileSystem.setReplication(sPath, (short) 1);
        close();
    }

    /**
     * 3、下载
     */
    @Test
    public void testGet() throws IOException {
        Path sPath = new Path("/hebei/WorkTmp.txt");
        Path dPath = new Path("C:\\Users\\james\\Desktop\\dest");
//        fileSystem.copyToLocalFile(sPath,dPath);
        fileSystem.copyToLocalFile(false,sPath,dPath,false);
        close();
    }

    /**
     * 4、重命名 和 移动
     */
    @Test
    public void rename() throws IOException {
        //参数一：原文件路径
        //参数二：目标文件路径
        Path sPath = new Path("/hebei/WorkTmp.txt");
        Path dPath = new Path("/henan/WorkTmp_tmp.txt");
        fileSystem.rename(sPath,dPath);
        close();
    }

    /**
     * 4、删除
     */
    @Test
    public void testDelete() throws IOException {
        //参数一：删除文件或目录
        //参数二：是否递归删除
        Path sPath = new Path("/hebei/WorkTmp_tmp.txt");
        fileSystem.delete(sPath,true);
        close();
    }


    /**
     * 判断是文件夹还是目录
     */
    @Test
    public void testFileOrDirectory() throws IOException {
        Path sPath = new Path("/bak");
        FileStatus[] fileStatuses = fileSystem.listStatus(sPath);
        for(FileStatus fileStatus : fileStatuses){
            log.info("=====>{}, is a {}",fileStatus.getPath().getName(),fileStatus.isDirectory()?"directory" : "file");
        }
    }

    /**
     * 6、读取文件信息
     */
    @Test
    public void testReadFileInfo() throws IOException {
        //参数一：目录或文件
        //参数二：是否递归读取
        Path sPath = new Path("/bak");
        RemoteIterator<LocatedFileStatus> fileIterator = fileSystem.listFiles(sPath, true);
        while(fileIterator.hasNext()){
            log.info("===========start===============");
            LocatedFileStatus fileStatus = fileIterator.next();
            log.info("path:{}",fileStatus.getPath());
            log.info("pathName:{}",fileStatus.getPath().getName());
            log.info("permission:{}",fileStatus.getPermission());
            log.info("owner:{}",fileStatus.getOwner());
            log.info("group:{}",fileStatus.getGroup());
            log.info("len:{}",fileStatus.getLen());
            log.info("modificationTime:{}",fileStatus.getModificationTime());
            log.info("replication:{}",fileStatus.getReplication());
            log.info("blockSize:{}",fileStatus.getBlockSize());

            //获取快信息
            BlockLocation[] blockLocations = fileStatus.getBlockLocations();
            log.info("block info:{}", JSON.toJSONString(blockLocations));
            log.info("============end==============");
        }
    }




}
