package com.lee.hadoop.mapReduce.mapJoin;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/4/17 16:53
 */
public class TableDriver {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException, URISyntaxException {
        Job job = Job.getInstance(new Configuration());

        job.setJarByClass(TableDriver.class);

        job.setMapperClass(TableMapper.class);
        //没有reducerClass

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(NullWritable.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(NullWritable.class);

        //将小文件读取到缓存
        job.addCacheFile(new URI("file:///D:/hadoop/reduceJoinInput/pd.txt"));

        FileInputFormat.setInputPaths(job,new Path("D:\\hadoop\\reduceJoinInput\\order.txt"));
        FileOutputFormat.setOutputPath(job,new Path("D:\\hadoop\\reduceJoinOutput1\\"));

        //Map端join的逻辑不需要Reduce阶段，设置ReduceTask数量为0
        job.setNumReduceTasks(0);

        job.waitForCompletion(true);

    }
}
