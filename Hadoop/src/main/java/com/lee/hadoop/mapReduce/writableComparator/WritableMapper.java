package com.lee.hadoop.mapReduce.writableComparator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author Lee
 * @version 1.0
 * @description 序列化Mapper
 * @date 2024/4/9 13:46
 * id    手机号      ip              上行流量   下行流量  网络状态码
 * 1   18310103915 www.baidu.com     1024   20480   200
 * 2   13930071233 www.bilibili.com  2048   30970   200
 * 3   15097846807 www.hao123.com    1024   37829   200
 * 4   18310103915 www.bilibili.com  1076   10520   200
 * 5   15097846807 www.baidu.com     1024   10000   200
 */
public class WritableMapper extends Mapper<LongWritable, Text, FlowBean,Text> {

    private FlowBean outKey = new FlowBean();

    private Text outValue = new Text();

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, FlowBean, Text>.Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] words = line.split("\\s+");
        String phone = words[0];
        outValue.set(phone);

        outKey.setUpFlow(Long.parseLong(words[1]));
        outKey.setDownFlow(Long.parseLong(words[2]));
        outKey.setTotalFlow(Long.parseLong(words[3]));

        context.write(outKey,outValue);

    }
}
