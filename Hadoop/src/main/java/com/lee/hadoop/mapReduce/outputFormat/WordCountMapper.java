package com.lee.hadoop.mapReduce.outputFormat;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @description 单词计数Mapper
 * @author Lee
 * @date 2024/4/8 16:57
 * @version 1.0
 * @入参：
 * KEYIN, Mapper的输入Key, 这里是 输入文字的偏移量用，LongWritable
 * VALUEIN, Mapper的输入value，这里是输入的文字，Text
 * KEYOUT,  Mapper的输出Key，这里是 输出的文字 Text
 * VALUEOUT  Mapper的输出value，这里是输出文件的数 IntWritable
 */
public class WordCountMapper extends Mapper<LongWritable, Text,Text, IntWritable> {
    private Text outKey = new Text();

    private IntWritable outValue = new IntWritable(1);

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        //读取一行数据
        String line = value.toString();
        //拆分，有的行的内容如 lee lee
        String[] words = line.split(" ");
        for (String word : words) {
            outKey.set(word);
            context.write(outKey,outValue);
        }
    }
}
