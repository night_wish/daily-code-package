package com.lee.hadoop.mapReduce.wordCount;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * @author Lee
 * @version 1.0
 * @description 单词计数Driver
 * @date 2024/4/8 16:57
 */
public class WordCountDriver {

    public static void main(String[] args)
          throws IOException, InterruptedException, ClassNotFoundException {
        //1、获取Job
        Configuration configuration = new Configuration();
        Job job = new Job(configuration);

        //2、设置jar包路径
        job.setJarByClass(WordCountDriver.class);

        //3、设置执行的mapper和reducer
        job.setMapperClass(WordCountMapper.class);
        job.setReducerClass(WordCountReducer.class);

        //4、设置mapper的key value
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        // 5、设置输出的key value
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        //6、设置输入文件的路径 和 输出的文件路径
//        FileInputFormat.setInputPaths(job, new Path("D:\\hadoop\\input\\myWords.txt"));
//        FileOutputFormat.setOutputPath(job,new Path("D:\\hadoop\\output1"));
        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        //7、提交
        job.waitForCompletion(true);
    }
}
