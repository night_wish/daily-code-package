package com.lee.hadoop.mapReduce.outputFormat;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * @author Lee
 * @version 1.0
 * @description  自定义outputFormat,  功能就是将  各个单词 分开输出，如Lee单独写到一个文件里，其他的单独写一个文件中，
 * @date 2024/4/17 9:12
 */
public class MyCustomOutputFormat extends FileOutputFormat<Text, IntWritable> {


    public RecordWriter<Text, IntWritable> getRecordWriter(TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {

        MyRecordWriter myRecordWriter = new MyRecordWriter(taskAttemptContext);
        return myRecordWriter;
    }
}
