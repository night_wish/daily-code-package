package com.lee.hadoop.mapReduce.writable;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author Lee
 * @version 1.0
 * @description 序列化Mapper
 * @date 2024/4/9 13:46
 * id    手机号      ip              上行流量   下行流量  网络状态码
 * 1   18310103915 www.baidu.com     1024   20480   200
 * 2   13930071233 www.bilibili.com  2048   30970   200
 * 3   15097846807 www.hao123.com    1024   37829   200
 * 4   18310103915 www.bilibili.com  1076   10520   200
 * 5   15097846807 www.baidu.com     1024   10000   200
 */
public class WritableMapper extends Mapper<LongWritable, Text, Text, FlowBean> {

    private FlowBean flowBean = new FlowBean();

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, FlowBean>.Context context) throws IOException, InterruptedException {
        String line = value.toString();

        String[] words = line.split("\\s+");
        flowBean.setUpFlow(Long.parseLong(words[3]));
        flowBean.setDownFlow(Long.parseLong(words[4]));
        flowBean.setTotalFlow();
        context.write(new Text(words[1]), flowBean);
    }
}
