package com.lee.hadoop.mapReduce.customPar;

import com.lee.hadoop.mapReduce.writable.FlowBean;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/4/16 13:53
 */
public class CustomPartitioner extends Partitioner<Text, FlowBean> {
    /**
     *
     * @param text   key
     * @param flowBean  value
     * @param i   partition数量
     * @return
     */
    public int getPartition(Text text, FlowBean flowBean, int i) {
        String phone = text.toString();
        String prefix = phone.substring(0, 3);
        if(prefix.equals("183")){
            return 0;
        }else if(prefix.equals("150")){
            return 1;
        } else if (prefix.equals("139")) {
            return 2;
        }else{
            return 3;
        }
    }
}
