package com.lee.hadoop.mapReduce.reduceJoin;


import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/4/17 14:37
 */
public class TableReducer extends Reducer<Text,TableBean,TableBean, NullWritable> {

    @Override
    protected void reduce(Text key, Iterable<TableBean> values, Reducer<Text, TableBean, TableBean, NullWritable>.Context context) {
        List<TableBean> resList = new ArrayList<TableBean>();
        Map<String,String> nameMap = new HashMap <String,String>();

        Iterator<TableBean> iterator = values.iterator();
        while(iterator.hasNext()){
            TableBean tableBean = iterator.next();
            if("pd".equals(tableBean.getTflag())){
                nameMap.put(tableBean.getPid(),tableBean.getPname());
            }else{
                TableBean dest = new TableBean();
                try {
                    BeanUtils.copyProperties(dest,tableBean);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
                resList.add(dest);
            }
        }

        for (TableBean value : resList) {
            value.setPname(nameMap.get(value.getPid()));
            try {
                context.write(value,NullWritable.get());
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

    }
}
