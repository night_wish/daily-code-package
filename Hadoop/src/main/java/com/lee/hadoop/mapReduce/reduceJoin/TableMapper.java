package com.lee.hadoop.mapReduce.reduceJoin;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/4/17 14:28
 */
public class TableMapper extends Mapper<LongWritable, Text,Text,TableBean> {

    private String fileName;

    private Text outKey = new Text();
    private TableBean outValue = new TableBean();

    /**
     * 根据inputSplit获取切面名（文件名）
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void setup(Mapper<LongWritable, Text, Text, TableBean>.Context context) throws IOException, InterruptedException {
        FileSplit inputSplit = (FileSplit) context.getInputSplit();
        fileName = inputSplit.getPath().getName();
    }

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, TableBean>.Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] split = line.split("\t");

        if(fileName.contains("order")) {
            outKey.set(split[1]);
            outValue.setId(split[0]);
            outValue.setPid(split[1]);
            outValue.setAmount(Integer.valueOf(split[2]));
            outValue.setPname("");
            outValue.setTflag("order");
        }else {
            outKey.set(split[0]);
            outValue.setId("");
            outValue.setPid(split[0]);
            outValue.setAmount(0);
            outValue.setPname(split[1]);
            outValue.setTflag("pd");
        }

        context.write(outKey,outValue);

    }
}
