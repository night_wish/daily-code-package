package com.lee.hadoop.mapReduce.writableComparator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/4/9 14:18
 */
public class WritableDriver {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        //1、初始化Job
        Configuration configuration = new Configuration();
        Job job = new Job(configuration);
        //2、设置jar包位置
        job.setJarByClass(WritableDriver.class);
        //3、设置mapper和reducer
        job.setMapperClass(WritableMapper.class);
        job.setReducerClass(WritableReducer.class);
        //4、设置mapper输出格式的key value
        job.setMapOutputKeyClass(FlowBean.class);
        job.setMapOutputValueClass(Text.class);
        //5、设置输出的k-v
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(FlowBean.class);
        //6、设置input和output位置
        FileInputFormat.setInputPaths(job,new Path("D:\\hadoop\\ouput-phone4"));
        FileOutputFormat.setOutputPath(job,new Path("D:\\hadoop\\output5"));
        //7、提交
        boolean res = job.waitForCompletion(true);
        System.out.println("jobId:"+job.getJobID());
        System.out.println("jobName:"+job.getJobName());
        System.out.println("jobFile:"+job.getJobFile());
        System.out.println("jobState:"+job.getJobState());
        System.out.println("jobHistoryUrl:"+job.getHistoryUrl());
        System.out.println(res);
    }
}
