package com.lee.hadoop.mapReduce.etl;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/4/17 17:28
 */
public class EtlLogMapper extends Mapper<LongWritable,Text, Text, NullWritable> {

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, NullWritable>.Context context) throws IOException, InterruptedException {
        //读取一行数据
        String line = value.toString();

        //数据清洗，判断数据是不是符合格式
        Boolean judgeRes = validateLine(line);

        //不满足格式 直接 跳过
        if(!judgeRes)return;

        //输出
        context.write(value, NullWritable.get());

    }

    private Boolean validateLine(String line) {
        //判断数据是否满足格式需要
        return true;
    }
}
