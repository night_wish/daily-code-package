package com.lee.hadoop.mapReduce.etl;

import com.lee.hadoop.mapReduce.mapJoin.TableDriver;
import com.lee.hadoop.mapReduce.mapJoin.TableMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.net.URI;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/4/17 17:31
 */
public class EtlLogDriver {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Job job = Job.getInstance(new Configuration());

        job.setJarByClass(EtlLogDriver.class);

        job.setMapperClass(EtlLogMapper.class);
        //没有reducerClass

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(NullWritable.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(NullWritable.class);


        FileInputFormat.setInputPaths(job,new Path("D:\\hadoop\\reduceJoinInput\\log.txt"));
        FileOutputFormat.setOutputPath(job,new Path("D:\\hadoop\\etlOutput\\"));

        job.waitForCompletion(true);
    }
}
