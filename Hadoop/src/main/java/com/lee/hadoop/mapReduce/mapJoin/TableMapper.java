package com.lee.hadoop.mapReduce.mapJoin;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/4/17 16:52
 */
public class TableMapper extends Mapper<LongWritable, Text,Text, NullWritable> {

    private Map<String,String> proMap = new HashMap<String,String>();

    @Override
    protected void setup(Mapper<LongWritable, Text, Text, NullWritable>.Context context) throws IOException, InterruptedException {
        URI[] cacheFiles = context.getCacheFiles();

        FileSystem fileSystem = FileSystem.get(context.getConfiguration());
        FSDataInputStream inputStream = fileSystem.open(new Path(cacheFiles[0]));
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while(StringUtils.isNotBlank(line = reader.readLine())){
            String[] split = line.split("\t");
            proMap.put(split[0],split[1]);
        }

    }

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, NullWritable>.Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] split = line.split("\t");

        String result = split[0]+" "+"\t"+proMap.get(split[1])+" "+"\t"+split[2];
        context.write(new Text(result),NullWritable.get());
    }
}
