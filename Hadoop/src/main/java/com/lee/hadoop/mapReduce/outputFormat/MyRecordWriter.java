package com.lee.hadoop.mapReduce.outputFormat;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;

/**
 * @author Lee
 * @version 1.0
 * @description
 * @date 2024/4/17 9:15
 */
public class MyRecordWriter extends RecordWriter<Text, IntWritable> {

    private FSDataOutputStream guiguOut;
    private FSDataOutputStream otherOut;

    public MyRecordWriter(TaskAttemptContext context) throws IOException {
        FileSystem fs = FileSystem.get(context.getConfiguration());
        guiguOut = fs.create(new Path("D:\\hadoop\\myrecord\\guigu.txt"));
        otherOut = fs.create(new Path("D:\\hadoop\\myrecord\\other.txt"));
    }


    public void write(Text text, IntWritable intWritable) throws IOException, InterruptedException {
        String word = text.toString();
        if("lee".equals(word)){
            guiguOut.write((word+" "+intWritable+"\n\t").getBytes());
        }else{
            otherOut.write((word+" "+intWritable+"\n\t").getBytes());
        }

    }

    public void close(TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
        IOUtils.closeStream(guiguOut);
        IOUtils.closeStream(otherOut);
    }
}
