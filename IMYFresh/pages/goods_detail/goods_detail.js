import {request} from "../../request/index.js";
Page({

  //页面的初始数据
  data: {
    goodsObj:{}
  },

  //商品信息
  goodsInfo:{},

  //监听页面加载
  onLoad: function (options) {
    const {goodsId} = options;
    
    //获取商品详情
    this.getGoodsDetail(goodsId);
  },

  //获取商品详情数据
  getGoodsDetail(goodsId){
    request({url:"/goods/detail",data:{goods_id:goodsId}})
    .then(res=>{
      this.goodsInfo = res;
      this.setData({
        goodsObj:{
          goods_id:res.goods_id,
          goods_name:res.goods_name,
          goods_price:res.goods_price,
          pics:res.pics,
          /**
           * 部分iphone手机不支持webp格式的图片，所以后台要有webp和jpg或png等两种图片
           * 供iphone部分机型使用。这里做临时改变
           */
          goods_introduce:res.goods_introduce.replace(/\.webp/g,'.jpg')
        }
      });
    });
  },

  //点击预览图片
  handlePreviewImage(e){
    const currentUrl = e.currentTarget.dataset.url;
    const pics = this.data.goodsObj.pics.map(v=>v.pics_mid);
    wx.previewImage({
      current: currentUrl,
      urls: pics
    });
  },

  //加入购物车
  handleCartAdd(){
    //判断缓存中是否有购物车数据
    let cart = wx.getStorageSync("cart")||[];
    //判断商品是否在购物车数据中
    let index = cart.findIndex(v=>v.goods_id==this.goodsInfo.goods_id);
    console.log(index);
    if(index===-1){
      //商品不存在,第一次添加
      this.goodsInfo.num=1;
      this.goodsInfo.checked=true;
      cart.push(this.goodsInfo);
    }else{
      //商品已存在
      cart[index].num++;
      cart[index].checked=true;
    }
    //商品选中
    //将购物车重新放回缓存
    wx.setStorageSync("cart", cart);
    //弹出提示窗,防止重复点击
    wx.showToast({
      title: '加入成功',
      icon: 'success',
      mask: true
    });
      

      
  }


})