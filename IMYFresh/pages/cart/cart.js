// pages/cart/cart.js
Page({

  data: {
    address:{},
    cart:{},
    allChecked:false,
    totalPrice:0,
    totalNum:0
  },

  //页面初始化时加载--二级页面返回来就不会加载了
  onLoad: function (options) {

  },

  //页面显示时加载
  onShow: function (){
    //设置收货地址
    let addressInfo = wx.getStorageSync("address");
    let cart = wx.getStorageSync("cart")||[];//若为空，则是一个空数组
    
    this.setData({
      address: addressInfo
    });

    this.setCartData(cart);
  },

  /**
   * wx.chooseAdress  获取收货地址
   * wx.getSetting    获取权限数据
   * wx.openSetting   进入授权页面
   * 
   * 1、获取小程序内用户对收货地址的权限 scope
   *    A、用户从没点击同意获取收货地址提示框
   *        authSetting scope.address 为 undefined
   *        当点击同意后可直接获取收货地址
   * 
   *    B、用户点击同意获取收货地址提示框
   *        authSetting scope.address 为 true
   *        可直接获取收货地址
   * 
   *    C、用户曾经拒绝获取收货地址
   *         authSetting scope.address 为 false
   *        此时要诱导用户进入 权限设置页面，重新授权
   *        授权成功后，获取收货地址
   * 
   * 2、将地址信息存入缓存
   */
  handleChooseAddress(){
    
      wx.getSetting({
        success: (result) => {

          const scopeAddress = result.authSetting["scope.address"]; 
          
          //尚未授权 或 已同意授权
          if(scopeAddress===true || scopeAddress===undefined){
            wx.chooseAddress({
              success: (res) => {
                res.detail = res.provinceName+res.cityName+res.countyName+res.detailInfo;
                wx.setStorageSync("address", res);
              }
            });

            //未同意授权
          }else{

            wx.openSetting({
              success: (result) => {
                wx.chooseAddress({
                  success: (res) => {
                    res.detail = res.provinceName+res.cityName+res.countyName+res.detailInfo;
                    wx.setStorageSync("address",res);
                  }
                });
              }
            });
              
          }
        }
      });
        
        
      
  },

  /**
   * checkebox点击事件
   **/
  handleItemChange(e){
    const goods_id = e.currentTarget.dataset.id;
    let cartInfo = this.data.cart;
    let index = cartInfo.findIndex(v=>v.goods_id===goods_id);
    cartInfo[index].checked = !cartInfo[index].checked;
    this.setCartData(cartInfo);
  },
  /**
   * 全选按钮的点击事件
   **/
  handleAllItemChange(){
    let {cart,allChecked} = this.data;
    allChecked = !allChecked;
    cart.forEach(v=>v.checked=allChecked);
    this.setCartData(cart);
  },

  /**
   * 购物车商品数量编辑 
   **/
  handleItemNumEdit(e){
    let {id,operation} = e.currentTarget.dataset;
    let cart = this.data.cart;
    let index = cart.findIndex(v=>v.goods_id===id);
    

    //如果数量为1且ope为-1，则提示是否删除商品
    if(cart[index].num===1&&operation===-1){
      wx.showModal({
        title: '提示',
        content: '是否要删除商品',
        success:(res)=>{
          if (res.confirm) {
            cart.splice(index,1);
            this.setCartData(cart);
          }
        }
      })
    }else{
      cart[index].num+=operation;
      this.setCartData(cart);
    }
  },

  /**
   *  封装代码：重新设置购物车数量和价格以及全选
   **/
  setCartData(cart){
    let allChecked = cart.length>0?cart.every(v=>v.checked):false;
    let totalPrice=0;
    let totalNum=0;

    cart.forEach(v=>{
      if(v.checked){
        totalPrice+=v.num*v.goods_price;
        totalNum+=v.num;
      }
    });
    
    this.setData({
      cart: cart,
      allChecked: allChecked,
      totalPrice: totalPrice,
      totalNum: totalNum
    });
    wx.setStorageSync("cart", cart);
  },

  /**
   * 商品结算按钮
   */
  handleGoodsPay(){
    let {address,totalNum} = this.data;
    //判断是否有收货地址
    if(!address.userName){
      wx.showToast({
        title: '请填写收货地址!',
        duration: 2000
      });
      return;
    }

    //判断是否添加购物商品
    if(totalNum===0){
      wx.showToast({
        title: '请添加购物商品!',
        duration: 2000
      });
      return;
    }

    //跳转支付
    wx.navigateTo({
      url: '/pages/pay/pay'
    });
      

  }

  
})