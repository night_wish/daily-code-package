import {request} from "../../request/index.js";
Page({

  data: {
    //左侧菜单
    leftMenuList:[],
    //右侧内容
    rightContent:[],
    //左侧被激活的菜单索引
    currentIndex:0,
    //右侧滚动条距离顶部的距离--每次刷新都需要重新设置的
    scrollTop:0
  },

  cates:[],

  onLoad: function (options) {
    //获取菜单数据
    //this.getCates();

    //使用微信小程序缓存
    const categories = wx.getStorageSync("categories");
    if(!categories){
      this.getCates();
    }else{
      if(Date.now()-categories.time>1000*10){
        this.getCates();
      }else{
        this.cates=categories.data;
        let leftMenuList = this.cates.map(v=>v.cat_name);
        let rightContent = this.cates[0].children;
        this.setData({
          leftMenuList,
          rightContent
        });
      }
    }
  },

  //获取菜单数据
  getCates(){
    request({url:"/categories"})
      .then(res=>{
        this.cates = res;

        //放入缓存
        wx.setStorageSync("categories", {time:Date.now(),data:this.cates});
          
        let leftMenuList = this.cates.map(v=>v.cat_name);
        let rightContent = this.cates[0].children;
        this.setData({
          leftMenuList,
          rightContent
        });
      });
  },

  //点击左侧菜单
  handleClickItem(e){
    //获取点击菜单索引
    const {index} = e.currentTarget.dataset;
    let rightContent = this.cates[index].children;
    this.setData({
      currentIndex:index,
      rightContent:rightContent,
      //每次刷新右侧内容-都需要设置滚动条距离顶部的距离
      scrollTop:0
    });
  }
 
})