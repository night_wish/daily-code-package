import {request} from '../../request/index.js';
Page({

  //页面的初始数据
  data: {
    tabs:[
      {
        id:0,
        value:'综合',
        isActive:true
      },
      {
        id:1,
        value:'销量',
        isActive:false
      },
      {
        id:2,
        value:'价格',
        isActive:false
      }
    ],
    goodsList:[]
  },

  //总页数
  totalPage:1,

  //接口要的参数
  queryParams:{
    query:"",
    cid:"",
    pagenum:1,
    pagesize:10
  },

  //生命周期函数--监听页面加载
  onLoad: function (options) {
    this.queryParams.cid = options.cid;
    //获取商品列表
    this.getGoodsList();
  },

  //获取商品列表
  getGoodsList(){
    request({url:"/goods/search",data:this.queryParams})
    .then(res=>{
      this.totalPage = Math.ceil(res.total/this.queryParams.pagesize);
      this.setData({
        goodsList:[...this.data.goodsList,...res.goods]
      });

      //关闭下拉刷新
      wx.stopPullDownRefresh();
    });
  },


  //tabs触发事件
  handleTabItemChange(e){
    const {index} = e.detail;
    let {tabs} = this.data;
    tabs.forEach((v,i)=>i===index?v.isActive=true:v.isActive=false);
    this.setData({
      tabs
    });
  },

  //滚动条触底事件
  onReachBottom(){
    //判断当前页面是否大于或等于总页面
    if(this.queryParams.pagenum>=this.totalPage){
      //加载完了
      wx.showToast({
        title: '数据加载完成'
      });
        
    }else{
      //加载下一页
      this.queryParams.pagenum++;
      this.getGoodsList();
    }
  },

  //下拉刷新页面
  onPullDownRefresh(){
    //重置数据
    this.setData({
      goodsList:[]
    });
    this.queryParams.pagenum=1;
    //重新获取商品列表
    this.getGoodsList();
  }

})