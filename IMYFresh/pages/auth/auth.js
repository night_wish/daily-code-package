import {request} from "../../request/index.js";
Page({

  data: {

  },

  onLoad: function (options) {

  },

  //获取用户信息和授权
  handleGetUserInfo(e){
    const {encryptedData,rawData,iv,signature} = e.detail;
    //获取code
    wx.login({
      timeout:10000,
      success:result=> {
         //这样是无法在login外侧获取code值得
        const {code} = result;
        const loginParams = {encryptedData,rawData,iv,signature,code};
        //请求用户登录token
        request({url:"/users/wxlogin",data:loginParams,method:"post"})
        .then(res=>{
          const {token} = res;
          wx.setStorageSync("token", token);
        });
      }
    });




      

  }

  
})