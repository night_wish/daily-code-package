/**
 * 引入用来发送请求的方法
 */
import {request} from '../../request/index.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList:[],
    cateList:[],
    floorList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    //请求轮播图
    this.getSwiperList();
    //请求导航
    this.getCateList();
    //请求楼层
    this.getFloorList();

  },


  /**
   * 请求轮播图
   */
  getSwiperList(){    
    request({url:"/home/swiperdata"})
    .then(res=>{
      this.setData({
        swiperList:res
      });
    });
  },

  /**
   * 请求导航
   */
  getCateList(){
    request({url:"/home/catitems"})
    .then(res=>{
      this.setData({
        cateList:res
      });
    });
  },

  /**
   * 请求楼层
   */
  getFloorList(){
    request({url:"/home/floordata"})
    .then(res=>{
      this.setData({
        floorList:res
      });
    });
  }

 
})