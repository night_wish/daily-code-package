// pages/cart/cart.js
Page({

  data: {
    address:{},
    cart:{},
    totalPrice:0,
    totalNum:0
  },

  //页面初始化时加载--二级页面返回来就不会加载了
  onLoad: function (options) {

  },

  //页面显示时加载
  onShow: function (){
    //设置收货地址
    let addressInfo = wx.getStorageSync("address");
    let cart = wx.getStorageSync("cart")||[];//若为空，则是一个空数组

    //这里的cart只能为checked为true的商品
    cart = cart.filter(v=>v.checked);

    let totalPrice=0;
    let totalNum=0;

    cart.forEach(v=>{
        totalPrice+=v.num*v.goods_price;
        totalNum+=v.num;
    });
    
    this.setData({
      cart: cart,
      address: addressInfo,
      totalPrice: totalPrice,
      totalNum: totalNum
    });
  },

  /**
   * 订单支付
   */
  handleOrderPay(){
    //从缓存中获取token
    let token = wx.getStorageSync("token");

    //如果缓存中不存在token，则证明用户尚未登录-跳转至登录授权页面
    if(!token){
      wx.navigateTo({
        url: '/pages/auth/auth'
      });
    }else{
      console.log("当前用户已登录!");
    }

  }

  
})