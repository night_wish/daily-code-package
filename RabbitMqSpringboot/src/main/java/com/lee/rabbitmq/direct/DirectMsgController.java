package com.lee.rabbitmq.direct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DirectMsgController {

    @Value("${exchange.directExchange.name}")
    public  String exchangeName;

    @Value("${exchange.directExchange.routingKey}")
    public  String routingKey;

    @Autowired
    private DirectMsgProducerService directMsgProducerService;

    @GetMapping("/senddirectmsg/{msg}")
    public String sendMsg(@PathVariable("msg")String msg){
        directMsgProducerService.sendMessage(exchangeName,routingKey,msg);
        return "ok";
    }
}
