package com.lee.rabbitmq.direct;

/**
 * 消息生产者
 */
public interface DirectMsgProducerService {

    void sendMessage(String exchangeName,String routingKey,String msg);
}
