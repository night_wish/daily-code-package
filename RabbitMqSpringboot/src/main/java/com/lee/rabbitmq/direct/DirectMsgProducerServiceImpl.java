package com.lee.rabbitmq.direct;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 消息生产者
 */
@Slf4j
@Service
public class DirectMsgProducerServiceImpl implements DirectMsgProducerService, ConfirmCallback, ReturnCallback {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void sendMessage(String exchangeName,String routingKey,String msg) {
        //消息发送失败返回到队列中, yml需要配置 publisher-returns: true
//        rabbitTemplate.setMandatory(true);//已经在配置文件中设置
        //消息消费者确认收到消息后，手动ack回执
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnCallback(this);

        //发送消息
        rabbitTemplate.convertAndSend(exchangeName,routingKey,msg);
    }


    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if(ack){
            log.info("-------ack is true confirm :  correlationData:{},cause:{}",correlationData,cause);
        }else {
            log.info("-------ack is false confirm :  correlationData:{},cause:{}",correlationData,cause);
        }
    }

    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        log.info("------return : replyCode:{}, replyText:{}, exchange:{}, routingKey:{}",replyCode,replyText,exchange,routingKey);
    }
}
