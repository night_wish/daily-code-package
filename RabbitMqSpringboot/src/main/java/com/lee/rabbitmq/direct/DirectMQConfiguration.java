package com.lee.rabbitmq.direct;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * exchange为direct模式下的配置
 */
@Slf4j
@SpringBootConfiguration
public class DirectMQConfiguration {

    public static final String QUEUE_NAME = "boot_direct_queue";

    @Value("${exchange.directExchange.name}")
    public  String exchangeName;

    @Value("${exchange.directExchange.durable}")
    public  Boolean durable;

    @Value("${exchange.directExchange.autoDelete}")
    public  Boolean autoDelete;

    @Value("${exchange.directExchange.routingKey}")
    public  String routingKey;


    /**
     * 交换机
     */
    @Bean
    public DirectExchange getDirectExchange(){
        return new DirectExchange(exchangeName,durable,autoDelete);
    }

    /**
     * 消息队列
     */
    @Bean
    public Queue getDirectQueue(){
        return new Queue(QUEUE_NAME,durable,false,autoDelete);
    }

    /**
     * 消息队列绑定交换机，并指定路由键
     */
    @Bean
    public Binding bindDirectExchangeAndQueue(@Qualifier("getDirectExchange") DirectExchange directExchange,
                                        @Qualifier("getDirectQueue") Queue queue){
        return BindingBuilder.bind(queue).to(directExchange).with(routingKey);
    }

}
