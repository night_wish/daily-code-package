package com.lee.rabbitmq.fanout;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FanoutMsgController {

    @Value("${exchange.fanoutExchange.name}")
    public  String exchangeName;

    @Autowired
    private FanoutMsgProducerService fanoutMsgProducerService;

    @GetMapping("/sendFanout/{msg}")
    public String sendMsg(@PathVariable("msg")String msg){
        fanoutMsgProducerService.sendMessage(exchangeName,msg);
        return "ok";
    }
}
