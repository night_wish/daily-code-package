package com.lee.rabbitmq.fanout;

/**
 * 消息生产者
 */
public interface FanoutMsgProducerService {

    void sendMessage(String exchangeName, String msg);
}
