package com.lee.rabbitmq.fanout;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * exchange为fanout模式下的配置
 */
@Slf4j
@Configuration
public class FanoutMQConfiguration {

    public static final String FIRST_QUEUE_NAME = "boot_fanout_1_queue";

    public static final String SECOND_QUEUE_NAME = "boot_fanout_2_queue";


    @Value("${exchange.fanoutExchange.name}")
    public  String exchangeName;

    @Value("${exchange.fanoutExchange.durable}")
    public  Boolean durable;

    @Value("${exchange.fanoutExchange.autoDelete}")
    public  Boolean autoDelete;


    /**
     * 交换机
     */
    @Bean
    public FanoutExchange getFanoutExchange(){
        return new FanoutExchange(exchangeName,durable,autoDelete);
    }

    /**
     * 消息队列1
     */
    @Bean
    public Queue getFanoutQueue_1(){
        return new Queue(FIRST_QUEUE_NAME,durable,false,autoDelete);
    }

    /**
     * 消息队列2
     */
    @Bean
    public Queue getFanoutQueue_2(){
        return new Queue(SECOND_QUEUE_NAME,durable,false,autoDelete);
    }

    /**
     * 消息队列1绑定交换机，并指定路由键
     */
    @Bean
    public Binding bindFanoutExchangeAndQueue_1(@Qualifier("getFanoutExchange") FanoutExchange FanoutExchange,
                                              @Qualifier("getFanoutQueue_1") Queue queue){
        return BindingBuilder.bind(queue).to(FanoutExchange);
    }


    /**
     * 消息队列2绑定交换机，并指定路由键
     */
    @Bean
    public Binding bindFanoutExchangeAndQueue_2(@Qualifier("getFanoutExchange") FanoutExchange FanoutExchange,
                                                @Qualifier("getFanoutQueue_2") Queue queue){
        return BindingBuilder.bind(queue).to(FanoutExchange);
    }
}
