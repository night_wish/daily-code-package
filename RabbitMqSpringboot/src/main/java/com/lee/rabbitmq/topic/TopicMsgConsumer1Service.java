package com.lee.rabbitmq.topic;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 消息消费者
 */
@Slf4j
@Service
@RabbitListener(queues = TopicMQConfiguration.FIRST_QUEUE_NAME)//监听指定队列1的信息
public class TopicMsgConsumer1Service {

    @RabbitHandler
    public void receiveMessage(String msg, Channel channel, Message message) throws InterruptedException, IOException {
        //模拟延迟消费
        Thread.sleep(8000);

        try {
            //正确执行  手动ack
            //假设收到消息失败呢？  假定收到消息是 message 表示失败
            if("message".equalsIgnoreCase(msg)){
//				channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,true);
                channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
                log.info("------->consumer 1  received msg failed msg :{} ",msg);
            }else{
                channel.basicAck(message.getMessageProperties().getDeliveryTag(),true);
                log.info("------->consumer 1  received msg success msg :{} ",msg);
            }

        } catch (Exception e) {
            //消费者处理出了问题，需要告诉队列信息消费失败
            channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,true);
            log.info("------>consumer 1  received msg failed msg :{} ",msg);
        }
    }


}
