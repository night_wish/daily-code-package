package com.lee.rabbitmq.topic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TopicMsgController {

    @Value("${exchange.topicExchange.name}")
    public  String exchangeName;

    @Autowired
    private TopicMsgProducerService topicMsgProducerService;

    @GetMapping("/sendtopicmsg1/{msg}")
    public String sendMsg1(@PathVariable("msg")String msg){
        topicMsgProducerService.sendMessage(exchangeName,"hebei.handan.shexian.guanfang",msg);
        return "ok";
    }

    @GetMapping("/sendtopicmsg2/{msg}")
    public String sendMsg2(@PathVariable("msg")String msg){
        topicMsgProducerService.sendMessage(exchangeName,"hebei.handan.shexian",msg);
        return "ok";
    }
}
