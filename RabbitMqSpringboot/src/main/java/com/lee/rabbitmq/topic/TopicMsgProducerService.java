package com.lee.rabbitmq.topic;

/**
 * 消息生产者
 */
public interface TopicMsgProducerService {

    void sendMessage(String exchangeName,String routingKey, String msg);
}
