package com.lee.rabbitmq.topic;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * exchange为Topic模式下的配置
 */
@Slf4j
@SpringBootConfiguration
public class TopicMQConfiguration {

    public static final String FIRST_QUEUE_NAME = "boot_Topic_1_queue";

    public static final String SECOND_QUEUE_NAME = "boot_Topic_2_queue";



    @Value("${exchange.topicExchange.name}")
    public  String exchangeName;

    @Value("${exchange.topicExchange.durable}")
    public  Boolean durable;

    @Value("${exchange.topicExchange.autoDelete}")
    public  Boolean autoDelete;

    @Value("${exchange.topicExchange.routingKey1}")
    public  String routingKey1;

    @Value("${exchange.topicExchange.routingKey2}")
    public  String routingKey2;


    /**
     * 交换机
     */
    @Bean
    public TopicExchange getTopicExchange(){
        return new TopicExchange(exchangeName,durable,autoDelete);
    }

    /**
     * 消息队列1
     */
    @Bean
    public Queue getTopicQueue_1(){
        return new Queue(FIRST_QUEUE_NAME,durable,false,autoDelete);
    }

    /**
     * 消息队列2
     */
    @Bean
    public Queue getTopicQueue_2(){
        return new Queue(SECOND_QUEUE_NAME,durable,false,autoDelete);
    }

    /**
     * 消息队列1绑定交换机，并指定路由键
     */
    @Bean
    public Binding bindTopicExchangeAndQueue_1(@Qualifier("getTopicExchange") TopicExchange TopicExchange,
                                        @Qualifier("getTopicQueue_1") Queue queue){
        return BindingBuilder.bind(queue).to(TopicExchange).with(routingKey1);
    }

    /**
     * 消息队列2绑定交换机，并指定路由键
     */
    @Bean
    public Binding bindTopicExchangeAndQueue_2(@Qualifier("getTopicExchange") TopicExchange TopicExchange,
                                               @Qualifier("getTopicQueue_2") Queue queue){
        return BindingBuilder.bind(queue).to(TopicExchange).with(routingKey2);
    }

}
