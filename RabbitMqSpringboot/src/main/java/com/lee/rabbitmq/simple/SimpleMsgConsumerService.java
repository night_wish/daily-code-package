package com.lee.rabbitmq.simple;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * 消息消费者---监听消息队列
 */
@Slf4j
@Service
public class SimpleMsgConsumerService {

    /**
     * 指明要监听的消息队列
     */
    @RabbitListener(queues = SimpleMQConfiguration.QUEUE_NAME)
    public void receiveMessage(String msg){
        log.info("simple consumer received message : {}",msg);
    }

}
