package com.lee.rabbitmq.simple;

/**
 * 消息生产者
 */
public interface SimpleMsgProducerService {

    public void sendMessage(String msg);
}
