package com.lee.rabbitmq.simple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 发送消息
 */
@RestController("/simple")
public class SimpleMsgController {

    @Autowired
    private SimpleMsgProducerService simpleMsgProducerService;

    @GetMapping("/sendmsg/{msg}")
    public String sendMsg(@PathVariable("msg")String msg){
        simpleMsgProducerService.sendMessage(msg);
        return "ok";
    }
}
