package com.lee.rabbitmq.simple;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置文件：指明消息队列、交换机、路由键
 */
@Configuration
public class SimpleMQConfiguration {

    public static final String QUEUE_NAME = "boot_simple_queue";

    public static final String EXCHANGE_NAME = "boot_simple_exchange";

    public static final String ROUTING_KEY = "boot_simple_routing_key";

    /**
     * 创建队列
     */
    @Bean
    public Queue getSimpleQueue(){
        return new Queue(QUEUE_NAME);
    }

    /**
     * 创建交换机
     */
    @Bean
    public DirectExchange getSimpleExchange(){
        //交换机名称、是否持久化队列、没有消费者连接后是否自动删除
        return new DirectExchange(EXCHANGE_NAME,false,false);
    }


    /**
     * 绑定交换机和消息队列，并指明路由键
     */
    @Bean
    public Binding bindSimpleExchangeToQueue(@Qualifier("getSimpleExchange") Exchange exchange,
                                             @Qualifier("getSimpleQueue") Queue queue){
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY).noargs();
    }


}
