package com.lee.rabbitmq.simple;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 消息生产者
 */
@Slf4j
@Service
public class SimpleMsgProducerServiceImpl implements SimpleMsgProducerService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送消息
     */
    @Override
    public void sendMessage(String msg) {

        //交换机、路由键、消息
        rabbitTemplate.convertAndSend(SimpleMQConfiguration.EXCHANGE_NAME,SimpleMQConfiguration.ROUTING_KEY,msg);
    }
}
